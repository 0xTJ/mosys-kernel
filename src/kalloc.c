#include "kalloc.h"
#include "kio/klog.h"
#include "mem/mem_mapping.h"
#include "ram_expansion.h"
#include "util/bst.h"
#include "util/mutex.h"
#include "util/util.h"
#include <assert.h>
#include <stdalign.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

typedef struct kalloc_block_header {
    uint32_t magic;
    size_t size;
    char desc[32];
    BST_INFO_OBJ(struct kalloc_block_header, kalloc_block_header, heap_info);
    char block[];
} kalloc_block_header;

#define SIZEOF_KALLOC_BLOCK_HEADER round_up(offsetof(kalloc_block_header, block), alignof(max_align_t))
#define BLOCK_START_OFFSET (SIZEOF_KALLOC_BLOCK_HEADER - offsetof(kalloc_block_header, block))
#define MIN_BLOCK_SPACE (SIZEOF_KALLOC_BLOCK_HEADER + alignof(max_align_t))
#define MAGIC(addr) (kheap_magic_base ^ bswap_u32((uint32_t) (addr)))

static bool kalloc_is_init = false;

static uint32_t kheap_magic_base;
static mutex kheap_mutex = MUTEX_INIT;
static kalloc_block_header *kheap_pointer = NULL;
static kalloc_block_header *used_kheap_pointer = NULL;

static int kalloc_block_header_spaceship(kalloc_block_header *a, kalloc_block_header *b);
BST_DECL(kalloc_block_header, kalloc_block_header, static);
static kalloc_block_header *kalloc_block_header_lookup_size(kalloc_block_header *node, size_t size);

static void kalloc_validate_worker(const char *action, const char *desc, kalloc_block_header *header);
static void kalloc_validate_no_lock(const char *action, const char *desc);

static int kalloc_block_header_spaceship(kalloc_block_header *a, kalloc_block_header *b) {
    // Sort first on size, then on address
    if (a->size < b->size) {
        return -1;
    } else if (a->size > b->size) {
        return 1;
    } else if ((uintptr_t) a < (uintptr_t) b) {
        return -1;
    } else if ((uintptr_t) a > (uintptr_t) b) {
        return 1;
    } else {
        return 0;
    }
}

BST_DEFN(kalloc_block_header, kalloc_block_header, heap_info, kalloc_block_header_spaceship, static)

static kalloc_block_header *kalloc_block_header_lookup_size(kalloc_block_header *node, size_t size) {
    if (!node) {
        return NULL;
    }

    if (node->heap_info.left && node->heap_info.left->size >= size) {
        return kalloc_block_header_lookup_size(node->heap_info.left, size);
    } else if (node->size >= size) {
        return node;
    } else {
        return kalloc_block_header_lookup_size(node->heap_info.right, size);
    }
}

void kalloc_init(void) {
    // Initialize magic number generator
    // Ideally this would be random
    kheap_magic_base = ((uint32_t) &kheap_magic_base);
    kheap_magic_base ^= 0xDEADBEEF;

    kalloc_is_init = true;
}

static void kalloc_validate_worker(const char *action, const char *desc, kalloc_block_header *header) {
    if (!header) {
        return;
    }
    if (header->magic != MAGIC(&header->magic)) {
        kpanic("Kernel heap magic number mismatch in header %p during %s of %s", (void *) header, action, desc);
    }
    if (header->heap_info.left &&
        kalloc_block_header_spaceship(header->heap_info.left, header) > 0) {
        kpanic("Kernel heap invalid tree structure in header %p size %zu during %s of %s; size isn't larger than left child %p, with size %zu",
               (void *) header, header->size, action, desc, (void *) header->heap_info.left, header->heap_info.left->size);
    }
    if (header->heap_info.right &&
        kalloc_block_header_spaceship(header->heap_info.right, header) < 0) {
        kpanic("Kernel heap invalid tree structure in header %p size %zu during %s of %s; size isn't smaller than right child %p, with size %zu",
               (void *) header, header->size, action, desc, (void *) header->heap_info.right, header->heap_info.right->size);
    }
    kalloc_validate_worker(action, desc, header->heap_info.left);
    kalloc_validate_worker(action, desc, header->heap_info.right);
}

static void kalloc_validate_no_lock(const char *action, const char *desc) {
    if (!kalloc_is_init) {
        kalloc_init();
    }
    kalloc_validate_worker(action, desc, kheap_pointer);
}

void kalloc_validate(const char *action, const char *desc) {
    MUTEX_WITH(&kheap_mutex, {
        kalloc_validate_no_lock(action, desc);
    })
}

static size_t kalloc_free_space_worker(kalloc_block_header *header) {
    if (!header) {
        return 0;
    }
    if (header->magic != MAGIC(&header->magic)) {
        kpanic("Kernel heap magic number mismatch");
    }
    return header->size + kalloc_free_space_worker(header->heap_info.left) + kalloc_free_space_worker(header->heap_info.right);
}

size_t kalloc_free_space(void) {
    size_t free_space;

    MUTEX_WITH(&kheap_mutex, {
        if (!kalloc_is_init) {
            kalloc_init();
        }
        free_space = kalloc_free_space_worker(kheap_pointer);
    })

    return free_space;
}

// header should already have been removed from tree
// size must be a multiple of alignof(max_align_t)
static void kalloc_block_split(kalloc_block_header *header, size_t size) {
    if (header->magic != MAGIC(&header->magic)) {
        kpanic("Kernel heap magic number mismatch");
    }

    if (header->size < size) {
        // Block is too small to split
        kpanic("Tried to split too-small block");
    } else if (header->size < size + MIN_BLOCK_SPACE) {
        // Block is too small to fit another block, don't split
    } else {
        // Split the block
        kalloc_block_header *new_header = (void *) (header->block + BLOCK_START_OFFSET + size);
        new_header->magic = MAGIC(&new_header->magic);
        new_header->size = header->size - size - SIZEOF_KALLOC_BLOCK_HEADER;
        header->size = size;
        kheap_pointer = BST_INSERT(kalloc_block_header)(kheap_pointer, new_header);
    }
}

// Must be called with kheap_mutex held
static bool kalloc_try_adding_pages(size_t min_size) {
    size_t mem_alloc_size = round_up(min_size + SIZEOF_KALLOC_BLOCK_HEADER, MEM_PAGE_SIZE);

    // Need to use special no kheap version, to avoid recursing on the kheap mutex
    ownedmem backing;
    mem_mapping map;
    if (!mem_mapping_new_no_kheap(mem_alloc_size, &backing, &map)) {
        return false;
    }

    kalloc_block_header *new_header = (kalloc_block_header *) mem_mapping_k_at(&map);
    new_header->magic = MAGIC(&new_header->magic);
    new_header->size = map.size - SIZEOF_KALLOC_BLOCK_HEADER;
    kheap_pointer = BST_INSERT(kalloc_block_header)(kheap_pointer, new_header);

    // We don't support freeing memory ever used for kheap, so discard backing and map
    return true;
}

ptr_range kalloc_range_with_desc(size_t size, const char *desc) {
    void *start;
    void *end;

    MUTEX_WITH(&kheap_mutex, {
        kalloc_validate_no_lock("kalloc_range", desc);

        if (!kalloc_is_init) {
            kalloc_init();
        }

        size = round_up(size, alignof(max_align_t));

        kalloc_block_header *found_header;

        if (true) {
            found_header = kalloc_block_header_lookup_size(kheap_pointer, size);
            if (found_header) {
                goto have_found_header;
            }
        }

        // TODO: Sweep tree to join blocks

        if (kalloc_try_adding_pages(size + 4 * MEM_PAGE_SIZE)) {
            found_header = kalloc_block_header_lookup_size(kheap_pointer, size);
            if (found_header) {
                goto have_found_header;
            }
        }

        if (kalloc_try_adding_pages(size + 1 * MEM_PAGE_SIZE)) {
            found_header = kalloc_block_header_lookup_size(kheap_pointer, size);
            if (found_header) {
                goto have_found_header;
            }
        }

        if (kalloc_try_adding_pages(size)) {
            found_header = kalloc_block_header_lookup_size(kheap_pointer, size);
            if (found_header) {
                goto have_found_header;
            }
        }

        kpanic("Out of dynamic memory");

have_found_header:
        kheap_pointer = BST_DELETE(kalloc_block_header)(kheap_pointer, found_header);
        kalloc_block_split(found_header, size);

        strncpy(found_header->desc, desc, sizeof found_header->desc);
        found_header->desc[(sizeof found_header->desc) - 1] = '\0';
        used_kheap_pointer = BST_INSERT(kalloc_block_header)(used_kheap_pointer, found_header);

        start = found_header->block + BLOCK_START_OFFSET;
        end = (char *) start + size;
    })

    fill_zero_size(start, (char *) end - (char *) start);

    return (ptr_range) { start, end };
}

void *kalloc_with_desc(size_t size, const char *desc) {
    return kalloc_range_with_desc(size, desc).start;
}

void *kalloc_end_with_desc(size_t size, const char *desc) {
    return kalloc_range_with_desc(size, desc).end;
}

ptr_range kalloc_clone_with_desc(ptr_range orig, const char *desc) {
    size_t size = (char *) orig.end - (char *) orig.start;
    ptr_range result = kalloc_range_with_desc(size, desc);

    if (result.start) {
        memcpy(result.start, orig.start, size);
    }

    return result;
}

static bool kheap_blocks_are_adjacent(kalloc_block_header *left, kalloc_block_header *right) {
    if (!left || !right) {
        return false;
    }

    return left->block + BLOCK_START_OFFSET + left->size == (char *) right;
}

static kalloc_block_header *kheap_find_adjacent_block(kalloc_block_header *root, kalloc_block_header *target) {
    if (!root) {
        return NULL;
    }

    if (kheap_blocks_are_adjacent(root, target) || kheap_blocks_are_adjacent(target, root)) {
        return root;
    }

    kalloc_block_header *result;
    if ((result = kheap_find_adjacent_block(root->heap_info.left, target))) {
        // Found an adjacent block on the left
    } else if ((result = kheap_find_adjacent_block(root->heap_info.right, target))) {
        // Found an adjacent block on the right
    } else {
        // Found no adjacent block
    }
    return result;
}

static void kfree_raw(kalloc_block_header *freed_header) {
    if (freed_header->magic != MAGIC(&freed_header->magic)) {
        kpanic("Kernel heap magic number mismatch");
    }

    // Remove from the used tree
    used_kheap_pointer = BST_DELETE(kalloc_block_header)(used_kheap_pointer, freed_header);

    // Remove and combine any adjacent free blocks
    kalloc_block_header *adjacent;
    do {
        adjacent = kheap_find_adjacent_block(kheap_pointer, freed_header);
        if (adjacent) {
            kheap_pointer = BST_DELETE(kalloc_block_header)(kheap_pointer, adjacent);

            kalloc_block_header *left;
            kalloc_block_header *right;

            ptrdiff_t comp = (char *) adjacent - (char *) freed_header;
            kassert(comp);
            if (comp < 0) {
                // Address of adjacent is less than address of freed_header, merge left to adjacent
                left = adjacent;
                right = freed_header;
            } else {
                // Address of adjacent is greater than address of freed_header, merge left to freed_header
                left = freed_header;
                right = adjacent;
            }

            left->size += SIZEOF_KALLOC_BLOCK_HEADER + right->size;
            right->magic = !MAGIC(&right->magic);   // Invalidate right as a block, usually to 0
            right = NULL;

            freed_header = left;
        }
    } while (adjacent);

    // Insert freed block into free blocks tree
    kheap_pointer = BST_INSERT(kalloc_block_header)(kheap_pointer, freed_header);
}

void kfree_with_desc(void *ptr, const char *desc) {
    if (!ptr) {
        return;
    }

    MUTEX_WITH(&kheap_mutex, {
        kalloc_validate_no_lock("kfree", desc);

        if (!kalloc_is_init) {
            kpanic("Freed before kalloc was initialized");
        }

        kalloc_block_header *freed_header = (void *) ((char *) ptr - SIZEOF_KALLOC_BLOCK_HEADER);
        kfree_raw(freed_header);
    })
}

void kfree_range_with_desc(ptr_range range, const char *desc) {
    if (!range.start) {
        return;
    }

    MUTEX_WITH(&kheap_mutex, {
        kalloc_validate_no_lock("kfree_range", desc);

        if (!kalloc_is_init) {
            kpanic("Freed before kalloc was initialized");
        }

        kalloc_block_header *freed_header = (void *) ((char *) range.start - SIZEOF_KALLOC_BLOCK_HEADER);
        kfree_raw(freed_header);
    })
}
