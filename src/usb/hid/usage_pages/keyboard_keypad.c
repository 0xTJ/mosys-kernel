#include "usb/hid/usage_pages/keyboard_keypad.h"
#include "util/util.h"
#include <stddef.h>

static const char *const usb_hid_usage_id_keyboard_keypad_strs[] = {
    [USB_HID_USAGE_ID_KEYBOARD_KEYPAD(keyboard_errorrollover)]  = "Keyboard ErrorRollOver",
    [USB_HID_USAGE_ID_KEYBOARD_KEYPAD(keyboard_postfail)]       = "Keyboard POSTFail",
    [USB_HID_USAGE_ID_KEYBOARD_KEYPAD(keyboard_errorundefined)] = "Keyboard ErrorUndefined",
    // TODO: Add the rest
};

const char *usb_hid_usage_id_keyboard_keypad_to_str(usb_hid_usage_id_keyboard_keypad usage_id) {
    if (usage_id < countof(usb_hid_usage_id_keyboard_keypad_strs)) {
        return usb_hid_usage_id_keyboard_keypad_strs[usage_id];
    } else {
        return NULL;
    }
}
