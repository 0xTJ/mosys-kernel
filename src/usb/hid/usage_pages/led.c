#include "usb/hid/usage_pages/led.h"
#include "util/util.h"
#include <stddef.h>

static const char *const usb_hid_usage_id_led_strs[] = {
    [USB_HID_USAGE_ID_LED(num_lock)]    = "Num Lock",
    [USB_HID_USAGE_ID_LED(caps_lock)]   = "Caps Lock",
    [USB_HID_USAGE_ID_LED(scroll_lock)] = "Scroll Lock",
    [USB_HID_USAGE_ID_LED(compose)]     = "Compose",
    [USB_HID_USAGE_ID_LED(kana)]        = "Kana",
    // TODO: Add the rest
};

const char *usb_hid_usage_id_led_to_str(usb_hid_usage_id_led usage_id) {
    if (usage_id < countof(usb_hid_usage_id_led_strs)) {
        return usb_hid_usage_id_led_strs[usage_id];
    } else {
        return NULL;
    }
}
