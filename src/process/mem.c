#include "process/mem.h"
#include "mem/mem_space.h"
#include "mem/mem_mapping.h"
#include "config.h"
#include "process/process.h"
#include "thread/thread.h"
#include "vfs/vfs.h"
#include "uapi/errno.h"
#include "uapi/sys/mman.h"
#include "util/util.h"

umem_ptr process_mmap(umem_ptr addr, size_t length, int prot, int flags, int fd, off_t offset, int *errno_ptr) {
#if !MEM_USE_MMU
    // MAP_FIXED is not supported without an MMU
    if (flags & MAP_FIXED) {
        *errno_ptr = EINVAL;
        return MAP_FAILED;
    }

    UNUSED(prot);
#endif

    // Check sharing types
    if ((bool) (flags & MAP_PRIVATE) == (bool) (flags & MAP_SHARED)) {
        // Either neither or both are set
        *errno_ptr = EINVAL;
        return MAP_FAILED;
    }

    // Check that we like the alignment and sizes
    if ((uintptr_t) addr % MEM_PAGE_SIZE    || offset % MEM_PAGE_SIZE   ||
        (uintptr_t) addr >= MEM_LEN         || length == 0              ||
        length > MEM_LEN - MEM_PAGE_SIZE    || MEM_LEN - length < (uintptr_t) addr) {
        *errno_ptr = EINVAL;
        return MAP_FAILED;
    }

    // TODO: Children will currently treats MAP_PRIVATE | MAP_ANONYMOUS as MAP_SHARED | MAP_ANONYMOUS


    size_t length_aligned = round_up(length, MEM_PAGE_SIZE);

    // Create a temporary kernel mapping for new_owndmm
    mem_mapping *new_mapping = mem_mapping_new(length_aligned);
    if (!new_mapping) {
        *errno_ptr = ENOMEM;
        return MAP_FAILED;
    }

    size_t inited_length = 0;

    if (flags & MAP_ANONYMOUS) {
        fd = -1;
        offset = 0;
    } else {
        // TODO: Check that fd is not no-exec if prot has PROT_EXEC

        ssize_t read_result = vfs_pread(fd, flex_mut_kernel(mem_mapping_k_at(new_mapping), length_aligned), length, offset, errno_ptr);
        if (read_result < 0) {
            // Unsuccesful read
            mem_mapping_delete(new_mapping);
            // *errno_ptr set by vfs_read()
            return MAP_FAILED;
        } else {
            // Succesful read
            inited_length = read_result;
        }

        if (inited_length != length) {
            // File is smaller than length
            mem_mapping_delete(new_mapping);
            *errno_ptr = EINVAL;
            return MAP_FAILED;
        }

        // TODO: Don't allow fd to be replaced between the file being loaded and the backing file of the memory being set
    }

    // 0-initialize any space left (the entire block for MAP_ANONYMOUS)
    memset(mem_mapping_k_at(new_mapping) + inited_length, 0, length_aligned - inited_length);

    umem_ptr result = mem_mapping_u_at(new_mapping);
    mem_space_append(&thread_current->owner_process->space, new_mapping);

    return result;
}

int process_munmap(umem_ptr addr, size_t length, int *errno_ptr) {
    // Check that we like the alignment and sizes
    if ((uintptr_t) addr % MEM_PAGE_SIZE    || (uintptr_t) addr >= MEM_LEN  ||
        length > MEM_LEN - MEM_PAGE_SIZE    || MEM_LEN - length < (uintptr_t) addr) {
        *errno_ptr = EINVAL;
        return -1;
    }

    // TODO: Implement
    *errno_ptr = ENOSYS;
    return -1;
}
