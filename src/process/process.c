#include "process/process.h"
#include "config.h"
#include "exception.h"
#include "file/elf.h"
#include "kalloc.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "util/mutex.h"
#include "process/mem.h"
#include "mem/mem_mapping.h"
#include "vfs/fdesc.h"
#include "vfs/vfs.h"
#include "util/util.h"
#include "uapi/errno.h"
#include "uapi/sys/mman.h"
#include "uapi/sys/wait.h"
#include <stdbool.h>
#include <string.h>

static pid_t pid_next = 0;
static mutex pid_next_mtx = MUTEX_INIT;

static process *sched_process = NULL;
static process *init_process = NULL;

static LIST(struct process) process_list;
static mutex process_list_mtx = MUTEX_INIT;

static mem_mapping *process_make_arg_env_space(flex_const_vect_mut argv, flex_const_vect_mut envp,
                                               int *new_argc_ptr, umem_ptr *new_argv_ptr, umem_ptr *new_envp_ptr,
                                               int *errno_ptr);
int process_execve_elf(process_execve_info *exec_info, int *errno_ptr);
int process_execve_enter(process_execve_info *exec_info, int *errno_ptr);

// Declared in "thread/thread.h"
struct process *process_get_sched(void) {
    return sched_process;
}

static void process_init_func() {
    const char *init_path = "/bin/init";
    int errno = 0;
    process_execve_info exec_info = PROCESS_EXECVE_INFO_INIT;
    exec_info.pathname = flex_const_str_kernel(init_path);
    exec_info.argv = FLEX_CONST_NULL;
    exec_info.envp = FLEX_CONST_NULL;
    int result = process_execve(&exec_info, &errno);

    kpanic("Supervisor continued in init process: Failed to execute %s: return=%d, errno=%d", init_path, result, errno);
}

void process_init(void) {
    sched_process = process_new(NULL);
    kassert(sched_process);

    sched_process->fdl = vfs_fdesc_list_new();
    kassert(sched_process->fdl);

    sched_process->cwd = NULL;
}

void process_exec_init(void) {
    kinfo ("Starting init");

    init_process = process_new(sched_process);
    kassert(init_process);
    kassert(init_process->pid == 1);

    thread *new_thread = thread_new();
    kassert(new_thread);

    process_give_thread(init_process, new_thread);

    init_process->fdl = vfs_fdesc_list_new();
    kassert(init_process->fdl);

    int dummy_errno;
    flex_const_str cwd_path_flx = flex_const_str_kernel("/");
    init_process->cwd = vfs_open_fdesc(cwd_path_flx, O_RDONLY, 0, &dummy_errno);
    if (!init_process->cwd) {
        kpanic("Failed to open root directory");
    }

    mem_mapping *sup_stack = mem_mapping_new(PROCESS_SUP_STACK_SIZE);
    kassert(sup_stack);

    thread_setup(new_thread, process_init_func, sup_stack);
    thread_wake(new_thread);
}

process *process_new(process *parent) {
    process *this = kalloc(sizeof(process));
    if (!this) return NULL;

    MUTEX_WITH(&pid_next_mtx, {
        this->pid = pid_next++;
    })

    process_adopt_by(this, parent);

    wakelist_init(&this->vfork_wklst);
    wakelist_init(&this->waitpid_wklst);

    MUTEX_WITH(&process_list_mtx, {
        list_push_back(&process_list, this, process_list_link);
    })

    return this;
}

void process_delete(process *this) {
    bool any_threads_left;
    SPINLOCK_WITH(&this->owned_threads.lock, {
        any_threads_left = list_head(&this->owned_threads.list, thread, owned_threads_link);
    })
    kassert(!any_threads_left);

    wakelist_deinit(&this->vfork_wklst);
    wakelist_deinit(&this->waitpid_wklst);

    kassert(!this->parent);

    MUTEX_WITH(&process_list_mtx, {
        list_remove(&process_list, this, process_list_link);
    })

    kfree(this);
}

void process_adopt_by(process *this, process *new_parent) {
    process *old_parent = this->parent;
    if (old_parent) MUTEX_WITH(&old_parent->children_mtx, {
        list_remove(&old_parent->children, this, children_link);
        this->parent = NULL;
    })

    if (new_parent) MUTEX_WITH(&new_parent->children_mtx, {
        this->parent = new_parent;
        list_push_back(&new_parent->children, this, children_link);
    })
}

void process_give_thread(process *this, thread *thrd) {
    kassert(!thrd->owner_process);

    SPINLOCK_WITH(&this->owned_threads.lock, {
        thrd->owner_process = this;
        list_push_back(&this->owned_threads.list, thrd, owned_threads_link);
    })
}

void process_take_thread(process *this, thread *thrd) {
    kassert(this == thrd->owner_process);

    SPINLOCK_WITH(&this->owned_threads.lock, {
        list_remove(&this->owned_threads.list, thrd, owned_threads_link);
        thrd->owner_process = NULL;
    })
}

noreturn void process_enter_sup(void *ssp, umem_ptr usp, process_entry pc, uint16_t sr) {
    process_enter(ssp, usp, pc, sr | 0x2000);
}

noreturn void process_enter_usr(void *ssp, umem_ptr usp, process_entry pc, uint16_t sr) {
    process_enter(ssp, usp, pc, sr & ~0x2000);
}

noreturn void process_enter(void *ssp, umem_ptr usp, process_entry pc, uint16_t sr) {
    struct exception_state *state = process_enter_setup(ssp, usp, pc, sr);
    process_enter_state(state);
}

struct exception_state *process_enter_setup(void *ssp, umem_ptr usp, process_entry pc, uint16_t sr) {
    struct exception_state *state = (struct exception_state *) ((char *) ssp - sizeof(struct exception_state));
    fill_zero(state);
    state->usp = (long) usp;
    state->sr = sr;
    state->pc = (long) pc;
    state->format_vector_offset = 0;
    return state;
}

void process_release_resources(process *this) {
    vfs_close_all();
    vfs_fdesc_list_unref(this->fdl);
    this->fdl = NULL;
    vfs_fdesc_unref(this->cwd);
    this->cwd = NULL;

    mem_space_release_maps_array(&this->space);
}

// TODO: Handle CLONE_SETTLS
process *process_clone(unsigned long flags, umem_ptr stack,
                    //    int *parent_tid, int *child_tid,
                       unsigned long tls,
                       int *errno_ptr) {
    process *current_process = thread_current->owner_process;
    process *new_process;
    if (flags & CLONE_THREAD) {
        new_process = thread_current->owner_process;
    } else {
        // TODO: Should we really be adding as a child before completing process creation?
        new_process = process_new(current_process);
        if (!new_process) {
            *errno_ptr = ENOMEM;
            return NULL;
        }

        new_process->fdl = vfs_fdesc_list_clone(current_process->fdl);
        if (!new_process->fdl) {
            process_adopt_by(new_process, NULL);
            process_delete(new_process);
            *errno_ptr = ENOMEM;
            return NULL;
        }

        new_process->cwd = vfs_fdesc_clone(current_process->cwd);
        if (!new_process->cwd) {
            vfs_fdesc_list_unref(new_process->fdl);
            new_process->fdl = NULL;
            process_adopt_by(new_process, NULL);
            process_delete(new_process);
            *errno_ptr = ENOMEM;
            return NULL;
        }
    }

    thread *new_thread = thread_new();
    if (!new_thread) {
        if (!(flags & CLONE_THREAD)) {
            vfs_fdesc_unref(new_process->cwd);
            new_process->cwd = NULL;
            vfs_fdesc_list_unref(new_process->fdl);
            new_process->fdl = NULL;
            process_adopt_by(new_process, NULL);
            process_delete(new_process);
        }
        *errno_ptr = ENOMEM;
        return NULL;
    }

    process_give_thread(new_process, new_thread);

    // TODO: Will the following functions clobber the current process's virtual memory
    // if CLONE_THREAD and CLONE_VM are both specified?

    if (flags & CLONE_VM) {
        if (!mem_space_clone(&new_process->space, &thread_current->owner_process->space)) {
            process_take_thread(new_process, new_thread);
            thread_delete(new_thread);
            if (!(flags & CLONE_THREAD)) {
                vfs_fdesc_unref(new_process->cwd);
                new_process->cwd = NULL;
                vfs_fdesc_list_unref(new_process->fdl);
                new_process->fdl = NULL;
                process_adopt_by(new_process, NULL);
                process_delete(new_process);
            }
            *errno_ptr = ENOMEM;
            return NULL;
        }
    } else {
        kpanic("TODO: Fail gracefully");
    }

    if (!process_threads_setup_clone(new_thread)) {
        kwarn("Failed to clone");
        // TODO:
        kwarn("Please report whether or not the system freezes after this");
        mem_space_release_maps_array(&new_process->space);
        process_take_thread(new_process, new_thread);
        thread_delete(new_thread);
        if (!(flags & CLONE_THREAD)) {
            vfs_fdesc_unref(new_process->cwd);
            new_process->cwd = NULL;
            vfs_fdesc_list_unref(new_process->fdl);
            new_process->fdl = NULL;
            process_adopt_by(new_process, NULL);
            process_delete(new_process);
        }
        *errno_ptr = ENOMEM;
        return NULL;
    }

    // TODO: This all needs to be rewritten

    char saved_user_stack[PROCESS_CLONE_USER_STACK_BYTES_SAVED];
    if (stack) {
        new_thread->user_state->usp = (long) stack;
    } else if (flags & CLONE_VFORK) {
        // Save some bytes above USP
        memcpy(saved_user_stack, (void *) thread_current->user_state->usp, PROCESS_CLONE_USER_STACK_BYTES_SAVED);
    }


    if (flags & CLONE_VFORK) {
        thread_scheduler_lock();

        wakelist_thread_node wakelist_node;
        WAKELIST_WITH_LOCK(&new_process->vfork_wklst, {
            thread_current->state = THREAD_STATE_SLEEPING;
            wakelist_add_self(&new_process->vfork_wklst, &wakelist_node);
        })

        thread_wake(new_thread);
        thread_schedule();

        WAKELIST_WITH_LOCK(&new_process->vfork_wklst, {
            wakelist_remove_self(&new_process->vfork_wklst);
        })

        thread_scheduler_unlock();
    } else {
        thread_wake(new_thread);
    }


    if (!stack && (flags & CLONE_VFORK)) {
        // Restore saved bytes above USP
        memcpy((void *) thread_current->user_state->usp, saved_user_stack, PROCESS_CLONE_USER_STACK_BYTES_SAVED);
    }

    return new_process;
}

noreturn void process_exit_thread(int status) {
    thread_scheduler_lock();

    process *owner_process = thread_current->owner_process;
    kassert(owner_process->pid != 1);

    // TODO: SIGEXIT all other threads, for now we assume there are no other threads
    // TODO: Wait on other threads to terminate

    owner_process->status = status & 0377;
    owner_process->state = PROCESS_STATE_EXIT;

    process_release_resources(owner_process);

    // Orphan process's children
    MUTEX_WITH(&owner_process->children_mtx, for (
        process *this_child = list_pop_front(&owner_process->children, process, children_link);
        this_child;
        this_child = list_pop_front(&owner_process->children, process, children_link)
    ) {
        MUTEX_WITH(&init_process->children_mtx, {
            this_child->parent = init_process;
            list_push_back(&init_process->children, this_child, children_link);
        })
    })

    // Unblock any vfork-waiting process
    bool woke_any_vfork;
    do {
        WAKELIST_WITH_LOCK(&owner_process->vfork_wklst, {
            woke_any_vfork = wakelist_wake_all(&owner_process->vfork_wklst);
        })
        if (woke_any_vfork) thread_schedule();
    } while (woke_any_vfork);

    // Remove this thread from process
    process_take_thread(owner_process, thread_current);

    // From this point on, the old process is not attached to any thread

    // Unblock any waitpid-waiting parent process
    process *parent = owner_process->parent;
    if (parent) {
        WAKELIST_WITH_LOCK(&parent->waitpid_wklst, {
            wakelist_wake_all(&parent->waitpid_wklst);
        })
    }

    thread_terminate();

    kpanic("Thread unblocked from exit");
}

// Only called by process_waitpid
static bool waitpid_find_child(process *this, pid_t pid, bool *found_some_appropriate_pid, process **found_process) {
    if (pid != -1 && pid != this->pid) {
        // If we're looking for a specific PID, and this isn't it, continue
        return true;
    }

    *found_some_appropriate_pid = true;

    if (this->state == PROCESS_STATE_EXIT) {
        *found_process = this;
        list_remove(&thread_current->owner_process->children, this, children_link);
        // End the apply early
        return false;
    }

    return true;
}

pid_t process_waitpid(pid_t pid, FLEX_MUT(int) wstatus, int options, int *errno_ptr) {
    // Validate options
    if (options & ~(WNOHANG | WCONTINUED | WUNTRACED)) {
        *errno_ptr = EINVAL;
        return -1;
    }

    // TODO: Groups don't exist, related options are invalid
    if (pid == 0 || pid < -1) {
        *errno_ptr = EINVAL;
        return -1;
    }

    // TODO: These options aren't yet handled
    if (options & (WCONTINUED | WUNTRACED)) {
        *errno_ptr = EINVAL;
        return -1;
    }

    process *found_process = NULL;

    thread_scheduler_lock();
    wakelist_lock(&thread_current->owner_process->waitpid_wklst);
    thread_current->state = THREAD_STATE_SLEEPING;
    wakelist_thread_node wakelist_node;
    wakelist_add_self(&thread_current->owner_process->waitpid_wklst, &wakelist_node);

    do {
        bool found_some_appropriate_pid = false;

        MUTEX_WITH(&thread_current->owner_process->children_mtx, {
            list_apply(&thread_current->owner_process->children,
                       waitpid_find_child, process, children_link,
                       pid, &found_some_appropriate_pid, &found_process);
        })

        if (found_process) {
            break;
        }

        if (!found_some_appropriate_pid) {
            thread_current->state = THREAD_STATE_ACTIVE;
            wakelist_remove_self(&thread_current->owner_process->waitpid_wklst);
            wakelist_unlock(&thread_current->owner_process->waitpid_wklst);
            thread_scheduler_unlock();
            *errno_ptr = ECHILD;
            return -1;
        }

        // Wait to be woken up by a child doing something relevant
        thread_current->state = THREAD_STATE_SLEEPING;
        wakelist_unlock(&thread_current->owner_process->waitpid_wklst);
        thread_schedule();
        wakelist_lock(&thread_current->owner_process->waitpid_wklst);
    } while (!(options & WNOHANG));

    thread_current->state = THREAD_STATE_ACTIVE;
    wakelist_remove_self(&thread_current->owner_process->waitpid_wklst);
    wakelist_unlock(&thread_current->owner_process->waitpid_wklst);
    thread_scheduler_unlock();

    if (found_process) {
        if (wstatus.ptr.user) {
            int wstatus_k = found_process->status;
            flex_to(wstatus, &wstatus_k, sizeof(wstatus_k));
        }
        return found_process->pid;
    }

    // TODO: Delete process when reaping

    return 0;
}

bool process_threads_setup_clone(thread *thrd) {
    thrd->sup_stack = mem_mapping_clone_copy(thread_current->sup_stack);
    if(!thrd->sup_stack) {
        return false;
    }

    thrd->user_state = (struct exception_state *) (
        (uintptr_t) thread_current->user_state -
        (uintptr_t) mem_mapping_k_at(thread_current->sup_stack) +
        (uintptr_t) mem_mapping_k_at(thrd->sup_stack));

    // Child returns with 0
    // TODO: Does it always?
    thrd->user_state->d[0] = 0;

    // Setup stack for thread_switch()
    thrd->ssp = (void *) thrd->user_state;
    thread_setup_stack(thrd, exception_leave);

    return true;
}
