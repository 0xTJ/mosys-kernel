#include "libc/assert.h"
#include "kio/klog.h"

noreturn void assert_fail(const char *assertion, const char *file, unsigned int line, const char *function) {
    if (function) {
        kpanic_loc(file, line, "%s: Assertion %s failed", function, assertion);
    } else {
        kpanic_loc(file, line, "Assertion %s failed", assertion);
    }
}
