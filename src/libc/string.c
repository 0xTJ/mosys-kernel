#include <string.h>
#include "config.h"

// The compiler can be smart enough to recognize the memmove pattern.
// If it does not, an area of size count is created on the stack.
// STRING_USE_UNSAFE_MEMMOVE forces array comparison between different objects,
// which entirely skips the temporary array in the C code.

// String manipulation

char *strcpy(char *restrict dest, const char *restrict src) {
    char *dest_orig = dest;
    do {
        *(dest++) = *src;
    } while (*(src++));
    return dest_orig;
}

char *strncpy(char *restrict dest, const char *restrict src, size_t count) {
    char *dest_orig = dest;
    while (count > 0) {
        count -= 1;
        *(dest++) = *src;
        if (!*(src++)) {
            break;
        }
    };
    while (count > 0) {
        count -= 1;
        *(dest++) = '\0';
    }
    return dest_orig;
}

// String examination

size_t strlen(const char *str) {
    size_t count = 0;
    while (*(str++)) {
        count += 1;
    }
    return count;
}

size_t strnlen(const char *str, size_t strsz) {
    size_t count = 0;
    while (count < strsz && *(str++)) {
        count += 1;
    }
    return count;
}

int strcmp(const char *lhs, const char *rhs) {
    while (*lhs || *rhs) {
        int cmp = (unsigned char) *(lhs++) - (unsigned char) *(rhs++);
        if (cmp != 0) {
            return cmp;
        }
    }
    return 0;
}

int strncmp(const char *lhs, const char *rhs, size_t count) {
    while (count > 0 && (*lhs || *rhs)) {
        count -= 1;
        int cmp = (unsigned char) *(lhs++) - (unsigned char) *(rhs++);
        if (cmp != 0) {
            return cmp;
        }
    }
    return 0;
}

char *strchr(const char *str, int ch) {
    do {
        if (*str == (char) ch) {
            return (char *) str;
        }
    } while (*(str++));
    return NULL;
}

char *strnchr(const char *str, size_t count, int ch) {
    size_t len = strnlen(str, count);
    if (len) {
        return memchr(str, ch, len);
    } else {
        return NULL;
    }
}

// Character array manipulation

void *memchr(const void *ptr, int ch, size_t count) {
    const unsigned char *ptr_ptr = ptr;
    while (count > 0) {
        count -= 1;
        if (*ptr_ptr == (unsigned char) ch) {
            return (void *) ptr_ptr;
        }
        ptr_ptr += 1;
    }
    return NULL;
}

int memcmp(const void *lhs, const void *rhs, size_t count) {
    const unsigned char *lhs_ptr = lhs;
    const unsigned char *rhs_ptr = rhs;

    while (count > 0) {
        count -= 1;
        int cmp = *(lhs_ptr++) - *(rhs_ptr++);
        if (cmp != 0) {
            return cmp;
        }
    }
    return 0;
}

void *memset(void *dest, int ch, size_t count) {
    void *dest_orig = dest;
    unsigned char *dest_ptr = dest;
    for (; count > 0; --count) {
        *(dest_ptr++) = (unsigned char) ch;
    }
    return dest_orig;
}

void *memcpy(void *restrict dest, const void *restrict src, size_t count) {
    void *dest_orig = dest;
    unsigned char *dest_ptr = dest;
    const unsigned char *src_ptr = src;
    for (; count > 0; --count) {
        *(dest_ptr++) = *(src_ptr++);
    }
    return dest_orig;
}

void *memmove(void* dest, const void* src, size_t count) {
#if STRING_USE_UNSAFE_MEMMOVE
    unsigned char *dest_ptr = dest;
    const unsigned char *src_ptr = src;
    if (dest_ptr < src_ptr) {
        for (; count > 0; --count) {
            *(dest_ptr++) = *(src_ptr++);
        }
    } else if (dest_ptr > src_ptr) {
        dest_ptr += count;
        src_ptr += count;
        for (; count > 0; --count) {
            *(--dest_ptr) = *(--src_ptr);
        }
    }
#else
    unsigned char tmp[count];
    memcpy(tmp, src, count);
    memcpy(dest, tmp, count);
#endif
    return dest;
}
