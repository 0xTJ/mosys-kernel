#include "config.h"
#include "device/device.h"
#include "vfs/devreg.h"
#include "mem/flex.h"
#include "gpio.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "uapi/errno.h"
#include "uapi/asm/ioctl.h"
#include "uapi/mosys/spi/spidev.h"
#include "util/circbuf.h"
#include <string.h>

#if DEV_SPIDEV_ENABLE

static module_status dev_spidev_init(void);
static void dev_spidev_deinit(void);

static inline void spidev_sclk_set(bool value);
static inline void spidev_mosi_set(gpio_mask all_bits);
static inline bool spidev_miso_get(void);
static inline void spidev_cs0_set(bool value);
static inline void spidev_csX_set(int minor, bool value);
static inline void spidev_delay_half_cycle(void);
static inline uint8_t spidev_send_recv_byte_mode0(uint8_t send_value) __attribute__((always_inline));
static inline uint8_t spidev_send_recv_byte_mode1(uint8_t send_value) __attribute__((always_inline));
static inline uint8_t spidev_send_recv_byte_mode2(uint8_t send_value) __attribute__((always_inline));
static inline uint8_t spidev_send_recv_byte_mode3(uint8_t send_value) __attribute__((always_inline));
static void spidev_send_recv_mode0(size_t send_count, const uint8_t send_buf[send_count], size_t recv_count, uint8_t recv_buf[recv_count]);
static void spidev_send_recv_mode1(size_t send_count, const uint8_t send_buf[send_count], size_t recv_count, uint8_t recv_buf[recv_count]);
static void spidev_send_recv_mode2(size_t send_count, const uint8_t send_buf[send_count], size_t recv_count, uint8_t recv_buf[recv_count]);
static void spidev_send_recv_mode3(size_t send_count, const uint8_t send_buf[send_count], size_t recv_count, uint8_t recv_buf[recv_count]);
static int spidev_transfer(int minor, struct spi_ioc_transfer *transfer, int *errno_ptr);
static int spidev_message(int minor, size_t count, struct spi_ioc_transfer transfer[count], int *errno_ptr);

static ssize_t dev_spidev_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_spidev_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int dev_spidev_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr);

static module dev_spidev = {
    .init = dev_spidev_init,
    .deinit = dev_spidev_deinit,
    .name = "dev_spidev",
};
MODULE_DEFINE(dev_spidev);

static const int dev_spidev_major = 3;

static unsigned char spi_mode = 0;
static unsigned char bits_per_word = 8;

static device_operations dev_spidev_dops = {
    .read = dev_spidev_read,
    .write = dev_spidev_write,
    .ioctl = dev_spidev_ioctl,
};

static module_status dev_spidev_init(void) {
    gpio_pindir_set(
        gpio_to_mask(GPIO_NUM_SPI_MISO),
        GPIO_PINDIR_INPUT
    );
    gpio_pindir_set(
        gpio_to_mask(GPIO_NUM_SPI_CS0) |
        gpio_to_mask(GPIO_NUM_SPI_SCLK) |
        gpio_to_mask(GPIO_NUM_SPI_MOSI) |
        gpio_to_mask(GPIO_NUM_SPI_CS1),
        GPIO_PINDIR_OUTPUT
    );

    if (dev_spidev_major != device_register(dev_spidev_major, &dev_spidev_dops)) {
        kpanic("Failed to register spidev devices");
    }

    devreg_new("spidev0.0", dev_spidev_major, 0);
    devreg_new("spidev0.1", dev_spidev_major, 1);

    return MODULE_LOADED;
}

// TODO: This belongs in SD code
void spidev_reset_sd0() {
    spidev_csX_set(0, true);
    for (int i = 0; i < 10; ++i) {
        switch (spi_mode) {
            case 0: spidev_send_recv_byte_mode0(0xFF); break;
            case 1: spidev_send_recv_byte_mode1(0xFF); break;
            case 2: spidev_send_recv_byte_mode2(0xFF); break;
            case 3: spidev_send_recv_byte_mode3(0xFF); break;
            default: unreachable();
        }
    }
}

static void dev_spidev_deinit(void) {
    device_unregister(dev_spidev_major);
}

static ssize_t dev_spidev_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    UNUSED(offset);

    switch(minor) {
    case 0: // spidev0.0
    case 1: // spidev0.1
        break;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }

    flex_mut_pinned buf_pin = flex_pin_must_cleanup(buf);
    if (buf_pin.fault || buf_pin.size < count) {
        *errno_ptr = EFAULT;
        return -1;
    }
    uint8_t *buf_ptr = buf_pin.ptr;

    spidev_csX_set(minor, false);
    switch (spi_mode) {
        case 0: spidev_send_recv_mode0(0, NULL, count, buf_ptr); break;
        case 1: spidev_send_recv_mode1(0, NULL, count, buf_ptr); break;
        case 2: spidev_send_recv_mode2(0, NULL, count, buf_ptr); break;
        case 3: spidev_send_recv_mode3(0, NULL, count, buf_ptr); break;
        default: unreachable();
    }
    spidev_csX_set(minor, true);

    flex_unpin(&buf_pin);
    buf_ptr = NULL;

    return count;
}

static ssize_t dev_spidev_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    UNUSED(offset);

    switch(minor) {
    case 0: // spidev0.0
    case 1: // spidev0.1
        break;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }

    flex_const_pinned buf_pin = flex_pin_must_cleanup(buf);
    if (buf_pin.fault || buf_pin.size < count) {
        *errno_ptr = EFAULT;
        return -1;
    }
    const uint8_t *buf_ptr = buf_pin.ptr;

    spidev_csX_set(minor, false);
    switch (spi_mode) {
        case 0: spidev_send_recv_mode0(count, buf_ptr, 0, NULL); break;
        case 1: spidev_send_recv_mode1(count, buf_ptr, 0, NULL); break;
        case 2: spidev_send_recv_mode2(count, buf_ptr, 0, NULL); break;
        case 3: spidev_send_recv_mode3(count, buf_ptr, 0, NULL); break;
        default: unreachable();
    }
    spidev_csX_set(minor, true);

    flex_unpin(&buf_pin);
    buf_ptr = NULL;

    return count;
}

static int dev_spidev_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr) {
    switch(minor) {
    case 0: // spidev0.0
    case 1: // spidev0.1
        break;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }

    if (_IOC_TYPE(request) != SPI_IOC_MAGIC) {
        *errno_ptr = EINVAL;
        return -1;
    }

    argp.size = _IOC_SIZE(request);
    flex_mut_pinned argp_pin = flex_pin_must_cleanup(argp);
    if (argp_pin.fault) {
        *errno_ptr = EFAULT;
        return -1;
    }
    // Check flex size before using argp_pin.ptr.

    switch(_IOC_NR(request)) {
    case 0:
        size_t count = argp_pin.size / sizeof(struct spi_ioc_transfer);
        int message_result = spidev_message(minor, count, argp_pin.ptr, errno_ptr);
        flex_unpin(&argp_pin);
        return message_result;

    default:
        flex_unpin(&argp_pin);
        *errno_ptr = EINVAL;
        return -1;
    }
}

static inline void spidev_sclk_set(bool value) {
    gpio_output_write(GPIO_NUM_SPI_SCLK, value);
}

static_assert((GPIO_NUM_SPI_MOSI - GPIO_NUM_OUTPUTS_START) < 8);
static inline void spidev_mosi_set(gpio_mask all_bits) {
    gpio_output_write_mask(gpio_to_mask(GPIO_NUM_SPI_MOSI), all_bits << GPIO_NUM_OUTPUTS_START);
}

static inline bool spidev_miso_get(void) {
    return gpio_input_read(GPIO_NUM_SPI_MISO);
}

static inline void spidev_csX_set(int minor, bool value) {
    switch(minor) {
    case 0: // spidev0.0
        gpio_output_write(GPIO_NUM_SPI_CS0, value);
        break;
    case 1: // spidev0.1
        gpio_output_write(GPIO_NUM_SPI_CS1, value);
        break;
    }
}

static inline void spidev_delay_half_cycle(void) {
    // We're already slow enough
}

// Upper 24 bits of result are undefined
static inline uint32_t spidev_pop_byte_top_bit_to_full_byte(uint8_t *in) {
    uint32_t out;
    __asm__ volatile (
        "add%.b %[in], %[in] \n"
        "scs %[out] \n"
        : [in] "+d" (*in), [out] "=d" (out)
        :
        : "cc"
    );
    return out;
}

// Assumes correct CS is asserted, thread-safety has been ensured, and SCLK is in the idle state
static inline uint8_t spidev_send_recv_byte_mode0(uint8_t value) {
    assert(bits_per_word == 8);

#pragma GCC unroll 8
    for (unsigned char bit_index = 0; bit_index < bits_per_word; ++bit_index) {
        spidev_mosi_set(spidev_pop_byte_top_bit_to_full_byte(&value));
        spidev_delay_half_cycle();
        spidev_sclk_set(true);
        value |= spidev_miso_get();
        spidev_delay_half_cycle();
        spidev_sclk_set(false);
    }

    return value;
}

// Assumes correct CS is asserted, thread-safety has been ensured, and SCLK is in the idle state
static inline uint8_t spidev_send_recv_byte_mode1(uint8_t value) {
    assert(bits_per_word == 8);

#pragma GCC unroll 8
    for (unsigned char bit_index = 0; bit_index < bits_per_word; ++bit_index) {
        spidev_sclk_set(true);
        spidev_mosi_set(spidev_pop_byte_top_bit_to_full_byte(&value));
        spidev_delay_half_cycle();
        spidev_sclk_set(false);
        value |= spidev_miso_get();
        spidev_delay_half_cycle();
    }

    return value;
}

// Assumes correct CS is asserted, thread-safety has been ensured, and SCLK is in the idle state
static inline uint8_t spidev_send_recv_byte_mode2(uint8_t value) {
    assert(bits_per_word == 8);

#pragma GCC unroll 8
    for (unsigned char bit_index = 0; bit_index < bits_per_word; ++bit_index) {
        spidev_mosi_set(spidev_pop_byte_top_bit_to_full_byte(&value));
        spidev_delay_half_cycle();
        spidev_sclk_set(false);
        value |= spidev_miso_get();
        spidev_delay_half_cycle();
        spidev_sclk_set(true);
    }

    return value;
}

// Assumes correct CS is asserted, thread-safety has been ensured, and SCLK is in the idle state
static inline uint8_t spidev_send_recv_byte_mode3(uint8_t value) {
    assert(bits_per_word == 8);

#pragma GCC unroll 8
    for (unsigned char bit_index = 0; bit_index < bits_per_word; ++bit_index) {
        spidev_sclk_set(false);
        spidev_mosi_set(spidev_pop_byte_top_bit_to_full_byte(&value));
        spidev_delay_half_cycle();
        spidev_sclk_set(true);
        value |= spidev_miso_get();
        spidev_delay_half_cycle();
    }

    return value;
}

// Assumes correct CS is asserted, thread-safety has been ensured, and SCLK is in the idle state
static void spidev_send_recv_mode0(size_t send_count, const uint8_t send_buf[send_count], size_t recv_count, uint8_t recv_buf[recv_count]) {
    if (send_count == recv_count) {
        // Send and receive equal counts
        size_t send_and_recv_count = send_count;
        for (size_t index = 0; index < send_and_recv_count; ++index) {
            recv_buf[index] = spidev_send_recv_byte_mode0(send_buf[index]);
        }
    } else if (recv_count == 0) {
        // Only send
        for (size_t index = 0; index < send_count; ++index) {
            spidev_send_recv_byte_mode0(send_buf[index]);
        }
    } else if (send_count == 0) {
        // Only receive
        for (size_t index = 0; index < recv_count; ++index) {
            recv_buf[index] = spidev_send_recv_byte_mode0(0);
        }
    } else {
        // Send and receive different counts
        for (size_t index = 0; index < send_count || index < recv_count; ++index) {
            uint8_t send_value = index < send_count ? send_buf[index] : 0;
            uint8_t recv_value = spidev_send_recv_byte_mode0(send_value);
            if (index < recv_count) {
                recv_buf[index] = recv_value;
            }
        }
    }
}

// Assumes correct CS is asserted, thread-safety has been ensured, and SCLK is in the idle state
static void spidev_send_recv_mode1(size_t send_count, const uint8_t send_buf[send_count], size_t recv_count, uint8_t recv_buf[recv_count]) {
    if (send_count == recv_count) {
        // Send and receive equal counts
        size_t send_and_recv_count = send_count;
        for (size_t index = 0; index < send_and_recv_count; ++index) {
            recv_buf[index] = spidev_send_recv_byte_mode1(send_buf[index]);
        }
    } else if (recv_count == 0) {
        // Only send
        for (size_t index = 0; index < send_count; ++index) {
            spidev_send_recv_byte_mode1(send_buf[index]);
        }
    } else if (send_count == 0) {
        // Only receive
        for (size_t index = 0; index < recv_count; ++index) {
            recv_buf[index] = spidev_send_recv_byte_mode1(0);
        }
    } else {
        // Send and receive different counts
        for (size_t index = 0; index < send_count || index < recv_count; ++index) {
            uint8_t send_value = index < send_count ? send_buf[index] : 0;
            uint8_t recv_value = spidev_send_recv_byte_mode1(send_value);
            if (index < recv_count) {
                recv_buf[index] = recv_value;
            }
        }
    }
}

// Assumes correct CS is asserted, thread-safety has been ensured, and SCLK is in the idle state
static void spidev_send_recv_mode2(size_t send_count, const uint8_t send_buf[send_count], size_t recv_count, uint8_t recv_buf[recv_count]) {
    if (send_count == recv_count) {
        // Send and receive equal counts
        size_t send_and_recv_count = send_count;
        for (size_t index = 0; index < send_and_recv_count; ++index) {
            recv_buf[index] = spidev_send_recv_byte_mode2(send_buf[index]);
        }
    } else if (recv_count == 0) {
        // Only send
        for (size_t index = 0; index < send_count; ++index) {
            spidev_send_recv_byte_mode2(send_buf[index]);
        }
    } else if (send_count == 0) {
        // Only receive
        for (size_t index = 0; index < recv_count; ++index) {
            recv_buf[index] = spidev_send_recv_byte_mode2(0);
        }
    } else {
        // Send and receive different counts
        for (size_t index = 0; index < send_count || index < recv_count; ++index) {
            uint8_t send_value = index < send_count ? send_buf[index] : 0;
            uint8_t recv_value = spidev_send_recv_byte_mode2(send_value);
            if (index < recv_count) {
                recv_buf[index] = recv_value;
            }
        }
    }
}

// Assumes correct CS is asserted, thread-safety has been ensured, and SCLK is in the idle state
static void spidev_send_recv_mode3(size_t send_count, const uint8_t send_buf[send_count], size_t recv_count, uint8_t recv_buf[recv_count]) {
    if (send_count == recv_count) {
        // Send and receive equal counts
        size_t send_and_recv_count = send_count;
        for (size_t index = 0; index < send_and_recv_count; ++index) {
            recv_buf[index] = spidev_send_recv_byte_mode3(send_buf[index]);
        }
    } else if (recv_count == 0) {
        // Only send
        for (size_t index = 0; index < send_count; ++index) {
            spidev_send_recv_byte_mode3(send_buf[index]);
        }
    } else if (send_count == 0) {
        // Only receive
        for (size_t index = 0; index < recv_count; ++index) {
            recv_buf[index] = spidev_send_recv_byte_mode3(0);
        }
    } else {
        // Send and receive different counts
        for (size_t index = 0; index < send_count || index < recv_count; ++index) {
            uint8_t send_value = index < send_count ? send_buf[index] : 0;
            uint8_t recv_value = spidev_send_recv_byte_mode3(send_value);
            if (index < recv_count) {
                recv_buf[index] = recv_value;
            }
        }
    }
}

static int spidev_transfer(int minor, struct spi_ioc_transfer *transfer, int *errno_ptr) {
    UNUSED(errno_ptr);

    size_t tx_len = transfer->tx_buf ? transfer->len : 0;
    size_t rx_len = transfer->rx_buf ? transfer->len : 0;
    // TODO: A non-zero len with both buffers being NULL will transfer no bytes
    spidev_csX_set(minor, false);
    switch (spi_mode) {
        case 0: spidev_send_recv_mode0(tx_len, transfer->tx_buf, rx_len, transfer->rx_buf); break;
        case 1: spidev_send_recv_mode1(tx_len, transfer->tx_buf, rx_len, transfer->rx_buf); break;
        case 2: spidev_send_recv_mode2(tx_len, transfer->tx_buf, rx_len, transfer->rx_buf); break;
        case 3: spidev_send_recv_mode3(tx_len, transfer->tx_buf, rx_len, transfer->rx_buf); break;
        default: unreachable();
    }
    if (transfer->cs_change) {
        spidev_csX_set(minor, true);
        spidev_mosi_set(true);
    }
    return 0;
}

// TODO: Check buffer pointers

static int spidev_message(int minor, size_t count, struct spi_ioc_transfer transfer[count], int *errno_ptr) {
    for (size_t transfers_done = 0; transfers_done < count; ++transfers_done) {
        int status = spidev_transfer(minor, &transfer[transfers_done], errno_ptr);
        if (status < 0) {
            spidev_csX_set(minor, true);
            return status;
        }
    }
    return 0;
}

#endif
