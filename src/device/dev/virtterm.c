#include "config.h"
#include "constants.h"
#include "safeprobe.h"
#include "timer.h"
#include "device/device.h"
#include "device/tty_util.h"
#include "device/virtterm.h"
#include "device/dev/tty.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "thread/thread.h"
#include "uapi/errno.h"
#include "uapi/limits.h"
#include "util/circbuf.h"
#include "util/rwlock.h"
#include "vfs/devreg.h"
#include <string.h>

#if DEV_VIRTTERM_ENABLE

static module_status dev_virtterm_init(void);
static void dev_virtterm_deinit(void);

static ssize_t dev_virtterm_read(struct tty_info *info, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_virtterm_write(struct tty_info *info, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int dev_virtterm_ioctl(struct tty_info *info, unsigned long request, flex_mut argp, int *errno_ptr);

static const char *const dev_virtterm_deps[] = {
    "fb",
    NULL,
};
static module dev_virtterm = {
    .init = dev_virtterm_init,
    .deinit = dev_virtterm_deinit,
    .name = "dev_virtterm",
    .deps = dev_virtterm_deps,
};
MODULE_DEFINE(dev_virtterm);

static tty_ops dev_virtterm_tops = {
    .read = dev_virtterm_read,
    .write = dev_virtterm_write,
    .ioctl = dev_virtterm_ioctl,
};

static module_status dev_virtterm_init(void) {
    devreg *screen_dr = devreg_lookup(DEV_VIRTTERM_DEV_OUT_FB);
    if (!screen_dr) {
        return MODULE_DEFERRED_LOAD;
    }

    devreg *kbd_dr = NULL;
#if DEV_VIRTTERM_KBD_ENABLE
    if (!kbd_dr) {
        // Try to use keyboard
        kbd_dr = devreg_lookup(DEV_VIRTTERM_DEV_IN_KBD);
    }
#endif
#if DEV_VIRTTERM_ALT_RAW_TTY_ENABLE
    if (!kbd_dr) {
        // If keyboard does not exist, use DEV_VIRTTERM_DEV_IN_ALT_RAW_TTY as input
        kbd_dr = devreg_lookup(DEV_VIRTTERM_DEV_IN_ALT_RAW_TTY);
        if (kbd_dr) {
            kinfo("Keyboard not available, using device %s as console input", DEV_VIRTTERM_DEV_IN_ALT_RAW_TTY);

            // Set device to raw mode
            // TODO: Check for errors
            int dummy_errno;
            tty_util_make_raw(kbd_dr->major, kbd_dr->minor, &dummy_errno);
        }
    }
#endif
#if DEV_VIRTTERM_REQUIRE_DEV_IN
    if (!kbd_dr) {
        devreg_unref(screen_dr);
        return MODULE_DEFERRED_LOAD;
    }
#endif

    virtterm_info *vt_info = kalloc(sizeof(virtterm_info));

    tty_info *info = tty_new();
    if (!info) {
        kfree(vt_info);
        devreg_unref(screen_dr);
        return MODULE_FAILED_LOAD;
    }

    vt_info->kbd_dr = kbd_dr;
    vt_info->screen_dr = screen_dr;

    // TODO: Check this results
    int dummy_errno;
    virtterm_init_vt_info(vt_info, &dummy_errno);

    info->ops = &dev_virtterm_tops;
    info->data_ptr = vt_info;

    tty_register(info, "tty%u");

    return MODULE_LOADED;
}

static void dev_virtterm_deinit(void) {
    // TODO: Delete TTY devices
}

static ssize_t dev_virtterm_read(struct tty_info *info, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    virtterm_info *vt_info = info->data_ptr;

    // It is possible for vt_info->kbd_dr to be NULL if DEV_VIRTTERM_REQUIRE_DEV_IN is 0
    if (!vt_info->kbd_dr) {
        return 0;
    }

    ssize_t result = device_read(vt_info->kbd_dr->major, vt_info->kbd_dr->minor, buf, count, offset, errno_ptr);
    if (result < 0) {
        return result;
    }

    return result;
}

static ssize_t dev_virtterm_write(struct tty_info *info, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    UNUSED(offset);

    flex_const_pinned buf_pin = flex_pin_must_cleanup(buf);
    if (buf_pin.fault || buf_pin.size < count) {
        *errno_ptr = EFAULT;
        return -1;
    }
    const uint8_t *buf_ptr = buf_pin.ptr;

    virtterm_info *vt_info = info->data_ptr;
    for (size_t i = 0; i < count; ++i) {
        uint8_t this_char = buf_ptr[i];
        virtterm_handle_code(vt_info, this_char, errno_ptr);
    }
    flex_unpin(&buf_pin);
    return count;
}

static int dev_virtterm_ioctl(tty_info *info, unsigned long request, flex_mut argp, int *errno_ptr) {
    UNUSED(info);
    UNUSED(request);
    UNUSED(argp);

    *errno_ptr = EINVAL;
    return -1;
}

#endif
