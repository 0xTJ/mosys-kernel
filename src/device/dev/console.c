#include "device/console.h"
#include "device/device.h"
#include "vfs/devreg.h"
#include "mem/flex.h"
#include "kernel/module.h"
#include "kio/klog.h"

static module_status dev_console_init(void);
static void dev_console_deinit(void);

static ssize_t dev_console_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_console_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int dev_console_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr);

static module dev_console = {
    .init = dev_console_init,
    .deinit = dev_console_deinit,
    .name = "dev_console",
};
MODULE_DEFINE(dev_console);

static device_operations dev_console_dops = {
    dev_console_read,
    dev_console_write,
    dev_console_ioctl,
};

static devreg *dev_console_dr = NULL;
static int dev_console_major = -1;

static module_status dev_console_init(void) {
    dev_console_major = device_register(0, &dev_console_dops);
    if (dev_console_major < 0) {
        return MODULE_FAILED_LOAD;
    }

    dev_console_dr = devreg_new("console", dev_console_major, 0);
    if (!dev_console_dr) {
        device_unregister(dev_console_major);
        dev_console_major = -1;
        return MODULE_FAILED_LOAD;
    }

    return MODULE_LOADED;
}

static void dev_console_deinit(void) {
    device_unregister(dev_console_major);
}

static ssize_t dev_console_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    if (minor != 0) {
        *errno_ptr = ENODEV;
    }
    return console_read(buf, count, offset, errno_ptr);
}

static ssize_t dev_console_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    if (minor != 0) {
        *errno_ptr = ENODEV;
    }
    return console_write(buf, count, offset, errno_ptr);
}

static int dev_console_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr) {
    if (minor != 0) {
        *errno_ptr = ENODEV;
    }
    return console_ioctl(request, argp, errno_ptr);
}
