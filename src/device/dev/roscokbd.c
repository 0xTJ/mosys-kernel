#include "config.h"
#include "constants.h"
#include "safeprobe.h"
#include "timer.h"
#include "device/device.h"
#include "device/tty_util.h"
#include "device/dev/tty.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "thread/thread.h"
#include "thread/timing.h"
#include "uapi/errno.h"
#include "util/circbuf.h"
#include "util/rwlock.h"
#include "vfs/devreg.h"
#include <string.h>

#if DEV_ROSCOKBD_ENABLE

static rwlock dev_roscokbd_serial_dr_rwl = RWLOCK_INIT;
static devreg *dev_roscokbd_serial_dr;
static devreg *dev_roscokbd_dr = NULL;
static int dev_roscokbd_major = -1;

static module_status dev_roscokbd_init(void);
static void dev_roscokbd_deinit(void);

static ssize_t dev_roscokbd_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_roscokbd_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int dev_roscokbd_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr);

static const char *const dev_roscokbd_deps[] = {
    "tty",
    NULL,
};
static module dev_roscokbd = {
    .init = dev_roscokbd_init,
    .deinit = dev_roscokbd_deinit,
    .name = "dev_roscokbd",
    .deps = dev_roscokbd_deps,
};
MODULE_DEFINE(dev_roscokbd);

static device_operations dev_roscokbd_dops = {
    .read = dev_roscokbd_read,
    .write = dev_roscokbd_write,
    .ioctl = dev_roscokbd_ioctl,
};

static module_status dev_roscokbd_init(void) {
    RWLOCK_WRITE_WITH(&dev_roscokbd_serial_dr_rwl, {
        dev_roscokbd_serial_dr = devreg_lookup(DEV_ROSCOKBD_TTY);
        if (!dev_roscokbd_serial_dr) {
            RWLOCK_GOTO_NO_UNLOCK(failed_lookup);
        }
    })

    // Set device to raw mode
    // TODO: Check for errors
    // TODO: Correctly manage MIN and TIME
    int dummy_errno;
    tty_util_make_raw(dev_roscokbd_serial_dr->major, dev_roscokbd_serial_dr->minor, &dummy_errno);

    // Detect rosco_kbd
    // TODO: Check for errors
    uint8_t ident_buf[0x10];
    unsigned long long serial_read_offset = 0;
    ssize_t serial_result;
    flex_mut ident_buf_flx = flex_mut_kernel(&ident_buf, sizeof ident_buf);
    // Flush input
    // TODO: This could cause a DoS if bytes come too fast
    while ((serial_result = device_read(dev_roscokbd_serial_dr->major, dev_roscokbd_serial_dr->minor, ident_buf_flx, sizeof ident_buf, serial_read_offset, &dummy_errno)) > 0) {
        serial_read_offset += serial_result;
    }
    ident_buf[0] = 0xF0;    // IDENT
    serial_result = device_write(dev_roscokbd_serial_dr->major, dev_roscokbd_serial_dr->minor, flex_make_const(ident_buf_flx), 1, 0, &dummy_errno);
    timing_spin_us(100U * 1000U);   // TODO: This should not be needed once MIN and TIME are set properly and work
    serial_result = device_read(dev_roscokbd_serial_dr->major, dev_roscokbd_serial_dr->minor, ident_buf_flx, 0x10, serial_read_offset, &dummy_errno);
    // Check size and contents of identification packet
    if (serial_result >= 0x10 && ident_buf[0xF] == 0xFF) {
        // Received identification packet with ACK
        if (memcmp(ident_buf + 0x0, "rosco_kbd", 0x9) != 0) {
            kwarn("Unexpected rosco_kbd IDENT string");
            goto failed_detect;
        }
        // TODO: Check/manipulate other fields 
    } else if (serial_result >= 0x01 && ident_buf[0x0] == 0x00) {
        // Received NAK
        kwarn("Received NAK on rosco_kbd IDENT");
        goto failed_detect;
    } else {
        // Did not receive an expected response
        goto failed_detect;
    }

    dev_roscokbd_major = device_register(0, &dev_roscokbd_dops);
    if (dev_roscokbd_major < 0) {
        goto failed_register;
    }

    dev_roscokbd_dr = devreg_new("kbd", dev_roscokbd_major, 0);
    if (!dev_roscokbd_dr) {
        goto failed_devreg_new;
    }

    return MODULE_LOADED;

failed_devreg_new:
    device_unregister(dev_roscokbd_major);
    dev_roscokbd_major = -1;
failed_register:
failed_detect:
    // TODO: Undo changes to TTY settings?
    RWLOCK_WRITE_WITH(&dev_roscokbd_serial_dr_rwl, {
        devreg_unref(dev_roscokbd_serial_dr);
        dev_roscokbd_serial_dr = NULL;
    })
    return MODULE_FAILED_LOAD;
failed_lookup:
    rwlock_write_end(&dev_roscokbd_serial_dr_rwl);
    return MODULE_DEFERRED_LOAD;
}

static void dev_roscokbd_deinit(void) {
    // TODO: Delete TTY devices
}

static ssize_t dev_roscokbd_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    UNUSED(offset);

    if (minor != 0) {
        *errno_ptr = ENOENT;
        return -1;
    }

    ssize_t result;

    RWLOCK_READ_WITH(&dev_roscokbd_serial_dr_rwl, {
        if (dev_roscokbd_serial_dr) {
            result = device_read(dev_roscokbd_serial_dr->major, dev_roscokbd_serial_dr->minor, buf, count, offset, errno_ptr);
        } else {
            *errno_ptr = ENODEV;
            result = -1;
        }
    })

    return result;
}

static ssize_t dev_roscokbd_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    UNUSED(minor);
    UNUSED(buf);
    UNUSED(count);
    UNUSED(offset);

    *errno_ptr = EINVAL;
    return -1;
}

static int dev_roscokbd_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr) {
    UNUSED(minor);
    UNUSED(request);
    UNUSED(argp);

    *errno_ptr = EINVAL;
    return -1;
}

#endif
