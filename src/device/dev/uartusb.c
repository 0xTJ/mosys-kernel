#include "config.h"
#include "device/device.h"
#include "vfs/devreg.h"
#include "mem/flex.h"
#include "kernel/module.h"
#include "kio/kio.h"
#include "kio/klog.h"
#include "device/dev/input.h"
#include "device/dev/uartusb/ch559_usb_host.h"
#include "uapi/asm/ioctl.h"
#include "uapi/errno.h"
#include "uapi/mosys/input.h"
#include "usb/hid.h"
#include "util/circbuf.h"
#include <string.h>

#if DEV_UARTUSB_ENABLE

static module_status dev_uartusb_init(void);
static void dev_uartusb_deinit(void);

static ssize_t dev_uartusb_read(input_info *info, FLEX_MUT(struct input_event) buf, size_t count, unsigned long long ppos, int *errno_ptr);
static ssize_t dev_uartusb_write(input_info *info, FLEX_CONST(struct input_event) buf, size_t count, unsigned long long ppos, int *errno_ptr);
static int dev_uartusb_ioctl(struct input_info *info, unsigned long request, flex_mut argp, int *errno_ptr);

static devreg *underlying_dvrg;
static input_info *input;

static const char *const dev_uartusb_deps[] = {
    UARTUSB_MODULE_DEPS,
    NULL,
};
static module dev_uartusb = {
    .init = dev_uartusb_init,
    .deinit = dev_uartusb_deinit,
    .name = "dev_uartusb",
    .deps = dev_uartusb_deps,
};
MODULE_DEFINE(dev_uartusb);

static input_ops dev_uartusb_inops = {
    .read = dev_uartusb_read,
    .write = dev_uartusb_write,
    .ioctl = dev_uartusb_ioctl,
};

static module_status dev_uartusb_init(void) {
    underlying_dvrg = devreg_lookup(UARTUSB_DEVICE);
    if (!underlying_dvrg) {
        return MODULE_FAILED_LOAD;
    }

    input = input_new();
    if (!input) {
        devreg_unref(underlying_dvrg);
        return MODULE_FAILED_LOAD;
    }
    input->ops = &dev_uartusb_inops;
    input_register(input);

    return MODULE_LOADED;
}

static void dev_uartusb_deinit(void) {
    // TODO: Unregister then destroy input
    // TODO: Unref underlying_dvrg
}

static void parse_hid_report(struct ch559_usb_host_message *message, size_t payload_length, const uint8_t payload[payload_length]) {
    kinfo("USB HID Report: device=%02hhX, endpoint=%02hhX",
          message->device, message->endpoint);

    unsigned indent = 0;
    uint16_t usage_page = 0;
    uint16_t usage_id = 0;
    uint32_t usage = 0;

    size_t i = 0;
    uint8_t b_size;
    uint8_t b_offset;
    do {
        b_size = (payload[i] >> 0) & 0x03;
        switch (b_size) {
        default: break;
        case 3: b_size = 4; break;
        }
        b_offset = 1;
        usb_hid_type b_type = (payload[i] >> 2) & 0x03;
        uint8_t b_tag = (payload[i] >> 4) & 0x0F;

        if (b_size == 2 && b_type == usb_hid_type_reserved && b_tag == 0x0F) {
            b_size = payload[i + b_offset + 0];
            b_tag = payload[i + b_offset + 1];
            b_offset = 3;
        }

        switch (b_type) {
        case usb_hid_type_main: {
            const uint32_t data =
                (uint32_t) (b_size >= 4 ? payload[i + b_offset + 3] : 0) << 24 |
                (uint32_t) (b_size >= 3 ? payload[i + b_offset + 2] : 0) << 16 |
                (uint32_t) (b_size >= 2 ? payload[i + b_offset + 1] : 0) <<  8 |
                (uint32_t) (b_size >= 1 ? payload[i + b_offset + 0] : 0) <<  0;

            switch (b_tag) {
            case usb_hid_main_item_tag_input:
                kinfo("%*s%s (%s, %s, %s, %s, %s, %s, %s, %s),",
                       indent * 4, "",
                       usb_hid_main_item_tag_to_str(b_tag),
                       data & bit_u32(0) ? "Constant" : "Data",
                       data & bit_u32(1) ? "Variable" : "Array",
                       data & bit_u32(2) ? "Relative" : "Absolute",
                       data & bit_u32(3) ? "Wrap" : "No Wrap",
                       data & bit_u32(4) ? "Non Linear" : "Linear",
                       data & bit_u32(5) ? "No Preferred" : "Preferred State",
                       data & bit_u32(6) ? "Null State" : "No Null Position",
                       data & bit_u32(8) ? "Buffered Bytes" : "Bit Field");
                break;
            case usb_hid_main_item_tag_output:
            case usb_hid_main_item_tag_feature:
                kinfo("%*s%s (%s, %s, %s, %s, %s, %s, %s, %s, %s),",
                       indent * 4, "",
                       usb_hid_main_item_tag_to_str(b_tag),
                       data & bit_u32(0) ? "Constant" : "Data",
                       data & bit_u32(1) ? "Variable" : "Array",
                       data & bit_u32(2) ? "Relative" : "Absolute",
                       data & bit_u32(3) ? "Wrap" : "No Wrap",
                       data & bit_u32(4) ? "Non Linear" : "Linear",
                       data & bit_u32(5) ? "No Preferred" : "Preferred State",
                       data & bit_u32(6) ? "Null State" : "No Null Position",
                       data & bit_u32(7) ? "Volatile" : "Non Volatile",
                       data & bit_u32(8) ? "Buffered Bytes" : "Bit Field");
                break;
            case usb_hid_main_item_tag_collection:
                kinfo("%*s%s (%s),",
                       indent++ * 4, "",
                       usb_hid_main_item_tag_to_str(b_tag),
                       data == 0x00 ? "Physical" :
                       data == 0x01 ? "Application" :
                       data == 0x02 ? "Logical" :
                       data == 0x03 ? "Report" :
                       data == 0x04 ? "Named Array" :
                       data == 0x05 ? "Usage Switch" :
                       data == 0x06 ? "Usage Modifier" :
                       0x80 <= data && data <= 0xFF ? "Vendor-defined" :
                       "Reserved");
                break;
            case usb_hid_main_item_tag_end_collection:
                kinfo("%*s%s,",
                       --indent * 4, "",
                       usb_hid_main_item_tag_to_str(b_tag));
                break;
            }
        } break;
        case usb_hid_type_global: {
            const uint32_t data_u =
                (uint32_t) (b_size >= 4 ? payload[i + b_offset + 3] : 0) << 24 |
                (uint32_t) (b_size >= 3 ? payload[i + b_offset + 2] : 0) << 16 |
                (uint32_t) (b_size >= 2 ? payload[i + b_offset + 1] : 0) <<  8 |
                (uint32_t) (b_size >= 1 ? payload[i + b_offset + 0] : 0) <<  0;

            const bool sign_ext_bit =
                b_size > 0 ?
                    (payload[i + b_offset + (b_size - 1)] & 0x80) :
                    false;
            const uint8_t sign_ext_bits =
                sign_ext_bit * 0xFF;
            const uint32_t data_su =
                (uint32_t) (b_size >= 4 ? payload[i + b_offset + 3] : sign_ext_bits) << 24 |
                (uint32_t) (b_size >= 3 ? payload[i + b_offset + 2] : sign_ext_bits) << 16 |
                (uint32_t) (b_size >= 2 ? payload[i + b_offset + 1] : sign_ext_bits) <<  8 |
                (uint32_t) (b_size >= 1 ? payload[i + b_offset + 0] : sign_ext_bits) <<  0;
            const int32_t data_s = (int32_t) data_su;

            switch (b_tag) {
            case usb_hid_global_item_tag_usage_page: {
                usage_page = data_u;
                const char *usage_page_str = usb_hid_usage_page_to_str(usage_page);
                if (usage_page_str) {
                    kinfo("%*s%s (%s),",
                          indent * 4, "",
                          usb_hid_global_item_tag_to_str(b_tag),
                          usage_page_str);
                } else {
                    kinfo("%*s%s (%02lX),",
                          indent * 4, "",
                          usb_hid_global_item_tag_to_str(b_tag),
                          data_u);
                }
            } break;
            case usb_hid_global_item_tag_logical_minimum:
            case usb_hid_global_item_tag_logical_maximum:
            case usb_hid_global_item_tag_physical_minimum:
            case usb_hid_global_item_tag_physical_maximum:
                kinfo("%*s%s (%ld),",
                       indent * 4, "",
                       usb_hid_global_item_tag_to_str(b_tag),
                       (signed long) data_s);
                break;
            case usb_hid_global_item_tag_unit_exponent:
                kinfo("%*s%s (%ld),",
                       indent * 4, "",
                       usb_hid_global_item_tag_to_str(b_tag),
                       (signed long) data_s);
                break;
            case usb_hid_global_item_tag_unit:
                kinfo("%*s%s (%ld),",
                       indent * 4, "",
                       usb_hid_global_item_tag_to_str(b_tag),
                       (signed long) data_s);
                break;
            case usb_hid_global_item_tag_report_size:
                kinfo("%*s%s (%lu),",
                       indent * 4, "",
                       usb_hid_global_item_tag_to_str(b_tag),
                       (unsigned long) data_u);
                break;
            case usb_hid_global_item_tag_report_id:
                kinfo("%*s%s (%lu),",
                       indent * 4, "",
                       usb_hid_global_item_tag_to_str(b_tag),
                       (unsigned long) data_u);
                break;
            case usb_hid_global_item_tag_report_count:
                kinfo("%*s%s (%lu),",
                       indent * 4, "",
                       usb_hid_global_item_tag_to_str(b_tag),
                       (unsigned long) data_u);
                break;
            case usb_hid_global_item_tag_push:
            case usb_hid_global_item_tag_pop:
                kinfo("%*s%s,",
                       indent * 4, "",
                       usb_hid_global_item_tag_to_str(b_tag));
                break;
            }
        } break;
        case usb_hid_type_local: {
            const uint32_t data =
                (uint32_t) (b_size >= 4 ? payload[i + b_offset + 3] : 0) << 24 |
                (uint32_t) (b_size >= 3 ? payload[i + b_offset + 2] : 0) << 16 |
                (uint32_t) (b_size >= 2 ? payload[i + b_offset + 1] : 0) <<  8 |
                (uint32_t) (b_size >= 1 ? payload[i + b_offset + 0] : 0) <<  0;

            switch (b_tag) {
            case usb_hid_local_item_tag_usage: {
                if (b_size >= 4) usage_page = data >> 16;
                usage_id = data & 0xFFFF;
                usage = (uint32_t) usage_page << 16 | usage_id;
                const char *usage_page_str = usb_hid_usage_page_to_str(usage_page);
                const char *usage_str = usb_hid_usage_to_str(usage);
                if (b_size >= 4) {
                    if (usage_page_str && usage_str) {
                        kinfo("%*s%s (%s: %s),",
                              indent * 4, "",
                              usb_hid_local_item_tag_to_str(b_tag),
                              usage_page_str, usage_str);
                    } else if (usage_page_str) {
                        kinfo("%*s%s (%s: %02hX),",
                              indent * 4, "",
                              usb_hid_local_item_tag_to_str(b_tag),
                              usage_page_str, (unsigned short) usage_id);
                    } else if (usage_str) {
                        kinfo("%*s%s (%02hX: %s),",
                              indent * 4, "",
                              usb_hid_local_item_tag_to_str(b_tag),
                              (unsigned short) usage_page, usage_str);
                    } else {
                        kinfo("%*s%s (%02hX: %02hX),",
                              indent * 4, "",
                              usb_hid_local_item_tag_to_str(b_tag),
                              (unsigned short) usage_page, (unsigned short) usage_id);
                    }
                } else {
                    if (usage_str) {
                        kinfo("%*s%s (%s),",
                              indent * 4, "",
                              usb_hid_local_item_tag_to_str(b_tag),
                              usage_str);
                    } else {
                        kinfo("%*s%s (%02hX),",
                              indent * 4, "",
                              usb_hid_local_item_tag_to_str(b_tag),
                              (unsigned short) usage_id);
                    }
                }
            } break;
            case usb_hid_local_item_tag_usage_minimum:
            case usb_hid_local_item_tag_usage_maximum:
            case usb_hid_local_item_tag_designator_index:
            case usb_hid_local_item_tag_designator_minimum:
            case usb_hid_local_item_tag_designator_maximum:
            case usb_hid_local_item_tag_string_index:
            case usb_hid_local_item_tag_string_minimum:
            case usb_hid_local_item_tag_string_maximum:
            case usb_hid_local_item_tag_delimiter:
                kinfo("%*s%s (%lu),",
                       indent * 4, "",
                       usb_hid_local_item_tag_to_str(b_tag),
                       (unsigned long) data);
                break;
            }
        } break;
        case usb_hid_type_reserved: {
            // TODO: Do something? The spec. says something about this.
        } break;
        }
    } while ((i += b_offset + b_size) < payload_length);
}

static int read_pending(int *errno_ptr) {
    int status = 0;
    uint8_t *payload = NULL;
    flex_mut read_flx = FLEX_MUT_NULL;

    unsigned timeout;
    const unsigned timeout_limit = 128;

    // Get the token or timeout
    bool found_token = false;
    uint8_t token;
    read_flx = flex_mut_kernel(&token, sizeof(token));
    for (timeout = 0; timeout < timeout_limit; ++timeout) {
        ssize_t read_status = device_read(underlying_dvrg->major, underlying_dvrg->minor, read_flx, read_flx.size, 0, errno_ptr);
        if (read_status == 1) {
            timeout = 0;
            if (token == CH559_USB_HOST_TOKEN) {
                found_token = true;
                break;
            }
        } else if (read_status < 0) {
            status = -1;
            goto done;
        }
    }

    // Exit early if we didn't get the token
    if (!found_token) {
        goto done;
    }

    // Get the message structure
    struct ch559_usb_host_message_raw message_raw;
    read_flx = flex_mut_kernel(&message_raw, sizeof(message_raw));
    while (read_flx.size > 0) {
        ssize_t read_status = device_read(underlying_dvrg->major, underlying_dvrg->minor, read_flx, read_flx.size, 0, errno_ptr);
        if (read_status < 0) {
            status = -1;
            goto done;
        }
        read_flx = flex_add(read_flx, read_status);
    }

    // Decode the packed message format
    ch559_usb_host_message message;
    ch559_usb_host_message_decode(&message_raw, &message);

    // Get the message payload
    const size_t payload_length = message.length;
    payload = kalloc(payload_length);
    read_flx = flex_mut_kernel(payload, payload_length);
    while (read_flx.size > 0) {
        ssize_t read_status = device_read(underlying_dvrg->major, underlying_dvrg->minor, read_flx, read_flx.size, 0, errno_ptr);
        if (read_status < 0) {
            status = -1;
            goto done;
        }
        read_flx = flex_add(read_flx, read_status);
    }

    // Get the newline at the end of the message
    char newline;
    read_flx = flex_mut_kernel(&newline, sizeof(newline));
    while (read_flx.size > 0) {
        ssize_t read_status = device_read(underlying_dvrg->major, underlying_dvrg->minor, read_flx, read_flx.size, 0, errno_ptr);
        if (read_status < 0) {
            status = -1;
            goto done;
        }
        read_flx = flex_add(read_flx, read_status);
    }

    // Check that we got a newline
    if (newline != '\n') {
        *errno_ptr = EIO;
        status = -1;
        goto done;
    }

    switch (message.message_type) {
    case ch559_usb_host_message_type_connected:
        kinfo("USB device number %02hhd connected, type %s",
              message.device, ch559_usb_host_device_type_to_str(message.device_type));
        break;
    case ch559_usb_host_message_type_disconnected:
        kinfo("USB device number %02hhd disconnected",
              message.device);
        break;
    case ch559_usb_host_message_type_error:
        ch559_usb_host_print_message(&message, payload_length, payload);
        break;
    case ch559_usb_host_message_type_device_poll:
        ch559_usb_host_print_message(&message, payload_length, payload);
        break;
    case ch559_usb_host_message_type_device_info:
        break;
    case ch559_usb_host_message_type_hid_info:
        parse_hid_report(&message, payload_length, payload);
        break;
    case ch559_usb_host_message_type_device_string:
        kinfo("USB device strings: %.*s",
              (int) payload_length, payload);
        break;
    case ch559_usb_host_message_type_startup:
        kinfo("USB to UART bridge startup");
        break;
    }

done:
    kfree(payload);
    return status;
}

static ssize_t dev_uartusb_read(input_info *info, FLEX_MUT(struct input_event) buf, size_t count, unsigned long long ppos, int *errno_ptr) {
    UNUSED(ppos);

    flex_mut_pinned buf_pin = flex_pin_must_cleanup(buf);
    if (buf_pin.fault || buf_pin.size < count) {
        *errno_ptr = EFAULT;
        return -1;
    }
    uint8_t *buf_ptr = buf_pin.ptr;

    int result = read_pending(errno_ptr);
    if (result < 0) {
        flex_unpin(&buf_pin);
        buf_ptr = NULL;
        return -1;
    } else {
        count = (size_t) result;
    }

    // TODO: Implement

    // struct input_event ev;
    // ev.time = (struct timeval) {0};
    // ev.type = message.type;
    // ev.code = payload[0];
    // ev.value = payload[1];

    // count = sizeof(ev);
    // flex_to(buf, &ev, sizeof(ev));

    flex_unpin(&buf_pin);
    buf_ptr = NULL;

    return count;
}

static ssize_t dev_uartusb_write(input_info *info, FLEX_CONST(struct input_event) buf, size_t count, unsigned long long ppos, int *errno_ptr) {
    UNUSED(info);
    UNUSED(buf);
    UNUSED(count);
    UNUSED(ppos);

    *errno_ptr = EBADF;
    return -1;
}

static int dev_uartusb_ioctl(struct input_info *info, unsigned long request, flex_mut argp, int *errno_ptr) {
    UNUSED(info);
    UNUSED(request);
    UNUSED(argp);

    *errno_ptr = EBADF;
    return -1;
}

#endif
