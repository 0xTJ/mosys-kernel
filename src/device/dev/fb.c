#include "device/dev/fb.h"
#include "device/device.h"
#include "kalloc.h"
#include "kio/klog.h"
#include "kio/kio.h"
#include "util/rwlock.h"
#include "util/util.h"
#include "vfs/devreg.h"
#include <string.h>

static module_status fb_init(void);
static void fb_deinit(void);

static ssize_t fb_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t fb_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int fb_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr);

static module fb = {
    .init = fb_init,
    .deinit = fb_deinit,
    .name = "fb",
};
MODULE_DEFINE(fb);

static const int fb_major = 10;

static fb_info *fb_list[FB_MAX] = {0};
static rwlock fb_list_lock;

static device_operations fb_dops = {
    .read = fb_read,
    .write = fb_write,
    .ioctl = fb_ioctl,
};

static module_status fb_init(void) {
    if (fb_major != device_register(fb_major, &fb_dops)) {
        kpanic("Failed to register fb devices");
    }

    return MODULE_LOADED;
}

static void fb_deinit(void) {

}

static ssize_t fb_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    ssize_t result;

    if (minor < 0 || minor >= FB_MAX) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    fb_info *fb;

    RWLOCK_READ_WITH(&fb_list_lock, {
        fb = fb_list[minor];
        // TODO: Need to lock fb?
    })

    if (!fb) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    if (fb->ops->read) {
        result = fb->ops->read(fb, buf, count, offset, errno_ptr);
    } else {
        *errno_ptr = EINVAL;
        result = -1;
    }

done:
    return result;
}

static ssize_t fb_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    ssize_t result;

    if (minor < 0 || minor >= FB_MAX) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    fb_info *fb;

    RWLOCK_READ_WITH(&fb_list_lock, {
        fb = fb_list[minor];
        // TODO: Need to lock fb?
    })

    if (!fb) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    if (fb->ops->write) {
        result = fb->ops->write(fb, buf, count, offset, errno_ptr);
    } else {
        *errno_ptr = EINVAL;
        result = -1;
    }

done:
    return result;
}

static int fb_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr) {
    int result;

    if (minor < 0 || minor >= FB_MAX) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    fb_info *fb;

    RWLOCK_READ_WITH(&fb_list_lock, {
        fb = fb_list[minor];
        // TODO: Need to lock fb?
    })

    if (!fb) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    if (_IOC_TYPE(request) == FB_MAGIC) {
        switch(_IOC_NR(request)) {
            // TODO: Handle generic cases
        default:
            *errno_ptr = EINVAL;
            result = -1;
        }
    } else if (fb->ops->ioctl) {
        result = fb->ops->ioctl(fb, request, argp, errno_ptr);
    } else {
        *errno_ptr = EINVAL;
        result = -1;
    }

done:
    return result;
}

fb_info *fb_new(void) {
    fb_info *result = kalloc(sizeof(fb_info));
    if (result) {
        // Do any init
    }
    return result;
}

void fb_delete(fb_info *fb) {
    kfree(fb);
}

// TODO: Maybe use a linked list
// TODO: Don't panic on fail
void fb_register(fb_info *fb) {
    bool success = false;

    RWLOCK_WRITE_WITH(&fb_list_lock, {
        for (size_t i = 0; i < FB_MAX; ++i) {
            if (!fb_list[i]) {
                fb_list[i] = fb;

                char tmp_name[DEVREG_NAME_MAX_LENGTH + 1];
                kio_snprintf(tmp_name, sizeof(tmp_name), "fb%u", (unsigned) i);
                devreg_new(tmp_name, fb_major, i);

                success = true;

                break;
            }
        }
    })

    if (!success) {
        kpanic("Too many fb devices");
    }
}
