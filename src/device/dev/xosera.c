#include "config.h"
#include "device/device.h"
#include "device/dev/fb.h"
#include "device/hardware/xosera.h"
#include "vfs/devreg.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "safeprobe.h"
#include "uapi/errno.h"
#include "uapi/asm/ioctl.h"
#include "uapi/mosys/xosera.h"
#include "util/circbuf.h"
#include "util/util.h"
#include <string.h>
#include <limits.h>

#if DEV_XOSERA_ENABLE

static module_status dev_xosera_init(void);
static void dev_xosera_deinit(void);

static ssize_t dev_xosera_read(fb_info *info, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_xosera_write(fb_info *info, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int dev_xosera_ioctl(fb_info *info, unsigned long request, flex_mut argp, int *errno_ptr);

static const char *const dev_xosera_deps[] = {
    "fb",
    "dev_mc68681",
    NULL,
};
static module dev_xosera = {
    .init = dev_xosera_init,
    .deinit = dev_xosera_deinit,
    .name = "dev_xosera",
    .deps = dev_xosera_deps,
};
MODULE_DEFINE(dev_xosera);

static fb_info *dev_xosera_fb;

static fb_ops dev_xosera_fbops = {
    .read = dev_xosera_read,
    .write = dev_xosera_write,
    .ioctl = dev_xosera_ioctl,
};

static const struct fb_fix_screeninfo fix_init = {
    .smem_start = MEM_ADDR_NULL,            // Xosera VRAM is non memory-mapped
    .smem_len   = 0,
    .type       = FB_TYPE_PACKED_PIXELS,
    .visual     = FB_VISUAL_PSEUDOCOLOR,
    .xpanstep   = 1,
    .ypanstep   = 1,
    .ywrapstep  = 0,
};

static const struct fb_var_screeninfo var_init = {
    .grayscale  = false,

    .red    = {
        .offset = 8,
        .length = 4,
        .msb_right = false,
    },
    .green  = {
        .offset = 4,
        .length = 4,
        .msb_right = false,
    },
    .blue   = {
        .offset = 0,
        .length = 4,
        .msb_right = false,
    },
    .transp = {
        .offset = 12,
        .length = 4,
        .msb_right = false,
    },

    .nonstd = 0,
};

static bool dev_xosera_probe(volatile void *base) {
    safeprobe_begin();
    bool ready = xosera_sync(base);
    if (!safeprobe_end_was_clean()) {
        return false;
    }
    return ready;
}

static module_status dev_xosera_init(void) {
    // TODO: This is using a physical address as virtual
    volatile void *base = XOSERA_BASE;

    if (!dev_xosera_probe(base)) {
        return MODULE_FAILED_LOAD;
    }

    xosera_init(base, 1);

    dev_xosera_fb = fb_new();
    if (!dev_xosera_fb) {
        return MODULE_FAILED_LOAD;
    }

    dev_xosera_fb->ops = &dev_xosera_fbops;

    dev_xosera_fb->fix = fix_init;
    dev_xosera_fb->fix.mmio_start   = (mem_addr) base;
    dev_xosera_fb->fix.mmio_len     = (XOSERA_XM_MAX + 1) * 4;  // Number of registers, as words, and interleaved

    dev_xosera_fb->var              = var_init;

    uint16_t hsize      = xosera_xr_get(base, XOSERA_XR_VID_HSIZE);
    uint16_t vsize      = xosera_xr_get(base, XOSERA_XR_VID_VSIZE);
    uint16_t left       = xosera_xr_get(base, XOSERA_XR_VID_LEFT);
    uint16_t right      = xosera_xr_get(base, XOSERA_XR_VID_RIGHT);
    uint16_t top        = 0;
    uint16_t bottom     = 0;
    uint32_t left_right = (uint32_t) left + (uint32_t) right;
    uint32_t top_bottom = (uint32_t) top + (uint32_t) bottom;

    uint16_t pa_gfx_ctrl = xosera_xr_get(base, XOSERA_XR_PA_GFX_CTRL);
    // TODO: Redo once enums have been made
    uint8_t v_repeat        = (pa_gfx_ctrl >> 0) & 0x03;
    uint8_t h_repeat        = (pa_gfx_ctrl >> 2) & 0x03;
    uint8_t bits_per_pixel  = (pa_gfx_ctrl >> 4) & 0x03;
    bool bitmap             = (pa_gfx_ctrl >> 6) & 0x01;
    bool blank              = (pa_gfx_ctrl >> 7) & 0x01;
    uint8_t colourbase      = (pa_gfx_ctrl >> 8) & 0xFF;

    uint8_t h_scale = h_repeat + 1;
    uint8_t v_scale = v_repeat + 1;
    bits_per_pixel =
        bits_per_pixel == 0x0 ? 1 :
        bits_per_pixel == 0x1 ? 4 :
        bits_per_pixel == 0x2 ? 8 :
        (kwarn("Unknown bits-per-pixel: %hhu", (unsigned char) bits_per_pixel), 0);

    // TODO: Do I want to floor or ceiling these divisions?
    hsize       = hsize / h_scale;
    vsize       = vsize / v_scale;
    left        = left / h_scale;
    right       = right / h_scale;
    top         = top / v_scale;
    bottom      = bottom / v_scale;
    left_right  = left_right / h_scale;
    top_bottom  = top_bottom / v_scale;

    dev_xosera_fb->var.xres             = hsize;
    dev_xosera_fb->var.yres             = vsize;
    dev_xosera_fb->var.xres_virtual     = dev_xosera_fb->var.xres >= left_right ? dev_xosera_fb->var.xres - left_right : 0;
    dev_xosera_fb->var.yres_virtual     = dev_xosera_fb->var.yres >= top_bottom ? dev_xosera_fb->var.yres - top_bottom : 0;
    dev_xosera_fb->var.xoffset          = left;
    dev_xosera_fb->var.yoffset          = top;

    dev_xosera_fb->var.bits_per_pixel   = bits_per_pixel;

    fb_register(dev_xosera_fb);

    return MODULE_LOADED;
}

static void dev_xosera_deinit(void) {
    // TODO: Unregister then destroy framebuffer
}

static ssize_t dev_xosera_read(fb_info *info, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    if (offset % 2) {
        *errno_ptr = EINVAL;
        return -1;
    } else if (offset / 2 > UINT16_MAX) {
        return 0;
    }
    uint16_t word_offset = offset / 2;

    count = round_down(count, 2);
    size_t word_count = count / 2;
    if (word_count > (size_t) (UINT16_MAX - word_offset) + 1) {
        word_count = (size_t) (UINT16_MAX - word_offset) + 1;
    }

    // TODO: This is using a physical address as virtual
    volatile void *base = (volatile void *) info->fix.mmio_start;

    flex_mut_pinned buf_pin = flex_pin_must_cleanup(buf);
    if (buf_pin.fault || buf_pin.size < count) {
        *errno_ptr = EFAULT;
        return -1;
    }
    uint8_t *buf_ptr = buf_pin.ptr;

    xosera_xm_set_w(base, XOSERA_XM_RD_INCR, 1);
    xosera_xm_set_w(base, XOSERA_XM_RD_ADDR, word_offset);

    for (size_t word_count_done = 0; word_count_done < word_count; ++word_count_done) {
        uint16_t tmp = xosera_xm_get_w(base, XOSERA_XM_DATA);
        buf_ptr[word_count_done * 2 + 0] = (tmp >> 8) & 0xFF;
        buf_ptr[word_count_done * 2 + 1] = (tmp >> 0) & 0xFF;
    }

    flex_unpin(&buf_pin);
    buf_ptr = NULL;

    return word_count * 2;
}

static ssize_t dev_xosera_write(fb_info *info, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    if (offset % 2) {
        *errno_ptr = EINVAL;
        return -1;
    } else if (offset / 2 > UINT16_MAX) {
        return 0;
    }
    uint16_t word_offset = offset / 2;

    count = round_down(count, 2);
    size_t word_count = count / 2;
    if (word_count > (size_t) (UINT16_MAX - word_offset) + 1) {
        word_count = (size_t) (UINT16_MAX - word_offset) + 1;
    }

    // TODO: This is using a physical address as virtual
    volatile void *base = (volatile void *) info->fix.mmio_start;

    flex_const_pinned buf_pin = flex_pin_must_cleanup(buf);
    if (buf_pin.fault || buf_pin.size < count) {
        *errno_ptr = EFAULT;
        return -1;
    }
    const uint8_t *buf_ptr = buf_pin.ptr;

    xosera_xm_set_w(base, XOSERA_XM_WR_INCR, 1);
    xosera_xm_set_w(base, XOSERA_XM_WR_ADDR, word_offset);

    for (size_t word_count_done = 0; word_count_done < word_count; ++word_count_done) {
        uint16_t tmp =
            (uint16_t) buf_ptr[word_count_done * 2 + 0] << 8 |
            (uint16_t) buf_ptr[word_count_done * 2 + 1] << 0;
        xosera_xm_set_w(base, XOSERA_XM_DATA, tmp);
    }

    flex_unpin(&buf_pin);
    buf_ptr = NULL;

    return word_count * 2;
}

static int dev_xosera_ioctl(fb_info *info, unsigned long request, flex_mut argp, int *errno_ptr) {
    UNUSED(info);

    if (_IOC_TYPE(request) != XOSERA_IOC_MAGIC) {
        *errno_ptr = EINVAL;
        return -1;
    }

    argp.size = _IOC_SIZE(request);
    flex_mut_pinned argp_pin = flex_pin_must_cleanup(argp);
    if (argp_pin.fault) {
        *errno_ptr = EFAULT;
        return -1;
    }
    // Check flex size before using argp_pin.ptr.

    switch(_IOC_NR(request)) {
    default:
        flex_unpin(&argp_pin);
        *errno_ptr = EINVAL;
        return -1;
    }
}

#endif
