#include "device/dev/tty.h"
#include "config.h"
#include "device/device.h"
#include "kernel/module.h"
#include "kio/kio.h"
#include "thread/timing.h"
#include "uapi/unistd.h"
#include "uapi/asm/ioctl.h"
#include "uapi/asm/ioctls.h"
#include "util/rwlock.h"
#include "vfs/devreg.h"

static module_status tty_init(void);
static void tty_deinit(void);

static ssize_t tty_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t tty_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int tty_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr);

static ssize_t tty_read_do_char(tty_info *tty, char *c, unsigned long long offset, int *errno_ptr);
static void tty_read_do_echo(tty_info *tty, char c, unsigned long long offset);
static ssize_t tty_read_do(tty_info *tty, flex_mut buf, size_t count, unsigned long long *offset, int *errno_ptr);
static ssize_t tty_read_inoncanon(tty_info *tty, flex_mut buf, size_t count, unsigned long long *offset, int *errno_ptr);
static ssize_t tty_read_icanon_char(tty_info *tty, char *c, unsigned long long offset, int *errno_ptr);
static ssize_t tty_read_icanon(tty_info *tty, flex_mut buf, size_t count, unsigned long long *offset, int *errno_ptr);

static ssize_t tty_write_do_char(tty_info *tty, char c, unsigned long long offset, int *errno_ptr);
static ssize_t tty_write_do(tty_info *tty, flex_const buf, size_t count, unsigned long long *offset, int *errno_ptr);
static ssize_t tty_write_raw_char(tty_info *tty, char c, unsigned long long offset, int *errno_ptr);
static ssize_t tty_write_raw(tty_info *tty, flex_const buf, size_t count, unsigned long long *offset, int *errno_ptr);
static ssize_t tty_write_opost_char(tty_info *tty, char c, unsigned long long offset, int *errno_ptr);
static ssize_t tty_write_opost(tty_info *tty, flex_const buf, size_t count, unsigned long long *offset, int *errno_ptr);

static module tty = {
    .init = tty_init,
    .deinit = tty_deinit,
    .name = "tty",
};
MODULE_DEFINE(tty);

static int tty_major = 0;

static tty_info *tty_list[TTY_NUM] = {0};
static rwlock tty_list_lock;

static device_operations tty_dops = {
    .read = tty_read,
    .write = tty_write,
    .ioctl = tty_ioctl,
};

static module_status tty_init(void) {
    tty_major = device_register(tty_major, &tty_dops);
    if (tty_major < 0) {
        kpanic("Failed to register tty devices");
    }

    return MODULE_LOADED;
}

static void tty_deinit(void) {
    // Do any cleanup
}

static ssize_t tty_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    ssize_t result = 0;

    if (minor < 0 || minor >= TTY_NUM) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    tty_info *tty;

    RWLOCK_READ_WITH(&tty_list_lock, {
        tty = tty_list[minor];
        // TODO: Need to lock tty?
    })

    if (!tty) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    if (tty->ops->read) {
        result = tty_read_do(tty, buf, count, &offset, errno_ptr);
    } else {
        *errno_ptr = EINVAL;
        result = -1;
    }

done:
    return result;
}

// TODO: Directly update offset in VFS somehow
static ssize_t tty_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    ssize_t result = 0;

    if (minor < 0 || minor >= TTY_NUM) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    tty_info *tty;

    RWLOCK_READ_WITH(&tty_list_lock, {
        tty = tty_list[minor];
        // TODO: Need to lock tty?
    })

    if (!tty) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    if (tty->ops->write) {
        result = tty_write_do(tty, buf, count, &offset, errno_ptr);
    } else {
        *errno_ptr = EINVAL;
        result = -1;
    }

done:
    return result;
}

static int tty_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr) {
    int result;

    if (minor < 0 || minor >= TTY_NUM) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    tty_info *tty;

    RWLOCK_READ_WITH(&tty_list_lock, {
        tty = tty_list[minor];
        // TODO: Need to lock tty?
    })

    if (!tty) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    flex_result flx_rslt;
    if (_IOC_TYPE(request) == 'T') {
        switch(_IOC_NR(request)) {
        // TODO: TCGETS and TCSETS should call into device driver to actually update registers

        case _IOC_NR(TCGETS):
            flx_rslt = flex_to(argp, &tty->termios_p, sizeof tty->termios_p);
            if (flx_rslt == FLEX_RESULT_SUCCESS) {
                result = 0;
            } else {
                *errno_ptr = EFAULT;
                result = -1;
            }
            break;

        case _IOC_NR(TCSETS):
            flx_rslt = flex_from(&tty->termios_p, argp, sizeof tty->termios_p);
            if (flx_rslt == FLEX_RESULT_SUCCESS) {
                result = 0;
            } else {
                *errno_ptr = EFAULT;
                result = -1;
            }
            break;

        // TODO: Handle other generic cases

        default:
            *errno_ptr = EINVAL;
            result = -1;
        }
    } else if (tty->ops->ioctl) {
        result = tty->ops->ioctl(tty, request, argp, errno_ptr);
    } else {
        *errno_ptr = EINVAL;
        result = -1;
    }

done:
    return result;
}

tty_info *tty_new(void) {
    tty_info *result = kalloc(sizeof(tty_info));
    if (result) {
        result->in_crcbf.buf = result->in_buf;
        result->in_crcbf.size = sizeof result->in_buf;
        result->in_crcbf.head = 0;
        result->in_crcbf.tail = 0;

        result->line_complete = false;

        result->termios_p.c_iflag =
            BRKINT | ISTRIP | ICRNL | IXANY;
        result->termios_p.c_oflag =
            OPOST | ONLCR;
        result->termios_p.c_lflag =
            ECHO | ICANON | ISIG | IEXTEN | ECHOE;
        result->termios_p.c_cflag =
            CREAD | CS8;    // These don't match some other implementations

        static const cc_t c_cc[NCCS] = {
            [VEOF]      = 'D' % 0x20,
            [VEOL]      = _POSIX_VDISABLE,
            // TODO: What is the correct default value?
            // [VERASE]    = '?' % 0x20,
            [VERASE]    = '\b',
            [VKILL]     = 'U' % 0x20,
            [VINTR]     = 'C' % 0x20,
            [VQUIT]     = '\\' % 0x20,
            [VSUSP]     = 'Z' % 0x20,
            [VSTART]    = 'Q' % 0x20,
            [VSTOP]     = 'S' % 0x20,
            [VMIN]      = 1,
            [VTIME]     = 0,
        };
        memcpy(result->termios_p.c_cc, c_cc, sizeof c_cc);

        // The underlying driver should configure itself according to these values,
        // or update them as needed, and set c_ispeed and c_ospeed according to device defaults
    }
    return result;
}

void tty_delete(tty_info *tty) {
    kfree(tty);
}

// name_format must have a single format specifier "%u"
// TODO: Maybe use a linked list
// TODO: Don't make array numbers equal to name numbers
// TODO: Don't panic on fail
void tty_register(tty_info *tty, const char *name_format) {
    bool success = false;

    RWLOCK_WRITE_WITH(&tty_list_lock, {
        int found_tty_minor = -1;
        for (int i = 0; i < TTY_NUM; ++i) {
            if (!tty_list[i]) {
                found_tty_minor = i;
                break;
            }
        }
        if (found_tty_minor == -1) {
            RWLOCK_LEAVE_UNLOCK();
        }

        bool found_tmp_name = false;
        char tmp_name[DEVREG_NAME_MAX_LENGTH + 1];
        for (int i = 0; i < TTY_NUM; ++i) {
            kio_snprintf(tmp_name, sizeof(tmp_name), name_format, (unsigned) i);

            // Check if this name is already used
            if (!devreg_exists(tmp_name)) {
                found_tmp_name = true;
                break;
            }
        }

        if (found_tmp_name) {
            tty_list[found_tty_minor] = tty;
            devreg_new(tmp_name, tty_major, found_tty_minor);
            success = true;
        }
    })

    if (!success) {
        kpanic("Too many tty devices");
    }
}

// TODO: Do we need to flush when turning on or off ICANON?

static inline ssize_t tty_read_do_char(tty_info *tty, char *c, unsigned long long offset, int *errno_ptr) {
    char raw_char;
    flex_mut raw_flex = flex_mut_kernel(&raw_char, 1);

    ssize_t result = tty->ops->read(tty, raw_flex, raw_flex.size, offset, errno_ptr);

    if (result <= 0) {
        return result;
    }

    // TODO: Handle other flags and characters

    if (tty->termios_p.c_iflag & ISTRIP) {
        // Strip character
        raw_char &= 0x7F;
    }

    if (tty->termios_p.c_iflag & INLCR && raw_char == '\n') {
        // Map NL to CR on input
        raw_char = '\r';
    } else if (tty->termios_p.c_iflag & IGNCR && raw_char == '\r') {
        // Ignore CR
        result = 0;
        return result;
    } else if (!(tty->termios_p.c_iflag & IGNCR) && tty->termios_p.c_iflag & ICRNL && raw_char == '\r') {
        // Map CR to NL on input
        raw_char = '\n';
    }

    *c = raw_char;
    return result;
}

static inline void tty_read_do_echo(tty_info *tty, char c, unsigned long long offset) {
    if (!tty->ops->write) {
        return;
    }

    bool do_echo = false;
    if (tty->termios_p.c_lflag & ECHO) {
        do_echo = true;
    } else if (tty->termios_p.c_lflag & ECHONL &&
               tty->termios_p.c_lflag & ICANON &&
               c == '\n') {
        do_echo = true;
    }

    if (do_echo) {
        int dummy_errno;
        tty_write_do_char(tty, c, offset, &dummy_errno); // Ignore write result
    }
}

static ssize_t tty_read_do(tty_info *tty, flex_mut buf, size_t count, unsigned long long *offset, int *errno_ptr) {
    if (tty->termios_p.c_lflag & ICANON) {
        return tty_read_icanon(tty, buf, count, offset, errno_ptr);
    } else {
        return tty_read_inoncanon(tty, buf, count, offset, errno_ptr);
    }
}

// TODO: Need to sleep as appropriate in this file

static ssize_t tty_read_inoncanon(tty_info *tty, flex_mut buf, size_t count, unsigned long long *offset, int *errno_ptr) {
    ssize_t result = 0;

    flex_mut_pinned buf_pin = flex_pin_must_cleanup(buf);
    if (buf_pin.fault || buf_pin.size < count) {
        *errno_ptr = EFAULT;
        return -1;
    }
    char *buf_ptr = buf_pin.ptr;

    // TODO: Implement proper non-canonical MIN and TIME
    // unsigned long long start_time = timing_uptime_get_us();

    for (size_t i = 0; i < count; ++i) {
        char c;
        ssize_t tmp_result = tty_read_do_char(tty, &c, *offset, errno_ptr);

        // TODO: What to do when tmp_result is an error after some bytes are read?
        if (tmp_result == 1) {
            buf_ptr[result++] = c;
        } else {
            if (tmp_result < 0 && result == 0) {
                result = tmp_result;
            }
            break;
        }
    }

    if (result >= 0) {
        *offset += (size_t) result;
    }

    flex_unpin(&buf_pin);
    buf_ptr = NULL;

    return result;
}

static inline ssize_t tty_read_icanon_char(tty_info *tty, char *c, unsigned long long offset, int *errno_ptr) {
    char read_c;

    ssize_t tmp_result = tty_read_do_char(tty, &read_c, offset, errno_ptr);
    if (tmp_result <= 0) {
        return tmp_result;
    }

    if (tty->termios_p.c_lflag & ECHOE &&
        tty->termios_p.c_cc[VERASE] != _POSIX_VDISABLE &&
        read_c == (char) tty->termios_p.c_cc[VERASE]) {
        // This is ERASE
        if (circbuf_char_remove_last(&tty->in_crcbf)) {
            tty_read_do_echo(tty, '\b', offset);
            tty_read_do_echo(tty, ' ', offset);
            tty_read_do_echo(tty, '\b', offset);
        }
    } else if (tty->termios_p.c_lflag & ECHOK &&
               tty->termios_p.c_cc[VKILL] != _POSIX_VDISABLE &&
               read_c == (char) tty->termios_p.c_cc[VKILL]) {
        // This is KILL
        if (circbuf_char_remove_all(&tty->in_crcbf)) {
            tty_read_do_echo(tty, '\n', offset);
        }
    } else {
        // Handle end of this line
        if (read_c == '\n') {
            tty->line_complete = true;
        } else if (tty->termios_p.c_cc[VEOF] != _POSIX_VDISABLE &&
                   read_c == (char) tty->termios_p.c_cc[VEOF]) {
            tty->line_complete = true;
        } else if (tty->termios_p.c_cc[VEOL] != _POSIX_VDISABLE &&
                   read_c == (char) tty->termios_p.c_cc[VEOL]) {
            tty->line_complete = true;
        }

        if (!circbuf_char_is_almost_full(&tty->in_crcbf) || (tty->line_complete && !circbuf_char_is_full(&tty->in_crcbf))) {
            kassert(circbuf_char_put(&tty->in_crcbf, read_c));
            tty_read_do_echo(tty, read_c, offset);
        } else {
            // TODO: Maybe send bell?
        }
    }

    *c = read_c;
    return 1;
}

static ssize_t tty_read_icanon(tty_info *tty, flex_mut buf, size_t count, unsigned long long *offset, int *errno_ptr) {
    ssize_t result = 0;

    while (!tty->line_complete) {
        char c;
        ssize_t tmp_result = tty_read_icanon_char(tty, &c, *offset, errno_ptr);

        if (tmp_result == 1) {
            *offset += 1;
            result += 1;
        } else if (tmp_result == 0) {
            continue;
        } else if (tmp_result < 0) {
            if (result <= 0) {
                result = -1;
            }
            break;
        } else {
            unreachable();
        }
    }

    if (result >= 0) {
        flex_mut_pinned buf_pin = flex_pin_must_cleanup(buf);
        if (buf_pin.fault || buf_pin.size < count) {
            *errno_ptr = EFAULT;
            return -1;
        }
        char *buf_ptr = buf_pin.ptr;

        result = circbuf_char_dump(&tty->in_crcbf, count, buf_ptr);

        flex_unpin(&buf_pin);
        buf_ptr = NULL;

        if (circbuf_char_is_empty(&tty->in_crcbf)) {
            tty->line_complete = false;
        }
    }

    return result;
}

// Returns the actual number of bytes written, or -1
static inline ssize_t tty_write_do_char(tty_info *tty, char c, unsigned long long offset, int *errno_ptr) {
    if (tty->termios_p.c_oflag & OPOST) {
        return tty_write_opost_char(tty, c, offset, errno_ptr);
    } else {
        return tty_write_raw_char(tty, c, offset, errno_ptr);
    }
}

static ssize_t tty_write_do(tty_info *tty, flex_const buf, size_t count, unsigned long long *offset, int *errno_ptr) {
    if (tty->termios_p.c_oflag & OPOST) {
        return tty_write_opost(tty, buf, count, offset, errno_ptr);
    } else {
        return tty_write_raw(tty, buf, count, offset, errno_ptr);
    }
}

// Returns the actual number of bytes written, or -1
static inline ssize_t tty_write_raw_char(tty_info *tty, char c, unsigned long long offset, int *errno_ptr) {
    flex_const raw_flex = flex_const_kernel(&c, 1);
    return tty->ops->write(tty, raw_flex, raw_flex.size, offset, errno_ptr);
}

static ssize_t tty_write_raw(tty_info *tty, flex_const buf, size_t count, unsigned long long *offset, int *errno_ptr) {
    ssize_t result = tty->ops->write(tty, buf, count, *offset, errno_ptr);
    if (result >= 0) {
        *offset += (size_t) result;
    }
    return result;
}

// Returns the actual number of bytes written, or -1
static inline ssize_t tty_write_opost_char(tty_info *tty, char c, unsigned long long offset, int *errno_ptr) {
    char raw_buf[8];
    size_t raw_size;

    // TODO: Handle ONOCR and ONLRET
    // TODO: Handle others
    if (c == '\n' && tty->termios_p.c_oflag & ONLCR) {
        raw_buf[0] = '\r';
        raw_buf[1] = '\n';
        raw_size = 2;
    } else if (c == '\r' && tty->termios_p.c_oflag & OCRNL) {
        raw_buf[0] = '\n';
        raw_size = 1;
    // TODO: Do other TABn values need to be handled?
    } else if (c == '\t' && (tty->termios_p.c_oflag & TABDLY) == TAB3) {
        memset(raw_buf, ' ', 8);
        raw_size = 8;
    } else {
        raw_buf[0] = c;
        raw_size = 1;
    }

    flex_const raw_flex = flex_const_kernel(raw_buf, raw_size);

    for (size_t raw_written = 0; raw_written < raw_size;) {
        ssize_t result = tty->ops->write(tty, raw_flex, raw_flex.size, offset, errno_ptr);

        if (result < 0) {
            return result;
        }

        size_t result_written = result;
        offset += result_written;
        raw_written += result_written;
        raw_flex = flex_add(raw_flex, result_written);
    }

    return raw_size;
}

static ssize_t tty_write_opost(tty_info *tty, flex_const buf, size_t count, unsigned long long *offset, int *errno_ptr) {
    ssize_t result = 0;
    unsigned long long offset_tmp = *offset;

    flex_const_pinned buf_pin = flex_pin_must_cleanup(buf);
    if (buf_pin.fault || buf_pin.size < count) {
        *errno_ptr = EFAULT;
        return -1;
    }
    const char *buf_ptr = buf_pin.ptr;

    for (size_t i = 0; i < count; ++i) {
        ssize_t char_result = tty_write_opost_char(tty, buf_ptr[i], offset_tmp, errno_ptr);

        if (char_result < 0) {
            result = -1;
            break;
        }

        size_t char_result_written = char_result;
        offset_tmp += char_result_written;
        result += 1;
    }

    flex_unpin(&buf_pin);
    buf_ptr = NULL;

    *offset = offset_tmp;

    return result;
}
