#include "device/dev/uartusb/ch559_usb_host.h"
#include "kio/klog.h"
#include "util/util.h"
#include <string.h>

static const char *message_types[] = {
    "connected",
    "disconnected",
    "error",
    "device_poll",
    "device_string",
    "device_info",
    "hid_info",
    "startup",
};

static const char *device_types[] = {
    "unknown",
    "pointer",
    "mouse",
    "reserved",
    "joystick",
    "gamepad",
    "keyboard",
    "keypad",
    "multi_axis",
    "system",
};

void ch559_usb_host_message_decode(const struct ch559_usb_host_message_raw *message_raw, ch559_usb_host_message *message) {
    message->length = from_little_u16(message_raw->length_le);
    message->message_type = message_raw->msgtype;
    message->device_type = message_raw->type;
    message->device = message_raw->device;
    message->endpoint = message_raw->endpoint;

    if (!message_raw->id_vendor_le && !message_raw->id_vendor_le) {
        message->format = ch559_usb_host_message_format_protocol;
    } else {
        message->format = ch559_usb_host_message_format_hid_poll;
        message->hid_poll.id_vendor = from_little_u16(message_raw->id_vendor_le);
        message->hid_poll.id_product = from_little_u16(message_raw->id_product_le);
    }
}

const char *ch559_usb_host_message_type_to_str(ch559_usb_host_message_type message_type) {
    if (message_type >= ch559_usb_host_message_type_connected && message_type <= ch559_usb_host_message_type_startup) {
        return message_types[message_type - ch559_usb_host_message_type_connected];
    } else {
        return "<unrecognized>";
    }
}

const char *ch559_usb_host_device_type_to_str(ch559_usb_host_device_type device_type) {
    if (device_type >= ch559_usb_host_device_type_unknown && device_type <= ch559_usb_host_device_type_system) {
        return device_types[device_type - ch559_usb_host_device_type_unknown];
    } else {
        return "<unrecognized>";
    }
}

#define PRINT_MESSAGE_PREFIX "Payload: %04zu-%04zu"
#define PRINT_MESSAGE_INCREMENT " %02hhX"
void ch559_usb_host_print_message(const struct ch559_usb_host_message *message, size_t payload_length, const uint8_t payload[payload_length]) {
    const char *message_type_str = ch559_usb_host_message_type_to_str(message->message_type);
    const char *device_type_str = ch559_usb_host_device_type_to_str(message->device_type);

    if (message->format == ch559_usb_host_message_format_hid_poll) {
        kdebug("Received protocol message: length=%zu, msgtype=%s, type=%s, device=%02hhX, endpoint=%02hhX, id_vendor=%04hX, id_product=%04hX",
            message->length, message_type_str, device_type_str, message->device, message->endpoint, message->hid_poll.id_vendor, message->hid_poll.id_product);
    } else {
        kdebug("Received protocol message: length=%zu, msgtype=%s, type=%s, device=%02hhX, endpoint=%02hhX",
            message->length, message_type_str, device_type_str, message->device, message->endpoint);
    }

    const size_t offset_len = strlen(PRINT_MESSAGE_PREFIX);
    const size_t increment_len = strlen(PRINT_MESSAGE_INCREMENT);

    for (size_t payload_offset = 0; payload_offset < payload_length; payload_offset += 8) {
        char format[] =
            PRINT_MESSAGE_PREFIX
            PRINT_MESSAGE_INCREMENT PRINT_MESSAGE_INCREMENT PRINT_MESSAGE_INCREMENT PRINT_MESSAGE_INCREMENT
            PRINT_MESSAGE_INCREMENT PRINT_MESSAGE_INCREMENT PRINT_MESSAGE_INCREMENT PRINT_MESSAGE_INCREMENT;
        unsigned short bytes_to_print = min((size_t) 8, payload_length - payload_offset);
        format[offset_len + increment_len * bytes_to_print] = '\0';

        kdebug(format,
            payload_offset, payload_offset + bytes_to_print - 1,
            payload[payload_offset + 0],
            payload[payload_offset + 1],
            payload[payload_offset + 2],
            payload[payload_offset + 3],
            payload[payload_offset + 4],
            payload[payload_offset + 5],
            payload[payload_offset + 6],
            payload[payload_offset + 7]);
    }
}
