#include "config.h"
#include "util/circbuf.h"
#include "device/device.h"
#include "vfs/devreg.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "kio/kio.h"
#include "device/partition.h"
#include "device/hardware/mc68901.h"
#include "uapi/errno.h"
#include "uapi/mosys/spi/spidev.h"
#include "util/util.h"
// #include "uapi/mosys/spi/sd.h"
#include <assert.h>
#include <limits.h>
#include <stdint.h>
#include <string.h>

#if DEV_SPISD_ENABLE

#if !DEV_SPIDEV_ENABLE
#error DEV_SPISD_ENABLE requires DEV_SPIDEV_ENABLE
#endif

#define DEV_SPISD_PARTITION_SHIFT   4
#define DEV_SPISD_PARTITION_GAP     (1U << DEV_SPISD_PARTITION_SHIFT)

#define NCS_MIN 0
#define NCR_MIN 0
#define NCR_MAX 8
#define NRC_MIN 1
#define NAC_MIN 1
#define NAC_MAX 20 // TODO: This should come from CSD
#define NWR_MIN 1
#define NEC_MIN 0
#define NDS_MIN 0
#define NBR_MIN 0
#define NBR_MAX 1

#define NCS (NCS_MIN + 1)
#define NRC (NRC_MIN + 1)
#define NWR (NWR_MIN + 1)
#define NEC (NEC_MIN + 1)
#define NDS (NDS_MIN + 1)

static_assert(NCS >= NCS_MIN, "Invalid SD parameter NCS");
static_assert(NRC >= NRC_MIN, "Invalid SD parameter NRC");
static_assert(NWR >= NWR_MIN, "Invalid SD parameter NWR");
static_assert(NEC >= NEC_MIN, "Invalid SD parameter NEC");
static_assert(NDS >= NDS_MIN, "Invalid SD parameter NDS");

#define CMD_GO_IDLE_STATE           0
#define CMD_SEND_OP_COND            1
#define CMD_SWITCH_FUNC             6
#define CMD_SEND_IF_COND            8
#define CMD_SEND_CSD                9
#define CMD_SEND_CID                10
#define CMD_STOP_TRASMISSION        12
#define CMD_SEND_STATUS             13
#define CMD_SET_BLOCKLEN            16
#define CMD_READ_SINGLE_BLOCK       17
#define CMD_READ_MULTIPLE_BLOCK     18
#define CMD_WRITE_BLOCK             24
#define CMD_WRITE_MULTIPLE_BLOCK    25
#define CMD_PROGRAM_CSD             27
#define CMD_SET_WRITE_PROT          28
#define CMD_CLR_WRITE_PROT          29
#define CMD_SEND_WRITE_PROT         30
#define CMD_ERASE_WR_BLK_START_ADDR 32
#define CMD_ERASE_WR_BLK_END_ADDR   33
#define CMD_ERASE                   38
#define CMD_LOCK_UNLOCK             42
#define CMD_APP_CMD                 55
#define CMD_GEN_CMD                 56
#define CMD_READ_OCR                58
#define CMD_CRC_ON_OFF              59

#define ACMD_SD_STATUS              13
#define ACMD_SEND_NUM_WR_BLOCKS     22
#define ACMD_SET_WR_BLK_ERASE_COUNT 23
#define ACMD_SEND_OP_COND           41
#define ACMD_SET_CLR_CARD_DETECT    42
#define ACMD_SEND_SCR               51

#define BLOCK_SIZE 512

#define DATA_RESPONSE_TOKEN_IS_DATA_ACCEPTED(token)         (((token) & 0x1F) == 0x05)
#define DATA_RESPONSE_TOKEN_IS_DATA_REJECTED_CRC(token)     (((token) & 0x1F) == 0x0B)
#define DATA_RESPONSE_TOKEN_IS_DATA_REJECTED_WRITE(token)   (((token) & 0x1F) == 0x0D)
#define DATA_RESPONSE_TOKEN_IS_DATA_REJECTED(token)          \
    (                                                        \
        DATA_RESPONSE_TOKEN_IS_DATA_REJECTED_CRC(token) ||   \
        DATA_RESPONSE_TOKEN_IS_DATA_REJECTED_WRITE(token)    \
    )
#define DATA_RESPONSE_TOKEN_IS_VALID(token)              \
    (                                                    \
        DATA_RESPONSE_TOKEN_IS_DATA_ACCEPTED(token)  ||  \
        DATA_RESPONSE_TOKEN_IS_DATA_REJECTED(token)      \
    )

#define DATA_TOKEN_SINGLE_READ_START     0xFE
#define DATA_TOKEN_SINGLE_WRITE_START    0xFE
#define DATA_TOKEN_MULTIPLE_READ_START   0xFE
#define DATA_TOKEN_MULTIPLE_WRITE_START  0xFC
#define DATA_TOKEN_MULTIPLE_WRITE_STOP   0xFD
#define DATA_TOKEN_IS_ERROR(token)       (((token) & 0xF0) == 0x00)
#define DATA_TOKEN_IS_VALID(token)                       \
    (                                                       \
        (token) == DATA_TOKEN_SINGLE_READ_START      ||  \
        (token) == DATA_TOKEN_SINGLE_WRITE_START     ||  \
        (token) == DATA_TOKEN_MULTIPLE_READ_START    ||  \
        (token) == DATA_TOKEN_MULTIPLE_WRITE_START   ||  \
        (token) == DATA_TOKEN_MULTIPLE_WRITE_STOP    ||  \
        DATA_TOKEN_IS_ERROR(token)                       \
    )

#define R1_PARAMETER_ERROR      0x40
#define R1_ADDRESS_ERROR        0x20
#define R1_ERASE_SEQUENCE_ERROR 0x10
#define R1_COM_CRC_ERROR        0x08
#define R1_ILLEGAL_COMMAND      0x04
#define R1_ERASE_RESET          0x02
#define R1_IN_IDLE_STATE        0x01

#define OCR_2_7_to_2_8  (1UL << 15)
#define OCR_2_8_to_2_9  (1UL << 16)
#define OCR_2_9_to_2_0  (1UL << 17)
#define OCR_3_0_to_3_1  (1UL << 18)
#define OCR_3_1_to_3_2  (1UL << 19)
#define OCR_3_2_to_3_3  (1UL << 20)
#define OCR_3_3_to_3_4  (1UL << 21)
#define OCR_3_4_to_3_5  (1UL << 22)
#define OCR_3_5_to_3_6  (1UL << 23)
#define OCR_S18A        (1UL << 24)
#define OCR_CO2T        (1UL << 27)
#define OCR_UHS_II      (1UL << 29)
#define OCR_CCS         (1UL << 30)
#define OCR_BUSY        (1UL << 31)

typedef enum sd_card_version {
    SD_CARD_VERSION_UNUSABLE,
    SD_CARD_VERSION_NOT_SD,
    SD_CARD_VERSION_1,
    SD_CARD_VERSION_2,
    SD_CARD_VERSION_HC,
} sd_card_version;

typedef uint8_t r1;

typedef struct r2 {
    r1 r1;
    uint8_t r2;
} __attribute__((packed)) r2;

typedef struct r3 {
    r1 r1;
    uint32_t ocr;
} __attribute__((packed)) r3;

typedef struct r7 {
    r1 r1;
    uint32_t r7;
} __attribute__((packed)) r7;

// Must be filled with 0xFF before use
uint8_t buf_idle[512];

static module_status dev_sd_init(void);
static void dev_sd_deinit(void);

static ssize_t dev_sd_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_sd_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
// static int dev_sd_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr);

static int send_idle(unsigned short card_num, size_t len, bool cs_change, int *errno_ptr) __attribute__((unused));
static inline int send_ncs(unsigned short card_num, int *errno_ptr) __attribute__((unused));
static inline int send_nrc(unsigned short card_num, int *errno_ptr) __attribute__((unused));
static inline int send_nwr(unsigned short card_num, int *errno_ptr) __attribute__((unused));
static inline int send_nec(unsigned short card_num, int *errno_ptr) __attribute__((unused));
static inline int send_nds(unsigned short card_num, int *errno_ptr) __attribute__((unused));

static int command(unsigned short card_num, int number, uint32_t argument, size_t response_len, void *response, int *errno_ptr) __attribute__((unused));
static int command_r1(unsigned short card_num, int number, uint32_t argument, r1 *response, int *errno_ptr) __attribute__((unused));
static int command_r3(unsigned short card_num, int number, uint32_t argument, r3 *response, int *errno_ptr) __attribute__((unused));
static int command_r7(unsigned short card_num, int number, uint32_t argument, r7 *response, int *errno_ptr) __attribute__((unused));

static int app_command_r1(unsigned short card_num, int number, uint32_t argument, r1 *response, int *errno_ptr) __attribute__((unused));

static int read_packet(unsigned short card_num, uint8_t *token, size_t data_len, uint8_t data[data_len], int *errno_ptr) __attribute__((unused));
static int write_packet(unsigned short card_num, uint8_t token, size_t data_len, const uint8_t data[data_len],
                        uint8_t *data_response, int *errno_ptr) __attribute__((unused));
static int write_stop_tran(unsigned short card_num, uint8_t token, int *errno_ptr) __attribute__((unused));

static int go_idle_state(unsigned short card_num, r1 *response, int *errno_ptr) __attribute__((unused));
static int send_op_cond(unsigned short card_num, r1 *response, int *errno_ptr) __attribute__((unused));
static int send_if_cond(unsigned short card_num, r7 *response, int *errno_ptr) __attribute__((unused));
static int read_ocr(unsigned short card_num, r3 *response, int *errno_ptr) __attribute__((unused));
static int app_send_op_cond(unsigned short card_num, bool hcs, r1 *response, int *errno_ptr) __attribute__((unused));
static int read_single_block(unsigned short card_num, uint32_t address, size_t data_len, uint8_t data[data_len],
                             r1 *response, int *errno_ptr) __attribute__((unused));
static int write_block(unsigned short card_num, uint32_t address, size_t data_len, uint8_t data[data_len],
                       r1 *response, int *errno_ptr) __attribute__((unused));
static int app_cmd(unsigned short card_num, r1 *response, int *errno_ptr) __attribute__((unused));

static module dev_sd = {
    .init = dev_sd_init,
    .deinit = dev_sd_deinit,
    .name = "dev_sd",
};
MODULE_DEFINE(dev_sd);

static int dev_major = 0;
static const int dev_spidev_major = 3;

static device_operations dev_dops = {
    .read = dev_sd_read,
    .write = dev_sd_write,
    // .ioctl = dev_sd_ioctl,
};

typedef struct dev_spisd_info {
    devreg *dvrg;
    devreg *dvrg_parts[PARTITION_ENTRIES_COUNT];
    partition_disk_info part_dsk_info;
    bool ccs;
} dev_spisd_info;

dev_spisd_info sd_card_info[DEV_SPISD_MAX_DEVICES];

// TODO: This should be in this file
void spidev_reset_sd0();

static module_status dev_sd_init(void) {
    dev_major = device_register(dev_major, &dev_dops);
    if (dev_major < 0) {
        kpanic("Failed to register sd devices");
    }

    fill_one(&buf_idle);

    int status;
    int errno_tmp;
    r1 r1;
    r3 r3;
    r7 r7;
    sd_card_version version;

    unsigned short card_num = 0;    // Matches spidev minor number

    // CMD0: Reset and go to idle state
    spidev_reset_sd0();
    status = go_idle_state(card_num, &r1, &errno_tmp);
    if (status < 0) {
        return MODULE_FAILED_LOAD;
    }
    if ((r1 & R1_IN_IDLE_STATE) != r1) {
        // Some error occured
        return MODULE_FAILED_LOAD;
    }
    if (r1 != R1_IN_IDLE_STATE) {
        // Card is not idle
        return MODULE_FAILED_LOAD;
    }

    // Card is now idle

    // CMD8: Detect card version
    status = send_if_cond(card_num, &r7, &errno_tmp);
    if (status < 0) {
        return MODULE_FAILED_LOAD;
    }
    if (r7.r1 & R1_ILLEGAL_COMMAND) {
        // Version 1.X card or not an SD card
        version = SD_CARD_VERSION_1;
    } else if (((r7.r7 >> 0) & 0xFF) == 0xAA) {
        // Version 2.00 or later card
        version = SD_CARD_VERSION_2;
    } else {
        // Unusable card
        version = SD_CARD_VERSION_UNUSABLE;
    }

    if (version == SD_CARD_VERSION_UNUSABLE || version == SD_CARD_VERSION_NOT_SD) {
        return MODULE_FAILED_LOAD;
    }

    // ACMD41: Go to ready state
    bool hcs = (version == SD_CARD_VERSION_2);
    do {
        // TODO: Delay at each, and cap to 1 second
        status = app_send_op_cond(card_num, hcs, &r1, &errno_tmp);
        if (status < 0) {
            return MODULE_FAILED_LOAD;
        }
        if (r1 & R1_ILLEGAL_COMMAND) {
            // Not an SD card
            version = SD_CARD_VERSION_NOT_SD;
            break;
        }
        // TODO: Time out after retries
    } while (r1 & R1_IN_IDLE_STATE);

    if (version == SD_CARD_VERSION_UNUSABLE || version == SD_CARD_VERSION_NOT_SD) {
        return MODULE_FAILED_LOAD;
    }

    // Card is now ready

    bool ccs = false;
    if (version == SD_CARD_VERSION_2) {
        // CMD58: Check for SDHC and SDXC
        status = read_ocr(card_num, &r3, &errno_tmp);
        if (status < 0) {
            return MODULE_FAILED_LOAD;
        }
        if (r3.r1 & R1_ILLEGAL_COMMAND) {
            version = SD_CARD_VERSION_UNUSABLE;
        }
        if (r3.ocr & OCR_CCS) {
            ccs = true;
            version = SD_CARD_VERSION_HC;
        }
    }

    if (version == SD_CARD_VERSION_UNUSABLE || version == SD_CARD_VERSION_NOT_SD) {
        return MODULE_FAILED_LOAD;
    }

    sd_card_info[card_num].ccs = ccs;

#if SPISD_VERBOSE_DEBUG
    kdebug("SD card version %s",
        version == SD_CARD_VERSION_1 ? "1" :
        version == SD_CARD_VERSION_2 ? "2" :
        version == SD_CARD_VERSION_HC ? "HC" :
        "UNEXPECTED VALUE, PLEASE REPORT");
#endif

    int minor = card_num << DEV_SPISD_PARTITION_SHIFT;

    char dev_name[DEVREG_NAME_MAX_LENGTH + 1];
    if (kio_snprintf(dev_name, sizeof(dev_name), "mmcblk%d", card_num) > DEVREG_NAME_MAX_LENGTH) {
        kpanic("Failed to assemble device name for card_num %d", card_num);
    }
    // TODO: Check result
    sd_card_info[card_num].dvrg = devreg_new(dev_name, dev_major, minor);

    bool part_scan_result = partition_scan_device(dev_major, minor, &sd_card_info[card_num].part_dsk_info, &errno_tmp);
    if (part_scan_result) {
        // Device has partitions that can be read
        for (unsigned short part_num = 0; part_num < PARTITION_ENTRIES_COUNT && part_num < DEV_SPISD_PARTITION_GAP; ++part_num) {
            partition_entry *entry = &sd_card_info[card_num].part_dsk_info.entries[part_num];
            if (entry->type != 0x00) {
                if (kio_snprintf(dev_name, sizeof(dev_name), "mmcblk%dp%hu", card_num, part_num) > DEVREG_NAME_MAX_LENGTH) {
                    kpanic("Failed to assemble device name for card_num %d part_num %d", card_num, part_num);
                }
                // TODO: Check result
                sd_card_info[card_num].dvrg_parts[part_num] = devreg_new(dev_name, dev_major, minor + (1 + part_num));
            }
        }
    }

    return MODULE_LOADED;
}

static void dev_sd_deinit(void) {
    device_unregister(dev_major);
}

static ssize_t dev_sd_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    unsigned short card_num = minor / DEV_SPISD_PARTITION_GAP;
    unsigned short part_num_coded = minor % DEV_SPISD_PARTITION_GAP;
    unsigned short part_num;
    if (part_num_coded > 0) {
        part_num = part_num_coded - 1;
    }

    if (card_num >= DEV_SPISD_MAX_DEVICES || !sd_card_info[card_num].dvrg) {
        *errno_ptr = ENODEV;
        return -1;
    }

    if (part_num_coded == 0) {
        // Is the raw device
    } else {
        // Is a partition
        // TODO: Limit to avoid overflow
        offset += sd_card_info[card_num].part_dsk_info.entries[part_num].first_sector * BLOCK_SIZE;

        // TODO: Need to limit offset + count to size of partition
    }

    if (offset > ULONG_MAX) {
        return 0;
    }
    unsigned long current_offset = offset;

    size_t count_done = 0;
    size_t count_left = count;

    while (count_left > 0) {
        unsigned long this_block_long = current_offset / BLOCK_SIZE;
        // CCS=0 cards need block-aligned byte address
        if (!sd_card_info[card_num].ccs) {
            this_block_long *= BLOCK_SIZE;
        }
        if (this_block_long > UINT32_MAX) {
            break;
        }
        uint32_t this_block = this_block_long;
        unsigned this_offset_in_block = current_offset % BLOCK_SIZE;
        size_t this_count = min(count_left, BLOCK_SIZE - this_offset_in_block);

        // TODO: Use a better block buffer
        uint8_t tmp_buffer[BLOCK_SIZE];

        r1 response;
        if (read_single_block(card_num, this_block, BLOCK_SIZE, tmp_buffer, &response, errno_ptr) < 0) {
            return -1;
        }
        if (response & 0xFE) {
            *errno_ptr = EIO;
            return -1;
        }

        flex_to(flex_add(buf, count_done), tmp_buffer + this_offset_in_block, this_count);

        current_offset += this_count;
        count_done += this_count;
        count_left -= this_count;
    }

    return count;
}

static ssize_t dev_sd_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    unsigned short card_num = minor / DEV_SPISD_PARTITION_GAP;
    unsigned short part_num_coded = minor % DEV_SPISD_PARTITION_GAP;
    unsigned short part_num;
    if (part_num_coded > 0) {
        part_num = part_num_coded - 1;
    }

    if (card_num >= DEV_SPISD_MAX_DEVICES || !sd_card_info[card_num].dvrg) {
        *errno_ptr = ENODEV;
        return -1;
    }

    if (part_num_coded == 0) {
        // Is the raw device
    } else {
        // Is a partition
        // TODO: Limit to avoid overflow
        offset += sd_card_info[card_num].part_dsk_info.entries[part_num].first_sector * BLOCK_SIZE;

        // TODO: Need to limit offset + count to size of partition
    }

    if (offset > ULONG_MAX) {
        return 0;
    }
    unsigned long current_offset = offset;

    size_t count_done = 0;
    size_t count_left = count;

    while (count_left > 0) {
        unsigned long this_block_long = current_offset / BLOCK_SIZE;
        // CCS=0 cards need block-aligned byte address
        if (!sd_card_info[card_num].ccs) {
            this_block_long *= BLOCK_SIZE;
        }
        if (this_block_long > UINT32_MAX) {
            break;
        }
        uint32_t this_block = this_block_long;
        unsigned this_offset_in_block = current_offset % BLOCK_SIZE;
        size_t this_count = min(count_left, BLOCK_SIZE - this_offset_in_block);

        // TODO: Use a better block buffer
        uint8_t tmp_buffer[BLOCK_SIZE];

        if (this_count != BLOCK_SIZE) {
            r1 response;
            if (read_single_block(card_num, this_block, BLOCK_SIZE, tmp_buffer, &response, errno_ptr) < 0) {
                return -1;
            }
            if (response & 0xFE) {
                *errno_ptr = EIO;
                return -1;
            }
        }

        flex_from(tmp_buffer + this_offset_in_block, flex_add(buf, count_done), this_count);

        r1 response;
        if (write_block(card_num, this_block, BLOCK_SIZE, tmp_buffer, &response, errno_ptr) < 0) {
            return -1;
        }
        if (response & 0xFE) {
            *errno_ptr = EIO;
            return -1;
        }

        current_offset += this_count;
        count_done += this_count;
        count_left -= this_count;
    }

    return count;
}

// TODO: Release CS when needed

static int send_idle(unsigned short card_num, size_t len, bool cs_change, int *errno_ptr) {
    if (len > sizeof(buf_idle)) {
        kpanic("Length too long for buf_idle");
    }
    struct spi_ioc_transfer transfer[1] = {
        {
            .tx_buf = buf_idle,
            .rx_buf = NULL,
            .len = len,
            .cs_change = cs_change,
        },
    };

    int status = device_ioctl(dev_spidev_major, card_num, SPI_IOC_MESSAGE(1),
                              flex_mut_kernel(transfer, sizeof(transfer)), errno_ptr);
    return status;
}

static inline int send_ncs(unsigned short card_num, int *errno_ptr) {
    return send_idle(card_num, NCS, false, errno_ptr);
}

static inline int send_nrc(unsigned short card_num, int *errno_ptr) {
    return send_idle(card_num, NRC, false, errno_ptr);
}

static inline int send_nwr(unsigned short card_num, int *errno_ptr) {
    return send_idle(card_num, NWR, false, errno_ptr);
}

static inline int send_nec(unsigned short card_num, int *errno_ptr) {
    return send_idle(card_num, NEC, true, errno_ptr);
}

static inline int send_nds(unsigned short card_num, int *errno_ptr) {
    return send_idle(card_num, NDS, false, errno_ptr);
}

static int command(unsigned short card_num, int number, uint32_t argument, size_t response_len, void *response, int *errno_ptr) {
    const size_t command_len = 6;
    uint8_t command_buf[command_len];
    fill_one_size(&command_buf, command_len);
    command_buf[0] = 0x40 | (number & 0x3F);
    command_buf[1] = (argument >> 24) & 0xFF;
    command_buf[2] = (argument >> 16) & 0xFF;
    command_buf[3] = (argument >> 8) & 0xFF;
    command_buf[4] = (argument >> 0) & 0xFF;
    command_buf[5] = (number == 0) ? 0x95 : (number == 8) ? 0x86 : 0xFF;

    int status;
    uint8_t response_buf[response_len ? response_len : 1];

    struct spi_ioc_transfer command_transfer[1] = {
        {
            .tx_buf = command_buf,
            .rx_buf = NULL,
            .len = command_len,
            .cs_change = false,
        },
    };
    status = device_ioctl(dev_spidev_major, card_num, SPI_IOC_MESSAGE(1),
                          flex_mut_kernel(command_transfer, sizeof(command_transfer)), errno_ptr);
    if (status != 0) {
        return status;
    }

    unsigned ncr;
    for (ncr = 0; ncr < NCR_MAX; ++ncr) {
        struct spi_ioc_transfer r1_transfer[1] = {
            {
                .tx_buf = buf_idle,
                .rx_buf = response_buf + 0,
                .len = 1,
                .cs_change = false,
            },
        };
        status = device_ioctl(dev_spidev_major, card_num, SPI_IOC_MESSAGE(1),
                              flex_mut_kernel(r1_transfer, sizeof(r1_transfer)), errno_ptr);
        if (status != 0) {
            return status;
        }

        if (!(response_buf[0] & 0x80)) {
            break;
        } else if (response_buf[0] != 0xFF) {
            // TODO: Set errno
            // TODO: What does the above TODO mean?
            *errno_ptr = EIO;
            status = -1;
            break;
        }
    }
    if (ncr >= NCR_MAX) {
        *errno_ptr = EIO;
        status = -1;
    }

    struct spi_ioc_transfer response_transfer[1] = {
        {
            .tx_buf = buf_idle,
            .rx_buf = response_buf + 1,
            .len = response_len ? response_len - 1 : 0,
            .cs_change = false,
        },
    };
    status = device_ioctl(dev_spidev_major, card_num, SPI_IOC_MESSAGE(1),
                          flex_mut_kernel(response_transfer, sizeof(response_transfer)), errno_ptr);
    if (status != 0) {
        return status;
    }

    if (status == 0) {
        memcpy(response, response_buf, response_len);
    }

    return status;
}

static int command_r1(unsigned short card_num, int number, uint32_t argument, r1 *response, int *errno_ptr) {
    return command(card_num, number, argument, sizeof(*response), response, errno_ptr);
}

static int command_r3(unsigned short card_num, int number, uint32_t argument, r3 *response, int *errno_ptr) {
    return command(card_num, number, argument, sizeof(*response), response, errno_ptr);
}

static int command_r7(unsigned short card_num, int number, uint32_t argument, r7 *response, int *errno_ptr) {
    return command(card_num, number, argument, sizeof(*response), response, errno_ptr);
}

static int app_command_r1(unsigned short card_num, int number, uint32_t argument, r1 *response, int *errno_ptr) {
    r1 app_cmd_r1;
    int status = app_cmd(card_num, &app_cmd_r1, errno_ptr);
    if (status != 0) {
        return status;
    }
    // TODO: Check app_cmd_r1

    status = command_r1(card_num, number, argument, response, errno_ptr);
    if (status < 0) return status;

    status = send_nec(card_num, errno_ptr);
    if (status < 0) return status;

    return status;
}

static int recv_byte(unsigned short card_num, uint8_t *result, int *errno_ptr) {
    int status;

    struct spi_ioc_transfer transfer[1] = {
        {
            .tx_buf = buf_idle,
            .rx_buf = result,
            .len = 1,
            .cs_change = false,
        },
    };

    status = device_ioctl(dev_spidev_major, card_num, SPI_IOC_MESSAGE(1),
                          flex_mut_kernel(transfer, sizeof(transfer)), errno_ptr);
    return status;
}

static int read_packet(unsigned short card_num, uint8_t *token, size_t data_len, uint8_t data[data_len], int *errno_ptr) {
    int status = 0;
    uint8_t recv_token;
    for (unsigned short nac = 0; nac <= NAC_MAX; ++nac) {
        status = recv_byte(card_num, &recv_token, errno_ptr);
        if (status < 0) return status;

        if (recv_token != 0xFF) {
            break;
        }
    }

    if (!DATA_TOKEN_IS_VALID(recv_token)) {
        // Invalid data token
        // TODO: Set errno
        *errno_ptr = EIO;
        return -1;
    }
    *token = recv_token;
    if (DATA_TOKEN_IS_ERROR(*token)) {
        // Error-signaling data token
        return 0;
    }

    if (data_len > sizeof(buf_idle)) {
        kpanic("Length too long for buf_idle");
    }
    uint8_t crc[2];
    struct spi_ioc_transfer transfer[2] = {
        {
            .tx_buf = buf_idle,
            .rx_buf = data,
            .len = data_len,
            .cs_change = false,
        },
        {
            .tx_buf = buf_idle,
            .rx_buf = crc,
            .len = 2,
            .cs_change = false,
        },
    };

    status = device_ioctl(dev_spidev_major, card_num, SPI_IOC_MESSAGE(2),
                          flex_mut_kernel(transfer, sizeof(transfer)), errno_ptr);
    return status;
}

static int write_packet(unsigned short card_num, uint8_t token, size_t data_len, const uint8_t data[data_len],
                           uint8_t *data_response, int *errno_ptr) {
    uint8_t crc[2] = { 0xFF, 0xFF };
    uint8_t data_response_recv;
    struct spi_ioc_transfer transfer[4] = {
        {
            .tx_buf = &token,
            .rx_buf = NULL,
            .len = 1,
            .cs_change = false,
        },
        {
            .tx_buf = data,
            .rx_buf = NULL,
            .len = data_len,
            .cs_change = false,
        },
        {
            .tx_buf = crc,
            .rx_buf = NULL,
            .len = 2,
            .cs_change = false,
        },
        {
            .tx_buf = buf_idle,
            .rx_buf = &data_response_recv,
            .len = 1,
            .cs_change = false,
        },
    };

    int status = device_ioctl(dev_spidev_major, card_num, SPI_IOC_MESSAGE(4),
                              flex_mut_kernel(transfer, sizeof(transfer)), errno_ptr);
    if (status < 0) {
        return status;
    }
    if (!DATA_RESPONSE_TOKEN_IS_VALID(data_response_recv)) {
        // Data response has incorrect format
        // TODO: Set errno
        *errno_ptr = EIO;
        return -1;
    }
    *data_response = data_response_recv;

    // TODO: Add cap?
    while (1) {
        uint8_t busy;
        status = recv_byte(card_num, &busy, errno_ptr);

        if (status < 0) {
            return status;
        }
        if (busy != 0x00) {
            break;
        }
    }

    return status;
}

static int write_stop_tran(unsigned short card_num, uint8_t token, int *errno_ptr) {
    struct spi_ioc_transfer transfer[2] = {
        {
            .tx_buf = &token,
            .rx_buf = NULL,
            .len = 1,
            .cs_change = false,
        },
        {
            .tx_buf = buf_idle,
            .rx_buf = NULL,
            .len = 1,
            .cs_change = false,
        },
    };

    int status = device_ioctl(dev_spidev_major, card_num, SPI_IOC_MESSAGE(2),
                              flex_mut_kernel(transfer, sizeof(transfer)), errno_ptr);
    if (status != 0) {
        return status;
    }

    // TODO: Add cap?
    while (1) {
        uint8_t busy;
        status = recv_byte(card_num, &busy, errno_ptr);

        if (status != 0) {
            return status;
        }
        if (busy != 0x00) {
            break;
        }
    }

    return status;
}

static int go_idle_state(unsigned short card_num, r1 *response, int *errno_ptr) {
    int status;

    status = send_ncs(card_num, errno_ptr);
    if (status < 0) return status;

    status = command_r1(card_num, CMD_GO_IDLE_STATE, 0, response, errno_ptr);
    if (status < 0) return status;

    status = send_nec(card_num, errno_ptr);
    if (status < 0) return status;

    return status;
}

static int send_op_cond(unsigned short card_num, r1 *response, int *errno_ptr) {
    int status;

    status = send_ncs(card_num, errno_ptr);
    if (status < 0) return status;

    status = command_r1(card_num, CMD_SEND_OP_COND, 0, response, errno_ptr);
    if (status < 0) return status;

    status = send_nec(card_num, errno_ptr);
    if (status < 0) return status;

    return status;
}

static int send_if_cond(unsigned short card_num, r7 *response, int *errno_ptr) {
    int status;

    status = send_ncs(card_num, errno_ptr);
    if (status < 0) return status;

    // TODO: Don't hardcode argument
    status = command_r7(card_num, CMD_SEND_IF_COND, 0x000001AA, response, errno_ptr);
    if (status < 0) return status;

    status = send_nec(card_num, errno_ptr);
    if (status < 0) return status;

    return status;
}

static int read_ocr(unsigned short card_num, r3 *response, int *errno_ptr) {
    int status;

    status = send_ncs(card_num, errno_ptr);
    if (status < 0) return status;

    // TODO: Don't hardcode argument
    status = command_r3(card_num, CMD_READ_OCR, 0, response, errno_ptr);
    if (status < 0) return status;

    status = send_nec(card_num, errno_ptr);
    if (status < 0) return status;

    return status;
}

static int app_send_op_cond(unsigned short card_num, bool hcs, r1 *response, int *errno_ptr) {
    uint32_t argument = (1UL << 30) * hcs;
    int status;

    status = app_command_r1(card_num, ACMD_SEND_OP_COND, argument, response, errno_ptr);
    if (status < 0) return status;

    return status;
}

static int read_single_block(unsigned short card_num, uint32_t address, size_t data_len, uint8_t data[data_len],
                                r1 *response, int *errno_ptr) {
    int status;

    status = send_ncs(card_num, errno_ptr);
    if (status < 0) return status;

    status = command_r1(card_num, CMD_READ_SINGLE_BLOCK, address, response, errno_ptr);
    if (status < 0) return status;
    if (*response & 0xFE) return 0;

    uint8_t token;
    status = read_packet(card_num, &token, data_len, data, errno_ptr);
    if (status < 0) return status;
    if (token != DATA_TOKEN_SINGLE_READ_START) {
        // TODO: Set errno
        *errno_ptr = EIO;
        return -1;
    }

    status = send_nec(card_num, errno_ptr);
    if (status < 0) return status;

    return status;
}

static int write_block(unsigned short card_num, uint32_t address, size_t data_len, uint8_t data[data_len],
                          r1 *response, int *errno_ptr) {
    int status;

    status = send_ncs(card_num, errno_ptr);
    if (status < 0) return status;

    status = command_r1(card_num, CMD_WRITE_BLOCK, address, response, errno_ptr);
    if (status < 0) return status;
    if (*response & 0xFE) return 0;

    status = send_nwr(card_num, errno_ptr);
    if (status < 0) return status;

    uint8_t data_response;
    status = write_packet(card_num, DATA_TOKEN_SINGLE_WRITE_START, data_len, data, &data_response, errno_ptr);
    if (status < 0) return status;
    if (!DATA_RESPONSE_TOKEN_IS_DATA_ACCEPTED(data_response)) {
        // TODO: Set errno
        *errno_ptr = EIO;
        status = -1;
    }

    status = send_nec(card_num, errno_ptr);
    if (status < 0) return status;

    return status;
}

static int app_cmd(unsigned short card_num, r1 *response, int *errno_ptr) {
    int status;

    status = send_ncs(card_num, errno_ptr);
    if (status < 0) return status;

    status = command_r1(card_num, CMD_APP_CMD, 0, response, errno_ptr);
    if (status < 0) return status;

    status = send_nrc(card_num, errno_ptr);
    if (status < 0) return status;

    return status;
}

#endif
