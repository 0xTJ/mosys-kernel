#include "config.h"
#include "device/device.h"
#include "vfs/devreg.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "safeprobe.h"
#include "device/dev/fb.h"
#include "device/hardware/v99x8.h"
#include "uapi/errno.h"
#include "uapi/asm/ioctl.h"
#include "uapi/mosys/v99x8.h"
#include "util/circbuf.h"
#include <string.h>

#if DEV_V99X8_ENABLE

static module_status dev_v99x8_init(void);
static void dev_v99x8_deinit(void);

static ssize_t dev_v99x8_read(fb_info *info, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_v99x8_write(fb_info *info, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int dev_v99x8_ioctl(fb_info *info, unsigned long request, flex_mut argp, int *errno_ptr);

static const char *const dev_v99x8_deps[] = {
    "fb",
    NULL,
};
static module dev_v99x8 = {
    .init = dev_v99x8_init,
    .deinit = dev_v99x8_deinit,
    .name = "dev_v99x8",
    .deps = dev_v99x8_deps,
};
MODULE_DEFINE(dev_v99x8);

static fb_info *dev_v99x8_fb;

static fb_ops dev_v99x8_fbops = {
    .read = dev_v99x8_read,
    .write = dev_v99x8_write,
    .ioctl = dev_v99x8_ioctl,
};

static bool dev_v99x8_probe(void) {
    safeprobe_begin();
    unsigned char id = (v99x8_status_register_1_read() & V99X8_STATUS_REGISTER_1_ID_MASK) >> V99X8_STATUS_REGISTER_1_ID_SHIFT;
    if (!safeprobe_end_was_clean()) {
        return false;
    } else if (id != V99X8_ID_V9958) {
        return false;
    } else {
        return true;
    }
}

static module_status dev_v99x8_init(void) {
    if (!dev_v99x8_probe()) {
        return MODULE_FAILED_LOAD;
    }

    // Set mode G7, disable interrupts and enable output
    v99x8_mode_r0_write(V99X8_MODE_R0_M5 | V99X8_MODE_R0_M4 | V99X8_MODE_R0_M3);
    v99x8_mode_r1_write(V99X8_MODE_R1_BL | V99X8_MODE_R1_SI);

    // Set PAL mode
    v99x8_mode_r9_write(V99X8_MODE_R9_LN | V99X8_MODE_R9_NT);
    v99x8_text_and_screen_margin_color_write(0x00);
    v99x8_mode_r8_write(V99X8_MODE_R8_VR);

    // Set pattern layout table to 0x0000
    v99x8_pattern_layout_table_write((0x0000 >> 11) | 0x1F);

    // Set sprite pattern generator table to 0xF000
    v99x8_sprite_pattern_generator_table_write((0xF000 >> 11) | 0x00);

    // Set sprite attribute table to 0xFA00
    v99x8_sprite_attribute_table_write((0xFA00 >> 7) | 0x03);

    // Set top line displayed to 0
    v99x8_vertical_offset_register_write(0);

    // Clear display
    v99x8_lmmv(0, 0, 256, 212, v99x8_rgb3x8_to_grb8(0, 0, 0), V99X8_ARGUMENT_MXD_VRAM | V99X8_ARGUMENT_DIY_DOWN | V99X8_ARGUMENT_DIX_RIGHT, V99X8_LOGICAL_OPERATION_IMP);

    // Clear sprites
    uint8_t cleared_4_pattern[4][8] = {0};
    v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 0, cleared_4_pattern);
    for (int sprite = 0; sprite < 32; ++sprite) {
        v99x8_sm2_sprite_attribute_write(0xFA00, sprite, 216, 0, 0);
    }

    dev_v99x8_fb = fb_new();
    if (!dev_v99x8_fb) {
        return MODULE_FAILED_LOAD;
    }

    dev_v99x8_fb->ops = &dev_v99x8_fbops;

    fb_register(dev_v99x8_fb);

    return MODULE_LOADED;
}

static void dev_v99x8_deinit(void) {
    // TODO: Unregister then destroy framebuffer
}

static ssize_t dev_v99x8_read(fb_info *info, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    // TODO: Handle more than one (in theory)
    // TODO: Return correct result if offset is out of range, also adjust countst count
    flex_mut_pinned buf_pin = flex_pin_must_cleanup(buf);
    v99x8_vram_read(false, offset, count, buf_pin.ptr);
    flex_unpin(&buf_pin);
    return count;
}

static ssize_t dev_v99x8_write(fb_info *info, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    // TODO: Handle more than one (in theory)
    // TODO: Return correct result if offset is out of range, also adjust count
    flex_const_pinned buf_pin = flex_pin_must_cleanup(buf);
    v99x8_vram_write(false, offset, count, buf_pin.ptr);
    flex_unpin(&buf_pin);
    return count;
}

static int dev_v99x8_ioctl(fb_info *info, unsigned long request, flex_mut argp, int *errno_ptr) {
    // TODO: Handle more than one (in theory)
    if (_IOC_TYPE(request) != V99X8_IOC_MAGIC) {
        *errno_ptr = EINVAL;
        return -1;
    }

    argp.size = _IOC_SIZE(request);
    flex_mut_pinned argp_pin = flex_pin_must_cleanup(argp);

    switch(_IOC_NR(request)) {
    case 0:
        // Control Register Write
        if (argp.size != sizeof(struct v99x8_ioc_control)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        struct v99x8_ioc_control *control = argp_pin.ptr;
        v99x8_control_register_write(control->cr, control->value);
        flex_unpin(&argp_pin);
        return 0;

    case 1:
        // Palette Register Write
        if (argp.size != sizeof(struct v99x8_ioc_palette)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        struct v99x8_ioc_palette *palette = argp_pin.ptr;
        v99x8_palette_register_write(palette->pr, palette->value);
        flex_unpin(&argp_pin);
        return 0;

    case 2:
        // Status Register Read
        if (argp.size != sizeof(struct v99x8_ioc_status)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        struct v99x8_ioc_status *status = argp_pin.ptr;
        status->value = v99x8_status_register_read(status->sr);
        flex_unpin(&argp_pin);
        return 0;

    case 0x1F:
        // HMMC
        if (argp.size != sizeof(struct v99x8_ioc_command)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        {
            struct v99x8_ioc_command *ioc_command = argp_pin.ptr;
            flex_const data_in_flx = flex_const_generic(argp.type, ioc_command->data_in, (size_t) ioc_command->number_of_dots_x * ioc_command->number_of_dots_y);
            flex_const_pinned data_in_pin = flex_pin_must_cleanup(data_in_flx);
            v99x8_hmmc(
                ioc_command->destination_x,
                ioc_command->destination_y,
                ioc_command->number_of_dots_x,
                ioc_command->number_of_dots_y,
                ioc_command->argument,
                data_in_pin.ptr
            );
            flex_unpin(&data_in_pin);
        }
        flex_unpin(&argp_pin);
        return 0;

    case 0x1E:
        // YMMM
        if (argp.size != sizeof(struct v99x8_ioc_command)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        {
            struct v99x8_ioc_command *ioc_command = argp_pin.ptr;
            v99x8_ymmm(
                ioc_command->source_y,
                ioc_command->destination_x,
                ioc_command->destination_y,
                ioc_command->number_of_dots_y,
                ioc_command->argument
            );
        }
        flex_unpin(&argp_pin);
        return 0;

    case 0x1D:
        // HMMM
        if (argp.size != sizeof(struct v99x8_ioc_command)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        {
            struct v99x8_ioc_command *ioc_command = argp_pin.ptr;
            v99x8_hmmm(
                ioc_command->source_x,
                ioc_command->source_y,
                ioc_command->destination_x,
                ioc_command->destination_y,
                ioc_command->number_of_dots_x,
                ioc_command->number_of_dots_y,
                ioc_command->argument
            );
        }
        flex_unpin(&argp_pin);
        return 0;

    case 0x1C:
        // HMMV
        if (argp.size != sizeof(struct v99x8_ioc_command)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        {
            struct v99x8_ioc_command *ioc_command = argp_pin.ptr;
            v99x8_hmmv(
                ioc_command->destination_x,
                ioc_command->destination_y,
                ioc_command->number_of_dots_x,
                ioc_command->number_of_dots_y,
                ioc_command->color,
                ioc_command->argument
            );
        }
        flex_unpin(&argp_pin);
        return 0;

    case 0x1B:
        // LMMC
        if (argp.size != sizeof(struct v99x8_ioc_command)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        {
            struct v99x8_ioc_command *ioc_command = argp_pin.ptr;
            flex_const data_in_flx = flex_const_generic(argp.type, ioc_command->data_in, (size_t) ioc_command->number_of_dots_x * ioc_command->number_of_dots_y);
            flex_const_pinned data_in_pin = flex_pin_must_cleanup(data_in_flx);
            v99x8_lmmc(
                ioc_command->destination_x,
                ioc_command->destination_y,
                ioc_command->number_of_dots_x,
                ioc_command->number_of_dots_y,
                ioc_command->argument,
                ioc_command->command,
                data_in_pin.ptr
            );
            flex_unpin(&data_in_pin);
        }
        flex_unpin(&argp_pin);
        return 0;

    case 0x1A:
        // LMCM
        if (argp.size != sizeof(struct v99x8_ioc_command)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        {
            struct v99x8_ioc_command *ioc_command = argp_pin.ptr;
            flex_mut data_out_flx = flex_mut_generic(argp.type, ioc_command->data_out, (size_t) ioc_command->number_of_dots_x * ioc_command->number_of_dots_y);
            flex_mut_pinned data_out_pin = flex_pin_must_cleanup(data_out_flx);
            v99x8_lmcm(
                ioc_command->source_x,
                ioc_command->source_y,
                ioc_command->number_of_dots_x,
                ioc_command->number_of_dots_y,
                ioc_command->argument,
                data_out_pin.ptr
            );
            flex_unpin(&data_out_pin);
        }
        flex_unpin(&argp_pin);
        return 0;

    case 0x19:
        // LMMM
        if (argp.size != sizeof(struct v99x8_ioc_command)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        {
            struct v99x8_ioc_command *ioc_command = argp_pin.ptr;
            v99x8_lmmm(
                ioc_command->source_x,
                ioc_command->source_y,
                ioc_command->destination_x,
                ioc_command->destination_y,
                ioc_command->number_of_dots_x,
                ioc_command->number_of_dots_y,
                ioc_command->argument,
                ioc_command->command
            );
        }
        flex_unpin(&argp_pin);
        return 0;

    case 0x18:
        // LMMV
        if (argp.size != sizeof(struct v99x8_ioc_command)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        {
            struct v99x8_ioc_command *ioc_command = argp_pin.ptr;
            v99x8_lmmv(
                ioc_command->destination_x,
                ioc_command->destination_y,
                ioc_command->number_of_dots_x,
                ioc_command->number_of_dots_y,
                ioc_command->color,
                ioc_command->argument,
                ioc_command->command
            );
        }
        flex_unpin(&argp_pin);
        return 0;

    case 0x17:
        // LINE
        if (argp.size != sizeof(struct v99x8_ioc_command)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        {
            struct v99x8_ioc_command *ioc_command = argp_pin.ptr;
            v99x8_line(
                ioc_command->destination_x,
                ioc_command->destination_y,
                ioc_command->number_of_dots_x,
                ioc_command->number_of_dots_y,
                ioc_command->color,
                ioc_command->argument,
                ioc_command->command
            );
        }
        flex_unpin(&argp_pin);
        return 0;

    case 0x16:
        // SRCH
        if (argp.size != sizeof(struct v99x8_ioc_command)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        {
            struct v99x8_ioc_command *ioc_command = argp_pin.ptr;
            ioc_command->srch_result = v99x8_srch(
                ioc_command->source_x,
                ioc_command->source_y,
                ioc_command->color,
                ioc_command->argument
            );
        }
        flex_unpin(&argp_pin);
        return 0;

    case 0x15:
        // PSET
        if (argp.size != sizeof(struct v99x8_ioc_command)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        {
            struct v99x8_ioc_command *ioc_command = argp_pin.ptr;
            v99x8_pset(
                ioc_command->destination_x,
                ioc_command->destination_y,
                ioc_command->color,
                ioc_command->argument,
                ioc_command->command
            );
        }
        flex_unpin(&argp_pin);
        return 0;

    case 0x14:
        // POINT
        if (argp.size != sizeof(struct v99x8_ioc_command)) {
            flex_unpin(&argp_pin);
            *errno_ptr = EINVAL;
            return -1;
        }
        {
            struct v99x8_ioc_command *ioc_command = argp_pin.ptr;
            ioc_command->point_result = v99x8_point(
                ioc_command->source_x,
                ioc_command->source_y,
                ioc_command->argument,
                ioc_command->command
            );
        }
        flex_unpin(&argp_pin);
        return 0;

    case 0x10:
        // STOP
        v99x8_stop();
        flex_unpin(&argp_pin);
        return 0;

    default:
        flex_unpin(&argp_pin);
        *errno_ptr = EINVAL;
        return -1;
    }
}

#endif
