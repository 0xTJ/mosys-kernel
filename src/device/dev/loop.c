#include "device/device.h"
#include "config.h"
#include "vfs/devreg.h"
#include "vfs/fdesc.h"
#include "uapi/errno.h"
#include "util/rwlock.h"
#include "kernel/module.h"
#include "kio/kio.h"
#include "kio/klog.h"
#include "uapi/mosys/loop.h"
#include "util/util.h"
#include "vfs/vfs.h"
#include <string.h>

#define DEV_LOOP_NAME_MAX_LENGTH 32

// TODO: Lock this
static rwlock dev_loop_fdscs_lock = RWLOCK_INIT;
static fdesc *dev_loop_fdscs[DEV_LOOP_NUM] = { 0 };
static struct loop_info dev_loop_infos[DEV_LOOP_NUM] = { 0 };

static module_status dev_loop_init(void);
static void dev_loop_deinit(void);

static ssize_t dev_loop_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_loop_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int dev_loop_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr);

static int dev_loop_set_fd(int minor, int backing_fd, int *errno_ptr);
static int dev_loop_clear_fd(int minor, int *errno_ptr);
static int dev_loop_set_status(int minor, const struct loop_info *info, int *errno_ptr);
static int dev_loop_get_status(int minor, struct loop_info *info, int *errno_ptr);

static module dev_loop = {
    .init = dev_loop_init,
    .deinit = dev_loop_deinit,
    .name = "dev_loop",
};
MODULE_DEFINE(dev_loop);

static int dev_loop_major = 0;

static device_operations dev_loop_dops = {
    dev_loop_read,
    dev_loop_write,
    dev_loop_ioctl,
};

static module_status dev_loop_init(void) {
    dev_loop_major = device_register(dev_loop_major, &dev_loop_dops);
    if (dev_loop_major < 0) {
        kpanic("Failed to register loop devices");
    }

    RWLOCK_WRITE_WITH(&dev_loop_fdscs_lock, {
        for (size_t i = 0; i < DEV_LOOP_NUM; ++i) {
            dev_loop_fdscs[i] = NULL;
            
            char tmp_name[DEV_LOOP_NAME_MAX_LENGTH + 1];
            kio_snprintf(tmp_name, sizeof(tmp_name), "loop%u", (unsigned) i);
            devreg_new(tmp_name, dev_loop_major, i);

            break;
        }
    })

    return MODULE_LOADED;
}

static void dev_loop_deinit(void) {
    device_unregister(dev_loop_major);
}

static ssize_t dev_loop_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    if (minor < 0 || minor >= DEV_LOOP_NUM) {
        *errno_ptr = EBADF;
        return -1;
    }
    
    if (dev_loop_fdscs[minor]) {
        return vfs_pread_fdesc(dev_loop_fdscs[minor], buf, count, offset + dev_loop_infos[minor].lo_offset, errno_ptr);
    } else {
        return 0;
    }
}

static ssize_t dev_loop_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    if (minor < 0 || minor >= DEV_LOOP_NUM) {
        *errno_ptr = EBADF;
        return -1;
    }
    
    if (dev_loop_fdscs[minor]) {
        return vfs_pwrite_fdesc(dev_loop_fdscs[minor], buf, count, offset + dev_loop_infos[minor].lo_offset, errno_ptr);
    } else {
        *errno_ptr = ENOSPC;
        return -1;
    }
}

// TODO: Add the ability to remove them, must call vfs_unref_fdsc
static int dev_loop_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr) {
    if (minor < 0 || minor >= DEV_LOOP_NUM) {
        *errno_ptr = EBADF;
        return -1;
    }

    if (_IOC_TYPE(request) != LOOP_IOC_MAGIC) {
        *errno_ptr = EINVAL;
        return -1;
    }

    argp.size = _IOC_SIZE(request);

    struct loop_info info;
    const unsigned long request_nr = _IOC_NR(request);
    if (request_nr == _IOC_NR(LOOP_SET_FD)) {
        int backing_fd = (int) flex_get_ptr(argp);
        return dev_loop_set_fd(minor, backing_fd, errno_ptr);
    } else if (request_nr == _IOC_NR(LOOP_CLR_FD)) {
        return dev_loop_clear_fd(minor, errno_ptr);
    } else if (request_nr == _IOC_NR(LOOP_SET_STATUS)) {
        flex_result flx_rslt = flex_from(&info, argp, sizeof(struct loop_info));
        if (flx_rslt != FLEX_RESULT_SUCCESS) {
            *errno_ptr = EFAULT;
            return -1;
        }
        return dev_loop_set_status(minor, &info, errno_ptr);
    } else if (request_nr == _IOC_NR(LOOP_GET_STATUS)) {
        if (dev_loop_get_status(minor, &info, errno_ptr) < 0) {
            return -1;
        }
        flex_result flx_rslt = flex_to(argp, &info, sizeof(struct loop_info));
        if (flx_rslt != FLEX_RESULT_SUCCESS) {
            *errno_ptr = EFAULT;
            return -1;
        } else {
            return 0;
        }
    } else {
        *errno_ptr = EINVAL;
        return -1;
    }
}

static int dev_loop_set_fd(int minor, int backing_fd, int *errno_ptr) {
    if (dev_loop_fdscs[minor]) {
        *errno_ptr = EBUSY;
        return -1;
    }

    fdesc *backing_fdsc = vfs_fdesc_from_fd(backing_fd);
    if (!backing_fdsc) {
        *errno_ptr = EINVAL;
        return -1;
    }

    dev_loop_fdscs[minor] = backing_fdsc;
    dev_loop_infos[minor].lo_offset = 0;
    return minor;
}

static int dev_loop_clear_fd(int minor, int *errno_ptr) {
    fdesc *backing_fdsc = dev_loop_fdscs[minor];

    if (!backing_fdsc) {
        *errno_ptr = ENXIO;
        return -1;
    }

    dev_loop_fdscs[minor] = NULL;
    memset(&dev_loop_infos[minor], 0, sizeof dev_loop_infos[minor]);
    vfs_fdesc_unref(backing_fdsc);
    backing_fdsc = NULL;
    return 0;
}

static int dev_loop_set_status(int minor, const struct loop_info *info, int *errno_ptr) {
    fdesc *backing_fdsc = dev_loop_fdscs[minor];

    if (!backing_fdsc) {
        *errno_ptr = ENXIO;
        return -1;
    }

    // Validate parameters
    if (info->lo_offset < 0) {
        *errno_ptr = EINVAL;
        return -1;
    }

    dev_loop_infos[minor].lo_offset = info->lo_offset;
    return 0;
}

static int dev_loop_get_status(int minor, struct loop_info *info, int *errno_ptr) {
    fdesc *backing_fdsc = dev_loop_fdscs[minor];

    if (!backing_fdsc) {
        *errno_ptr = ENXIO;
        return -1;
    }

    memset(info, 0, sizeof *info);
    info->lo_offset = dev_loop_infos[minor].lo_offset;
    return 0;
}
