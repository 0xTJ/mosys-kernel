#include "config.h"
#include "device/device.h"
#include "device/hardware/sst39sf0x0.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "uapi/errno.h"
#include "util/util.h"
#include "vfs/devreg.h"
#include <string.h>
#include <limits.h>

#if DEV_SST39SF0X0_ENABLE

#define DEV_SST39SF0X0_SINGLE_START 0
#define DEV_SST39SF0X0_SINGLE_END 16
#define DEV_SST39SF0X0_DUAL_START DEV_SST39SF0X0_SINGLE_END
#define DEV_SST39SF0X0_DUAL_END 32

static module_status dev_sst39sf0x0_init(void);
static void dev_sst39sf0x0_deinit(void);

static ssize_t dev_sst39sf0x0_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_sst39sf0x0_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int dev_sst39sf0x0_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr);

static module dev_sst39sf0x0 = {
    .init = dev_sst39sf0x0_init,
    .deinit = dev_sst39sf0x0_deinit,
    .name = "dev_sst39sf0x0",
};
MODULE_DEFINE(dev_sst39sf0x0);

static int dev_sst39sf0x0_major = 0;

static device_operations dev_sst39sf0x0_dops = {
    .read = dev_sst39sf0x0_read,
    .write = dev_sst39sf0x0_write,
    .ioctl = dev_sst39sf0x0_ioctl,
};

static module_status dev_sst39sf0x0_init(void) {
    dev_sst39sf0x0_major = device_register(dev_sst39sf0x0_major, &dev_sst39sf0x0_dops);
    if (dev_sst39sf0x0_major < 0) {
        kpanic("Failed to register sst39sf0x0 devices");
    }

    devreg_new("flash0", dev_sst39sf0x0_major, DEV_SST39SF0X0_SINGLE_START + SST39SF0X0_LOCATION_EVEN);
    devreg_new("flash1", dev_sst39sf0x0_major, DEV_SST39SF0X0_SINGLE_START + SST39SF0X0_LOCATION_ODD);
    devreg_new("flash0,1", dev_sst39sf0x0_major, DEV_SST39SF0X0_DUAL_START + SST39SF0X0_DUAL_LOCATION_ALL);

    return MODULE_LOADED;
}

static void dev_sst39sf0x0_deinit(void) {
    device_unregister(dev_sst39sf0x0_major);
}

static ssize_t dev_sst39sf0x0_read_single(sst39sf0x0_location location, uint8_t *buf_ptr, size_t count, unsigned long long offset, int *errno_ptr) {
    switch(location) {
    case SST39SF0X0_LOCATION_EVEN:
    case SST39SF0X0_LOCATION_ODD:
        // These exist
        break;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }

    if (offset > UINT32_MAX) {
        return 0;
    }
    // TODO: Check against actual chip size

    for (size_t i = 0; i < count; ++i) {
        buf_ptr[i] = sst39sf0x0_byte_read(location, offset++);
    }

    return count;
}

static ssize_t dev_sst39sf0x0_read_dual(sst39sf0x0_dual_location location, uint8_t *buf_ptr, size_t count, unsigned long long offset, int *errno_ptr) {
    switch(location) {
    case SST39SF0X0_DUAL_LOCATION_ALL:
        // These exist
        break;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }

    if (offset > UINT32_MAX) {
        return 0;
    }
    // TODO: Check against actual chip size

    for (size_t i = 0; i < count; ++i) {
        buf_ptr[i] = sst39sf0x0_dual_byte_read(location, offset++);
    }

    return count;
}

static ssize_t dev_sst39sf0x0_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    flex_mut_pinned buf_pin = flex_pin_must_cleanup(buf);
    if (buf_pin.fault || buf_pin.size < count) {
        *errno_ptr = EFAULT;
        return -1;
    }
    uint8_t *buf_ptr = buf_pin.ptr;

    ssize_t result;

    if (DEV_SST39SF0X0_SINGLE_START <= minor && minor < DEV_SST39SF0X0_SINGLE_END) {
        sst39sf0x0_location location = minor - DEV_SST39SF0X0_SINGLE_START;
        result = dev_sst39sf0x0_read_single(location, buf_ptr, count, offset, errno_ptr);
    } else if (DEV_SST39SF0X0_DUAL_START <= minor && minor < DEV_SST39SF0X0_DUAL_END) {
        sst39sf0x0_dual_location location = minor - DEV_SST39SF0X0_DUAL_START;
        result = dev_sst39sf0x0_read_dual(location, buf_ptr, count, offset, errno_ptr);
    } else {
        *errno_ptr = ENOENT;
        result = -1;
    }

    flex_unpin(&buf_pin);
    buf_ptr = NULL;
    return result;
}

static ssize_t dev_sst39sf0x0_write_single(sst39sf0x0_location location, const uint8_t *buf_ptr, size_t count, unsigned long long offset, int *errno_ptr) {
    switch(location) {
    case SST39SF0X0_LOCATION_EVEN:
    case SST39SF0X0_LOCATION_ODD:
        // These exist
        break;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }

    if (offset > UINT32_MAX) {
        return 0;
    }
    // TODO: Check against actual chip size

    for (size_t i = 0; i < count; ++i) {
        sst39sf0x0_byte_program(location, offset++, buf_ptr[i]);
    }

    return count;
}

static ssize_t dev_sst39sf0x0_write_dual(sst39sf0x0_dual_location location, const uint8_t *buf_ptr, size_t count, unsigned long long offset, int *errno_ptr) {
    switch(location) {
    case SST39SF0X0_DUAL_LOCATION_ALL:
        // These exist
        break;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }

    if (offset > UINT32_MAX) {
        return 0;
    }
    // TODO: Check against actual chip size

    for (size_t i = 0; i < count; ++i) {
        sst39sf0x0_dual_byte_program(location, offset++, buf_ptr[i]);
    }

    return count;
}

static ssize_t dev_sst39sf0x0_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    flex_const_pinned buf_pin = flex_pin_must_cleanup(buf);
    if (buf_pin.fault || buf_pin.size < count) {
        *errno_ptr = EFAULT;
        return -1;
    }
    const uint8_t *buf_ptr = buf_pin.ptr;

    ssize_t result;

    if (DEV_SST39SF0X0_SINGLE_START <= minor && minor < DEV_SST39SF0X0_SINGLE_END) {
        sst39sf0x0_location location = minor - DEV_SST39SF0X0_SINGLE_START;
        result = dev_sst39sf0x0_write_single(location, buf_ptr, count, offset, errno_ptr);
    } else if (DEV_SST39SF0X0_DUAL_START <= minor && minor < DEV_SST39SF0X0_DUAL_END) {
        sst39sf0x0_dual_location location = minor - DEV_SST39SF0X0_DUAL_START;
        result = dev_sst39sf0x0_write_dual(location, buf_ptr, count, offset, errno_ptr);
    } else {
        *errno_ptr = ENOENT;
        result = -1;
    }

    flex_unpin(&buf_pin);
    buf_ptr = NULL;
    return result;
}

static int dev_sst39sf0x0_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr) {
    UNUSED(minor);
    UNUSED(request);
    UNUSED(argp);

    *errno_ptr = EINVAL;
    return -1;
}

#endif
