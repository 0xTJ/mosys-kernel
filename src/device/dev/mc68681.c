#include "config.h"
#include "constants.h"
#include "safeprobe.h"
#include "timer.h"
#include "device/device.h"
#include "device/dev/tty.h"
#include "device/hardware/mc68681.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "thread/thread.h"
#include "uapi/errno.h"
#include "util/circbuf.h"
#include "vfs/devreg.h"
#include <string.h>

#if DEV_MC68681_ENABLE

static bool dev_mc68681_probe(void);
static module_status dev_mc68681_init(void);
static void dev_mc68681_deinit(void);
static void dev_mc68681_interrupt(void);

static ssize_t dev_mc68681_read(struct tty_info *info, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_mc68681_write(struct tty_info *info, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int dev_mc68681_ioctl(struct tty_info *info, unsigned long request, flex_mut argp, int *errno_ptr);

static const char *const dev_mc68681_deps[] = {
    "tty",
    NULL,
};
static module dev_mc68681 = {
    .init = dev_mc68681_init,
    .deinit = dev_mc68681_deinit,
    .name = "dev_mc68681",
    .deps = dev_mc68681_deps,
};
MODULE_DEFINE(dev_mc68681);

#if DEV_MC68681_USE_INTERRUPTS
static uint8_t rx_a_buf[DEV_MC68681_BUFFER_SIZE];
static uint8_t tx_a_buf[DEV_MC68681_BUFFER_SIZE];
static uint8_t rx_b_buf[DEV_MC68681_BUFFER_SIZE];
static uint8_t tx_b_buf[DEV_MC68681_BUFFER_SIZE];
static circbuf_uint8_t rx_a_circbuf = {
    rx_a_buf,
    sizeof(rx_a_buf),
};
static circbuf_uint8_t tx_a_circbuf = {
    tx_a_buf,
    sizeof(tx_a_buf),
};
static circbuf_uint8_t rx_b_circbuf = {
    rx_b_buf,
    sizeof(rx_b_buf),
};
static circbuf_uint8_t tx_b_circbuf = {
    tx_b_buf,
    sizeof(tx_b_buf),
};
#else
static circbuf_uint8_t rx_a_circbuf = { NULL, 0 };
static circbuf_uint8_t tx_a_circbuf = { NULL, 0 };
static circbuf_uint8_t rx_b_circbuf = { NULL, 0 };
static circbuf_uint8_t tx_b_circbuf = { NULL, 0 };
#endif

static tty_ops dev_mc68681_tops = {
    .read = dev_mc68681_read,
    .write = dev_mc68681_write,
    .ioctl = dev_mc68681_ioctl,
};

static enum mc68681_imr current_imr;
static void dev_mc68681_imr_enable_no_lock(enum mc68681_imr mask) {
    enum mc68681_imr old_imr = current_imr;
    current_imr = (current_imr | mask) & MC68681_IMR_MASK;
    if (current_imr != old_imr) {
        mc68681_imr_set(current_imr);
    }
}
static void dev_mc68681_imr_enable(enum mc68681_imr mask) {
    thread_scheduler_lock();
    dev_mc68681_imr_enable_no_lock(mask);
    thread_scheduler_unlock();
}
static void dev_mc68681_imr_disable_no_lock(enum mc68681_imr mask) {
    enum mc68681_imr old_imr = current_imr;
    current_imr = (current_imr & ~mask) & MC68681_IMR_MASK;
    if (current_imr != old_imr) {
        mc68681_imr_set(current_imr);
    }
}
static void dev_mc68681_imr_disable(enum mc68681_imr mask) {
    thread_scheduler_lock();
    dev_mc68681_imr_disable_no_lock(mask);
    thread_scheduler_unlock();
}

static bool dev_mc68681_probe(void) {
    safeprobe_begin();
    uint8_t orig_ivr = mc68681_ivr_get();
    if (!safeprobe_end_was_clean()) {
        return false;
    }

    // Disable all MC68681 interrupts
    mc68681_imr_set(current_imr = MC68681_IMR_ALL_OFF);

    uint8_t test_values[] = {
        orig_ivr,
        orig_ivr ^ 0xAA,
        orig_ivr ^ 0x55,
        0x00,
        0xFF,
        0x0F,
    };

    for (unsigned short i = 0; i < countof(test_values); ++i) {
        uint8_t test_ivr = test_values[i];
        mc68681_ivr_set(test_ivr);
        if (mc68681_ivr_get() != test_ivr) {
            return false;
        }
    }

    return true;
}

static module_status dev_mc68681_init(void) {
    if (!dev_mc68681_probe()) {
        return MODULE_FAILED_LOAD;
    }

    // Interrupts were disabled by probe

    // Install interrupt handlers
    mc68681_ivr_set(75);
    mc68681_interrupt_install(dev_mc68681_interrupt);

#if DEV_MC68681_TICK_CLOCK_ON_RED_LED
    // OP3 is C/T #1 Output, others are OPR
    mc68681_opcr_set(0x1 << 2);
#else
    // All OP are OPR
    mc68681_opcr_set(0);
#endif

    // Use BRG Set 2 and set C/T to Timer with CLK/16
    mc68681_acr_set(MC68681_ACR_BRG_SET_SELECT_SET_2 |
                    MC68681_ACR_CT_MODE_TIMER_CLK_DIVIDED_BY_16);
    unsigned long ct_freq = mc68681_xtal_freq_get() / 16;
    unsigned long timer_freq = ct_freq / 2;

    // Enable UART A at 115200 8N1, no flow control
    mc68681_cra_set(MC68681_CR_SET_TX_BRG_SELECT_EXTEND_BIT);
    mc68681_cra_set(MC68681_CR_SET_RX_BRG_SELECT_EXTEND_BIT);
    mc68681_csra_set(0x88);
    mc68681_mra_set((struct mc68681_mr) {
        .mr1 =  MC68681_MR1_RXRTS_CONTROL_NO |
                MC68681_MR1_RXINT_SELECT_RXRDY |
                MC68681_MR1_ERROR_MODE_CHAR |
                MC68681_MR1_PARITY_MODE_NO_PARITY |
                MC68681_MR1_BITS_PER_CHARACTER_8,
        .mr2 =  MC68681_MR2_CHANNEL_MODE_NORMAL |
                MC68681_MR2_TXRTS_CONTROL_NO |
                MC68681_MR2_CTS_ENABLE_TX_NO |
                MC68681_MR2_STOP_BIT_LENGTH_1_000,
    });
    mc68681_cra_set(MC68681_CR_ENABLE_TX_YES | MC68681_CR_ENABLE_RX_YES);

    // Enable UART B at 115200 8N1, no flow control
    mc68681_crb_set(MC68681_CR_SET_TX_BRG_SELECT_EXTEND_BIT);
    mc68681_crb_set(MC68681_CR_SET_RX_BRG_SELECT_EXTEND_BIT);
    mc68681_csrb_set(0x88);
    mc68681_mrb_set((struct mc68681_mr) {
        .mr1 =  MC68681_MR1_RXRTS_CONTROL_NO |
                MC68681_MR1_RXINT_SELECT_RXRDY |
                MC68681_MR1_ERROR_MODE_CHAR |
                MC68681_MR1_PARITY_MODE_NO_PARITY |
                MC68681_MR1_BITS_PER_CHARACTER_8,
        .mr2 =  MC68681_MR2_CHANNEL_MODE_NORMAL |
                MC68681_MR2_TXRTS_CONTROL_NO |
                MC68681_MR2_CTS_ENABLE_TX_NO |
                MC68681_MR2_STOP_BIT_LENGTH_1_000,
    });
    mc68681_crb_set(MC68681_CR_ENABLE_TX_YES | MC68681_CR_ENABLE_RX_YES);

    // Start C/T at DEV_MC68681_TICK_FREQUENCY Hz
    mc68681_ctr_set(timer_freq / DEV_MC68681_TICK_FREQUENCY);
    mc68681_scc();

    // Enable counter interrupt
    dev_mc68681_imr_enable(MC68681_IMR_COUNTER_READY_ON);

#if DEV_MC68681_USE_INTERRUPTS
    // Enable UART interrupts
    dev_mc68681_imr_enable(
        MC68681_IMR_RXRDY_FFULLB_ON | MC68681_IMR_TXRDYB_ON |
        MC68681_IMR_RXRDY_FFULLA_ON | MC68681_IMR_TXRDYA_ON);
#endif

    tty_info *info_a = tty_new();
    if (!info_a) {
        return MODULE_FAILED_LOAD;
    }
    tty_info *info_b = tty_new();
    if (!info_b) {
        tty_delete(info_a);
        return MODULE_FAILED_LOAD;
    }

    info_a->ops = &dev_mc68681_tops;
    info_a->data_mem = (mem_addr) MC68681_BASE;
    info_a->data_int = 0;

    info_b->ops = &dev_mc68681_tops;
    info_b->data_mem = (mem_addr) MC68681_BASE;
    info_b->data_int = 1;

    tty_register(info_a, "ttyS%u");
    tty_register(info_b, "ttyS%u");

    return MODULE_LOADED;
}

static void dev_mc68681_deinit(void) {
    // TODO: Delete TTY devices
}

static int dev_mc68681_recv_a(void) {
    int result = -1;
    thread_scheduler_lock();

#if DEV_MC68681_USE_INTERRUPTS
    uint8_t c;
    if (circbuf_uint8_t_get(&rx_a_circbuf, &c)) {
        result = c;
    }
    dev_mc68681_imr_enable(MC68681_IMR_RXRDY_FFULLA_ON);
#else
    if ((mc68681_sra_get() & MC68681_SR_RXRDY_MASK) == MC68681_SR_RXRDY_YES) {
        result = mc68681_rhra_get();
    }
#endif

    thread_scheduler_unlock();
    return result;
}

static int dev_mc68681_recv_b(void) {
    int result = -1;
    thread_scheduler_lock();

#if DEV_MC68681_USE_INTERRUPTS
    uint8_t c;
    if (circbuf_uint8_t_get(&rx_b_circbuf, &c)) {
        result = c;
    }
    dev_mc68681_imr_enable(MC68681_IMR_RXRDY_FFULLB_ON);
#else
    if ((mc68681_srb_get() & MC68681_SR_RXRDY_MASK) == MC68681_SR_RXRDY_YES) {
        result = mc68681_rhrb_get();
    }
#endif

    thread_scheduler_unlock();
    return result;
}

static void dev_mc68681_send_a(uint8_t c) {
    bool has_sent = false;
    while (!has_sent) {
        thread_scheduler_lock();

#if DEV_MC68681_USE_INTERRUPTS
        has_sent = circbuf_uint8_t_put(&tx_a_circbuf, c);
        dev_mc68681_imr_enable(MC68681_IMR_TXRDYA_ON);
#else
        if ((mc68681_sra_get() & MC68681_SR_TXRDY_MASK) == MC68681_SR_TXRDY_YES) {
            mc68681_thra_set(c);
            has_sent = true;
        }
#endif

        thread_scheduler_unlock();
    }
}

static void dev_mc68681_send_b(uint8_t c) {
    bool has_sent = false;
    while (!has_sent) {
        thread_scheduler_lock();

#if DEV_MC68681_USE_INTERRUPTS
        has_sent = circbuf_uint8_t_put(&tx_b_circbuf, c);
        dev_mc68681_imr_enable(MC68681_IMR_TXRDYB_ON);
#else
        if ((mc68681_srb_get() & MC68681_SR_TXRDY_MASK) == MC68681_SR_TXRDY_YES) {
            mc68681_thrb_set(c);
            has_sent = true;
        }
#endif

        thread_scheduler_unlock();
    }
}

// TODO: Use the physical address in info

static ssize_t dev_mc68681_read(struct tty_info *info, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    UNUSED(offset);

    flex_mut_pinned buf_pin = flex_pin_must_cleanup(buf);
    if (buf_pin.fault || buf_pin.size < count) {
        *errno_ptr = EFAULT;
        return -1;
    }
    uint8_t *buf_ptr = buf_pin.ptr;

    ssize_t result;

    switch(info->data_int) {
    case 0: // uart1
        for (size_t i = 0; i < count; ++i) {
            int tmp = dev_mc68681_recv_a();
            if (tmp >= 0) {
                buf_ptr[i] = tmp;
            } else {
                result = i;
                goto done;
            }
        }
        result = count;
        break;
    case 1: // uart2
        for (size_t i = 0; i < count; ++i) {
            int tmp = dev_mc68681_recv_b();
            if (tmp >= 0) {
                buf_ptr[i] = tmp;
            } else {
                result = i;
                goto done;
            }
        }
        result = count;
        break;
    default:
        *errno_ptr = ENOENT;
        result = -1;
    }

done:
    flex_unpin(&buf_pin);
    buf_ptr = NULL;

    return result;
}

static ssize_t dev_mc68681_write(struct tty_info *info, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    UNUSED(offset);

    flex_const_pinned buf_pin = flex_pin_must_cleanup(buf);
    if (buf_pin.fault || buf_pin.size < count) {
        *errno_ptr = EFAULT;
        return -1;
    }
    const uint8_t *buf_ptr = buf_pin.ptr;

    switch(info->data_int) {
    case 0: // uart1
        for (size_t i = 0; i < count; ++i) {
            dev_mc68681_send_a(buf_ptr[i]);
        }
        flex_unpin(&buf_pin);
        buf_ptr = NULL;
        return count;
    case 1: // uart2
        for (size_t i = 0; i < count; ++i) {
            dev_mc68681_send_b(buf_ptr[i]);
        }
        flex_unpin(&buf_pin);
        buf_ptr = NULL;
        return count;
    default:
        flex_unpin(&buf_pin);
        buf_ptr = NULL;
        *errno_ptr = ENOENT;
        return -1;
    }
}

static int dev_mc68681_ioctl(tty_info *info, unsigned long request, flex_mut argp, int *errno_ptr) {
    UNUSED(info);
    UNUSED(request);
    UNUSED(argp);

    *errno_ptr = EINVAL;
    return -1;
}

INTERRUPT_ATTR static void dev_mc68681_interrupt(void) {
    enum mc68681_isr misr = mc68681_misr_get();
    enum mc68681_imr imr_to_disable = (enum mc68681_imr) misr;

    CRITICAL({
        if ((misr & MC68681_ISR_COUNTER_READY_MASK) == MC68681_ISR_COUNTER_READY_YES) {
            mc68681_stc();
            thread_system_tick(NANO_PER / DEV_MC68681_TICK_FREQUENCY);
            imr_to_disable &= ~MC68681_IMR_COUNTER_READY_MASK;
        }

        if (DEV_MC68681_USE_INTERRUPTS) {
            if ((misr & MC68681_ISR_RXRDY_FFULLB_MASK) == MC68681_ISR_RXRDY_FFULLB_YES) {
                while ((mc68681_srb_get() & MC68681_SR_RXRDY_MASK) == MC68681_SR_RXRDY_YES) {
                    if (!circbuf_uint8_t_is_full(&rx_b_circbuf)) {
                        uint8_t c = mc68681_rhrb_get();
                        imr_to_disable &= ~MC68681_IMR_RXRDY_FFULLB_MASK;
                        kassert(circbuf_uint8_t_put(&rx_b_circbuf, c));
                    }
                }
            }

            if ((misr & MC68681_ISR_TXRDYB_MASK) == MC68681_ISR_TXRDYB_YES) {
                uint8_t c;
                if (circbuf_uint8_t_get(&tx_b_circbuf, &c)) {
                    mc68681_thrb_set(c);
                    imr_to_disable &= ~MC68681_IMR_TXRDYB_MASK;
                }
            }

            if ((misr & MC68681_ISR_RXRDY_FFULLA_MASK) == MC68681_ISR_RXRDY_FFULLA_YES) {
                while ((mc68681_sra_get() & MC68681_SR_RXRDY_MASK) == MC68681_SR_RXRDY_YES) {
                    if (!circbuf_uint8_t_is_full(&rx_a_circbuf)) {
                        uint8_t c = mc68681_rhra_get();
                        imr_to_disable &= ~MC68681_IMR_RXRDY_FFULLA_MASK;
                        kassert(circbuf_uint8_t_put(&rx_a_circbuf, c));
                    }
                }
            }

            if ((misr & MC68681_ISR_TXRDYA_MASK) == MC68681_ISR_TXRDYA_YES) {
                uint8_t c;
                if (circbuf_uint8_t_get(&tx_a_circbuf, &c)) {
                    mc68681_thra_set(c);
                    imr_to_disable &= ~MC68681_IMR_TXRDYA_MASK;
                }
            }
        }

        if (imr_to_disable) {
            dev_mc68681_imr_disable_no_lock(imr_to_disable);
        }
    })
}

#endif
