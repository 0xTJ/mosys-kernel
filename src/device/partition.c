#include "device/partition.h"
#include "device/device.h"
#include "kio/klog.h"
#include "uapi/errno.h"
#include "util/util.h"
#include <string.h>

bool partition_scan_device(int major, int minor, partition_disk_info *disk_info, int *errno_ptr) {
    uint8_t mbr_buf[PARTITION_SECTOR_SIZE];
    flex_mut mbr_buf_flx = flex_mut_kernel(mbr_buf, sizeof(mbr_buf));
    ssize_t read_result = device_read(major, minor, mbr_buf_flx, PARTITION_SECTOR_SIZE, 0, errno_ptr);

    if (read_result < 0) {
        return false;
    } else if (read_result != PARTITION_SECTOR_SIZE) {
        *errno_ptr = EIO;
        return false;
    }

    if (mbr_buf [0x01FE] == 0x55 && mbr_buf [0x01FF] == 0xAA) {
        // Valid boot signature
    } else {
        *errno_ptr = EILSEQ;
        return false;
    }

    for (unsigned short part_num = 0; part_num < PARTITION_ENTRIES_COUNT; ++part_num) {
        unsigned short offset = 0x01BE + part_num * 0x10;

        uint8_t status = from_little_u8(mbr_buf[offset + 0x00]);
        uint8_t type = from_little_u8(mbr_buf[offset + 0x04]);
        uint32_t first_sector;
        memmove(&first_sector, mbr_buf + offset + 0x08, 4);
        first_sector = from_little_u32(first_sector);
        uint32_t count_sectors;
        memmove(&count_sectors, mbr_buf + offset + 0x0C, 4);
        count_sectors = from_little_u32(count_sectors);

        disk_info->entries[part_num].status = status;
        disk_info->entries[part_num].type = type;
        disk_info->entries[part_num].first_sector = first_sector;
        disk_info->entries[part_num].count_sectors = count_sectors;
    }

    return true;
}
