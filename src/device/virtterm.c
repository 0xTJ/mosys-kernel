#include "device/virtterm.h"

int virtterm_handle_code(virtterm_info *vt_info, int c, int *errno_ptr) {
    if (vt_info->state == VIRTTERM_STATE_NORMAL) {
        if (0x00 <= c && c <= 0x1F) {
            return virtterm_handle_c0(vt_info, c, errno_ptr);
        } else if (0x20 <= c && c <= 0xFF && c != 0x7F) {
            return virtterm_draw_char(vt_info, c, errno_ptr);
        } else if (c == 0x7F) {
            // DELETE, ignore
            return 0;
        } else {
            // Ignore unknown code
            return 0;
        }
    } else if (vt_info->state == VIRTTERM_STATE_FE) {
        if (0x40 <= c && c <= 0x5F) {
            return virtterm_handle_fe(vt_info, c, errno_ptr);
        } else {
            // Ignore unknown code, return to normal state
            vt_info->state = VIRTTERM_STATE_NORMAL;
            return 0;
        }
    } else if (vt_info->state == VIRTTERM_STATE_CSI) {
        if (0x20 <= c && c <= 0x7E) {
            return virtterm_handle_csi(vt_info, c, errno_ptr);
        } else {
            // Ignore unknown code, return to normal state
            vt_info->state = VIRTTERM_STATE_NORMAL;
            return 0;
        }
    } else {
        unreachable();
    }
}

inline int virtterm_handle_c0(virtterm_info *vt_info, int c, int *errno_ptr) {
    switch (c) {
    case 0x08:  // BS
        if (vt_info->current_col > 0) {
            vt_info->current_col -= 1;
            return virtterm_draw_char_no_adv(vt_info, '\0', errno_ptr);
        }
        return 0;

    case 0x09:  // HT
        vt_info->current_col = (vt_info->current_col + 4) % 4;
        if (vt_info->current_col >= vt_info->cols) {
            vt_info->current_col = vt_info->cols - 1;
        }
        return 0;

    case 0x0A:  // LF
        vt_info->current_row += 1;
        virtterm_fix_rows(vt_info, errno_ptr);
        return 0;

    case 0x0C:  // FF
        return 0;

    case 0x0D:  // CR
        vt_info->current_col = 0;
        return 0;

    case 0x1B:  // ESC
        vt_info->state = VIRTTERM_STATE_FE;
        return 0;

    default:
        return 0;
    }
}

inline int virtterm_handle_fe(virtterm_info *vt_info, int c, int *errno_ptr) {
    UNUSED(errno_ptr);

    switch (c) {
    case 0x5B:  // CSI
        vt_info->state = VIRTTERM_STATE_CSI;
        vt_info->csi_accept_param = true;
        vt_info->csi_param_count = 0;
        vt_info->csi_inter_count = 0;
        vt_info->csi_final = '\0';
        return 0;

    default:
        vt_info->state = VIRTTERM_STATE_NORMAL;
        return 0;
    }
}

inline int virtterm_handle_csi(virtterm_info *vt_info, int c, int *errno_ptr) {
    if (vt_info->csi_accept_param && 0x30 <= c && c <= 0x3F) {
        // Accept parameter byte
        if (vt_info->csi_param_count < VT_CSI_PARAM_COUNT_MAX) {
            vt_info->csi_param[vt_info->csi_param_count++] = c;
        } else {
            // TODO: Do something about this
            kwarn("Too many CSI parameter bytes");
            vt_info->state = VIRTTERM_STATE_NORMAL;
        }
    } else if (0x20 <= c && c <= 0x2F) {
        // Accept intermediate byte
        vt_info->csi_accept_param = false;
        if (vt_info->csi_inter_count < VT_CSI_INTER_COUNT_MAX) {
            vt_info->csi_inter[vt_info->csi_inter_count++] = c;
        } else {
            // TODO: Do something about this
            kwarn("Too many CSI intermediate bytes");
            vt_info->state = VIRTTERM_STATE_NORMAL;
        }
    } else if (0x40 <= c && c <= 0x7E) {
        // Accept final byte
        vt_info->csi_final = c;
        vt_info->state = VIRTTERM_STATE_NORMAL;
        return virtterm_process_csi(vt_info, errno_ptr);
    } else {
        // Unexpected byte
        vt_info->state = VIRTTERM_STATE_NORMAL;
    }

    return 0;
}

inline int virtterm_process_csi(virtterm_info *vt_info, int *errno_ptr) {
    if (vt_info->csi_inter_count == 0) {
        // No intermediate bytes
        switch (vt_info->csi_final) {
        case 0x6D:  // SGR
            return virtterm_process_sgr(vt_info, errno_ptr);

        default:
            return 0;
        }
    } else {
        // At least one intermediate byte
        // Currently not handled
        return 0;
    }
}

inline int virtterm_process_sgr(virtterm_info *vt_info, int *errno_ptr) {
    int n = 0;
    for (int i = 0; i < vt_info->csi_param_count; ++i) {
        int c = vt_info->csi_param[i];
        if (0x30 <= c && c <= 0x39) {
            // Is a digit
            int this_dig = c - 0x30;
            if (n > (INT_MAX / 10) || (n * 10) > (INT_MAX - this_dig)) {
                // Would overflow, stop processing
                return 0;
            } 
            n = (n * 10) + this_dig;
        } else if (c == 0x3B) {
            virtterm_process_sgr_aspect(vt_info, n, errno_ptr);
            n = 0;
        } else {
            // Incorrect character in sequence, stop processing
            return 0;
        }
    }
    virtterm_process_sgr_aspect(vt_info, n, errno_ptr);
    return 0;
}


int virtterm_draw_char(virtterm_info *vt_info, int c, int *errno_ptr) {
    virtterm_draw_char_no_adv(vt_info, c, errno_ptr);
    vt_info->current_col += 1;
    if (vt_info->current_col >= vt_info->cols) {
        vt_info->current_col = 0;
        vt_info->current_row += 1;
        virtterm_fix_rows(vt_info, errno_ptr);
    }
    // TODO: Return errors
    return 0;
}
