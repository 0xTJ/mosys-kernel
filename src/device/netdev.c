#include "device/netdev.h"
#include "network/link/link.h"
#include "kio/kio.h"
#include "kio/klog.h"
#include "util/rwlock.h"
#include "util/util.h"
#include <string.h>

#if NETWORK_LINK_ENABLE

static netdev *netdev_list = NULL;
static rwlock netdev_list_rwlock = RWLOCK_INIT;

// netdev_list_rwlock must be held with at least read
// Structures are not ref'ed by these
static netdev **netdev_list_get_tail_ptr_nolock(void);
static netdev *netdev_by_name_nolock(const char *name);
static netdev *netdev_by_ipv4_nolock(ipv4_addr target_addr, ipv4_addr *actual_addr_out);

netdev *netdev_new(size_t priv_size, const char *name, const link_ops *dtlnk_ops) {
    kassert(name);
    kassert(dtlnk_ops);

    netdev *ntdv = dynobj_mtx_new(netdev, dynobj_no_cleanup);
    if (!ntdv) {
        goto failed_alloc_ntdv;
    }

    void *priv;
    if (priv_size) {
        priv = kalloc(priv_size);
        if (!priv) {
            goto failed_alloc_priv;
        }
    } else {
        priv = NULL;
    }

    // Generate name
    bool name_is_unique = false;
    for (unsigned counter = 0; !name_is_unique && counter < NETDEV_AUTO_NAME_LIMIT; ++counter) {
        kio_snprintf(ntdv->name, sizeof(ntdv->name), name, counter);
        if (!netdev_by_name(ntdv->name)) {
            name_is_unique = true;
            break;
        }
    }
    if (!name_is_unique) {
        goto failed_unique_name;
    }

    ntdv->dtlnk_ops = dtlnk_ops;
    ntdv->priv = priv;

    return ntdv;

failed_unique_name:
    if (priv)
        kfree(priv);
failed_alloc_priv:
    dynobj_mtx_unref(netdev, &ntdv->dnobj);
failed_alloc_ntdv:
    return NULL;
}

void netdev_ref(netdev *ntdv) {
    dynobj_mtx_ref(netdev, &ntdv->dnobj);
}

void netdev_unref(netdev *ntdv) {
    dynobj_mtx_unref(netdev, &ntdv->dnobj);
}

static netdev **netdev_list_get_tail_ptr_nolock(void) {
    netdev **result = &netdev_list;
    while (*result) {
        result = &(*result)->next;
    }
    return result;
}

bool netdev_register(netdev *ntdv) {
    bool result;
    RWLOCK_WRITE_WITH(&netdev_list_rwlock, {
        if (!netdev_by_name_nolock(ntdv->name)) {
            netdev **tail_ptr = netdev_list_get_tail_ptr_nolock();
            netdev_ref(ntdv);
            *tail_ptr = ntdv;
            ntdv->next = NULL;
            result = true;
        } else {
            // A netdev with this name already exists
            result = false;
        }
    })
    return result;
}

static netdev *netdev_by_name_nolock(const char *name) {
    netdev *result = NULL;
    for (netdev *it = netdev_list; it; it = it->next) {
        MUTEX_WITH(&it->dnobj.mtx) {
            if (strncmp(it->name, name, sizeof(it->name)) == 0) {
                result = it;
            }
        }
        if (result) {
            break;
        }
    }
    return result;
}

netdev *netdev_by_name(const char *name) {
    netdev *result;
    RWLOCK_READ_WITH(&netdev_list_rwlock, {
        result = netdev_by_name_nolock(name);
        if (result) {
            netdev_ref(result);
        }
    })
    return result;
}

#if NETWORK_INTERNET_IPV4_ENABLE

static netdev *netdev_by_ipv4_nolock(ipv4_addr target_addr, ipv4_addr *actual_addr_out) {
    netdev *result = NULL;
    for (netdev *it = netdev_list; it; it = it->next) {
        MUTEX_WITH(&it->dnobj.mtx, {
            if (it->has_ipv4_address) {
                if (ipv4_subnet_contains_addr(it->ipv4_address, it->ipv4_netmask, target_addr)) {
                    result = it;
                    *actual_addr_out = it->ipv4_address;
                }
            }
        })
        if (result) {
            break;
        }
    }
    return result;
}

netdev *netdev_by_ipv4(ipv4_addr target_addr, ipv4_addr *actual_addr_out) {
    netdev *result;
    RWLOCK_READ_WITH(&netdev_list_rwlock, {
        result = netdev_by_ipv4_nolock(target_addr, actual_addr_out);
        if (result) {
            netdev_ref(result);
        }
    })
    return result;
}

#endif
#endif
