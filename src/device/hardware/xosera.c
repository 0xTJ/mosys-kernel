#include "device/hardware/xosera.h"
#include "config.h"
#include "thread/timing.h"

#if DEV_XOSERA_ENABLE

bool xosera_sync(volatile void *base) {
    xosera_xm_set_w(base, XOSERA_XM_RD_ADDR, 0xF5A5);
    if (xosera_xm_get_w(base, XOSERA_XM_RD_ADDR) != 0xF5A5)
    {
        return false;        // not detected
    }
    xosera_xm_set_w(base, XOSERA_XM_RD_ADDR, 0xFA5A);
    if (xosera_xm_get_w(base, XOSERA_XM_RD_ADDR) != 0xFA5A)
    {
        return false;        // not detected
    }
    return true;
}

bool xosera_init(volatile void *base, int boot_config) {
    // Detect Xosera
    for (int r = 0; r < 200; ++r) {
        if (xosera_sync(base)) {
            break;
        }
        timing_spin_us(10000);
    }

    if ((boot_config & 3) == boot_config) {
        // Reboot FPGA to boot_config
        // TODO: Use mask values when added
        xosera_xm_set_w(base, XOSERA_XM_SYS_CTRL, 0x800F | (uint16_t) (boot_config << 13));

        // Wait for Xosera to respond (~80 ms)
        for (int r = 0; r < 200; r++) {
            timing_spin_us(10000);
            if (xosera_sync(base)) {
                break;
            }
        }
    }

    return xosera_sync(base);
}

#endif
