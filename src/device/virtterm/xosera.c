#include "device/virtterm/xosera.h"
#include "config.h"

#if VIRTTERM_IS_XOSERA

#define VT_XOSERA_GREEN_ON_BLACK ((0x0 << 4) | (0x2 << 0))

int virtterm_xosera_init_vt_info(virtterm_info *vt_info, int *errno_ptr) {
    vt_info->rows = VT_ROWS;
    vt_info->cols = VT_COLS;
    vt_info->current_row = VT_ROWS;
    vt_info->current_col = 0;

    vt_info->xosera_colour_code = VT_XOSERA_GREEN_ON_BLACK;

    virtterm_xosera_fix_rows(vt_info, errno_ptr);

    return 0;
}

int virtterm_xosera_process_sgr_aspect(virtterm_info *vt_info, int aspect, int *errno_ptr) {
    UNUSED(errno_ptr);

    switch (aspect) {
    case 0:
        vt_info->xosera_colour_code = VT_XOSERA_GREEN_ON_BLACK;
        break;
    case 30:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0x0 << 0);
        break;
    case 31:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0x4 << 0);
        break;
    case 32:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0x2 << 0);
        break;
    case 33:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0x6 << 0);
        break;
    case 34:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0x1 << 0);
        break;
    case 35:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0x5 << 0);
        break;
    case 36:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0x3 << 0);
        break;
    case 37:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0x7 << 0);
        break;
    case 39:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0x7 << 0);
        break;
    case 40:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0x0 << 4);
        break;
    case 41:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0x4 << 4);
        break;
    case 42:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0x2 << 4);
        break;
    case 43:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0x6 << 4);
        break;
    case 44:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0x1 << 4);
        break;
    case 45:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0x5 << 4);
        break;
    case 46:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0x3 << 4);
        break;
    case 47:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0x7 << 4);
        break;
    case 49:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0x0 << 4);
        break;
    case 90:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0x8 << 0);
        break;
    case 91:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0xC << 0);
        break;
    case 92:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0xA << 0);
        break;
    case 93:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0xE << 0);
        break;
    case 94:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0x9 << 0);
        break;
    case 95:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0xD << 0);
        break;
    case 96:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0xB << 0);
        break;
    case 97:
        vt_info->xosera_colour_code &= 0xF0;
        vt_info->xosera_colour_code |= (0xF << 0);
        break;
    case 100:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0x8 << 4);
        break;
    case 101:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0xC << 4);
        break;
    case 102:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0xA << 4);
        break;
    case 103:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0xE << 4);
        break;
    case 104:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0x9 << 4);
        break;
    case 105:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0xD << 4);
        break;
    case 106:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0xB << 4);
        break;
    case 107:
        vt_info->xosera_colour_code &= 0x0F;
        vt_info->xosera_colour_code |= (0xF << 4);
        break;
    }
    return 0;
}

int virtterm_xosera_fix_rows(virtterm_info *vt_info, int *errno_ptr) {
    const int row_size = vt_info->cols * 2;

    while (vt_info->current_row >= vt_info->rows) {
        flex_mut blank_row_buf_flx = flex_mut_kernel(vt_info->row_buffer, row_size);

        // Scroll screen
        for (int row = 1; row < vt_info->rows; ++row) {
            // TODO: Check results
            device_read(vt_info->screen_dr->major, vt_info->screen_dr->minor, blank_row_buf_flx, row_size, row * row_size, errno_ptr);
            device_write(vt_info->screen_dr->major, vt_info->screen_dr->minor, flex_make_const(blank_row_buf_flx), row_size, (row - 1) * row_size, errno_ptr);
        }
        
        // Clear this line
        memset(vt_info->row_buffer, 0, row_size);
        device_write(vt_info->screen_dr->major, vt_info->screen_dr->minor, flex_make_const(blank_row_buf_flx), row_size, (vt_info->rows - 1) * row_size, errno_ptr);

        vt_info->current_row -= 1;
    }

    return 0;
}

int virtterm_xosera_draw_char_no_adv(virtterm_info *vt_info, int c, int *errno_ptr) {
    unsigned long screen_offset = vt_info->current_row * vt_info->cols + vt_info->current_col;

    uint8_t tmp_buf[2] = {
        vt_info->xosera_colour_code,
        c,
    };
    flex_const tmp_buf_flx = flex_const_kernel(&tmp_buf, 2);
    device_write(vt_info->screen_dr->major, vt_info->screen_dr->minor, tmp_buf_flx, 2, screen_offset * 2, errno_ptr);
    // TODO: Return errors
    return 0;
}

#endif
