#include "device/device.h"
#include "kalloc.h"
#include "kio/klog.h"
#include "util/rwlock.h"
#include "util/util.h"
#include "vfs/vfs.h"
#include <string.h>

static device *device_new(device_operations *dops);
static void device_delete(device *dev);

device *devices[DEVICE_NUM] = {0};
rwlock devices_rwl = RWLOCK_INIT;

static device *device_new(device_operations *dops) {
    device *dev = kalloc(sizeof(device));
    if (dev) {
        dev->dops = dops;
    }
    return dev;
}

static void device_delete(device *dev) {
    kfree(dev);
}

int device_register(int major, device_operations *dops) {
    device *dev = device_new(dops);
    if (!dev) {
        return -1;
    }

    RWLOCK_WRITE_WITH(&devices_rwl, {
        if (major == 0) {
            // Find unused major number
            for (major = DEVICE_MAJOR_UNUSED_START; major < DEVICE_NUM; ++major) {
                if (!devices[major]) {
                    devices[major] = dev;
                    break;
                }
            }
            if (major >= DEVICE_NUM) {
                // No free device
                major = -1;
            }
        } else if (major > 0 && major < DEVICE_NUM) {
            // Use specific major number
            if (!devices[major]) {
                devices[major] = dev;
            } else {
                // Already in use
                major = -1;
            }
        } else {
            // Invalid number
            major = -1;
        }
    })

    if (major < 0) {
        device_delete(dev);
    }
    return major;
}

void device_unregister(int major) {
    RWLOCK_WRITE_WITH(&devices_rwl, {
        if (major > 0 && major < DEVICE_NUM) {
            kpanic("Tried to unregister invalid major device number");
        } else if (!devices[major]) {
            kpanic("Tried to unregister unused major device number");
        } else {
            device_delete(devices[major]);
            devices[major] = NULL;
        }
    })
}

ssize_t device_read(int major, int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    if (major < 0 || major >= DEVICE_NUM) {
        *errno_ptr = EINVAL;
        return -1;
    }

    device *this_device;

    RWLOCK_READ_WITH(&devices_rwl, {
        this_device = devices[major];
        if (!this_device) {
            rwlock_read_end(&devices_rwl);
            *errno_ptr = EINVAL;
            RWLOCK_RETURN_VALUE_NO_UNLOCK(-1);
        }
    })

    // TODO: Make sure this device can't be deleted until this completes
    // TODO: Make sure this actually exists
    return this_device->dops->read(minor, buf, count, offset, errno_ptr);
}

ssize_t device_write(int major, int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    if (major < 0 || major >= DEVICE_NUM) {
        *errno_ptr = EINVAL;
        return -1;
    }

    device *this_device;

    RWLOCK_READ_WITH(&devices_rwl, {
        this_device = devices[major];
        if (!this_device) {
            rwlock_read_end(&devices_rwl);
            *errno_ptr = EINVAL;
            RWLOCK_RETURN_VALUE_NO_UNLOCK(-1);
        }
    })

    // TODO: Make sure this device can't be deleted until this completes
    // TODO: Make sure this actually exists
    return this_device->dops->write(minor, buf, count, offset, errno_ptr);
}

int device_ioctl(int major, int minor, unsigned long request, flex_mut argp, int *errno_ptr) {
    if (major < 0 || major >= DEVICE_NUM) {
        *errno_ptr = EINVAL;
        return -1;
    }

    device *this_device;

    RWLOCK_READ_WITH(&devices_rwl, {
        this_device = devices[major];
        if (!this_device) {
            rwlock_read_end(&devices_rwl);
            *errno_ptr = EINVAL;
            RWLOCK_RETURN_VALUE_NO_UNLOCK(-1);
        }
    })

    // TODO: Make sure this device can't be deleted until this completes
    // TODO: Make sure this actually exists
    return this_device->dops->ioctl(minor, request, argp, errno_ptr);
}
