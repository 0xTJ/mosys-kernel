#include "device/tty_util.h"
#include "device/device.h"
#include "mem/flex.h"
#include "uapi/termios.h"
#include "uapi/asm/ioctl.h"
#include "uapi/asm/ioctls.h"

int tty_util_make_raw(int major, int minor, int *errno_ptr) {
    int result;
    struct termios termios_p;
    flex_mut termios_p_flx = flex_mut_kernel(&termios_p, sizeof termios_p);

    result = device_ioctl(major, minor, TCGETS, termios_p_flx, errno_ptr);
    if (result < 0) {
        return result;
    }

    termios_p.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    termios_p.c_oflag &= ~(OPOST);
    termios_p.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);

    result = device_ioctl(major, minor, TCSETS, termios_p_flx, errno_ptr);
    return result;
}