#include <stdint.h>

extern uint32_t _text[], _text_end[];
extern uint32_t _data[], _data_end[];
extern uint32_t _bss[], _bss_end[];

void _kinit() {
    // Zero BSS
    for (uint32_t *dst = _bss; dst < _bss_end; dst += 1) {
        *dst = 0;
    }
}
