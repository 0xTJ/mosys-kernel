#include "thread/timing.h"
#include "thread/thread_list_scheduler.h"

static unsigned long long uptime_us = 0;
static thread_list sleeping_list = THREAD_LIST_INIT;

// Only to be called in scheduler context
void timing_uptime_tick_us(unsigned long long addend) {
    uptime_us += addend;
}

// Only called by timing_uptime_wake_sleeping
static bool wake_done_sleeping(thread *thrd, unsigned long long until_us) {
    if (thrd->sleep_until_us < until_us) {
        thread_wake(thrd);
    }
    return true;
}

// Only to be called in scheduler context
void timing_uptime_wake_sleeping(void) {
    list_apply(&sleeping_list.list, wake_done_sleeping, thread, scheduler_link, uptime_us);
}

unsigned long long timing_uptime_get_us(void) {
    return uptime_us;
}

void timing_sleep_until_us(unsigned long long time_us) {
    thread_scheduler_lock();
    thread_current->sleep_until_us = time_us;
    thread_current->state = THREAD_STATE_SLEEPING;
    thread_list_scheduler_enqueue(&sleeping_list, thread_current);
    thread_schedule();
    thread_scheduler_unlock();
}
