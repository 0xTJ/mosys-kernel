#include "network/link/link.h"
#include "network/network_types.h"
#include "network/internet/internet.h"
#include "network/link/link_generic.h"
#include "kalloc.h"
#include "kio/klog.h"
#include "uapi/errno.h"
#include "util/util.h"
#include <string.h>

#if NETWORK_LINK_ENABLE

ssize_t link_needed_frame_length(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length,
    int *errno_ptr
) {
    if (ntdv->dtlnk_ops->needed_frame_length) {
        return ntdv->dtlnk_ops->needed_frame_length(ntdv, info, payload_length, errno_ptr);
    } else {
        kpanic("Expected netdev::needed_frame_length");
    }
}

bool link_alloc_raw_frame(
    netdev *ntdv,
    size_t frame_length, char **frame_out,
    int *errno_ptr
) {
    if (ntdv->dtlnk_ops->alloc_raw_frame) {
        return ntdv->dtlnk_ops->alloc_raw_frame(ntdv, frame_length, frame_out, errno_ptr);
    } else {
        return link_generic_alloc_raw_frame(ntdv, frame_length, frame_out, errno_ptr);
    }
}

char *link_alloc_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length,
    size_t *frame_length_out,
    int *errno_ptr
) {
    if (payload_length > ntdv->mtu) {
        // TODO: Is EINVAL correct?
        *errno_ptr = EINVAL;
        return NULL;
    }

    if (ntdv->dtlnk_ops->alloc_frame) {
        return ntdv->dtlnk_ops->alloc_frame(ntdv, info, payload_length, frame_length_out, errno_ptr);
    } else {
        return link_generic_alloc_frame(ntdv, info, payload_length, frame_length_out, errno_ptr);
    }
}

void link_dealloc_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
) {
    if (ntdv->dtlnk_ops->dealloc_frame) {
        ntdv->dtlnk_ops->dealloc_frame(ntdv, info, frame_length, frame, errno_ptr);
    } else {
        link_generic_dealloc_frame(ntdv, info, frame_length, frame, errno_ptr);
    }
}

char *link_payload_in_frame(
    struct netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    size_t payload_length,
    int *errno_ptr
) {
    if (ntdv->dtlnk_ops->payload_in_frame) {
        return ntdv->dtlnk_ops->payload_in_frame(ntdv, info, frame_length, frame, payload_length, errno_ptr);
    } else {
        kpanic("Expected netdev::payload_in_frame");
    }
}

bool link_make_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char **payload_out,
    size_t *frame_length_out, char **frame_out,
    int *errno_ptr
) {
    if (ntdv->dtlnk_ops->make_frame) {
        return ntdv->dtlnk_ops->make_frame(ntdv, info, payload_length, payload_out, frame_length_out, frame_out, errno_ptr);
    } else {
        return link_generic_make_frame(ntdv, info, payload_length, payload_out, frame_length_out, frame_out, errno_ptr);
    }
}

size_t link_encode_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char payload[payload_length],
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
) {
    if (ntdv->dtlnk_ops->encode_frame) {
        return ntdv->dtlnk_ops->encode_frame(ntdv, info, payload_length, payload, frame_length, frame, errno_ptr);
    } else {
        kpanic("Expected netdev::encode_frame");
    }
}

bool link_decode_frame(
    netdev *ntdv,
    size_t frame_length, char frame[frame_length],
    link_frame_info *info_out,
    size_t *payload_length_out, char **payload_out,
    int *errno_ptr
) {
    if (ntdv->dtlnk_ops->decode_frame) {
        return ntdv->dtlnk_ops->decode_frame(ntdv, frame_length, frame, info_out, payload_length_out, payload_out, errno_ptr);
    } else {
        kpanic("Expected netdev::decode_frame");
    }
}

bool link_send_encoded_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
) {
    if (ntdv->dtlnk_ops->send_encoded_frame) {
        return ntdv->dtlnk_ops->send_encoded_frame(ntdv, info, frame_length, frame, errno_ptr);
    } else {
        kpanic("Expected netdev::send_encoded_frame");
    }
}

void link_receive_decoded_frame(
    netdev *ntdv,
    size_t frame_length, char frame[frame_length],
    const link_frame_info *info,
    size_t payload_length, char payload[payload_length],
    int *errno_ptr
) {
    internet_type net_type = link_identify_network_type(ntdv, info);
    if (net_type == INTERNET_TYPE_NONE) {
        // No type identified, return early
        kwarn("Network frame received with no identifiable network type");
        return;
    }

    internet_receive_packet(net_type, ntdv, info, payload_length, payload);
}

bool link_send_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char payload[payload_length],
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
) {
    frame_length = link_encode_frame(ntdv, info, payload_length, payload, frame_length, frame, errno_ptr);
    if (!frame_length) {
        return false;
    }

    return link_send_encoded_frame(ntdv, info, frame_length, frame, errno_ptr);
}

void link_receive_frame(
    netdev *ntdv,
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
) {
    link_frame_info info;
    size_t payload_length;
    char *payload;

    if (!link_decode_frame(ntdv, frame_length, frame, &info, &payload_length, &payload, errno_ptr)) {
        return;
    }

    link_receive_decoded_frame(ntdv, frame_length, frame, &info, payload_length, payload, errno_ptr);
}

bool link_poll(
    struct netdev *ntdv,
    int *errno_ptr
) {
    if (ntdv->dtlnk_ops->poll) {
        return ntdv->dtlnk_ops->poll(ntdv, errno_ptr);
    } else {
        return false;
    }
}

internet_type link_identify_network_type(
    struct netdev *ntdv, const link_frame_info *info
) {
    if (ntdv->dtlnk_ops->identify_network_type) {
        return ntdv->dtlnk_ops->identify_network_type(ntdv, info);
    } else {
        return INTERNET_TYPE_NONE;
    }
}

#endif
