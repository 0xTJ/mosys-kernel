#include "network/link/link_generic.h"
#include "kio/klog.h"
#include "uapi/errno.h"

#if NETWORK_LINK_ENABLE

bool link_generic_alloc_raw_frame(
    netdev *ntdv,
    size_t frame_length, char **frame_out,
    int *errno_ptr
) {
    char *frame = kalloc(frame_length);
    if (!frame) {
        *errno_ptr = ENOMEM;
        return false;
    }

    if (frame_out) {
        *frame_out = frame;
    }
    return true;
}

char *link_generic_alloc_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length,
    size_t *frame_length_out,
    int *errno_ptr
) {
    ssize_t frame_length = link_needed_frame_length(ntdv, info, payload_length, errno_ptr);
    if (frame_length < 0) {
        // *errno_ptr set by netdev::needed_frame_length()
        return NULL;
    }

    char *frame;
    if (!link_alloc_raw_frame(ntdv, frame_length, &frame, errno_ptr)) {
        return NULL;
    }

    if (frame_length_out) {
        *frame_length_out = frame_length;
    }
    return frame;
}

void link_generic_dealloc_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
) {
    kfree(frame);
}

bool link_generic_make_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char **payload_out,
    size_t *frame_length_out, char **frame_out,
    int *errno_ptr
) {
    size_t frame_length;
    char *frame = link_alloc_frame(ntdv, info, payload_length, &frame_length, errno_ptr);
    if (!frame) {
        // *errno_ptr set by netdev::alloc_frame()
        return false;
    }

    char *payload = link_payload_in_frame(ntdv, info, frame_length, frame, payload_length, errno_ptr);
    if (!payload) {
        link_dealloc_frame(ntdv, info, frame_length, frame, errno_ptr);
        return false;
    }

    if (payload_out) {
        *payload_out = payload;
    }
    if (frame_length_out) {
        *frame_length_out = frame_length;
    }
    if (frame_out) {
        *frame_out = frame;
    }
    return true;
}

#endif
