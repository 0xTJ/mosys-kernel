#include "network/link/link_slip.h"
#include "network/link/link.h"
#include "network/link/link_generic.h"
#include "device/device.h"
#include "uapi/errno.h"
#include "uapi/limits.h"
#include "kio/klog.h"

#if NETWORK_LINK_SLIP_ENABLE

#define LINK_SLIP_FRAME_LENGTH(payload_length) (((size_t) (payload_length)) * 2 + 2)
#define LINK_SLIP_MAX_FRAME_LENGTH LINK_SLIP_FRAME_LENGTH(LINK_SLIP_MTU)

#define LINK_SLIP_END '\300'
#define LINK_SLIP_ESC '\333'
#define LINK_SLIP_ESC_END '\334'
#define LINK_SLIP_ESC_ESC '\335'

const link_ops link_slip_dtlnk_ops = {
    .needed_frame_length = link_slip_needed_frame_length,
    .alloc_raw_frame = NULL,
    .alloc_frame = NULL,
    .dealloc_frame = NULL,
    .make_frame = NULL,
    .payload_in_frame = link_slip_payload_in_frame,
    .encode_frame = link_slip_encode_frame,
    .decode_frame = link_slip_decode_frame,
    .send_encoded_frame = link_slip_send_encoded_frame,
    .poll = link_slip_poll,
    .identify_network_type = link_slip_identify_network_type,
};

ssize_t link_slip_needed_frame_length(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length,
    int *errno_ptr
) {
    if (payload_length > (SSIZE_MAX - 1) / 2) {
        *errno_ptr = EINVAL;
        return -1;
    }
    return LINK_SLIP_FRAME_LENGTH(payload_length);
}

char *link_slip_payload_in_frame(
    struct netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    size_t payload_length,
    int *errno_ptr
) {
    return frame + payload_length;
}

size_t link_slip_encode_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char payload[payload_length],
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
) {
    if (info->type != LINK_TYPE_SLIP) {
        kpanic("link_frame_info::type mismatch");
    }

    size_t payload_start = payload_length;
    size_t frame_offset = 0;

    // Add initial END to help in removing junk
    frame[frame_offset++] = LINK_SLIP_END;

    for (size_t payload_offset = 0; payload_offset < payload_length; ++payload_offset) {
        if (frame_offset >= frame_length) {
            *errno_ptr = EINVAL;
            return 0;
        }

        switch (payload[payload_offset]) {
        case LINK_SLIP_END:
            frame[frame_offset++] = LINK_SLIP_ESC;
            frame[frame_offset++] = LINK_SLIP_ESC_END;
            break;
        case LINK_SLIP_ESC:
            frame[frame_offset++] = LINK_SLIP_ESC;
            frame[frame_offset++] = LINK_SLIP_ESC_ESC;
            break;
        default:
            frame[frame_offset++] = frame[payload_start + payload_offset];
            break;
        }
    }

    if (frame_offset >= frame_length) {
        *errno_ptr = EINVAL;
        return 0;
    }

    frame[frame_offset++] = LINK_SLIP_END;

    return frame_offset;
}

bool link_slip_decode_frame(
    struct netdev *ntdv,
    size_t frame_length, char frame[frame_length],
    link_frame_info *info_out,
    size_t *payload_length_out, char **payload_out,
    int *errno_ptr
) {
    size_t payload_offset = 0;
    char *payload = frame;
    bool escape_next = false;
    bool ended = false;
    for (size_t frame_offset = 0; frame_offset < frame_length && !ended; ++frame_offset) {
        switch (frame[frame_offset]) {
        case LINK_SLIP_END:
            // Silently accept an END following an ESC
            ended = true;
            break;
        case LINK_SLIP_ESC:
            // Silently accept an ESC following an ESC
            escape_next = true;
            break;
        case LINK_SLIP_ESC_END:
            if (escape_next) {
                payload[payload_offset++] = LINK_SLIP_END;
                escape_next = false;
            } else {
                payload[payload_offset++] = LINK_SLIP_ESC_END;
            }
            break;
        case LINK_SLIP_ESC_ESC:
            if (escape_next) {
                payload[payload_offset++] = LINK_SLIP_ESC;
                escape_next = false;
            } else {
                payload[payload_offset++] = LINK_SLIP_ESC_ESC;
            }
            break;
        default:
            payload[payload_offset++] = frame[frame_offset];
            // Silently accept a character that follows an ESC that isn't ESC_END or ESC_ESC
            escape_next = false;
            break;
        }
    }

    if (ended) {
        info_out->type = LINK_TYPE_SLIP;

        if (payload_length_out) {
            *payload_length_out = payload_offset;
        }
        if (payload_out) {
            *payload_out = payload;
        }
    }
    return ended;
}

// TODO: Use open() and close() equivalents, when added to device

bool link_slip_send_encoded_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
) {
    flex_const frame_flx = flex_const_kernel(frame, frame_length);
    while (frame_length > 0) {
        ssize_t amount_written = device_write(2, 0, frame_flx, frame_length, 0, errno_ptr);
        if (amount_written < 0) {
            return false;
        } else {
            frame_length -= amount_written;
            flex_add(frame_flx, amount_written);
        }
    }

    return true;
}

bool link_slip_poll(
    struct netdev *ntdv,
    int *errno_ptr
) {
    link_slip_priv *priv = ntdv->priv;

    bool found_end = circbuf_char_contains(&priv->cb, LINK_SLIP_END);

    while (!found_end) {
        char tmp_buf[32];
        flex_mut tmp_buf_flx = flex_mut_kernel(tmp_buf, 32);
        ssize_t amount_read = device_read(2, 0, tmp_buf_flx, sizeof(tmp_buf), 0, errno_ptr);
        if (amount_read < 0) {
            return false;
        } else if (amount_read == 0) {
            break;
        }

        for (size_t i = 0; i < (size_t) amount_read; ++i) {
            if (circbuf_char_is_full(&priv->cb)) {
                char tmp;
                circbuf_char_get(&priv->cb, &tmp);
            }

            if (tmp_buf[i] == LINK_SLIP_END) {
                found_end = true;
            }
            circbuf_char_put(&priv->cb, tmp_buf[i]);
        }
    }

    if (found_end) {
        size_t frame_length = LINK_SLIP_MAX_FRAME_LENGTH;
        char *frame;
        if (!link_alloc_raw_frame(ntdv, frame_length, &frame, errno_ptr)) {
            return false;
        }
        frame_length = circbuf_char_dump_until(&priv->cb, frame_length, frame, LINK_SLIP_END);
        // Ignore END-only frames
        if (frame_length > 1) {
            link_receive_frame(ntdv, frame_length, frame, errno_ptr);
        }
    }

    return found_end;
}

internet_type link_slip_identify_network_type(
    struct netdev *ntdv, const link_frame_info *info
) {
    if (info->type != LINK_TYPE_SLIP) {
        kpanic("link_frame_info::type mismatch");
    }

    return INTERNET_TYPE_IPV4;
}

#endif
