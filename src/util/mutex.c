#include "util/mutex.h"
#include "kio/klog.h"
#include "thread/thread.h"

void mutex_init(mutex *mtx) {
    mtx->is_held = false;
    wakelist_init(&mtx->wklst);
}

void mutex_deinit(mutex *mtx) {
    wakelist_deinit(&mtx->wklst);
}

void mutex_lock(mutex *mtx) {
    CRITICAL(WAKELIST_WITH_LOCK(&mtx->wklst, {
        // Try to take
        if (!mtx->is_held) {
            mtx->is_held = true;
            WAKELIST_LEAVE_UNLOCK();
        }

        wakelist_thread_node wakelist_node;
        wakelist_add_self(&mtx->wklst, &wakelist_node);

        do {
            thread_current->state = THREAD_STATE_SLEEPING;
            wakelist_unlock(&mtx->wklst);
            thread_schedule();
            wakelist_lock(&mtx->wklst);
        } while (mtx->is_held);
        mtx->is_held = true;

        wakelist_remove_self(&mtx->wklst);
    }))
}

bool mutex_trylock(mutex *mtx) {
    bool result;

    CRITICAL(WAKELIST_WITH_LOCK(&mtx->wklst, {
        if (!mtx->is_held) {
            mtx->is_held = true;
            result = true;
        } else {
            result = false;
        }
    }))

    return result;
}

void mutex_unlock(mutex *mtx) {
    CRITICAL(WAKELIST_WITH_LOCK(&mtx->wklst, {
        kassert(mtx->is_held);
        mtx->is_held = false;
        wakelist_wake_one(&mtx->wklst);
    }))
}
