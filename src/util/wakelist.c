#include "util/wakelist.h"
#include "kalloc.h"
#include "kio/klog.h"

void wakelist_init(wakelist *wklst) {
    wklst->spnlck = (spinlock) SPINLOCK_INIT;
    wklst->list = NULL;
}

void wakelist_deinit(wakelist *wklst) {
    spinlock_lock(&wklst->spnlck);
    kassert(!wklst->list);
}

bool wakelist_wake_one(wakelist *wklst) {
    bool woken = false;

    wakelist_thread_node *itr;
    for (itr = wklst->list; itr && !woken; itr = itr->next) {
        thread *ptr = itr->ptr;
        woken |= thread_wake(ptr);
    }

    return woken;
}

size_t wakelist_wake_all(wakelist *wklst) {
    size_t woken_count = 0;

    wakelist_thread_node *itr;
    for (itr = wklst->list; itr; itr = itr->next) {
        thread *ptr = itr->ptr;
        woken_count += thread_wake(ptr);
    }

    return woken_count;
}

void wakelist_add_self(wakelist *wklst, wakelist_thread_node *node) {
    node->ptr = thread_current;
    node->next = NULL;

    wakelist_thread_node **itr_ptr;
    for (itr_ptr = &wklst->list; *itr_ptr; itr_ptr = &(*itr_ptr)->next) {
        // Advance through list
    }

    *itr_ptr = node;
}

void wakelist_remove_self(wakelist *wklst) {
    wakelist_thread_node **itr_ptr;
    for (itr_ptr = &wklst->list; *itr_ptr; itr_ptr = &(*itr_ptr)->next) {
        wakelist_thread_node *itr = *itr_ptr;
        if (itr->ptr == thread_current) {
            *itr_ptr = itr->next;
            itr->next = NULL;
        }

        // End if we advanced the iterator to the end when removing
        if (!*itr_ptr) {
            break;
        }
    }
}
