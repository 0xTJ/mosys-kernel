#include "util/circbuf.h"

// Passing a size of 0 is invalid
static inline size_t next(size_t value, size_t size) {
    value += 1;
    if (value >= size)
        value = 0;
    return value;
}

// Passing a size of 0 is invalid
static inline size_t prev(size_t value, size_t size) {
    if (value == 0) {
        value = size;
    }
    value -= 1;
    return value;
}

#define next_head(crcbf) (next((crcbf)->head, (crcbf)->size))
#define next_tail(crcbf) (next((crcbf)->tail, (crcbf)->size))
#define prev_head(crcbf) (prev((crcbf)->head, (crcbf)->size))
#define prev_tail(crcbf) (prev((crcbf)->tail, (crcbf)->size))

#define CIRCBUF_SOURCE_TEMPLATE(type)                                                               \
inline bool circbuf_ ## type ## _is_empty(const circbuf_ ## type *crcbf) {                          \
    return crcbf->tail == crcbf->head;                                                              \
}                                                                                                   \
                                                                                                    \
inline bool circbuf_ ## type ## _is_full(const circbuf_ ## type *crcbf) {                           \
    return next_tail(crcbf) == crcbf->head;                                                         \
}                                                                                                   \
                                                                                                    \
inline bool circbuf_ ## type ## _is_almost_full(const circbuf_ ## type *crcbf) {                    \
    return circbuf_ ## type ## _is_full(crcbf) || next_tail(crcbf) == prev_head(crcbf);             \
}                                                                                                   \
                                                                                                    \
bool circbuf_ ## type ## _put(circbuf_ ## type *crcbf, type value) {                                \
    if (!circbuf_ ## type ## _is_full(crcbf)) {                                                     \
        crcbf->buf[crcbf->tail] = value;                                                            \
        crcbf->tail = next_tail(crcbf);                                                             \
        return true;                                                                                \
    } else {                                                                                        \
        return false;                                                                               \
    }                                                                                               \
}                                                                                                   \
                                                                                                    \
bool circbuf_ ## type ## _get(circbuf_ ## type *crcbf, type *value_p) {                             \
    if (!circbuf_ ## type ## _is_empty(crcbf)) {                                                    \
        *value_p = crcbf->buf[crcbf->head];                                                         \
        crcbf->head = next_head(crcbf);                                                             \
        return true;                                                                                \
    } else {                                                                                        \
        return false;                                                                               \
    }                                                                                               \
}                                                                                                   \
                                                                                                    \
size_t circbuf_ ## type ## _dump(circbuf_ ## type *crcbf, size_t n, type a[n]) {                    \
    size_t i;                                                                                       \
    for (i = 0; i < n; ++i) {                                                                       \
        if (!circbuf_ ## type ## _get(crcbf, &a[i])) {                                              \
            break;                                                                                  \
        }                                                                                           \
    }                                                                                               \
    return i;                                                                                       \
}                                                                                                   \
                                                                                                    \
size_t circbuf_ ## type ## _dump_until(circbuf_ ## type *crcbf, size_t n, type a[n], type term) {   \
    size_t i;                                                                                       \
    bool found_term = false;                                                                        \
    for (i = 0; i < n && !found_term; ++i) {                                                        \
        if (!circbuf_ ## type ## _get(crcbf, &a[i])) {                                              \
            break;                                                                                  \
        }                                                                                           \
        if (a[i] == term) {                                                                         \
            found_term = true;                                                                      \
        }                                                                                           \
    }                                                                                               \
    return i;                                                                                       \
}                                                                                                   \
                                                                                                    \
bool circbuf_ ## type ## _contains(circbuf_ ## type *crcbf, type val) {                             \
    for (size_t i = crcbf->head; i != crcbf->tail; i = next(i, crcbf->size)) {                      \
        if (crcbf->buf[i] == val) {                                                                 \
            return true;                                                                            \
        }                                                                                           \
    }                                                                                               \
    return false;                                                                                   \
}                                                                                                   \
                                                                                                    \
bool circbuf_ ## type ## _remove_last(circbuf_ ## type *crcbf) {                                    \
    if (!circbuf_ ## type ## _is_empty(crcbf)) {                                                    \
        crcbf->tail = prev_tail(crcbf);                                                             \
        return true;                                                                                \
    } else {                                                                                        \
        return false;                                                                               \
    }                                                                                               \
}                                                                                                   \
                                                                                                    \
bool circbuf_ ## type ## _remove_all(circbuf_ ## type *crcbf) {                                     \
    if (!circbuf_ ## type ## _is_empty(crcbf)) {                                                    \
        crcbf->head = 0;                                                                            \
        crcbf->tail = 0;                                                                            \
        return true;                                                                                \
    } else {                                                                                        \
        return false;                                                                               \
    }                                                                                               \
}

CIRCBUF_SOURCE_TEMPLATE(char)
CIRCBUF_SOURCE_TEMPLATE(uint8_t)
