#include "util/hashtable.h"
#include "config.h"
#include "kalloc.h"
#include "kio/klog.h"
#include "util/util.h"
#include <stdbool.h>

#define HASHTABLE_INDEX_ADVANCE(hshtbl, index) (((index) + 1) & hshtbl->table_index_mask)

// Special value used to mark unused entries in hashtable
static const char tombstone_obj;
#define TOMBSTONE ((void *) &tombstone_obj)

static bool hashtable_insert_raw(hashtable *hshtbl, uint64_t key, void *value);
static hashtable_entry *hashtable_lookup_entry(hashtable *hshtbl, uint64_t key);

static inline bool is_power_of_two(size_t val) {
    return (val != 0) && ((val & (val - 1)) == 0);
}

static inline bool hashtable_entry_is_unused(hashtable_entry *entry) {
    return (entry->value == NULL) || (entry->value == TOMBSTONE);
}

static uint64_t hashtable_hash_key(uint64_t key) {
    key =   ((key >> 1) & 0x5555555555555555ULL) |
            ((key << 1) & 0xAAAAAAAAAAAAAAAAULL);
    key =   ((key >> 2) & 0x3333333333333333ULL) |
            ((key << 2) & 0xCCCCCCCCCCCCCCCCULL);
    key =   ((key >> 4) & 0x0F0F0F0F0F0F0F0FULL) |
            ((key << 4) & 0xF0F0F0F0F0F0F0F0ULL);
    return key;
}

static bool hashtable_insert_raw(hashtable *hshtbl, uint64_t key, void *value) {
    const uint64_t hash = hashtable_hash_key(key);
    size_t index = hash & (hshtbl->table_len - 1);
    const size_t stop_index = index;

    do {
        hashtable_entry *this_entry = &hshtbl->table[index];
        if (hashtable_entry_is_unused(this_entry) || this_entry->key == key) {
            // Can insert/replace here
            this_entry->key = key;
            this_entry->value = value;
            return true;
        }

        index = HASHTABLE_INDEX_ADVANCE(hshtbl, index);
    } while (index != stop_index);

    // Advanced back to the original index, could not insert value
    return false;
}

static hashtable_entry *hashtable_lookup_entry(hashtable *hshtbl, uint64_t key) {
    if (hshtbl->table_len == 0) {
        return NULL;
    }

    const uint64_t hash = hashtable_hash_key(key);
    size_t index = hash & (hshtbl->table_len - 1);
    const size_t stop_index = index;

    do {
        hashtable_entry *this_entry = &hshtbl->table[index];
        if (!this_entry->value) {
            // End of chain, key not found
            return NULL;
        }

        if (this_entry->value != TOMBSTONE && this_entry->key == key) {
            return this_entry;
        }

        index = HASHTABLE_INDEX_ADVANCE(hshtbl, index);
    } while (index != stop_index);

    // Advanced back to the original index, key not found
    return NULL;
}

bool hashtable_insert(hashtable *hshtbl, uint64_t key, void *value) {
    if (hshtbl->table_len == 0) {
        hashtable_resize(hshtbl, HASHTABLE_DEFAULT_SIZE);
    }

    // Try to overwrite existing entry
    hashtable_entry *existing_entry = hashtable_lookup_entry(hshtbl, key);
    if (existing_entry) {
        existing_entry->value = value;
        return true;
    }

    while (!hashtable_insert_raw(hshtbl, key, value)) {
        if (!hashtable_resize(hshtbl, hshtbl->table_len *= 2)) {
            return false;
        }
    }

    return true;
}

void *hashtable_remove(hashtable *hshtbl, uint64_t key) {
    hashtable_entry *found_entry = hashtable_lookup_entry(hshtbl, key);
    if (!found_entry) {
        return NULL;
    }

    void *found_value = found_entry->value;
    found_entry->value = TOMBSTONE;
    return found_value;
}

void *hashtable_lookup(hashtable *hshtbl, uint64_t key) {
    hashtable_entry *found_entry = hashtable_lookup_entry(hshtbl, key);
    if (!found_entry) {
        return NULL;
    }

    void *found_value = found_entry->value;
    return found_value;
}

bool hashtable_resize(hashtable *hshtbl, size_t count) {
    kassert(is_power_of_two(count));
    count = min(count, UINT64_MAX);

    hashtable tmp_hshtbl;
    memcpy(&tmp_hshtbl, hshtbl, sizeof tmp_hshtbl);

    tmp_hshtbl.table = kalloc(count * (sizeof *tmp_hshtbl.table));
    if (!tmp_hshtbl.table) {
        return false;
    }
    tmp_hshtbl.table_len = count;
    tmp_hshtbl.table_index_mask = count - 1;

    if (hshtbl->table) {
        for (size_t i = 0; i < hshtbl->table_len; ++i) {
            hashtable_entry *this_entry = &hshtbl->table[i];
            if (hashtable_entry_is_unused(this_entry)) {
                continue;
            }

            if (!hashtable_insert_raw(&tmp_hshtbl, this_entry->key, this_entry->value)) {
                kfree(tmp_hshtbl.table);
                return false;
            }
        }

        // Free backing memory for original table
        kfree(hshtbl->table);
    }

    // Overwrite target table data
    memcpy(hshtbl, &tmp_hshtbl, sizeof *hshtbl);
    return true;
}
