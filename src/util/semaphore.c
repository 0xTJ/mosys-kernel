#include "util/semaphore.h"
#include "thread/thread.h"

void semaphore_init(semaphore *smphr, semaphore_value value) {
    smphr->value = value;
    wakelist_init(&smphr->wklst);
}

void semaphore_deinit(semaphore *smphr) {
    wakelist_deinit(&smphr->wklst);
}

void semaphore_wait(semaphore *smphr) {
    CRITICAL(WAKELIST_WITH_LOCK(&smphr->wklst, {
        // Try to decrement
        if (smphr->value >= 1) {
            smphr->value -= 1;
            WAKELIST_LEAVE_UNLOCK();
        }

        wakelist_thread_node wakelist_node;
        wakelist_add_self(&smphr->wklst, &wakelist_node);

        do {
            thread_current->state = THREAD_STATE_SLEEPING;
            wakelist_unlock(&smphr->wklst);
            thread_schedule();
            wakelist_lock(&smphr->wklst);
        } while (smphr->value < 1);
        smphr->value -= 1;

        wakelist_remove_self(&smphr->wklst);
    }))
}

bool semaphore_trywait(semaphore *smphr) {
    bool result;

    CRITICAL(WAKELIST_WITH_LOCK(&smphr->wklst, {
        if (smphr->value >= 1) {
            smphr->value -= 1;
            result = true;
        } else {
            result = false;
        }
    }))

    return result;
}

void semaphore_signal(semaphore *smphr) {
    CRITICAL(WAKELIST_WITH_LOCK(&smphr->wklst, {
        smphr->value += 1;
        wakelist_wake_one(&smphr->wklst);
    }))
}
