#include "file/tar.h"
#include "kio/klog.h"
#include <stdint.h>
#include <string.h>

// len == -1 means no length limit
unsigned long tar_oct_atoi(const char *str, ssize_t len) {
    unsigned long val = 0;
    size_t chars_left = (len == -1) ? SIZE_MAX : (size_t) len;
    while (chars_left > 0 && (*str) != '\0' && (*str) != ' ') {
        if ((*str) < '0' || (*str) > '7') {
            // TODO: Don't panic, this is a user error, not a system one
            kpanic("Unexpected character encountered while parsing tar numeric field");
        }
        val *= 8;
        val += *str - '0';
        str += 1;
        chars_left -= 1;
    }
    return val;
}

bool tar_header_is_empty(struct tar_entry *ptr) {
    return tar_oct_atoi(ptr->header.chksum, sizeof(ptr->header.chksum) - 1) == 0;
}

bool tar_header_is_end(struct tar_entry *ptr) {
    return tar_header_is_empty(ptr) && tar_header_is_empty(tar_get_next_entry_nocheck(ptr));
}

struct tar_entry *tar_get_next_entry_nocheck(struct tar_entry *ptr) {
    unsigned long size = tar_oct_atoi(ptr->header.size, sizeof(ptr->header.size) - 1);

    if (size % 512 != 0)
        size += (512 - (size % 512));

    return (struct tar_entry *) ((char *) ptr + 512 + size);
}

struct tar_entry *tar_get_next_entry(struct tar_entry *ptr) {
    if (!ptr)
        return NULL;

    if (tar_header_is_end(ptr))
        return NULL;

    struct tar_entry *found_entry = tar_get_next_entry_nocheck(ptr);
    if (tar_header_is_end(found_entry))
        return NULL;

    return found_entry;
}

struct tar_entry *tar_get_nth_entry(struct tar_entry *ptr, size_t n) {
    if (!ptr)
        return NULL;

    if (n == 0) {
        return ptr;
    } else {
        return tar_get_nth_entry(tar_get_next_entry(ptr), n - 1);
    }
}

struct tar_entry *tar_lookup_name(struct tar_entry *ptr, const char *name, size_t *result_index_ptr) {
    size_t result_index = 0;
    while (ptr) {
        // TODO: Use strncmp when it's working in rosco_m68k
        // if (strncmp(name, ptr->header.name, sizeof(ptr->header.name) - 1) == 0) {
        if (strcmp(name, ptr->header.name) == 0) {
            break;
        } else {
            ptr = tar_get_next_entry(ptr);
        }

        result_index += 1;
    }

    if (ptr && result_index_ptr) {
        *result_index_ptr = result_index;
    }
    return ptr;
}
