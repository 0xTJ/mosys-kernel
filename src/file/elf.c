#include "file/elf.h"
#include "mem/flex.h"
#include "kio/klog.h"
#include "vfs/vfs.h"
#include "util/util.h"
#include "uapi/errno.h"
#include <string.h>

// TODO: This is a Frankenstein combination of a few different tutorials/references for ELF parsing. Rewrite it.
// TODO: This isn't MMU-compatible, we directly access physical addresses

#define ELF_RELOC_ERR (-1)

static inline Elf32_Shdr *elf_sheader(Elf32_Ehdr *ehdr);
static inline Elf32_Phdr *elf_pheader(Elf32_Ehdr *ehdr);
static inline Elf32_Shdr *elf_section(Elf32_Ehdr *ehdr, size_t idx);
static inline char *elf_str_table(Elf32_Ehdr *ehdr);
static inline char *elf_lookup_string(Elf32_Ehdr *ehdr, size_t offset);
static inline void *elf_lookup_symbol(const char *name);
int elf_get_symval(Elf32_Ehdr *ehdr, int table, size_t idx);
bool elf_load(int fd, mem_space *space, Elf32_Ehdr *ehdr, size_t phnum, Elf32_Phdr unloaded_phdr[phnum], umem_ptrdiff *load_offset_out, umem_ptr *phdr_vaddr_out, int *errno_ptr);
void elf_dynamic(mem_space *space, Elf32_Phdr *dynamic_phdr, umem_ptr load_offset);
bool elf_program_headers(mem_space *space, Elf32_Ehdr *ehdr, size_t phnum, Elf32_Phdr phdr[phnum], umem_ptr load_offset, int *errno_ptr);
static inline bool elf_load_exec(int fd, mem_space *space, Elf32_Ehdr *ehdr, umem_ptrdiff *load_offset_out, int *errno_ptr);

static inline Elf32_Shdr *elf_sheader(Elf32_Ehdr *ehdr) {
    return (Elf32_Shdr *) ((char *) ehdr + ehdr->e_shoff);
}

static inline Elf32_Phdr *elf_pheader(Elf32_Ehdr *ehdr) {
    return (Elf32_Phdr *) ((char *) ehdr + ehdr->e_phoff);
}

static inline Elf32_Shdr *elf_section(Elf32_Ehdr *ehdr, size_t idx) {
    return &elf_sheader(ehdr)[idx];
}

static inline char *elf_str_table(Elf32_Ehdr *ehdr) {
    if (ehdr->e_shstrndx == SHN_UNDEF) {
        return NULL;
    }
    return (char *) ehdr + elf_section(ehdr, ehdr->e_shstrndx)->sh_offset;
}

static inline char *elf_lookup_string(Elf32_Ehdr *ehdr, size_t offset) {
    char *strtab = elf_str_table(ehdr);
    if (strtab == NULL) {
        return NULL;
    }
    return strtab + offset;
}

static inline void *elf_lookup_symbol(const char *name) {
    // We don't handle symbols yet
    UNUSED(name);
    return NULL;
}

int elf_get_symval(Elf32_Ehdr *ehdr, int table, size_t idx) {
    if (table == SHN_UNDEF || idx == SHN_UNDEF)
        return 0;
    Elf32_Shdr *symtab = elf_section(ehdr, table);

    uint32_t symtab_entries = symtab->sh_size / symtab->sh_entsize;
    if (idx >= symtab_entries) {
        // TODO: Direct this message somewhere more useful
        kwarn("Symbol index is out of range (%d:%u)", table, (unsigned) idx);
        return ELF_RELOC_ERR;
    }

    int symaddr = (int) ehdr + symtab->sh_offset;
    Elf32_Sym *symbol = &((Elf32_Sym *)symaddr)[idx];

    if(symbol->st_shndx == SHN_UNDEF) {
        // External symbol, lookup value
        Elf32_Shdr *strtab = elf_section(ehdr, symtab->sh_link);
        const char *name = (const char *)ehdr + strtab->sh_offset + symbol->st_name;

        void *target = elf_lookup_symbol(name);

        if(target == NULL) {
            // Extern symbol not found
            if(ELF32_ST_BIND(symbol->st_info) & STB_WEAK) {
                // Weak symbol initialized as 0
                return 0;
            } else {
                // TODO: Direct this message somewhere more useful
                kwarn("Undefined external symbol : %s", name);
                return ELF_RELOC_ERR;
            }
        } else {
            return (int)target;
        }
    } else if(symbol->st_shndx == SHN_ABS) {
        // Absolute symbol
        return symbol->st_value;
    } else {
        // Internally defined symbol
        Elf32_Shdr *target = elf_section(ehdr, symbol->st_shndx);
        return (int)ehdr + symbol->st_value + target->sh_offset;
    }
}

// TODO: Check that everything read is within the ELF file
bool elf_load(int fd, mem_space *space, Elf32_Ehdr *ehdr, size_t phnum, Elf32_Phdr unloaded_phdr[phnum], umem_ptrdiff *load_offset_out, umem_ptr *phdr_vaddr_out, int *errno_ptr) {
    // Get PT_PHDR for later
    // TODO: Can this be included in the loop below?
    umem_ptr phdr_vaddr = UMEM_NULL;
    for (size_t i = 0; i < ehdr->e_phnum; ++i) {
        if (unloaded_phdr[i].p_type == PT_PHDR) {
            phdr_vaddr = unloaded_phdr[i].p_vaddr;
            break;
        }
    }

    // Find the first and last PT_LOAD segments
    Elf32_Phdr *first_load = NULL;
    Elf32_Phdr *last_load = NULL;
    for (size_t i = 0; i < ehdr->e_phnum; ++i) {
        if (unloaded_phdr[i].p_type == PT_LOAD) {
            if (!first_load) {
                first_load = &unloaded_phdr[i];
            }
            last_load = &unloaded_phdr[i];
        }
    }
    if (!first_load) {
        *errno_ptr = ENOEXEC;
        return false;
    }

    // Validate PT_LOAD segments
    // ELF PT_LOAD segments must be in ascending order
    if (first_load->p_vaddr > last_load->p_vaddr) {
        *errno_ptr = ENOEXEC;
        return false;
    }

    // Create a single memory block for the entire load area, becasue of no MMU
    umem_ptr load_vaddr_start = round_down(first_load->p_vaddr, first_load->p_align);
    umem_ptr load_vaddr_end = round_up(last_load->p_vaddr + last_load->p_memsz, last_load->p_align);
    size_t load_size = load_vaddr_end - load_vaddr_start;
    mem_mapping *load = mem_mapping_new(load_size);
    if (!load) {
        *errno_ptr = ENOMEM;
        return false;
    }
    flex_mut load_flx = flex_mut_kernel(mem_mapping_k_at(load), load_size);
    flex_set(load_flx, 0, load_size);

    // Copy all PT_LOAD segments
    for (size_t i = 0; i < ehdr->e_phnum; ++i) {
        if (unloaded_phdr[i].p_type == PT_LOAD) {
            if (unloaded_phdr[i].p_vaddr >= load_vaddr_start && unloaded_phdr[i].p_vaddr + unloaded_phdr[i].p_memsz < load_vaddr_end) {
                size_t offset = unloaded_phdr[i].p_vaddr - load_vaddr_start;
                // TODO: Handle result
                vfs_pread(fd, flex_add(load_flx, offset), unloaded_phdr[i].p_filesz, unloaded_phdr[i].p_offset, errno_ptr);
            } else {
                kwarn("ELF file contained out-of-order PT_LOAD segments");
                // TODO: Maybe error on this?
            }
        }
    }

    umem_ptrdiff load_offset = mem_mapping_u_at(load) - load_vaddr_start;

    mem_space_append(space, load);
    *load_offset_out = load_offset;
    if (phdr_vaddr) *phdr_vaddr_out = phdr_vaddr + load_offset;

    return true;
}

// TODO: Check that everything is within the program load space
void elf_dynamic(mem_space *space, Elf32_Phdr *dynamic_phdr, umem_ptr load_offset) {
    Elf32_Dyn *dynamic = mem_space_u_to_k_sized(space, dynamic_phdr->p_vaddr + load_offset, dynamic_phdr->p_filesz);
    if (!dynamic) {
        // TODO: Do what follows?
        // *errno_ptr = ENOEXEC;
        // return false;
        return;
    }

    Elf32_Rela *rela = NULL;
    size_t relasz = 0;
    size_t relaent = 0;

    const size_t dynamic_count = dynamic_phdr->p_filesz / sizeof(Elf32_Dyn);

    for (size_t i = 0; i < dynamic_count; ++i) {
        switch (dynamic[i].d_tag) {
            case DT_NULL:
                // Finish looping
                i = dynamic_count - 1;
                break;
            case DT_RELA:
                rela = mem_space_u_to_k_sized(space, dynamic[i].d_un.d_ptr + load_offset, sizeof(*rela));
                break;
            case DT_RELASZ:
                relasz = dynamic[i].d_un.d_val;
                break;
            case DT_RELAENT:
                relaent = dynamic[i].d_un.d_val;
                break;
        }
    }

    if (rela && relasz && relaent) {
        size_t relacount = relasz / relaent;
        for (size_t i = 0; i < relacount; ++i) {
            switch (ELF32_R_TYPE(rela[i].r_info)) {
            case R_68K_32:
                // TODO: Figure out if something needs to be done with these
                break;
            case R_68K_RELATIVE: {
                uint32_t *location = mem_space_u_to_k_sized(space, rela[i].r_offset + load_offset, sizeof(*location));
                // TODO: Handle failure of this (bad ELF)
                uint32_t new_value = rela[i].r_addend + load_offset;
                *location = new_value;
                break;
            }
            default:
                kwarn("Unhandled relocation type 0x%hX", ELF32_R_TYPE(rela[i].r_info));
            }
        }
    }
}

bool elf_program_headers(mem_space *space, Elf32_Ehdr *ehdr, size_t phnum, Elf32_Phdr phdr[phnum], umem_ptr load_offset, int *errno_ptr) {
    UNUSED(ehdr);
    UNUSED(errno_ptr);

    for (size_t i = 0; i < phnum; ++i) {
        if (phdr[i].p_type == PT_DYNAMIC) {
            elf_dynamic(space, &phdr[i], load_offset);
            break; // Only do first PT_DYNAMIC if multiple are present
        }
    }

    return true;
}

static inline bool elf_load_exec(int fd, mem_space *space, Elf32_Ehdr *ehdr, umem_ptrdiff *load_offset_out, int *errno_ptr) {
    umem_ptrdiff load_offset;           // Offset from file address to loaded address
    umem_ptr phdr_vaddr = UMEM_NULL;    // Virtual address of the program header table, if any

    // If I'm doing this kind of thing, this should be its own function
    {
        // TODO: This leads to a stack overflow attack
        // Load in the array of Phdr
        Elf32_Phdr unloaded_phdr[ehdr->e_phnum];
        flex_mut unloaded_phdr_flx = flex_mut_kernel(&unloaded_phdr, sizeof(unloaded_phdr));
        ssize_t pread_result = vfs_pread(fd, unloaded_phdr_flx, sizeof(unloaded_phdr), ehdr->e_phoff, errno_ptr);
        if (pread_result < 0) {
            return false;
        } else if ((size_t) pread_result < sizeof(unloaded_phdr)) {
            *errno_ptr = ENOEXEC;
            return false;
        }

        // Load ELF to program memory
        // After this, switch from using file offset to vaddr
        if (!elf_load(fd, space, ehdr, ehdr->e_phnum, unloaded_phdr, &load_offset, &phdr_vaddr, errno_ptr)) {
            return false;
        }
    }

    // Do work that would normally be done in user-space
    if (phdr_vaddr) {
        Elf32_Phdr *loaded_phdr = mem_space_u_to_k_sized(space, phdr_vaddr, ehdr->e_phnum * sizeof(Elf32_Phdr));
        if (!loaded_phdr) {
            *errno_ptr = ENOEXEC;
            return false;
        }
        if (!elf_program_headers(space, ehdr, ehdr->e_phnum, loaded_phdr, load_offset, errno_ptr)) {
            return false;
        }
    }

    // kinfo("Program loaded with load_offset %p, load_vaddr %p", load_offset, load_vaddr);
    *load_offset_out = load_offset;

    return true;
}

bool elf_load_file(int fd, mem_space *space, umem_ptr *entry, int *errno_ptr) {
    umem_ptrdiff load_offset;   // Offset from file address to loaded address

    Elf32_Ehdr ehdr;
    flex_mut ehdr_flx = flex_mut_kernel(&ehdr, sizeof(ehdr));
    ssize_t pread_result = vfs_pread(fd, ehdr_flx, sizeof(ehdr), 0, errno_ptr);
    if (pread_result < 0) {
        return false;
    } else if ((size_t) pread_result < sizeof(ehdr)) {
        *errno_ptr = ENOEXEC;
        return false;
    }

    // if(!elf_check_supported(ehdr)) {
    //     kwarn("ELF File cannot be loaded.");
    //     return NULL;
    // }
    switch(ehdr.e_type) {
        case ET_EXEC:
        case ET_REL:
        case ET_DYN: {
            bool result = elf_load_exec(fd, space, &ehdr, &load_offset, errno_ptr);
            if (result) {
                *entry = ehdr.e_entry + load_offset;
            }
            return result;
        }
        default:
            kwarn("Unknown ehdr->e_type: %u", (unsigned) ehdr.e_type);
            return false;
    }
}
