#include "config.h"
#include "device/console.h"
#include "exception.h"
#include "mem/flex.h"
#include "vfs/initrd.h"
#include "kernel/module.h"
#include "kio/kio.h"
#include "kio/klog.h"
#include "mem/mem_mapping.h"
#include "process/process.h"
#include "ram_expansion.h"
#include "syscall.h"
#include "mem/mem.h"
#include "vfs/vfs.h"

int mount_wrapped(const char *source, const char *target, const char *filesystemtype,
                  unsigned long mountflags, const void *data,
                  int *errno_ptr) {
    flex_const_str source_flx = flex_const_kernel(source, strlen(source) + 1);
    flex_const_str target_flx = flex_const_kernel(target, strlen(target) + 1);
    flex_const_str filesystemtype_flx = flex_const_kernel(filesystemtype, strlen(filesystemtype) + 1);
    flex_const data_flx = flex_const_kernel(data, FLEX_INF);
    return vfs_mount(source_flx, target_flx, filesystemtype_flx,
                     mountflags, data_flx,
                     errno_ptr);
}

void kmain() {
    kio_printf("Starting Mosys %u.%u.%u%s\n",
        (unsigned) INFO_VERSION_MAJOR,
        (unsigned) INFO_VERSION_MINOR,
        (unsigned) INFO_VERSION_PATCH,
        INFO_VERSION_IS_RELEASE ? "" : "-WIP");

    exception_init();
    syscall_init();

    ram_expansion_scan();   // Must occur before any mem or kalloc calls
    mem_init();

    process_init();
    thread_init();
    process_give_thread(process_get_sched(), thread_current);
    kinfo("Kernel scheduler process started");

    module_init_static();

    if (console_init(CONSOLE_DEVICE_NAME) || console_init(CONSOLE_BACKUP_DEVICE_NAME)) {
        // Succeeded with creating console
    } else {
        kpanic("Failed to initialize console on device \"%s\" or device \"%s\"", CONSOLE_DEVICE_NAME, CONSOLE_BACKUP_DEVICE_NAME);
    }
    kio_set_use_console(true);

    size_t initrd_size;
    const unsigned char *initrd = initrd_get(&initrd_size);

    // Dummy errno
    int errno;

    // Mount initial ramfs, device fs, and socket fs
    if (mount_wrapped("", "/",      "initramfs",    0, initrd,  &errno) < 0)
        kpanic("Failed to mount initramfs: errno = %d", -errno);

    process_exec_init();
    thread_idle_loop();
}
