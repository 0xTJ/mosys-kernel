#include "file/cpio.h"
#include "device/device.h"
#include "vfs/devreg.h"
#include "vfs/fdesc.h"
#include "kalloc.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "util/hashtable.h"
#include "util/util.h"
#include "vfs/superblock.h"
#include "vfs/vfs.h"
#include <assert.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <string.h>

#define INITRAMFS_ROOT_INODE ((ino_t) -1)

static module_status initramfs_init(void);
static void initramfs_deinit(void);

static vnode *initramfs_fsops_new_vnode(superblock *sb, int *errno_ptr);
static void initramfs_fsops_cleanup_vnode(superblock *sb, vnode *vn);
static vnode *initramfs_fsops_mount(flex_const_str source,
                                    unsigned long mountflags, flex_const data,
                                    int *errno_ptr);
static vnode *initramfs_fsops_lookup(vnode *sb, const char *subpath, int *errno_ptr);

static ssize_t initramfs_fops_pread(fdesc *fdsc, flex_mut buf, size_t count, off_t offset, int *errno_ptr);
static ssize_t initramfs_fops_getdents(fdesc *fdsc, FLEX_MUT(dirent) drnt, size_t count, int *errno_ptr);

static atomic_bool initramfs_is_init = false;

static module initramfs = {
    .init = initramfs_init,
    .deinit = initramfs_deinit,
    .name = "initramfs",
};
MODULE_DEFINE(initramfs);

static fs_operations initramfs_fsops = {
    .new_vnode = initramfs_fsops_new_vnode,
    .cleanup_vnode = initramfs_fsops_cleanup_vnode,
    .mount = initramfs_fsops_mount,
    .lookup = initramfs_fsops_lookup,
};

static file_operations initramfs_fops = {
    .pread = initramfs_fops_pread,
    .getdents = initramfs_fops_getdents,
};

static filesystem_type initramfs_fstype = {
    .name = "initramfs",
    .fsops = &initramfs_fsops,
};

static module_status initramfs_init(void) {
    vfs_filesystem_type_register(&initramfs_fstype);

    initramfs_is_init = true;
    return MODULE_LOADED;
}

static void initramfs_deinit(void) {
    initramfs_is_init = false;

    // TODO: Figure out what to actually do here
}

static vnode *initramfs_fsops_new_vnode(superblock *sb, int *errno_ptr) {
    vfs_superblock_ref(sb);
    vnode *vn = vfs_vnode_new(sb);
    if (vn) {
        MUTEX_WITH(&vn->dnobj.mtx, {
            vn->fops = &initramfs_fops;
        })
    } else {
        *errno_ptr = ENOMEM;
        vfs_superblock_unref(sb);
    }
    return vn;
}

static void initramfs_fsops_cleanup_vnode(superblock *sb, vnode *vn) {
    assert(vn->sb == sb);

    MUTEX_WITH(&vn->sb->vn_hshtbl_mtx, {
        hashtable_remove(&vn->sb->vn_hshtbl, (uint64_t) vn->inode);
    })

    vfs_superblock_unref(vn->sb);
    vn->sb = NULL;
}

// Data should be a pointer to a static cpio_odc blob
static vnode *initramfs_fsops_mount(flex_const_str source,
                                    unsigned long mountflags, flex_const data,
                                    int *errno_ptr) {
    UNUSED(source);
    UNUSED(mountflags);

    superblock *sb = vfs_superblock_new(&initramfs_fstype);
    if (!sb) {
        *errno_ptr = ENOMEM;
        return NULL;
    }

    // Only the kernel can mount this type of fs
    if (data.type != FLEX_TYPE_KERNEL) {
        *errno_ptr = ENODEV;
        return NULL;
    }

    vnode *root_vn = sb->type->fsops->new_vnode(sb, errno_ptr);
    vfs_superblock_unref(sb);
    if (!root_vn) {
        *errno_ptr = ENOMEM;
        return NULL;
    }

    MUTEX_WITH(&root_vn->dnobj.mtx, {
        root_vn->type = FILE_TYPE_DIRECTORY;
        root_vn->inode = INITRAMFS_ROOT_INODE;
        root_vn->fops = &initramfs_fops;
    })

    bool inserted_vnode;
    MUTEX_WITH(&sb->vn_hshtbl_mtx, {
        inserted_vnode = hashtable_insert(&sb->vn_hshtbl, (uint64_t) INITRAMFS_ROOT_INODE, root_vn);
    })
    if (!inserted_vnode) {
        vfs_vnode_unref(root_vn);
        *errno_ptr = ENOMEM;
        return NULL;
    }

    MUTEX_WITH(&root_vn->sb->dnobj.mtx, {
        vfs_vnode_ref(root_vn);
        sb->mounted = root_vn;
        // TODO: No cheating casts
        sb->sb_info = (void *) data.ptr.kernel;
    })

    return root_vn;
}

static vnode *initramfs_fsops_lookup(vnode *base_vn, const char *subpath, int *errno_ptr) {
    // TODO: base_vn should be locked before accesses

    char *base_entry_name;
    size_t base_entry_namesize;
    size_t base_entry_add_size;
    if (base_vn->inode == INITRAMFS_ROOT_INODE) {
        base_entry_name = "";
        base_entry_namesize = 0;
        base_entry_add_size = 0;
    } else {
        struct cpio_odc_header *base_entry = cpio_odc_get_nth_entry(base_vn->sb->sb_info, base_vn->inode);
        base_entry_name = cpio_odc_get_name(base_entry);
        base_entry_namesize = strnlen(base_entry_name, cpio_odc_get_namesize(base_entry));
        base_entry_add_size = base_entry_namesize + 1;
    }

    const size_t subpath_strlen = strlen(subpath);
    const size_t full_subpath_len = base_entry_add_size + subpath_strlen;
    if (full_subpath_len > NAME_MAX) {
        *errno_ptr = ENAMETOOLONG;
        return NULL;
    }

    char full_path[NAME_MAX + 1];
    if (base_entry_namesize > 0)
        strncpy(full_path, base_entry_name, base_entry_namesize);
    if (base_entry_add_size > 0)
        full_path[base_entry_namesize] = '/';
    strncpy(full_path + base_entry_add_size, subpath, subpath_strlen);
    full_path[full_subpath_len] = '\0';

    size_t inode;
    struct cpio_odc_header *found_entry = cpio_odc_lookup_name(base_vn->sb->sb_info, full_path, &inode);
    if (!found_entry) {
        *errno_ptr = ENOENT;
        return NULL;
    }

    vnode *vn;

    MUTEX_WITH(&base_vn->sb->vn_hshtbl_mtx, {
        vn = hashtable_lookup(&base_vn->sb->vn_hshtbl, inode);
    })
    if (vn) {
        return vn;
    }

    vfs_superblock_ref(base_vn->sb);
    vn = initramfs_fsops_new_vnode(base_vn->sb, errno_ptr);
    if (!vn) {
        vfs_superblock_unref(base_vn->sb);
        return NULL;
    }

    MUTEX_WITH(&vn->dnobj.mtx, {
        switch(cpio_odc_get_mode(found_entry) & CPIO_MODE_TYPE_MASK) {
        case CPIO_MODE_TYPE_REGULAR_FILE:
            vn->type = FILE_TYPE_PLAIN;
            break;
        case CPIO_MODE_TYPE_DIRECTORY:
            vn->type = FILE_TYPE_DIRECTORY;
            break;
        default:
            kpanic("Unsupported CPIO entry type");
        }
        vn->inode = inode;
        vn->fops = &initramfs_fops;
    })

    bool inserted_vnode;
    MUTEX_WITH(&base_vn->sb->vn_hshtbl_mtx, {
        inserted_vnode = hashtable_insert(&base_vn->sb->vn_hshtbl, inode, vn);
    })
    if (!inserted_vnode) {
        vfs_vnode_unref(vn);
        *errno_ptr = ENOMEM;
        return NULL;
    }

    return vn;
}

static ssize_t initramfs_fops_pread(fdesc *fdsc, flex_mut buf, size_t count, off_t offset, int *errno_ptr) {
    struct cpio_odc_header *entry = cpio_odc_get_nth_entry(fdsc->vn->sb->sb_info, fdsc->vn->inode);
    assert(entry);

    size_t size = cpio_odc_get_filesize(entry);
    if (offset < 0 || (uint64_t) offset > SIZE_MAX || size < (size_t) offset) {
        *errno_ptr = EINVAL;
        return -1;
    }
    else if (size == (size_t) offset) {
        return 0;
    }
    size_t size_left = size - offset;
    if (count > size_left) {
        count = size_left;
    }

    flex_to(buf, cpio_odc_get_file(entry) + offset, count);

    return count;
}

static ssize_t initramfs_fops_getdents(fdesc *fdsc, FLEX_MUT(dirent) drnt, size_t count, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_DIRECTORY)
        kpanic("Expected directory");

    // dnrt is already sized for count
    flex_mut_pinned drnt_pin = flex_pin_must_cleanup(drnt);
    if (drnt_pin.fault || drnt_pin.size < count * sizeof(dirent)) {
        *errno_ptr = EFAULT;
        return -1;
    }
    dirent *drnt_ptr = drnt_pin.ptr;

    char *base_string = "";
    size_t base_string_len = 0;
    if (fdsc->vn != fdsc->vn->sb->mounted) {
        struct cpio_odc_header *entry = cpio_odc_get_nth_entry(fdsc->vn->sb->sb_info, fdsc->vn->inode);
        base_string = cpio_odc_get_name(entry);
        base_string_len = strnlen(base_string, cpio_odc_get_namesize(entry));
    }

    size_t prefix_len;
    if (base_string_len > 0) {
        prefix_len = base_string_len + 1;
    } else {
        prefix_len = 0;
    }

    size_t done_count = 0;
    while (1) {
        // Find entry
        struct cpio_odc_header *entry = cpio_odc_get_nth_entry(fdsc->vn->sb->sb_info, fdsc->offset);
        if (!entry)
            break;

        // Copy entry if possible
        size_t needed_count = sizeof(dirent);
        if (needed_count <= count) {
            bool is_valid = true;

            char *entry_name = cpio_odc_get_name(entry);
            size_t entry_namesize = strnlen(entry_name, cpio_odc_get_namesize(entry));

            // Check if the file path starts with the current directory
            if (is_valid && prefix_len && strncmp(base_string, entry_name, base_string_len) != 0) {
                is_valid = false;
            }
            // Check if the file path has anything after the current directory
            if (is_valid && prefix_len && entry_name[base_string_len] != '/') {
                is_valid = false;
            }

            // Start this file's name after the /
            char *file_name = entry_name + prefix_len;
            size_t file_namesize = entry_namesize - prefix_len;

            // Check if the file path is a deeper nested file
            const char *slash_pos = strnchr(file_name, file_namesize, '/');
            if (is_valid && slash_pos && slash_pos + 1 != file_name + file_namesize) {
                is_valid = false;
            }

            if (is_valid) {
                count -= needed_count;
                drnt_ptr->ino = fdsc->offset;
                strncpy(drnt_ptr->name, file_name, sizeof(drnt_ptr->name));
                drnt_ptr->name[sizeof(drnt_ptr->name) - 1] = '\0';
                drnt_ptr = (dirent *) ((char *) drnt_ptr + needed_count);
                done_count += needed_count;
            }
        } else {
            if (done_count == 0) {
                *errno_ptr = EINVAL;
                done_count = -1;
            }
            break;
        }
        fdsc->offset += 1;
    }

    flex_unpin(&drnt_pin);
    drnt_ptr = NULL;

    return done_count;
}
