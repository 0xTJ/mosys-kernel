#include "device/device.h"
#include "vfs/devreg.h"
#include "vfs/fdesc.h"
#include "mem/flex.h"
#include "kalloc.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "util/hashtable.h"
#include "vfs/vfs.h"
#include <assert.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <string.h>

static module_status devfs_init(void);
static void devfs_deinit(void);

static vnode *devfs_fsops_new_vnode(superblock *sb, int *errno_ptr);
static void devfs_fsops_cleanup_vnode(superblock *sb, vnode *vn);
static vnode *devfs_fsops_mount(flex_const_str source,
                                unsigned long mountflags, flex_const data,
                                int *errno_ptr);
static vnode *devfs_fsops_lookup(vnode *sb, const char *subpath, int *errno_ptr);

static ssize_t devfs_fops_pread(fdesc *fdsc, flex_mut buf, size_t count, off_t offset, int *errno_ptr);
static ssize_t devfs_fops_pwrite(fdesc *fdsc, flex_const buf, size_t count, off_t offset, int *errno_ptr);
static int devfs_fops_ioctl(struct fdesc *fdsc, unsigned long request, flex_mut argp, int *errno_ptr);
static ssize_t devfs_fops_getdents(fdesc *fdsc, FLEX_MUT(dirent) drnt, size_t count, int *errno_ptr);

static atomic_bool devfs_is_init = false;

static module devfs = {
    .init = devfs_init,
    .deinit = devfs_deinit,
    .name = "devfs",
};
MODULE_DEFINE(devfs);

static fs_operations devfs_fsops = {
    .new_vnode = devfs_fsops_new_vnode,
    .cleanup_vnode = devfs_fsops_cleanup_vnode,
    .mount = devfs_fsops_mount,
    .lookup = devfs_fsops_lookup,
};

static file_operations devfs_fops = {
    .pread = devfs_fops_pread,
    .pwrite = devfs_fops_pwrite,
    .ioctl = devfs_fops_ioctl,
    .getdents = devfs_fops_getdents,
};

static filesystem_type devfs_fstype = {
    .name = "devfs",
    .fsops = &devfs_fsops,
};

superblock *devfs_sb = NULL;

static module_status devfs_init(void) {
    vfs_filesystem_type_register(&devfs_fstype);

    devfs_is_init = true;
    return MODULE_LOADED;
}

static void devfs_deinit(void) {
    devfs_is_init = false;

    // TODO: Figure out what to actually do here
}

static vnode *devfs_fsops_new_vnode(superblock *sb, int *errno_ptr) {
    vfs_superblock_ref(sb);
    vnode *vn = vfs_vnode_new(sb);
    if (vn) {
        MUTEX_WITH(&vn->dnobj.mtx, {
            vn->fops = &devfs_fops;
        })
    } else {
        *errno_ptr = ENOMEM;
        vfs_superblock_unref(sb);
    }
    return vn;
}

static void devfs_fsops_cleanup_vnode(superblock *sb, vnode *vn) {
    assert(vn->sb == sb);

    MUTEX_WITH(&vn->sb->vn_hshtbl_mtx, {
        hashtable_remove(&vn->sb->vn_hshtbl, (uint64_t) vn->inode);
    })

    if (vn->type == FILE_TYPE_DEVICE) {
        devreg_unref(vn->device.dr);
        vn->device.dr = NULL;
    }

    vfs_superblock_unref(vn->sb);
    vn->sb = NULL;
}

static vnode *devfs_fsops_mount(flex_const_str source,
                                unsigned long mountflags, flex_const data,
                                int *errno_ptr) {
    UNUSED(source);
    UNUSED(mountflags);
    UNUSED(data);

    superblock *sb = vfs_superblock_new(&devfs_fstype);
    if (!sb) {
        *errno_ptr = ENOMEM;
        return NULL;
    }

    vnode *root_vn = sb->type->fsops->new_vnode(sb, errno_ptr);
    vfs_superblock_unref(sb);
    if (!root_vn) {
        *errno_ptr = ENOMEM;
        return NULL;
    }

    MUTEX_WITH(&root_vn->dnobj.mtx, {
        root_vn->type = FILE_TYPE_DIRECTORY;
        root_vn->inode = 0;
    })

    bool inserted_vnode;
    MUTEX_WITH(&sb->vn_hshtbl_mtx, {
        inserted_vnode = hashtable_insert(&sb->vn_hshtbl, (uint64_t) 0, root_vn);
    })
    if (!inserted_vnode) {
        vfs_vnode_unref(root_vn);
        *errno_ptr = ENOMEM;
        return NULL;
    }

    MUTEX_WITH(&root_vn->sb->dnobj.mtx, {
        vfs_vnode_ref(root_vn);
        sb->mounted = root_vn;
    })

    devfs_sb = sb;

    return root_vn;
}

static vnode *devfs_fsops_lookup(vnode *base_vn, const char *subpath, int *errno_ptr) {
    // base_vn must be the superblock root
    if (base_vn != base_vn->sb->mounted) {
        *errno_ptr = ENOTDIR;
        return NULL;
    }

    if (0 == strcmp("", subpath)) {
        vfs_vnode_ref(base_vn);
        return base_vn;
    } else {
        devreg *found_dr = devreg_lookup(subpath);
        if (!found_dr) {
            *errno_ptr = ENOENT;
            return NULL;
        }

        ino_t inode = found_dr->major * 256 + found_dr->minor;

        vnode *vn;

        MUTEX_WITH(&base_vn->sb->vn_hshtbl_mtx, {
            vn = hashtable_lookup(&base_vn->sb->vn_hshtbl, inode);
        })
        if (vn) {
            return vn;
        }

        vfs_superblock_ref(base_vn->sb);
        vn = devfs_fsops_new_vnode(base_vn->sb, errno_ptr);
        if (!vn) {
            vfs_superblock_unref(base_vn->sb);
            devreg_unref(found_dr);
            return NULL;
        }

        MUTEX_WITH(&vn->dnobj.mtx, {
            vn->type = FILE_TYPE_DEVICE;
            vn->device.dr = found_dr;
            vn->inode = inode;
            vn->fops = &devfs_fops;
        })

        bool inserted_vnode;
        MUTEX_WITH(&base_vn->sb->vn_hshtbl_mtx, {
            inserted_vnode = hashtable_insert(&base_vn->sb->vn_hshtbl, inode, vn);
        })
        if (!inserted_vnode) {
            vfs_vnode_unref(vn);
            *errno_ptr = ENOMEM;
            return NULL;
        }

        return vn;
    }
}

static ssize_t devfs_fops_pread(fdesc *fdsc, flex_mut buf, size_t count, off_t offset, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_DEVICE)
        kpanic("Passed non-device fdesc as device");

    ssize_t result;
    RWLOCK_READ_WITH(&fdsc->vn->device.dr->dnobj.rwl, {
        result = device_read(fdsc->vn->device.dr->major, fdsc->vn->device.dr->minor, buf, count, offset, errno_ptr);
    })
    return result;
}

static ssize_t devfs_fops_pwrite(fdesc *fdsc, flex_const buf, size_t count, off_t offset, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_DEVICE)
        kpanic("Passed non-device fdesc as device");

    ssize_t result;
    RWLOCK_READ_WITH(&fdsc->vn->device.dr->dnobj.rwl, {
        result = device_write(fdsc->vn->device.dr->major, fdsc->vn->device.dr->minor, buf, count, offset, errno_ptr);
    })
    return result;
}

static int devfs_fops_ioctl(struct fdesc *fdsc, unsigned long request, flex_mut argp, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_DEVICE)
        kpanic("Passed non-device fdesc as device");

    ssize_t result;
    RWLOCK_READ_WITH(&fdsc->vn->device.dr->dnobj.rwl, {
        result = device_ioctl(fdsc->vn->device.dr->major, fdsc->vn->device.dr->minor, request, argp, errno_ptr);
    })
    return result;
}

static ssize_t devfs_fops_getdents(fdesc *fdsc, FLEX_MUT(dirent) drnt, size_t count, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_DIRECTORY)
        kpanic("Expected directory");
    if (fdsc->vn != fdsc->vn->sb->mounted)
        kpanic("Expected devfs_vn_root as only directory");

    // dnrt is already sized for count
    flex_mut_pinned drnt_pin = flex_pin_must_cleanup(drnt);
    if (drnt_pin.fault || drnt_pin.size < count * sizeof(dirent)) {
        *errno_ptr = EFAULT;
        return -1;
    }
    dirent *drnt_ptr = drnt_pin.ptr;

    size_t done_count = 0;
    while (1) {
        devreg *dr = devreg_get_nth(fdsc->offset);
        if (!dr)
            break;
        size_t needed_count = sizeof(dirent);
        if (needed_count <= count) {
            count -= needed_count;
            drnt_ptr->ino = dr->major * 256 + dr->minor;
            strncpy(drnt_ptr->name, dr->name, sizeof(drnt_ptr->name));
            drnt_ptr->name[sizeof(drnt_ptr->name) - 1] = '\0';
            devreg_unref(dr);
            drnt_ptr = (dirent *) ((char *) drnt_ptr + needed_count);
            done_count += needed_count;
        } else {
            devreg_unref(dr);
            if (done_count == 0) {
                *errno_ptr = EINVAL;
                done_count = -1;
            }
            break;
        }
        fdsc->offset += 1;
    }

    flex_unpin(&drnt_pin);
    drnt_ptr = NULL;

    return done_count;
}
