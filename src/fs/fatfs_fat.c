#include "fs/fatfs_fat.h"
#include "fs/fatfs.h"
#include "util/endian.h"
#include "config.h"

static bool fatfs_fat_read_fat12(superblock *sb, fatfs_cluster_fat12 *cluster_val, fatfs_cluster_fat12 cluster_idx, int *errno_ptr);
static bool fatfs_fat_read_fat16(superblock *sb, fatfs_cluster_fat16 *cluster_val, fatfs_cluster_fat16 cluster_idx, int *errno_ptr);
static bool fatfs_fat_read_fat32(superblock *sb, fatfs_cluster_fat32 *cluster_val, fatfs_cluster_fat32 cluster_idx, int *errno_ptr);
static bool fatfs_cluster_offset_advance_fat12(superblock *sb, fatfs_cluster_fat12 *cluster, off_t *offset, int *errno_ptr);
static bool fatfs_cluster_offset_advance_fat16(superblock *sb, fatfs_cluster_fat16 *cluster, off_t *offset, int *errno_ptr);
static bool fatfs_cluster_offset_advance_fat32(superblock *sb, fatfs_cluster_fat32 *cluster, off_t *offset, int *errno_ptr);

// TODO: Add error checking
static bool fatfs_fat_read_fat12(superblock *sb, fatfs_cluster_fat12 *cluster_val, fatfs_cluster_fat12 cluster_idx, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    fatfs_sector_idx fat_region_start = fatfs_data->reserved_sector_count;
    unsigned long long offset =
        (unsigned long long) fat_region_start * sb->block_size + (cluster_idx + (cluster_idx / 2));
    u16le fat_cluster_val_little;
    flex_mut fat_cluster_val_little_flx =
        flex_mut_kernel(&fat_cluster_val_little, sizeof fat_cluster_val_little);

    ssize_t device_result = device_read(sb->device_major, sb->device_minor, fat_cluster_val_little_flx, FATFS_CLUSTER_BYTES_FAT12, offset, errno_ptr);
    if (device_result < 0) {
        return false;
    } else if ((size_t) device_result != FATFS_CLUSTER_BYTES_FAT12) {
        *errno_ptr = EINVAL;
        return false;
    } else {
        if (cluster_idx % 2 == 0) {
            *cluster_val = (from_u16le(fat_cluster_val_little) >> 0) & 0x0FFF;
        } else {
            *cluster_val = (from_u16le(fat_cluster_val_little) >> 4) & 0x0FFF;
        }
        return true;
    }
}

// TODO: Add error checking
static bool fatfs_fat_read_fat16(superblock *sb, fatfs_cluster_fat16 *cluster_val, fatfs_cluster_fat16 cluster_idx, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    fatfs_sector_idx fat_region_start = fatfs_data->reserved_sector_count;
    unsigned long long offset =
        (unsigned long long) fat_region_start * sb->block_size + FATFS_CLUSTER_BYTES_FAT16 * cluster_idx;
    u16le fat_cluster_val_little;
    flex_mut fat_cluster_val_little_flx =
        flex_mut_kernel(&fat_cluster_val_little, sizeof fat_cluster_val_little);

    ssize_t device_result = device_read(sb->device_major, sb->device_minor, fat_cluster_val_little_flx, FATFS_CLUSTER_BYTES_FAT16, offset, errno_ptr);
    if (device_result < 0) {
        return false;
    } else if ((size_t) device_result != FATFS_CLUSTER_BYTES_FAT16) {
        *errno_ptr = EINVAL;
        return false;
    } else {
        *cluster_val = from_u16le(fat_cluster_val_little);
        return true;
    }
}

// TODO: Add error checking
static bool fatfs_fat_read_fat32(superblock *sb, fatfs_cluster_fat32 *cluster_val, fatfs_cluster_fat32 cluster_idx, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    fatfs_sector_idx fat_region_start = fatfs_data->reserved_sector_count;
    unsigned long long offset =
        (unsigned long long) fat_region_start * sb->block_size + FATFS_CLUSTER_BYTES_FAT32 * cluster_idx;
    u32le fat_cluster_val_little;
    flex_mut fat_cluster_val_little_flx =
        flex_mut_kernel(&fat_cluster_val_little, sizeof fat_cluster_val_little);

    ssize_t device_result = device_read(sb->device_major, sb->device_minor, fat_cluster_val_little_flx, FATFS_CLUSTER_BYTES_FAT32, offset, errno_ptr);
    if (device_result < 0) {
        return false;
    } else if ((size_t) device_result != FATFS_CLUSTER_BYTES_FAT32) {
        *errno_ptr = EINVAL;
        return false;
    } else {
        *cluster_val = from_u32le(fat_cluster_val_little);
        return true;
    }
}

static bool fatfs_cluster_offset_advance_fat12(superblock *sb, fatfs_cluster_fat12 *cluster, off_t *offset, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    size_t cluster_size = fatfs_data->sectors_per_cluster * sb->block_size;
    fatfs_cluster_fat12 cluster_local = *cluster;
    off_t offset_local = *offset;

    if (offset_local < 0) {
        kwarn("Negative offset in cluster");
        *errno_ptr = EINVAL;
        return false;
    } else if (offset_local > SIZE_MAX) {
        kwarn("Offset in cluster is too large");
        *errno_ptr = EINVAL;
        return false;
    }

    if (cluster == 0) {
        if (offset_local / 32 >= fatfs_data->root_entry_count) {
            // Out of bounds in root directory
            *errno_ptr = EINVAL;
            return false;
        }
    } else {
        while (
            (size_t) offset_local >= cluster_size &&
            cluster_local < FATFS_CLUSTER_LIMIT_FAT12
        ) {
            if (!fatfs_fat_read_fat12(sb, &cluster_local, cluster_local, errno_ptr)) {
                return false;
            }
            offset_local -= cluster_size;
        }
    }

    *cluster = cluster_local;
    *offset = offset_local;
    return true;
}

static bool fatfs_cluster_offset_advance_fat16(superblock *sb, fatfs_cluster_fat16 *cluster, off_t *offset, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    size_t cluster_size = fatfs_data->sectors_per_cluster * sb->block_size;
    fatfs_cluster_fat16 cluster_local = *cluster;
    off_t offset_local = *offset;

    if (offset_local < 0) {
        kwarn("Negative offset in cluster");
        *errno_ptr = EINVAL;
        return false;
    } else if (offset_local > SIZE_MAX) {
        kwarn("Offset in cluster is too large");
        *errno_ptr = EINVAL;
        return false;
    }

    if (cluster == 0) {
        if (offset_local / 32 >= fatfs_data->root_entry_count) {
            // Out of bounds in root directory
            *errno_ptr = EINVAL;
            return false;
        }
    } else {
        while (
            (size_t) offset_local >= cluster_size &&
            cluster_local < FATFS_CLUSTER_LIMIT_FAT16
        ) {
            if (!fatfs_fat_read_fat16(sb, &cluster_local, cluster_local, errno_ptr)) {
                return false;
            }
            offset_local -= cluster_size;
        }
    }

    *cluster = cluster_local;
    *offset = offset_local;
    return true;
}

static bool fatfs_cluster_offset_advance_fat32(superblock *sb, fatfs_cluster_fat32 *cluster, off_t *offset, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    size_t cluster_size = fatfs_data->sectors_per_cluster * sb->block_size;
    fatfs_cluster_fat32 cluster_local = *cluster;
    off_t offset_local = *offset;

    if (offset_local < 0) {
        kwarn("Negative offset in cluster");
        *errno_ptr = EINVAL;
        return false;
    } else if (offset_local > SIZE_MAX) {
        kwarn("Offset in cluster is too large");
        *errno_ptr = EINVAL;
        return false;
    }

    while (
        (size_t) offset_local >= cluster_size &&
        cluster_local < FATFS_CLUSTER_LIMIT_FAT32
    ) {
        if (!fatfs_fat_read_fat32(sb, &cluster_local, cluster_local, errno_ptr)) {
            return false;
        }
        offset_local -= cluster_size;
    }

    *cluster = cluster_local;
    *offset = offset_local;
    return true;
}

bool fatfs_cluster_offset_advance(superblock *sb, fatfs_cluster_fat_any *cluster, off_t *offset, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    int result;
    fatfs_cluster_fat12 cluster_fat12;
    fatfs_cluster_fat16 cluster_fat16;
    fatfs_cluster_fat32 cluster_fat32;
    
    switch (fatfs_data->fat_type) {
    case FATFS_FAT_TYPE_FAT12:
        cluster_fat12 = *cluster;
        result = fatfs_cluster_offset_advance_fat12(sb, &cluster_fat12, offset, errno_ptr);
        *cluster = cluster_fat12;
        break;

    case FATFS_FAT_TYPE_FAT16:
        cluster_fat16 = *cluster;
        result = fatfs_cluster_offset_advance_fat16(sb, &cluster_fat16, offset, errno_ptr);
        *cluster = cluster_fat16;
        break;

    case FATFS_FAT_TYPE_FAT32:
        cluster_fat32 = *cluster;
        result = fatfs_cluster_offset_advance_fat32(sb, &cluster_fat32, offset, errno_ptr);
        *cluster = cluster_fat32;
        break;

    default:
        unreachable();
    }

    return result;
}
