#include "device/device.h"
#include "vfs/devreg.h"
#include "vfs/fdesc.h"
#include "mem/flex.h"
#include "kalloc.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "util/hashtable.h"
#include "util/util.h"
#include "vfs/vfs.h"
#include <assert.h>
#include <limits.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#define SIMPLEFS_SUPERBLOCK_SIZE 0x200
#define SIMPLEFS_ENTRY_SIZE 0x40

enum simplefs_entry_type {
    simplefs_entry_type_volume_identifier   = 0x01,
    simplefs_entry_type_starting_marker     = 0x02,
    simplefs_entry_type_unused              = 0x10,
    simplefs_entry_type_directory           = 0x11,
    simplefs_entry_type_file                = 0x12,
    simplefs_entry_type_unusable            = 0x18,
    simplefs_entry_type_deleted_directory   = 0x19,
    simplefs_entry_type_deleted_file        = 0x1A,
};

#pragma pack(push, 1)

// TODO: Need to make sure sb lock is held everywhere needed
// TODO: Need to flush superblock to disk better
// TODO: Check every place for integer overflow

struct simplefs_superblock {
    uint8_t reserved_0[11];             // Reserved for compatibility (boot code)
    uint8_t reserved_1[21];             // Reserved for compatibility (legacy BIOS parameter block)
    uint8_t reserved_2[372];            // Reserved for compatibility (boot code)
    uint64_t time_stamp;                // Time stamp
    uint64_t data_size_blocks;          // Size of data area in blocks
    uint64_t index_size_bytes;          // Size of index area in bytes
    union {
        struct {
            union {
                struct {
                    uint8_t magic[3];   // Magic number (0x534653)
                    uint8_t version;    // Simple File System version number (0x10 for Version 1.0)
                };
                uint32_t magic_version; // 0x10534653
            };
            uint64_t total_blocks;      // Total number of blocks
            uint32_t reserved_blocks;   // Number of reserved blocks
            uint8_t block_size;         // Block size
            uint8_t checksum;           // Checksum
        };
        uint8_t checksummed[18];
    };
    uint8_t reserved_3[64];             // Reserved for compatibility (partition table)
    uint8_t reserved_4[2];              // Reserved for compatibility (boot signature)
};
static_assert(sizeof(struct simplefs_superblock) == SIMPLEFS_SUPERBLOCK_SIZE, "SimpleFS superblock has wrong size");

typedef struct simplefs_sb_data {
    uint64_t    data_size_blocks;
    uint64_t    index_size_bytes;
    uint64_t    total_blocks;
    uint32_t    reserved_blocks;

    struct simplefs_superblock disk_sb;
} simplefs_sb_data;

union simplefs_entry {
    struct {
        uint8_t entry_type;
    };
    struct simplefs_entry_volume_identifier {
        uint8_t entry_type;             // Entry type (0x01 for the Volume Identifier Entry)
        uint8_t reserved_0[3];          // Unused/reserved (must be zero)
        uint64_t time_stamp;            // Time stamp
        uint8_t name[52];               // Volume name in UTF-8, including zero terminator
    } volume_identifier;
    struct simplefs_entry_starting_marker {
        uint8_t entry_type;             // Entry type (0x02 for the Starting Marker Entry)
        uint8_t reserved_0[63];         // Unused/reserved
    } starting_marker;
    struct simplefs_entry_unused {
        uint8_t entry_type;             // Entry type (0x10 for Unused Entries)
        uint8_t reserved_0[63];         // Unused/reserved
    } unused;
    struct simplefs_entry_directory {
        uint8_t entry_type;             // Entry type (0x11 for Directory Entries)
        uint8_t continuation_entries;   // Number of continuation entries following this entry
        uint64_t time_stamp;            // Time stamp
        uint8_t name[54];               // Directory name in UTF-8
    } directory;
    struct simplefs_entry_file {
        uint8_t entry_type;             // Entry type (0x12 for File Entries)
        uint8_t continuation;           // Number of continuation entries following this entry
        uint64_t time_stamp;            // Time stamp
        uint64_t starting_block;        // Starting block number in the data area
        uint64_t ending_block;          // Ending block number in the data area
        uint64_t file_length;           // File length in bytes
        uint8_t name[30];               // File name in UTF-8
    } file;
    struct simplefs_entry_unusable {
        uint8_t entry_type;             // Entry type (0x18 for Unusable Entries)
        uint8_t reserved_0[9];          // Unused/reserved
        uint64_t starting_block;        // Starting block number in the data area
        uint64_t ending_block;          // Ending block number in the data area
        uint8_t reserved_1[38];         // Unused/reserved
    } unusable;
    struct simplefs_entry_deleted_directory {
        uint8_t entry_type;             // Entry type (0x19 for Deleted Directory Entries)
        uint8_t continuation_entries;   // Number of continuation entries following this entry
        uint64_t time_stamp;            // Time stamp
        uint8_t name[54];               // Directory name in UTF-8
    } deleted_directory;
    struct simplefs_entry_deleted_file {
        uint8_t entry_type;             // Entry type (0x1A for Deleted File Entries)
        uint8_t continuation;           // Number of continuation entries following this entry
        uint64_t time_stamp;            // Time stamp
        uint64_t starting_block;        // Starting block number in the data area
        uint64_t ending_block;          // Ending block number in the data area
        uint64_t file_length;           // File length in bytes
        uint8_t name[30];               // File name in UTF-8
    } deleted_file;
    struct simplefs_entry_continuation {
        uint8_t name[64];               // Entry name in UTF-8
    } continuation;
};
static_assert(sizeof(union simplefs_entry) == SIMPLEFS_ENTRY_SIZE, "SimpleFS index entry has wrong size");

#pragma pack(pop)

static module_status simplefs_init(void);
static void simplefs_deinit(void);

static uint8_t simplefs_checksum_validate(struct simplefs_superblock *sfs_sb);

// Require sb->mtx to be held
// TODO: Check that it is being held here
static int simplefs_device_read(superblock *sb, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static int simplefs_device_write(superblock *sb, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int simplefs_index_entry_read(superblock *sb, uint64_t entry_index, union simplefs_entry *entry, int *errno_ptr);
static int simplefs_index_entry_write(superblock *sb, uint64_t entry_index, const union simplefs_entry *entry, int *errno_ptr);
static int simplefs_data_read(superblock *sb, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static int simplefs_data_write(superblock *sb, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int simplefs_starting_marker_write(superblock *sb, int *errno_ptr);
static bool simplefs_alloc_index_entries(superblock *sb, uint64_t index_entry_count, uint64_t *index_entry_out, int *errno_ptr);

static vnode *simplefs_vnode_from_index(superblock *sb, uint64_t entry_index, int *errno_ptr);

static vnode *simplefs_fsops_new_vnode(superblock *sb, int *errno_ptr);
static void simplefs_fsops_cleanup_vnode(superblock *sb, vnode *vn);
static int simplefs_fsops_readsb(superblock *sb, int *errno_ptr);
static int simplefs_fsops_writesb(superblock *sb, int *errno_ptr);
static vnode *simplefs_fsops_mount(flex_const_str source,
                                   unsigned long mountflags, flex_const data,
                                   int *errno_ptr);
static vnode *simplefs_fsops_create(vnode *sb, const char *subpath, int *errno_ptr);
static vnode *simplefs_fsops_lookup(vnode *sb, const char *subpath, int *errno_ptr);

static ssize_t simplefs_fops_pread(fdesc *fdsc,flex_mut buf, size_t count, off_t offset, int *errno_ptr);
static ssize_t simplefs_fops_pwrite(fdesc *fdsc, flex_const buf, size_t count, off_t offset, int *errno_ptr);
static ssize_t simplefs_fops_getdents(fdesc *fdsc, FLEX_MUT(dirent) drnt, size_t count, int *errno_ptr);

static atomic_bool simplefs_is_init = false;

static module simplefs = {
    .init = simplefs_init,
    .deinit = simplefs_deinit,
    .name = "simplefs",
};
MODULE_DEFINE(simplefs);

static fs_operations simplefs_fsops = {
    .new_vnode = simplefs_fsops_new_vnode,
    .cleanup_vnode = simplefs_fsops_cleanup_vnode,
    .mount = simplefs_fsops_mount,
    .create = simplefs_fsops_create,
    .lookup = simplefs_fsops_lookup,
};

static file_operations simplefs_fops = {
    .pread = simplefs_fops_pread,
    .pwrite = simplefs_fops_pwrite,
    .getdents = simplefs_fops_getdents,
};

static filesystem_type simplefs_fstype = {
    .name = "simplefs",
    .fsops = &simplefs_fsops,
};

static module_status simplefs_init(void) {
    vfs_filesystem_type_register(&simplefs_fstype);

    simplefs_is_init = true;
    return MODULE_LOADED;
}

static void simplefs_deinit(void) {
    simplefs_is_init = false;

    // TODO: Figure out what to actually do here
}

static uint8_t simplefs_checksum_validate(struct simplefs_superblock *sfs_sb) {
    uint8_t sum = 0;
    for (unsigned i = 0; i < sizeof(sfs_sb->checksummed); ++i) {
        sum += sfs_sb->checksummed[i];
    }
    return sum;
}

static vnode *simplefs_fsops_new_vnode(superblock *sb, int *errno_ptr) {
    vfs_superblock_ref(sb);
    vnode *vn = vfs_vnode_new(sb);
    if (vn) {
        MUTEX_WITH(&vn->dnobj.mtx, {
            vn->fops = &simplefs_fops;
        })
    } else {
        *errno_ptr = ENOMEM;
        vfs_superblock_unref(sb);
    }
    return vn;
}

static void simplefs_fsops_cleanup_vnode(superblock *sb, vnode *vn) {
    assert(vn->sb == sb);

    MUTEX_WITH(&vn->sb->vn_hshtbl_mtx, {
        hashtable_remove(&vn->sb->vn_hshtbl, (uint64_t) vn->inode);
    })

    vfs_superblock_unref(vn->sb);
    vn->sb = NULL;
}

static vnode *simplefs_fsops_mount(flex_const_str source,
                                   unsigned long mountflags, flex_const data,
                                   int *errno_ptr) {
    UNUSED(mountflags);
    UNUSED(data);

    superblock *sb = vfs_superblock_new(&simplefs_fstype);
    if (!sb) {
        *errno_ptr = ENOMEM;
        return NULL;
    }

    MUTEX_WITH(&sb->dnobj.mtx, {
        // TODO: This process of getting device info by full path should be factored out somewhere
        fdesc *fdsc = vfs_open_fdesc(source, 0, 0, errno_ptr);
        if (!fdsc) {
            MUTEX_GOTO_NO_UNLOCK(failed_source_dev_lookup);
        }
        if (fdsc->vn->type != FILE_TYPE_DEVICE) {
            *errno_ptr = EINVAL;    // TODO: Should be ENOTBLK
            vfs_fdesc_unref(fdsc);
            MUTEX_GOTO_NO_UNLOCK(failed_source_dev_lookup);
        }

        sb->device_major = fdsc->vn->device.dr->major;
        sb->device_minor = fdsc->vn->device.dr->minor;

        vfs_fdesc_unref(fdsc);
        fdsc = NULL;

        simplefs_sb_data *sb_info = kalloc(sizeof(simplefs_sb_data));
        if (!sb_info) {
            MUTEX_GOTO_NO_UNLOCK(failed_sb_info_alloc);
        }
        sb->sb_info = sb_info;
    
        if (simplefs_fsops_readsb(sb, errno_ptr) < 0) {
            MUTEX_GOTO_NO_UNLOCK(failed_sb_reading);
        }

        union simplefs_entry tmp_entry = {0};

        if (simplefs_index_entry_read(sb, 0, &tmp_entry, errno_ptr) < 0) {
            MUTEX_GOTO_NO_UNLOCK(failed_sb_volume_reading);
        }
        if (tmp_entry.entry_type != simplefs_entry_type_volume_identifier) {
            MUTEX_GOTO_NO_UNLOCK(failed_sb_volume_checking);
        }

        if (simplefs_index_entry_read(sb, (sb_info->index_size_bytes / SIMPLEFS_ENTRY_SIZE) - 1, &tmp_entry, errno_ptr) < 0) {
            MUTEX_GOTO_NO_UNLOCK(failed_sb_starting_reading);
        }
        if (tmp_entry.entry_type != simplefs_entry_type_starting_marker) {
            MUTEX_GOTO_NO_UNLOCK(failed_sb_starting_checking);
        }
    })

    // TODO: I don't think root is always at 1
    vnode *root_vn = simplefs_vnode_from_index(sb, 1, errno_ptr);
    if (!root_vn) {
        goto failed_vnode_from_index;
    }

    bool inserted_vnode;
    MUTEX_WITH(&sb->vn_hshtbl_mtx, {
        inserted_vnode = hashtable_insert(&sb->vn_hshtbl, (uint64_t) 0, root_vn);
    })
    if (!inserted_vnode) {
        vfs_vnode_unref(root_vn);
        *errno_ptr = ENOMEM;
        goto failed_hashtable_insert;
    }

    MUTEX_WITH(&sb->dnobj.mtx, {
        vfs_vnode_ref(root_vn);
        sb->mounted = root_vn;
    })

    return root_vn;

failed_hashtable_insert:
failed_vnode_from_index:
    MUTEX_WITH(&sb->dnobj.mtx, kfree(sb->sb_info););
    goto failed_after_unlock;
failed_sb_starting_checking:
failed_sb_starting_reading:
failed_sb_volume_checking:
failed_sb_volume_reading:
failed_sb_reading:
    kfree(sb->sb_info);
failed_sb_info_alloc:
failed_source_dev_lookup:
    mutex_unlock(&sb->dnobj.mtx);
failed_after_unlock:
    vfs_superblock_unref(sb);
    return NULL;
}

static int simplefs_device_read(superblock *sb, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    ssize_t result = device_read(sb->device_major, sb->device_minor, buf, count, offset, errno_ptr);
    if (result < 0){
        return -1;
    } else if ((size_t) result != count) {
        *errno_ptr = EINVAL;
        return -1;
    } else {
        return 0;
    }
}

static int simplefs_device_write(superblock *sb, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    ssize_t result = device_write(sb->device_major, sb->device_minor, buf, count, offset, errno_ptr);
    if (result < 0){
        return -1;
    } else if ((size_t) result != count) {
        *errno_ptr = EINVAL;
        return -1;
    } else {
        return 0;
    }
}

static int simplefs_index_entry_read(superblock *sb, uint64_t entry_index, union simplefs_entry *entry, int *errno_ptr) {
    simplefs_sb_data *sb_info = sb->sb_info;

    if (entry_index > ULLONG_MAX / sizeof(union simplefs_entry)) goto error_inval;

    if (sb_info->total_blocks > ULLONG_MAX >> sb->block_size_width) goto error_inval;
    unsigned long long index_area_end = sb_info->total_blocks << sb->block_size_width;
    if (sb_info->index_size_bytes > index_area_end) goto error_inval;
    unsigned long long index_area_start = index_area_end - sb_info->index_size_bytes;

    if (entry_index * sizeof(union simplefs_entry) > index_area_end) goto error_inval;
    unsigned long long entry_end = index_area_end - entry_index * sizeof(union simplefs_entry);
    if (sizeof(union simplefs_entry) > entry_end) goto error_inval;
    unsigned long long entry_offset = entry_end - sizeof(union simplefs_entry);
    if (entry_offset < index_area_start) goto error_inval;

    FLEX_MUT(union simplefs_entry) entry_flx = flex_mut_kernel(entry, sizeof(*entry));

    return simplefs_device_read(sb, entry_flx, sizeof(*entry), entry_offset, errno_ptr);

error_inval:
    *errno_ptr = EINVAL;
    return -1;
}

static int simplefs_index_entry_write(superblock *sb, uint64_t entry_index, const union simplefs_entry *entry, int *errno_ptr) {
    simplefs_sb_data *sb_info = sb->sb_info;

    if (entry_index > ULLONG_MAX / sizeof(union simplefs_entry)) goto error_inval;

    if (sb_info->total_blocks > ULLONG_MAX >> sb->block_size_width) goto error_inval;
    unsigned long long index_area_end = sb_info->total_blocks << sb->block_size_width;
    if (sb_info->index_size_bytes > index_area_end) goto error_inval;
    unsigned long long index_area_start = index_area_end - sb_info->index_size_bytes;

    if (entry_index * sizeof(union simplefs_entry) > index_area_end) goto error_inval;
    unsigned long long entry_end = index_area_end - entry_index * sizeof(union simplefs_entry);
    if (sizeof(union simplefs_entry) > entry_end) goto error_inval;
    unsigned long long entry_offset = entry_end - sizeof(union simplefs_entry);
    if (entry_offset < index_area_start) goto error_inval;

    FLEX_CONST(union simplefs_entry) entry_flx = flex_const_kernel(entry, sizeof(*entry));

    return simplefs_device_write(sb, entry_flx, sizeof(*entry), entry_offset, errno_ptr);

error_inval:
    *errno_ptr = EINVAL;
    return -1;
}

static int simplefs_data_read(superblock *sb, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    simplefs_sb_data *sb_info = sb->sb_info;

    uint64_t data_area_start_block = sb_info->reserved_blocks;
    uint64_t data_area_end_block = data_area_start_block + sb_info->data_size_blocks;

    // Check that the data area is valid
    // TODO: Decide if this belongs here, or should be trusted at this point
    if (data_area_end_block < data_area_start_block ||
        data_area_end_block > ULLONG_MAX >> sb->block_size_width) {
        goto error_inval;
    }

    unsigned long long data_area_start = data_area_start_block << sb->block_size_width;
    unsigned long long data_area_end = data_area_end_block << sb->block_size_width;

    // Check that the data is within the data area
    if (offset > data_area_end ||
        data_area_end - offset < data_area_start ||
        count > data_area_end - offset ||
        data_area_end - offset - count < data_area_start) {
        goto error_inval;
    }

    unsigned long long data_start = data_area_start + offset;

    return simplefs_device_read(sb, buf, count, data_start, errno_ptr);

error_inval:
    *errno_ptr = EINVAL;
    return -1;
}

static int simplefs_data_write(superblock *sb, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    simplefs_sb_data *sb_info = sb->sb_info;

    uint64_t data_area_start_block = sb_info->reserved_blocks;
    uint64_t data_area_end_block = data_area_start_block + sb_info->data_size_blocks;

    // Check that the data area is valid
    // TODO: Decide if this belongs here, or should be trusted at this point
    if (data_area_end_block < data_area_start_block ||
        data_area_end_block > ULLONG_MAX >> sb->block_size_width) {
        goto error_inval;
    }

    unsigned long long data_area_start = data_area_start_block << sb->block_size_width;
    unsigned long long data_area_end = data_area_end_block << sb->block_size_width;

    // Check that the data is within the data area
    if (offset > data_area_end ||
        data_area_end - offset < data_area_start ||
        count > data_area_end - offset ||
        data_area_end - offset - count < data_area_start) {
        goto error_inval;
    }

    unsigned long long data_start = data_area_start + offset;

    return simplefs_device_write(sb, buf, count, data_start, errno_ptr);

error_inval:
    *errno_ptr = EINVAL;
    return -1;
}

static int simplefs_starting_marker_write(superblock *sb, int *errno_ptr) {
    simplefs_sb_data *sb_info = sb->sb_info;

    union simplefs_entry starting_marker;
    // TODO: Do reserved bytes need to be 0?
    starting_marker.starting_marker.entry_type = simplefs_entry_type_starting_marker;

    return simplefs_index_entry_write(sb, (sb_info->index_size_bytes / SIMPLEFS_ENTRY_SIZE) - 1, &starting_marker, errno_ptr);
}

static bool simplefs_alloc_index_entries(superblock *sb, uint64_t index_entry_count, uint64_t *index_entry_out, int *errno_ptr) {
    simplefs_sb_data *sb_info = sb->sb_info;

    uint64_t old_index_size_bytes = sb_info->index_size_bytes;
    uint64_t new_index_size_bytes = old_index_size_bytes + (index_entry_count * SIMPLEFS_ENTRY_SIZE);
    uint64_t new_index_size_blocks = new_index_size_bytes / sb->block_size;
    uint64_t new_needed_blocks = sb_info->reserved_blocks + sb_info->data_size_blocks + new_index_size_blocks;
    if (new_needed_blocks > sb_info->total_blocks) {
        *errno_ptr = ENOSPC;
        return false;
    }
    if (old_index_size_bytes < SIMPLEFS_ENTRY_SIZE) {
        // Can't allocate an entry when we're not replacing a starting marker
        *errno_ptr = EINVAL;
        return false;
    }

    sb_info->index_size_bytes = new_index_size_bytes;

    *index_entry_out = (old_index_size_bytes / SIMPLEFS_ENTRY_SIZE) - 1;
    return true;
}

static bool simplefs_alloc_data_blocks(superblock *sb, uint64_t data_block_count, uint64_t *data_block_out, int *errno_ptr) {
    simplefs_sb_data *sb_info = sb->sb_info;

    uint64_t old_data_size_blocks = sb_info->data_size_blocks;
    uint64_t new_data_size_blocks = old_data_size_blocks + data_block_count;
    uint64_t index_size_blocks = sb_info->index_size_bytes >> sb->block_size_width;
    uint64_t new_needed_blocks = sb_info->reserved_blocks + new_data_size_blocks + index_size_blocks;
    if (new_needed_blocks > sb_info->total_blocks) {
        *errno_ptr = ENOSPC;
        return false;
    }

    sb_info->data_size_blocks = new_data_size_blocks;

    *data_block_out = old_data_size_blocks;
    return true;
}

static vnode *simplefs_vnode_from_index(superblock *sb, uint64_t entry_index, int *errno_ptr) {
    union simplefs_entry this_entry;
    if (simplefs_index_entry_read(sb, entry_index, &this_entry, errno_ptr) < 0) {
        return NULL;
    }

    if (this_entry.entry_type != simplefs_entry_type_directory && this_entry.entry_type != simplefs_entry_type_file) {
        *errno_ptr = ENOENT;
        return NULL;
    }

    vnode *vn = sb->type->fsops->new_vnode(sb, errno_ptr);
    if (!vn) {
        return NULL;
    }

    MUTEX_WITH(&vn->dnobj.mtx, {
        vn->type = this_entry.entry_type == simplefs_entry_type_directory ? FILE_TYPE_DIRECTORY : FILE_TYPE_PLAIN;
        vn->inode = entry_index;
        vn->fops = &simplefs_fops;
    })

    return vn;
}

// TODO: Write back sb at appropriate times

static int simplefs_fsops_readsb(superblock *sb, int *errno_ptr) {
    simplefs_sb_data *sb_info = sb->sb_info;

    FLEX_MUT(struct simplefs_superblock) disk_sb_flx = flex_mut_kernel(&sb_info->disk_sb, sizeof(sb_info->disk_sb));

    ssize_t result = device_read(sb->device_major, sb->device_minor, disk_sb_flx, sizeof(sb_info->disk_sb), 0, errno_ptr);
    if (result != sizeof(sb_info->disk_sb)) {
        if (result != -1) {
            *errno_ptr = EINVAL;
            result = -1;
        }
        return result;
    }

    // Validate checksum
    if (simplefs_checksum_validate(&sb_info->disk_sb) != 0) {
        goto error_inval;
    }

    // Validate and calculate block size
    unsigned char block_size_width_base = from_little_u8(sb_info->disk_sb.block_size);
    if (block_size_width_base < 1 || block_size_width_base >= (sizeof(sb->block_size) * CHAR_BIT) - 7) {
        goto error_inval;
    }
    sb->block_size_width = block_size_width_base + 7;
    sb->block_size = 1UL << sb->block_size_width;

    // Validate number of reserved blocks
    sb_info->reserved_blocks = from_little_u32(sb_info->disk_sb.reserved_blocks);
    if (sb_info->reserved_blocks < 1) {
        goto error_inval;
    }

    // Get number of total blocks
    sb_info->total_blocks = from_little_u64(sb_info->disk_sb.total_blocks);

    // Validate magic number and version
    if (from_little_u32(sb_info->disk_sb.magic_version) != 0x10534653) {
        goto error_inval;
    }

    // Validate size of index area
    sb_info->index_size_bytes = from_little_u64(sb_info->disk_sb.index_size_bytes);
    if (sb_info->index_size_bytes % SIMPLEFS_ENTRY_SIZE != 0) {
        goto error_inval;
    }

    // Get size of data area
    sb_info->data_size_blocks = from_little_u64(sb_info->disk_sb.data_size_blocks);

    // Get timestamp
    sb->timestamp = from_little_u64(sb_info->disk_sb.time_stamp);

    return 0;

error_inval:
    *errno_ptr = EINVAL;
    return -1;
}

// TODO: Clean this up by only regnerating fields in disk_sb that can change
static int simplefs_fsops_writesb(superblock *sb, int *errno_ptr) {
    simplefs_sb_data *sb_info = sb->sb_info;

    // Validate and set block size
    if (sb->block_size != 1UL << sb->block_size_width ||
        sb->block_size_width < 7 ||
        sb->block_size_width - 7 < 1) {
        goto error_inval;
    }
    unsigned char block_size_width_base = sb->block_size_width - 7;
    sb_info->disk_sb.block_size = to_little_u8(block_size_width_base);

    // Validate and set number of reserved blocks
    if (sb_info->reserved_blocks < 1) {
        goto error_inval;
    }
    sb_info->disk_sb.reserved_blocks = to_little_u32(sb_info->reserved_blocks);

    // Set number of total blocks
    sb_info->disk_sb.total_blocks = to_little_u64(sb_info->total_blocks);

    // Set magic number and version
    sb_info->disk_sb.magic_version = to_little_u32(0x10534653);

    // Validate size of index area
    if (sb_info->index_size_bytes % SIMPLEFS_ENTRY_SIZE != 0) {
        goto error_inval;
    }
    sb_info->disk_sb.index_size_bytes = to_little_u64(sb_info->index_size_bytes);

    // Get size of data area
    sb_info->disk_sb.data_size_blocks = to_little_u64(sb_info->data_size_blocks);

    // Get timestamp
    sb_info->disk_sb.time_stamp = to_little_u64(sb->timestamp);

    // Generate checksum
    sb_info->disk_sb.checksum = 0;
    sb_info->disk_sb.checksum = ~simplefs_checksum_validate(&sb_info->disk_sb) + 1;

    FLEX_MUT(struct simplefs_superblock) disk_sb_flx = flex_mut_kernel(&sb_info->disk_sb, sizeof(sb_info->disk_sb));

    ssize_t result = device_write(sb->device_major, sb->device_minor, flex_make_const(disk_sb_flx), sizeof(sb_info->disk_sb), 0, errno_ptr);
    if (result != sizeof(sb_info->disk_sb)) {
        if (result != -1) {
            *errno_ptr = EINVAL;
            result = -1;
        }
        return result;
    }

    result = simplefs_starting_marker_write(sb, errno_ptr);

    return result;

error_inval:
    *errno_ptr = EINVAL;
    return -1;
}

// TODO: Don't duplicate

static vnode *simplefs_fsops_create(vnode *base_vn, const char *subpath, int *errno_ptr) {
    if (base_vn->type != FILE_TYPE_DIRECTORY) {
        *errno_ptr = ENOTDIR;
        return NULL;
    }

    union simplefs_entry base_entry;
    if (simplefs_index_entry_read(base_vn->sb, base_vn->inode, &base_entry, errno_ptr) < 0) {
        return NULL;
    }
    if (base_entry.entry_type != simplefs_entry_type_directory) {
        // TODO: This shouldn't be fatal, disk could have changed
        kpanic("Type on disc doesn't match type in VFS");
    }

    // TODO: Support continuation entries

    char complete_subpath[strlen(subpath) + strnlen((char *) base_entry.directory.name, sizeof(base_entry.directory.name)) + 1];
    strcpy(complete_subpath, subpath);
    // TODO: Use strncat()
    // strncat(complete_subpath, (char *) base_entry.directory.name, sizeof(base_entry.directory.name));
    strcpy(&complete_subpath[strlen(complete_subpath)], (char *) base_entry.directory.name);

    // TODO: Check that it doesn't exist?

    vnode *result;
    bool failed = false;
    uint64_t new_index_entry;

    MUTEX_WITH(&base_vn->sb->dnobj.mtx, {
        if (!simplefs_alloc_index_entries(base_vn->sb, 1, &new_index_entry, errno_ptr)) {
            result = NULL;
            failed = true;
            MUTEX_LEAVE_UNLOCK();
        }

        union simplefs_entry new_entry = { .file = {
            .entry_type = simplefs_entry_type_file,
            .continuation = to_little_u8(0),
            .time_stamp = to_little_u64(0),
            .starting_block = to_little_u64(0),
            .ending_block = to_little_u64(0),
            .file_length = to_little_u64(0),
        } };
        strncpy((char *) new_entry.file.name, complete_subpath, sizeof(new_entry.file.name));

        if (simplefs_index_entry_write(base_vn->sb, new_index_entry, &new_entry, errno_ptr) < 0) {
            result = NULL;
            failed = true;
            MUTEX_LEAVE_UNLOCK();
        }
    })

    if (!failed) {
        vfs_superblock_ref(base_vn->sb);
        result = simplefs_vnode_from_index(base_vn->sb, new_index_entry, errno_ptr);
        if (!result) {
            vfs_superblock_unref(base_vn->sb);
        }
    }

    if (simplefs_fsops_writesb(base_vn->sb, errno_ptr) < 0) {
        result = NULL;
    }

    return result;
}

static vnode *simplefs_fsops_lookup(vnode *base_vn, const char *subpath, int *errno_ptr) {
    if (base_vn->type != FILE_TYPE_DIRECTORY) {
        *errno_ptr = ENOTDIR;
        return NULL;
    }

    if (0 == strcmp("", subpath)) {
        vfs_vnode_ref(base_vn);
        return base_vn;
    }

    union simplefs_entry base_entry;
    if (simplefs_index_entry_read(base_vn->sb, base_vn->inode, &base_entry, errno_ptr) < 0) {
        return NULL;
    }
    if (base_entry.entry_type != simplefs_entry_type_directory) {
        // TODO: This shouldn't be fatal, disk could have changed
        kpanic("Type on disc doesn't match type in VFS");
    }

    // TODO: Support continuation entries

    char complete_subpath[strlen(subpath) + strnlen((char *) base_entry.directory.name, sizeof(base_entry.directory.name)) + 1];
    strcpy(complete_subpath, subpath);
    // TODO: Use strncat()
    // strncat(complete_subpath, (char *) base_entry.directory.name, sizeof(base_entry.directory.name));
    strcpy(&complete_subpath[strlen(complete_subpath)], (char *) base_entry.directory.name);

    // TODO: Need to lock sb?
    simplefs_sb_data *sb_info = base_vn->sb->sb_info;

    uint64_t index_limit = sb_info->index_size_bytes / sizeof(union simplefs_entry);
    bool found_entry = false;
    uint64_t inode;
    union simplefs_entry this_entry;
    for (uint64_t i = 0; i < index_limit; ++i) {
        if (simplefs_index_entry_read(base_vn->sb, i, &this_entry, errno_ptr) < 0) {
            return NULL;
        }

        const char *name;
        size_t name_size;
        if (this_entry.entry_type == simplefs_entry_type_directory) {
            name = (char *) this_entry.directory.name;
            name_size = sizeof(this_entry.directory.name);
        } else if (this_entry.entry_type == simplefs_entry_type_file) {
            name = (char *) this_entry.file.name;
            name_size = sizeof(this_entry.file.name);
        } else {
            continue;
        }

        // Check if this is a match
        if (0 != strncmp(complete_subpath, name, name_size)) {
            continue;
        }

        found_entry = true;
        inode = i;
        break;
    }

    if (!found_entry) {
        *errno_ptr = ENOENT;
        return NULL;
    }

    vnode *vn;

    MUTEX_WITH(&base_vn->sb->vn_hshtbl_mtx, {
        vn = hashtable_lookup(&base_vn->sb->vn_hshtbl, inode);
    })
    if (vn) {
        return vn;
    }

    vfs_superblock_ref(base_vn->sb);
    vn = simplefs_vnode_from_index(base_vn->sb, inode, errno_ptr);
    if (!vn) {
        vfs_superblock_unref(base_vn->sb);
        return NULL;
    }

    bool inserted_vnode;
    MUTEX_WITH(&base_vn->sb->vn_hshtbl_mtx, {
        inserted_vnode = hashtable_insert(&base_vn->sb->vn_hshtbl, inode, vn);
    })
    if (!inserted_vnode) {
        vfs_vnode_unref(vn);
        *errno_ptr = ENOMEM;
        return NULL;
    }

    return vn;
}

static ssize_t simplefs_fops_pread(fdesc *fdsc, flex_mut buf, size_t count, off_t offset, int *errno_ptr) {
    union simplefs_entry entry;
    if (simplefs_index_entry_read(fdsc->vn->sb, fdsc->vn->inode, &entry, errno_ptr) == -1) {
        return -1;
    }

    if (entry.entry_type != simplefs_entry_type_file) {
        *errno_ptr = EINVAL;
        return -1;
    }

    if (offset < 0) {
        *errno_ptr = EINVAL;
        return -1;
    }
    if (UINT64_MAX - count < (uint64_t) offset) {
        *errno_ptr = EFBIG;
        return -1;
    }

    uint64_t ending_block = from_little_u64(entry.file.ending_block);
    uint64_t starting_block = from_little_u64(entry.file.starting_block);
    if (ending_block <= starting_block) {
        if (ending_block == 0 && starting_block == 0) {
            // This is fine
        } else {
            *errno_ptr = EINVAL;
            return -1;
        }
    }

    uint64_t block_count = ending_block - starting_block;
    uint64_t file_length = from_little_u64(entry.file.file_length);
    uint64_t offset_blocks = offset >> fdsc->vn->sb->block_size_width;
    if (offset_blocks >= block_count || (uint64_t) offset >= file_length) {
        count = 0;
    } else if (count > 0 && file_length - offset < count) {
        count = file_length - offset;
    }

    uint64_t file_data_offset = starting_block << fdsc->vn->sb->block_size_width;
    uint64_t curr_data_offset = file_data_offset + offset;

    // TODO: Limit count to size left of file
    if (simplefs_data_read(fdsc->vn->sb, buf, count, curr_data_offset, errno_ptr) == -1) {
        return -1;
    }

    return count;
}

// TODO: Implement block growing

static ssize_t simplefs_fops_pwrite(fdesc *fdsc, flex_const buf, size_t count, off_t offset, int *errno_ptr) {
    union simplefs_entry entry;
    if (simplefs_index_entry_read(fdsc->vn->sb, fdsc->vn->inode, &entry, errno_ptr) == -1) {
        return -1;
    }

    if (entry.entry_type != simplefs_entry_type_file) {
        *errno_ptr = EINVAL;
        return -1;
    }

    if (offset < 0) {
        *errno_ptr = EINVAL;
        return -1;
    }
    if (UINT64_MAX - count < (uint64_t) offset) {
        *errno_ptr = EFBIG;
        return -1;
    }

    uint64_t ending_block = from_little_u64(entry.file.ending_block);
    uint64_t starting_block = from_little_u64(entry.file.starting_block);
    if (ending_block <= starting_block) {
        if (ending_block == 0 && starting_block == 0) {
            // This is fine
        } else {
            *errno_ptr = EINVAL;
            return -1;
        }
    }

    if (ending_block == 0 && starting_block == 0) {
        const uint64_t init_blocks = 128;   // TODO: For now, each file is capped at 64k
        if (!simplefs_alloc_data_blocks(fdsc->vn->sb, init_blocks, &starting_block, errno_ptr)) {
            return -1;
        }
        ending_block = starting_block + init_blocks;
    }

    uint64_t block_count = ending_block - starting_block;
    uint64_t file_length = from_little_u64(entry.file.file_length);
    uint64_t file_space = block_count << fdsc->vn->sb->block_size_width;
    // TODO: Extend length to offset first, for zero-filling
    if (file_space <= (uint64_t) offset) {
        count = 0;
    } else if (file_space - offset < count) {
        count = file_space - offset;
    }

    uint64_t start_data_offset = starting_block << fdsc->vn->sb->block_size_width;
    uint64_t end_data_offset = ending_block << fdsc->vn->sb->block_size_width;
    uint64_t curr_data_offset = start_data_offset + offset;
    if (end_data_offset < curr_data_offset || end_data_offset - curr_data_offset < count) {
        *errno_ptr = ENOSPC;
        return -1;
    }
    if (end_data_offset - count < curr_data_offset) {
        count = end_data_offset - curr_data_offset;
    }

    if (simplefs_data_write(fdsc->vn->sb, buf, count, curr_data_offset, errno_ptr) == -1) {
        return -1;
    }

    entry.file.file_length = to_little_u64(offset + count);
    entry.file.ending_block = to_little_u64(ending_block);
    entry.file.starting_block = to_little_u64(starting_block);
    if (simplefs_index_entry_write(fdsc->vn->sb, fdsc->vn->inode, &entry, errno_ptr) == -1) {
        return -1;
    }

    if (simplefs_fsops_writesb(fdsc->vn->sb, errno_ptr) < 0) {
        return -1;
    }

    return count;
}

static ssize_t simplefs_fops_getdents(fdesc *fdsc, FLEX_MUT(dirent) drnt, size_t count, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_DIRECTORY)
        kpanic("Expected directory");

    // dnrt is already sized for count
    flex_mut_pinned drnt_pin = flex_pin_must_cleanup(drnt);
    if (drnt_pin.fault || drnt_pin.size < count * sizeof(dirent)) {
        *errno_ptr = EFAULT;
        return -1;
    }
    dirent *drnt_ptr = drnt_pin.ptr;

    union simplefs_entry base_entry;
    if (simplefs_index_entry_read(fdsc->vn->sb, fdsc->vn->inode, &base_entry, errno_ptr) < 0) {
        return -1;
    }
    if (base_entry.entry_type != simplefs_entry_type_directory) {
        // TODO: This shouldn't be fatal, disk could have changed
        kpanic("Type on disc doesn't match type in VFS");
    }

    // TODO: Support continuation entries

    size_t base_subpath_len = strnlen((char *) base_entry.directory.name, sizeof(base_entry.directory.name)) + 1;
    char base_subpath[base_subpath_len];
    strncpy(base_subpath, (char *) base_entry.directory.name, base_subpath_len);
    base_subpath[base_subpath_len - 1] = '\0';

    // TODO: Need to lock sb?
    simplefs_sb_data *sb_info = fdsc->vn->sb->sb_info;

    ssize_t done_count = 0;
    while (1) {
        // Find entry
        uint64_t index_limit = sb_info->index_size_bytes / sizeof(union simplefs_entry);
        size_t found_entries = 0;
        uint64_t inode = 0;
        const char *found_name = NULL;
        size_t found_name_size;
        union simplefs_entry found_entry;
        for (uint64_t i = 0; i < index_limit; ++i) {
            if (simplefs_index_entry_read(fdsc->vn->sb, i, &found_entry, errno_ptr) < 0) {
                return -1;
            }

            if (found_entry.entry_type == to_little_u8(simplefs_entry_type_directory)) {
                found_name = (char *) found_entry.directory.name;
                found_name_size = sizeof(found_entry.directory.name);
            } else if (found_entry.entry_type == to_little_u8(simplefs_entry_type_file)) {
                found_name = (char *) found_entry.file.name;
                found_name_size = sizeof(found_entry.file.name);
            } else {
                continue;
            }

            // Check if this is a match
            if (strnlen(found_name, found_name_size) <= sizeof(base_subpath) - 1) {
                continue;
            }
            if (strncmp(found_name, base_subpath, sizeof(base_subpath) - 1) != 0) {
                continue;
            }

            // Check if the offset has been reached
            // Keep looping if it hasn't
            found_entries += 1;
            if (found_entries <= fdsc->offset) {
                continue;
            }

            inode = i;
            break;
        }

        if (found_entries <= fdsc->offset || found_name == NULL) {
            break;
        }

        // Add entry if possible
        size_t needed_count = sizeof(dirent);
        if (needed_count <= count) {
            count -= needed_count;
            drnt_ptr->ino = inode;
            strncpy(drnt_ptr->name, found_name, sizeof(drnt_ptr->name));
            drnt_ptr->name[sizeof(drnt_ptr->name) - 1] = '\0';
            drnt_ptr = (dirent *) ((char *) drnt_ptr + needed_count);
            done_count += needed_count;
        } else {
            if (done_count == 0) {
                *errno_ptr = EINVAL;
                done_count = -1;
            }
            break;
        }
        fdsc->offset += 1;
    }

    flex_unpin(&drnt_pin);
    drnt_ptr = NULL;

    return done_count;
}
