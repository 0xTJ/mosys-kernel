#include "fs/fatfs.h"
#include "fs/fatfs_fat.h"
#include "device/device.h"
#include "vfs/devreg.h"
#include "vfs/fdesc.h"
#include "mem/flex.h"
#include "kalloc.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "util/hashtable.h"
#include "util/util.h"
#include "vfs/vfs.h"
#include <assert.h>
#include <limits.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

// TODO: Need to make sure sb lock is held everywhere needed
// TODO: Need to flush superblock to disk better
// TODO: Check every place for integer overflow

struct fatfs_vnode_data {
    fatfs_cluster_fat_any parent_cluster;
    uint32_t index_in_parent;
};

static module_status fatfs_init(void);
static void fatfs_deinit(void);

// Require sb->mtx to be held
// TODO: Check that it is being held here
static int fatfs_in_cluster_read(superblock *sb, flex_mut buf, fatfs_cluster_fat_any cluster, size_t offset_in_cluster, size_t count, int *errno_ptr);
static int fatfs_in_cluster_write(superblock *sb, flex_const buf, fatfs_cluster_fat_any cluster, size_t offset_in_cluster, size_t count, int *errno_ptr);
static int fatfs_dir_entry_read(superblock *sb, fatfs_cluster_fat_any dir_cluster, uint32_t index_in_dir, struct fatfs_entry *entry, int *errno_ptr);
static int fatfs_dir_entry_advancing_read(superblock *sb, fatfs_cluster_fat_any *dir_cluster, uint32_t *index_in_dir, struct fatfs_entry *entry, int *errno_ptr);

static vnode *fatfs_vnode_root_dir(superblock *sb, int *errno_ptr);
static vnode *fatfs_vnode_from_entry(superblock *sb, fatfs_cluster_fat_any parent_cluster, uint32_t index_in_parent, const struct fatfs_entry *entry, int *errno_ptr);

static vnode *fatfs_fsops_new_vnode(superblock *sb, int *errno_ptr);
static void fatfs_fsops_cleanup_vnode(superblock *sb, vnode *vn);
static int fatfs_fsops_readsb(superblock *sb, int *errno_ptr);
static int fatfs_fsops_writesb(superblock *sb, int *errno_ptr);
static vnode *fatfs_fsops_mount(flex_const_str source,
                                   unsigned long mountflags, flex_const data,
                                   int *errno_ptr);
static vnode *fatfs_fsops_create(vnode *sb, const char *subpath, int *errno_ptr);
static vnode *fatfs_fsops_lookup(vnode *sb, const char *subpath, int *errno_ptr);

static ssize_t fatfs_fops_pread(fdesc *fdsc,flex_mut buf, size_t count, off_t offset, int *errno_ptr);
static ssize_t fatfs_fops_pwrite(fdesc *fdsc, flex_const buf, size_t count, off_t offset, int *errno_ptr);
static ssize_t fatfs_fops_getdents(fdesc *fdsc, FLEX_MUT(dirent) drnt, size_t count, int *errno_ptr);

static atomic_bool fatfs_is_init = false;

static module fatfs = {
    .init = fatfs_init,
    .deinit = fatfs_deinit,
    .name = "fatfs",
};
MODULE_DEFINE(fatfs);

static fs_operations fatfs_fsops = {
    .new_vnode = fatfs_fsops_new_vnode,
    .cleanup_vnode = fatfs_fsops_cleanup_vnode,
    .mount = fatfs_fsops_mount,
    .create = fatfs_fsops_create,
    .lookup = fatfs_fsops_lookup,
};

static file_operations fatfs_fops = {
    .pread = fatfs_fops_pread,
    .pwrite = fatfs_fops_pwrite,
    .getdents = fatfs_fops_getdents,
};

static filesystem_type fatfs_fstype = {
    .name = "fat",
    .fsops = &fatfs_fsops,
};

static module_status fatfs_init(void) {
    vfs_filesystem_type_register(&fatfs_fstype);

    fatfs_is_init = true;
    return MODULE_LOADED;
}

static void fatfs_deinit(void) {
    fatfs_is_init = false;

    // TODO: Figure out what to actually do here
}

static vnode *fatfs_fsops_new_vnode(superblock *sb, int *errno_ptr) {
    struct fatfs_vnode_data *fatfs_data = kalloc(sizeof(struct fatfs_vnode_data));
    if (!fatfs_data) {
        *errno_ptr = ENOMEM;
        return NULL;
    }
    
    vfs_superblock_ref(sb);
    vnode *vn = vfs_vnode_new(sb);
    if (vn) {
        MUTEX_WITH(&vn->dnobj.mtx, {
            vn->fops = &fatfs_fops;
            vn->data_ptr = fatfs_data;
        })
    } else {
        *errno_ptr = ENOMEM;
        kfree(fatfs_data);
        vfs_superblock_unref(sb);
    }

    return vn;
}

static void fatfs_fsops_cleanup_vnode(superblock *sb, vnode *vn) {
    assert(vn->sb == sb);

    MUTEX_WITH(&vn->sb->vn_hshtbl_mtx, {
        hashtable_remove(&vn->sb->vn_hshtbl, (uint64_t) vn->inode);
    })

    kfree(vn->data_ptr);

    vfs_superblock_unref(vn->sb);
    vn->sb = NULL;
}

static vnode *fatfs_fsops_mount(flex_const_str source,
                                unsigned long mountflags, flex_const data,
                                int *errno_ptr) {
    UNUSED(mountflags);
    UNUSED(data);

    superblock *sb = vfs_superblock_new(&fatfs_fstype);
    if (!sb) {
        *errno_ptr = ENOMEM;
        return NULL;
    }

    MUTEX_WITH(&sb->dnobj.mtx, {
        // TODO: This process of getting device info by full path should be factored out somewhere
        fdesc *fdsc = vfs_open_fdesc(source, 0, 0, errno_ptr);
        if (!fdsc) {
            MUTEX_GOTO_NO_UNLOCK(failed_source_dev_lookup);
        }
        if (fdsc->vn->type != FILE_TYPE_DEVICE) {
            *errno_ptr = EINVAL;    // TODO: Should be ENOTBLK
            vfs_fdesc_unref(fdsc);
            MUTEX_GOTO_NO_UNLOCK(failed_source_dev_lookup);
        }

        sb->device_major = fdsc->vn->device.dr->major;
        sb->device_minor = fdsc->vn->device.dr->minor;

        vfs_fdesc_unref(fdsc);
        fdsc = NULL;

        fatfs_bpb_data *sb_info = kalloc(sizeof(fatfs_bpb_data));
        if (!sb_info) {
            MUTEX_GOTO_NO_UNLOCK(failed_sb_info_alloc);
        }
        sb->sb_info = sb_info;

        if (fatfs_fsops_readsb(sb, errno_ptr) < 0) {
            MUTEX_GOTO_NO_UNLOCK(failed_sb_reading);
        }
    })

    vnode *root_vn = fatfs_vnode_root_dir(sb, errno_ptr);
    if (!root_vn) {
        goto failed_vnode_from_index;
    }

    bool inserted_vnode;
    MUTEX_WITH(&sb->vn_hshtbl_mtx, {
        inserted_vnode = hashtable_insert(&sb->vn_hshtbl, (uint64_t) 0, root_vn);
    })
    if (!inserted_vnode) {
        vfs_vnode_unref(root_vn);
        *errno_ptr = ENOMEM;
        return NULL;
    }

    MUTEX_WITH(&sb->dnobj.mtx, {
        vfs_vnode_ref(root_vn);
        sb->mounted = root_vn;
    })

    return root_vn;

failed_vnode_from_index:
    goto failed_after_unlock;
failed_sb_reading:
    kfree(sb->sb_info);
failed_sb_info_alloc:
failed_source_dev_lookup:
    mutex_unlock(&sb->dnobj.mtx);
failed_after_unlock:
    vfs_superblock_unref(sb);
    return NULL;
}

// Cluster 0 reads in the root directory, treated as a single cluster regardless of its size
// Data to read must not span a cluster boundary
static int fatfs_in_cluster_read(superblock *sb, flex_mut buf, fatfs_cluster_fat_any cluster, size_t offset_in_cluster, size_t count, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    fatfs_sector_idx fat_region_start = fatfs_data->reserved_sector_count;
    fatfs_sector_idx root_directory_region_start =
        fat_region_start + (fatfs_data->number_of_fats * fatfs_data->fat_size);
    fatfs_sector_idx data_region_start =
        root_directory_region_start + (((fatfs_data->root_entry_count * 32) + (sb->block_size - 1)) / sb->block_size);

    fatfs_sector_idx cluster_sector;
    if (cluster >= 2) {
        cluster_sector = data_region_start + (fatfs_sector_idx) (cluster - 2) * fatfs_data->sectors_per_cluster;
    } else if (cluster == 0) {
        cluster_sector = root_directory_region_start;
    } else {
        *errno_ptr = EINVAL;
        return -1;
    }
    unsigned long long offset_of_cluster_sector = (unsigned long long) cluster_sector * sb->block_size;

    ssize_t device_result = device_read(sb->device_major, sb->device_minor, buf, count, offset_of_cluster_sector + offset_in_cluster, errno_ptr);
    if (device_result < 0) {
        return -1;
    } else if ((size_t) device_result != count) {
        *errno_ptr = EINVAL;
        return -1;
    } else {
        return 0;
    }
}

// Cluster 0 writes in the root directory, treated as a single cluster regardless of its size
// Data to write must not span a cluster boundary
static int fatfs_in_cluster_write(superblock *sb, flex_const buf, fatfs_cluster_fat_any cluster, size_t offset_in_cluster, size_t count, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    fatfs_sector_idx fat_region_start = fatfs_data->reserved_sector_count;
    fatfs_sector_idx root_directory_region_start =
        fat_region_start + (fatfs_data->number_of_fats * fatfs_data->fat_size);
    fatfs_sector_idx data_region_start =
        root_directory_region_start + (((fatfs_data->root_entry_count * 32) + (sb->block_size - 1)) / sb->block_size);

    fatfs_sector_idx cluster_sector;
    if (cluster >= 2) {
        cluster_sector = data_region_start + (fatfs_sector_idx) (cluster - 2) * fatfs_data->sectors_per_cluster;
    } else if (cluster == 0) {
        cluster_sector = root_directory_region_start;
    } else {
        *errno_ptr = EINVAL;
        return -1;
    }
    unsigned long long offset_of_cluster_sector = (unsigned long long) cluster_sector * sb->block_size;

    ssize_t device_result = device_write(sb->device_major, sb->device_minor, buf, count, offset_of_cluster_sector + offset_in_cluster, errno_ptr);
    if (device_result < 0) {
        return -1;
    } else if ((size_t) device_result != count) {
        *errno_ptr = EINVAL;
        return -1;
    } else {
        return 0;
    }
}

static ssize_t fatfs_data_advancing_read(superblock *sb, flex_mut *buf, fatfs_cluster_fat_any *cluster, off_t *offset, size_t *count, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    if (!fatfs_cluster_offset_advance(sb, cluster, offset, errno_ptr)) {
        return -1;
    }

    fatfs_cluster_fat_any cluster_limit;
    switch (fatfs_data->fat_type) {
    case FATFS_FAT_TYPE_FAT12: cluster_limit = FATFS_CLUSTER_LIMIT_FAT12; break;
    case FATFS_FAT_TYPE_FAT16: cluster_limit = FATFS_CLUSTER_LIMIT_FAT16; break;
    case FATFS_FAT_TYPE_FAT32: cluster_limit = FATFS_CLUSTER_LIMIT_FAT32; break;
    default: unreachable();
    }
    const size_t cluster_size = fatfs_data->sectors_per_cluster * sb->block_size;
    
    assert(*offset >= 0);
    if (*cluster >= cluster_limit || *offset >= (ssize_t) cluster_size) {
        // End-of-file
        return 0;
    }

    size_t count_left_in_cluster = cluster_size - *offset;
    size_t this_count = min(count_left_in_cluster, *count);

    int result = fatfs_in_cluster_read(sb, *buf, *cluster, *offset, this_count, errno_ptr);
    if (result < 0) {
        return result;
    }

    *buf = flex_add(*buf, this_count);
    *offset += this_count;
    *count -= this_count;
    return this_count;
}

static ssize_t fatfs_data_advancing_write(superblock *sb, flex_const *buf, fatfs_cluster_fat_any *cluster, off_t *offset, size_t *count, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    if (!fatfs_cluster_offset_advance(sb, cluster, offset, errno_ptr)) {
        return -1;
    }

    fatfs_cluster_fat_any cluster_limit;
    switch (fatfs_data->fat_type) {
    case FATFS_FAT_TYPE_FAT12: cluster_limit = FATFS_CLUSTER_LIMIT_FAT12; break;
    case FATFS_FAT_TYPE_FAT16: cluster_limit = FATFS_CLUSTER_LIMIT_FAT16; break;
    case FATFS_FAT_TYPE_FAT32: cluster_limit = FATFS_CLUSTER_LIMIT_FAT32; break;
    default: unreachable();
    }
    const size_t cluster_size = fatfs_data->sectors_per_cluster * sb->block_size;
    
    assert(*offset >= 0);
    if (*cluster >= cluster_limit || *offset >= (ssize_t) cluster_size) {
        // End-of-file
        return 0;
    }

    size_t count_left_in_cluster = cluster_size - *offset;
    size_t this_count = min(count_left_in_cluster, *count);

    int result = fatfs_in_cluster_write(sb, *buf, *cluster, *offset, this_count, errno_ptr);
    if (result < 0) {
        return result;
    }

    *buf = flex_add(*buf, this_count);
    *offset += this_count;
    *count -= this_count;
    return this_count;
}

// TODO: This function needs to have some values cast to allow the full possible range, and more checks added
// TODO: Is uint16_t the right type for index_in_dir?
static int fatfs_dir_entry_read(superblock *sb, fatfs_cluster_fat_any dir_cluster, uint32_t index_in_dir, struct fatfs_entry *entry, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    if (dir_cluster == 0) {
        if (fatfs_data->fat_type == FATFS_FAT_TYPE_FAT32) {
            // Is a root directory entry on a FAT32 disk
            goto error_inval;
        }
        if (index_in_dir >= fatfs_data->root_entry_count) {
            // Is an entry out-of-bounds entry in the FAT12/FAT16 root directory
            goto error_inval;
        }
    }

    FLEX_MUT(struct fatfs_entry) entry_flx = flex_mut_kernel(entry, sizeof(*entry));

    return fatfs_in_cluster_read(sb, entry_flx, dir_cluster, (unsigned long long) index_in_dir * 32, sizeof(*entry), errno_ptr);

error_inval:
    *errno_ptr = EINVAL;
    return -1;
}

// TODO: This function needs to have some values cast to allow the full possible range, and more checks added
// Returns -1 on failure, 0 on EOF, 1 on success
static int fatfs_dir_entry_advancing_read(superblock *sb, fatfs_cluster_fat_any *dir_cluster, uint32_t *index_in_dir, struct fatfs_entry *entry, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    off_t offset = (off_t) *index_in_dir * 32;
    if (*dir_cluster != 0) {
        if (fatfs_cluster_offset_advance(sb, dir_cluster, &offset, errno_ptr)) {
            // Update index_in_dir from updated offset
            *index_in_dir = offset / 32;
        } else {
            return -1;
        }
    }

    const size_t cluster_size =
        (*dir_cluster == 0) ?
            fatfs_data->root_entry_count * 32 :
            fatfs_data->sectors_per_cluster * sb->block_size;
    assert(offset >= 0);
    if (offset >= (ssize_t) cluster_size) {
        // End-of-file
        return 0;
    }

    int result = fatfs_dir_entry_read(sb, *dir_cluster, *index_in_dir, entry, errno_ptr);
    if (result < 0) {
        return result;
    }

    *index_in_dir += 1;
    return 1;
}

static vnode *fatfs_vnode_root_dir(superblock *sb, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    vnode *vn = sb->type->fsops->new_vnode(sb, errno_ptr);
    if (!vn) {
        return NULL;
    }

    MUTEX_WITH(&vn->dnobj.mtx, {
        vn->type = FILE_TYPE_DIRECTORY;
        vn->inode = (fatfs_data->fat_type == FATFS_FAT_TYPE_FAT32) ? fatfs_data->root_cluster : 0;
        vn->fops = &fatfs_fops;

        struct fatfs_vnode_data *vnode_data = vn->data_ptr;
        vnode_data->parent_cluster = 0;
        switch (fatfs_data->fat_type) {
        case FATFS_FAT_TYPE_FAT12: vnode_data->index_in_parent = FATFS_CLUSTER_LIMIT_FAT12; break;
        case FATFS_FAT_TYPE_FAT16: vnode_data->index_in_parent = FATFS_CLUSTER_LIMIT_FAT16; break;
        case FATFS_FAT_TYPE_FAT32: vnode_data->index_in_parent = FATFS_CLUSTER_LIMIT_FAT32; break;
        default: unreachable();
        }
    })

    return vn;
}

static vnode *fatfs_vnode_from_entry(superblock *sb, fatfs_cluster_fat_any parent_cluster, uint32_t index_in_parent, const struct fatfs_entry *entry, int *errno_ptr) {
    vnode *vn = sb->type->fsops->new_vnode(sb, errno_ptr);
    if (!vn) {
        return NULL;
    }

    MUTEX_WITH(&vn->dnobj.mtx, {
        vn->type = (from_u8le(entry->attr) & 0x10) ? FILE_TYPE_DIRECTORY : FILE_TYPE_PLAIN;
        vn->inode = from_u16le(entry->fst_clus_lo);
        vn->fops = &fatfs_fops;

        struct fatfs_vnode_data *fatfs_data = vn->data_ptr;
        fatfs_data->parent_cluster = parent_cluster;
        fatfs_data->index_in_parent = index_in_parent;
    })

    return vn;
}

// TODO: Write back sb at appropriate times

static int fatfs_fsops_readsb(superblock *sb, int *errno_ptr) {
    fatfs_bpb_data *fatfs_data = sb->sb_info;

    FLEX_MUT(struct fatfs_bpb) bpb_flx = flex_mut_kernel(&fatfs_data->disk_bpb, sizeof(fatfs_data->disk_bpb));

    ssize_t result = device_read(sb->device_major, sb->device_minor, bpb_flx, sizeof(fatfs_data->disk_bpb), 0, errno_ptr);
    if (result != sizeof(fatfs_data->disk_bpb)) {
        if (result != -1) {
            goto error_inval;
        }
        return result;
    }

    // TODO: Validate all of these fields

    sb->block_size = from_u16le(fatfs_data->disk_bpb.byts_per_sec);

    fatfs_data->root_dir_sectors =
        ((from_u16le(fatfs_data->disk_bpb.root_ent_cnt) * 32UL) + (sb->block_size - 1U)) / sb->block_size;

    if (from_u16le(fatfs_data->disk_bpb.fat_sz_16) != 0) {
        fatfs_data->fat_size = from_u16le(fatfs_data->disk_bpb.fat_sz_16);
    } else {
        fatfs_data->fat_size = from_u32le(fatfs_data->disk_bpb.fat32.fat_sz_32);
    }

    if (from_u16le(fatfs_data->disk_bpb.tot_sec_16) != 0) {
        fatfs_data->total_sector_count = from_u16le(fatfs_data->disk_bpb.tot_sec_16);
    } else {
        fatfs_data->total_sector_count = from_u32le(fatfs_data->disk_bpb.tot_sec_32);
    }

    fatfs_data->number_of_fats = from_u8le(fatfs_data->disk_bpb.num_fats);
    fatfs_data->reserved_sector_count = from_u16le(fatfs_data->disk_bpb.rsvd_sec_cnt);
    fatfs_data->sectors_per_cluster = from_u8le(fatfs_data->disk_bpb.sec_per_clus);
    fatfs_data->root_entry_count = from_u16le(fatfs_data->disk_bpb.root_ent_cnt);
    fatfs_data->first_data_sector = fatfs_data->reserved_sector_count + (fatfs_data->number_of_fats * fatfs_data->fat_size) + fatfs_data->root_dir_sectors;
    fatfs_data->data_sectors_count = fatfs_data->total_sector_count - (fatfs_data->reserved_sector_count + (fatfs_data->number_of_fats * fatfs_data->fat_size) + fatfs_data->root_dir_sectors);
    fatfs_data->count_of_clusters = fatfs_data->data_sectors_count / fatfs_data->sectors_per_cluster;

    if (fatfs_data->count_of_clusters < 4085) {
        // Volume is FAT12
        fatfs_data->fat_type = FATFS_FAT_TYPE_FAT12;
        fatfs_data->root_cluster = 0;
    } else if (fatfs_data->count_of_clusters < 65525) {
        // Volume is FAT16
        fatfs_data->fat_type = FATFS_FAT_TYPE_FAT16;
        fatfs_data->root_cluster = 0;
    } else {
        // Volume is FAT32
        fatfs_data->fat_type = FATFS_FAT_TYPE_FAT32;
        fatfs_data->root_cluster = from_u32le(fatfs_data->disk_bpb.fat32.root_clus);
    }

    return 0;

error_inval:
    *errno_ptr = EINVAL;
    return -1;
}

static int fatfs_fsops_writesb(superblock *sb, int *errno_ptr) {
    UNUSED(sb);
//     struct fatfs_bpb disk_sb;

//     // Validate and set block size
//     if (sb->block_size != 1UL << sb->block_size_width ||
//         sb->block_size_width < 7 ||
//         sb->block_size_width - 7 < 1) {
//         goto error_inval;
//     }
//     unsigned char block_size_width_base = sb->block_size_width - 7;
//     disk_sb.block_size = to_u8le(block_size_width_base);

//     // Validate and set number of reserved blocks
//     if (fatfs->reserved_blocks < 1) {
//         goto error_inval;
//     }
//     disk_sb.reserved_blocks = to_u32le(fatfs->reserved_blocks);

//     // Set number of total blocks
//     disk_sb.total_blocks = to_little_u64(fatfs->total_blocks);

//     // Set magic number and version
//     disk_sb.magic_version = to_u32le(0x10534653);

//     // Validate size of index area
//     if (fatfs->index_size_bytes % FATFS_ENTRY_SIZE != 0) {
//         goto error_inval;
//     }
//     disk_sb.index_size_bytes = to_little_u64(fatfs->index_size_bytes);

//     // Get size of data area
//     disk_sb.data_size_blocks = to_little_u64(fatfs->data_size_blocks);

//     // Get timestamp
//     disk_sb.time_stamp = to_little_u64(sb->timestamp);

//     // Generate checksum
//     disk_sb.checksum = 0;
//     disk_sb.checksum = ~fatfs_checksum_validate(&disk_sb) + 1;

//     FLEX_MUT(struct fatfs_bpb) bpb_flx = flex_mut_kernel(&disk_sb, sizeof(disk_sb));

//     ssize_t result = device_write(sb->device_major, sb->device_minor, flex_make_const(bpb_flx), sizeof(disk_sb), 0, errno_ptr);
//     if (result != sizeof(disk_sb)) {
//         if (result != -1) {
//             *errno_ptr = EINVAL;
//             result = -1;
//         }
//         return result;
//     }

//     result = fatfs_starting_marker_write(sb, errno_ptr);

//     return result;

// error_inval:
    *errno_ptr = EINVAL;
    return -1;
}

// TODO: Don't duplicate

static vnode *fatfs_fsops_create(vnode *base_vn, const char *subpath, int *errno_ptr) {
    if (base_vn->type != FILE_TYPE_DIRECTORY) {
        *errno_ptr = ENOTDIR;
        return NULL;
    }
    
    UNUSED(subpath);
    *errno_ptr = ENOSYS;
    return NULL;

    // struct fatfs_entry base_entry;
    // if (fatfs_dir_entry_read(base_vn->sb, base_vn->inode, &base_entry, errno_ptr) < 0) {
    //     return NULL;
    // }
    // if (base_entry.entry_type != fatfs_entry_type_directory) {
    //     // TODO: This shouldn't be fatal, disk could have changed
    //     kpanic("Type on disc doesn't match type in VFS");
    // }

    // // TODO: Support continuation entries

    // char complete_subpath[strlen(subpath) + strnlen((char *) base_entry.directory.name, sizeof(base_entry.directory.name)) + 1];
    // strcpy(complete_subpath, subpath);
    // // TODO: Use strncat()
    // // strncat(complete_subpath, (char *) base_entry.directory.name, sizeof(base_entry.directory.name));
    // strcpy(&complete_subpath[strlen(complete_subpath)], (char *) base_entry.directory.name);

    // // TODO: Check that it doesn't exist?

    // vnode *result;
    // bool failed = false;
    // uint64_t new_index_entry;

    // MUTEX_WITH(&base_vn->sb->mtx, {
    //     if (!fatfs_alloc_index_entries(base_vn->sb, 1, &new_index_entry, errno_ptr)) {
    //         result = NULL;
    //         failed = true;
    //         MUTEX_LEAVE_UNLOCK();
    //     }

    //     struct fatfs_entry new_entry = { .file = {
    //         .entry_type = fatfs_entry_type_file,
    //         .continuation = to_u8le(0),
    //         .time_stamp = to_little_u64(0),
    //         .starting_block = to_little_u64(0),
    //         .ending_block = to_little_u64(0),
    //         .file_length = to_little_u64(0),
    //     } };
    //     strncpy((char *) new_entry.file.name, complete_subpath, sizeof(new_entry.file.name));

    //     if (fatfs_index_entry_write(base_vn->sb, new_index_entry, &new_entry, errno_ptr) < 0) {
    //         result = NULL;
    //         failed = true;
    //         MUTEX_LEAVE_UNLOCK();
    //     }
    // })

    // if (!failed) {
    //     vfs_superblock_ref(base_vn->sb);
    //     result = fatfs_vnode_from_index(base_vn->sb, new_index_entry, errno_ptr);
    //     if (!result) {
    //         vfs_superblock_unref(base_vn->sb);
    //     }
    // }

    // if (fatfs_fsops_writesb(base_vn->sb, errno_ptr) < 0) {
    //     result = NULL;
    // }

    // return result;
}

// Does not use a null terminator, treats trailling spaces as padding
static size_t fatfs_strlen(size_t len, const uint8_t str[len]) {
    size_t current_len = 0;
    for (size_t i = 0; i < len; ++i) {
        if (str[i] != ' ') {
            current_len = i + 1;
        }
    }
    return current_len;
}

static inline char fatfs_toupper(char c) {
    if ('a' <= c && c <= 'z') {
        return c - 'a' + 'A';
    } else {
        return c;
    }
}

static int fatfs_strcmp_nocase(const char *a, const char *b) {
    for (size_t i = 0;; ++i) {
        char a_i_upper = fatfs_toupper(a[i]);
        char b_i_upper = fatfs_toupper(b[i]);

        int cmp = a_i_upper - b_i_upper;
        if (cmp != 0) {
            return cmp;
        } else if (a_i_upper == '\0') {
            return 0;
        }
    }
}

static bool fatfs_names_equal(const uint8_t fat_name[11], const char *name) {
    size_t name_len = fatfs_strlen(8, fat_name + 0);
    size_t ext_len = fatfs_strlen(3, fat_name + 8);

    char fat_name_conv[NAME_MAX + 1];
    size_t idx = 0;
    memcpy(fat_name_conv + idx, fat_name + 0, name_len);
    idx += name_len;
    if (ext_len > 0) {
        fat_name_conv[idx++] = '.';
        memcpy(fat_name_conv + idx, fat_name + 8, ext_len);
        idx += ext_len;
    }
    fat_name_conv[idx++] = '\0';

    return fatfs_strcmp_nocase(fat_name_conv, name) == 0;
}

static vnode *fatfs_fsops_lookup(vnode *base_vn, const char *subpath, int *errno_ptr) {
    if (base_vn->type != FILE_TYPE_DIRECTORY) {
        *errno_ptr = ENOTDIR;
        return NULL;
    }

    const size_t subpath_len = strlen(subpath);
    if (subpath_len == 0) {
        vfs_vnode_ref(base_vn);
        return base_vn;
    }

    bool found_entry = false;
    struct fatfs_entry this_entry;
    uint32_t index_in_parent;
    // TODO: Don't limit like this
    for (uint64_t i = 0; i < 64; ++i) {
        if (fatfs_dir_entry_read(base_vn->sb, base_vn->inode, i, &this_entry, errno_ptr) < 0) {
            return NULL;
        }

        uint8_t attributes = from_u8le(this_entry.attr);
        if (attributes & 0x08) {
            // Is volume label
            continue;
        } else if (this_entry.name[0] == 0x00) {
            // No more entries
            break;
        } else if (this_entry.name[0] == 0xE5) {
            // Free entry
            continue;
        } else if (this_entry.name[0] == 0x05) {
            // Actual value is 0xE5
            this_entry.name[0] = 0xE5;
        }

        if (fatfs_names_equal(this_entry.name, subpath)) {
            found_entry = true;
            index_in_parent = i;
            break;
        }
    }

    if (!found_entry) {
        *errno_ptr = ENOENT;
        return NULL;
    }

    // TODO: Need to lock sb?
    fatfs_bpb_data *fatfs_data = base_vn->sb->sb_info;

    ino_t this_inode;
    switch (fatfs_data->fat_type) {
    case FATFS_FAT_TYPE_FAT12:
    case FATFS_FAT_TYPE_FAT16:
        this_inode = from_u16le(this_entry.fst_clus_lo);
        break;
    case FATFS_FAT_TYPE_FAT32:
        this_inode =
            (uint32_t) from_u16le(this_entry.fst_clus_hi) << 16 |
            (uint32_t) from_u16le(this_entry.fst_clus_lo) << 0;
        break;
    default: unreachable();
    } 

    vnode *vn = NULL;

    MUTEX_WITH(&base_vn->sb->vn_hshtbl_mtx, {
        vn = hashtable_lookup(&base_vn->sb->vn_hshtbl, this_inode);
    })
    if (vn) {
        return vn;
    }

    vfs_superblock_ref(base_vn->sb);
    vn = fatfs_vnode_from_entry(base_vn->sb, base_vn->inode, index_in_parent, &this_entry, errno_ptr);
    if (!vn) {
        vfs_superblock_unref(base_vn->sb);
        return NULL;
    }

    bool inserted_vnode;
    MUTEX_WITH(&base_vn->sb->vn_hshtbl_mtx, {
        inserted_vnode = hashtable_insert(&base_vn->sb->vn_hshtbl, vn->inode, vn);
    })
    if (!inserted_vnode) {
        vfs_vnode_unref(vn);
        *errno_ptr = ENOMEM;
        return NULL;
    }

    return vn;
}

static ssize_t fatfs_fops_pread(fdesc *fdsc, flex_mut buf, size_t count, off_t offset, int *errno_ptr) {
    // TODO: Is a lock needed?
    vnode *vn = fdsc->vn;
    superblock *sb = vn->sb;

    if (offset < 0) {
        *errno_ptr = EINVAL;
        return -1;
    }
    if (UINT64_MAX - count < (uint64_t) offset) {
        *errno_ptr = EFBIG;
        return -1;
    }

    fatfs_cluster_fat_any cluster = vn->inode;

    size_t count_done = 0;
    while (count > 0) {
        ssize_t adv_read_result = fatfs_data_advancing_read(sb, &buf, &cluster, &offset, &count, errno_ptr);
        if (adv_read_result < 0) {
            return adv_read_result;
        } else if (adv_read_result == 0) {
            break;
        } else {
            count_done += adv_read_result;
        }
    }

    // TODO: Properly handle a read on EOF?
    return count_done;
}

// TODO: Allow partial writes
static ssize_t fatfs_fops_pwrite(fdesc *fdsc, flex_const buf, size_t count, off_t offset, int *errno_ptr) {
    // TODO: Is a lock needed?
    vnode *vn = fdsc->vn;
    superblock *sb = vn->sb;

    if (offset < 0) {
        *errno_ptr = EINVAL;
        return -1;
    }
    if (UINT64_MAX - count < (uint64_t) offset) {
        *errno_ptr = EFBIG;
        return -1;
    }

    *errno_ptr = EINVAL;
    return -1;

    fatfs_cluster_fat_any cluster = vn->inode;

    size_t count_done = 0;
    while (count > 0) {
        ssize_t adv_write_result = fatfs_data_advancing_write(sb, &buf, &cluster, &offset, &count, errno_ptr);
        if (adv_write_result < 0) {
            return adv_write_result;
        } else if (adv_write_result == 0) {
            break;
        } else {
            count_done += adv_write_result;
        }
    }

    // TODO: Properly handle a write on EOF?
    return count_done;
}

static ssize_t fatfs_fops_getdents(fdesc *fdsc, FLEX_MUT(dirent) drnt, size_t count, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_DIRECTORY)
        kpanic("Expected directory");

    if (count < sizeof(dirent)) {
        *errno_ptr = EINVAL;
        return -1;
    }

    // dnrt is already sized for count
    flex_mut_pinned drnt_pin = flex_pin_must_cleanup(drnt);
    if (drnt_pin.fault || drnt_pin.size < count * sizeof(dirent)) {
        *errno_ptr = EFAULT;
        return -1;
    }
    dirent *drnt_ptr = drnt_pin.ptr;

    ssize_t done_count = 0;
    fatfs_cluster_fat_any dir_cluster = fdsc->vn->inode;
    uint32_t local_offset = fdsc->offset;
    while (count >= sizeof(dirent)) {
        struct fatfs_entry entry;
        int read_result = fatfs_dir_entry_advancing_read(fdsc->vn->sb, &dir_cluster, &local_offset, &entry, errno_ptr);
        if (read_result < 0) {
            local_offset = fdsc->offset;    // Leave offset unchanged
            done_count = -1;
            break;
        } else if (read_result <= 0) {
            break;
        }

        uint8_t attributes = from_u8le(entry.attr);
        if (attributes & 0x08) {
            // Is volume label
            continue;
        } else if (entry.name[0] == 0x00) {
            // No more entries
            break;
        } else if (entry.name[0] == 0xE5) {
            // Free entry
            continue;
        }

        drnt_ptr->ino = from_u16le(entry.fst_clus_lo);

        uint8_t char_idx = 0;
        memcpy(&(drnt_ptr->name[char_idx]), entry.name + 0, 8);
        char_idx += 8;
        // Trim trailing spaces
        for (; char_idx > 0; --char_idx) {
            if (drnt_ptr->name[char_idx - 1] != ' ') {
                break;
            }
        }
        if (entry.name[8] != ' ') {
            drnt_ptr->name[char_idx++] = '.';
            memcpy(&(drnt_ptr->name[char_idx]), entry.name + 8, 3);
            char_idx += 3;
            // Trim trailing spaces
            for (; char_idx > 0; --char_idx) {
                if (drnt_ptr->name[char_idx - 1] != ' ') {
                    break;
                }
            }
        }
        drnt_ptr->name[char_idx] = '\0';

        drnt_ptr = (dirent *) ((char *) drnt_ptr + sizeof(dirent));
        done_count += sizeof(dirent);
        count -= sizeof(dirent);
    }
    fdsc->offset = local_offset;

    flex_unpin(&drnt_pin);
    drnt_ptr = NULL;

    return done_count;
}
