#include "kernel/export.h"
#include "kio/klog.h"

extern const exported_symbol _symbol_tab[];
extern const exported_symbol _symbol_tab_end[];

const exported_symbol *exported_symbol_from_name(const char *name) {
    for (exported_symbol *symbol_tab_ptr = _symbol_tab; symbol_tab_ptr < _symbol_tab_end; ++symbol_tab_ptr) {
        exported_symbol *symbol = symbol_tab_ptr;

        if (strcmp(name, symbol->name) == 0) {
            return symbol;
        }
    }

    return NULL;
}
