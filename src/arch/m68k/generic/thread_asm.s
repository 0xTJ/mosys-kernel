    .struct 0
thread_ssp:
    .struct thread_ssp + 4
thread_usp:
    .struct thread_usp + 4

    .text

| Return %address
| %fp
| %a5
| %a4
| %a3
| %a2
| %d7
| %d6
| %d5
| %d4
| %d3
| %d2

    .globl  thread_setup_stack
    .type   thread_setup_stack, @function
| void thread_setup_stack(thread *thread_ptr, void (*func)())
thread_setup_stack:
    move.l  (4,%sp),%a0             | thread_ptr to %a0
    move.l  (thread_ssp,%a0),%a1    | thread_ptr->ssp to %a1

    move.l  (8,%sp),-(%a1)          | Push func on %a1
    move.l  #thread_on_start,-(%a1) | Push thread_on_start on %a1
    movem.l %d2-%d7/%a2-%fp,-(%a1)  | Push saved registers on %a1

    move.l  %a1,(thread_ssp,%a0)    | %a1 to thread_ptr->ssp

    rts

    .globl  thread_switch
    .type   thread_switch, @function
| thread *thread_switch(thread *thread_ptr)
| Only to be called by thread_schedule()
thread_switch:
    move.l  (4,%sp),%a0             | thread_ptr to %a0

    movem.l %d2-%d7/%a2-%fp,-(%sp)  | Push saved registers

    move.l  %a0,%a2                 | thread_ptr to %a2

    jsr     thread_pre_switch

    move.l  thread_current,%a3      | thread_current to %a3
    move.l  %sp,(thread_ssp,%a3)    | SSP to thread_current->ssp
    move.l  %usp,%a4                | %usp to %a4
    move.l  %a4,(thread_usp,%a3)    | %usp in %a4 to thread_current->usp

    move.l  %a2,thread_current      | thread_ptr to thread_current
    move.l  (thread_usp,%a2),%a4    | thread_ptr->usp to %a4
    move.l  %a4,%usp                | thread_ptr->usp in %a4 to %usp
    move.l  (thread_ssp,%a2),%sp    | thread_ptr->ssp to SSP

    jsr     thread_post_switch

    move.l  %a3,%d0                 | Old thread_current to %d0

    movem.l (%sp)+,%d2-%d7/%a2-%fp  | Pop saved registers

    rts
