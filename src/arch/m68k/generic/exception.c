#include "exception.h"
#include "config.h"
#include "kio/klog.h"
#include "kio/kio.h"
#include "process/process.h"
#include "thread/thread.h"
#if EXCEPTION_TABLE_USE_ATOMIC
#include <stdatomic.h>
#endif
#include <string.h>

static void exception_unhandled(struct exception_state *state);
static void exception_access_fault(struct exception_state *state);
static void exception_address_error(struct exception_state *state);
static void exception_illegal_instruction(struct exception_state *state);
static void exception_integer_divide_by_zero(struct exception_state *state);
static void exception_chk_chk2_instructions(struct exception_state *state);
static void exception_ftrapcc_trapcc_trapv_instruction(struct exception_state *state);
static void exception_privilege_violation(struct exception_state *state);
static void exception_trace(struct exception_state *state);
static void exception_line_1010_emulator(struct exception_state *state);
static void exception_line_1111_emulator(struct exception_state *state);
static void exception_format_error(struct exception_state *state);
static void exception_uninitialized_interrupt(struct exception_state *state);
static void exception_spurious_interrupt(struct exception_state *state);

_Atomic exception_func exception_table[EXCEPTION_VECTOR_END] = {0};

exception_raw_func __attribute__((no_sanitize("undefined"))) exception_vector_raw_replace(exception_raw_func function, enum exception_vector vector) {
    kassert(vector < EXCEPTION_VECTOR_END);

    _Atomic exception_raw_func *const exception_vector_table = (_Atomic exception_raw_func *) 0x00000000;
    exception_raw_func old_func;
#if EXCEPTION_TABLE_USE_ATOMIC
    old_func = atomic_exchange(&exception_vector_table[vector], function);
#else
    old_func = exception_vector_table[vector];
    exception_vector_table[vector] = function;
#endif
    return old_func;
}

exception_func exception_vector_replace(exception_func function, enum exception_vector vector) {
    kassert(vector < EXCEPTION_VECTOR_END);

    exception_func old_func;
#if EXCEPTION_TABLE_USE_ATOMIC
    old_func = atomic_exchange(&exception_table[vector], function);
#else
    old_func = exception_table[vector];
    exception_table[vector] = function;
#endif
    // TODO: Find a way to also save raw vectors (struct with union?)
    exception_raw_func old_raw_func = exception_vector_raw_replace(exception_entry, vector);
    if (old_raw_func != exception_entry) {
        // TODO: Find another solution than assigning NULL to a function pointer
        old_func = NULL;
    }
    return old_func;
}

void exception_init(void) {
    for (int i = EXCEPTION_VECTOR_START; i < EXCEPTION_VECTOR_END; ++i) {
        if (
            i == EXCEPTION_VECTOR_RESET_INITIAL_INTERRUPT_STACK_POINTER ||  // Reset Initial Interrupt Stack Pointer
            i == EXCEPTION_VECTOR_RESET_INITIAL_PROGRAM_COUNTER         ||  // Reset Initial Program Counter
            (EXCEPTION_VECTOR_LEVEL_1_INTERRUPT_AUTOVECTOR <= i &&          // Interrupt Autovectors
                i <= EXCEPTION_VECTOR_LEVEL_7_INTERRUPT_AUTOVECTOR)     ||
            i == EXCEPTION_VECTOR_TRAP_INSTRUCTION_VECTORS + 14         ||  // TRAP 14
            (EXCEPTION_VECTOR_USER_DEFINED_VECTORS + 0 <= i &&              // MC68901
                i < EXCEPTION_VECTOR_USER_DEFINED_VECTORS + 16)
        ) {
            // Don't replace vector
        } else {
            exception_vector_replace(exception_unhandled, i);
        }
    }

    exception_vector_replace(
        exception_access_fault,
        EXCEPTION_VECTOR_ACCESS_FAULT);
    exception_vector_replace(
        exception_address_error,
        EXCEPTION_VECTOR_ADDRESS_ERROR);
    exception_vector_replace(
        exception_illegal_instruction,
        EXCEPTION_VECTOR_ILLEGAL_INSTRUCTION);
    exception_vector_replace(
        exception_integer_divide_by_zero,
        EXCEPTION_VECTOR_INTEGER_DIVIDE_BY_ZERO);
    exception_vector_replace(
        exception_chk_chk2_instructions,
        EXCEPTION_VECTOR_CHK_CHK2_INSTRUCTIONS);
    exception_vector_replace(
        exception_ftrapcc_trapcc_trapv_instruction,
        EXCEPTION_VECTOR_FTRAPCC_TRAPCC_TRAPV_INSTRUCTIONS);
    exception_vector_replace(
        exception_privilege_violation,
        EXCEPTION_VECTOR_PRIVILEGE_VIOLATION);
    exception_vector_replace(
        exception_trace,
        EXCEPTION_VECTOR_TRACE);
    exception_vector_replace(
        exception_line_1010_emulator,
        EXCEPTION_VECTOR_LINE_1010_EMULATOR);
    exception_vector_replace(
        exception_line_1111_emulator,
        EXCEPTION_VECTOR_LINE_1111_EMULATOR);
    exception_vector_replace(
        exception_format_error,
        EXCEPTION_VECTOR_FORMAT_ERROR);
    exception_vector_replace(
        exception_uninitialized_interrupt,
        EXCEPTION_VECTOR_UNINITIALIZED_INTERRUPT);
    exception_vector_replace(
        exception_spurious_interrupt,
        EXCEPTION_VECTOR_SPURIOUS_INTERRUPT);
}

void exception(struct exception_state *state) {
    unsigned short vector_offset = state->format_vector_offset & 0xFFF;
    enum exception_vector vector_number = vector_offset / 4;
    kassert(vector_number < EXCEPTION_VECTOR_END);

    if (exception_table[vector_number]) {
        exception_table[vector_number](state);
    } else {
        kpanic("High-level exception vector %hu is not initialized", (unsigned short) vector_number);
    }
}

void exception_stack_frame_print(struct exception_state *state, void *ssp) {
    const char *format4reg = "%4s 0x%08lX\t%4s 0x%08lX\t%4s 0x%08lX\t%4s 0x%08lX\n";

    kio_printf(format4reg,
               "D0", (unsigned long) state->d[0],
               "D1", (unsigned long) state->d[1],
               "D2", (unsigned long) state->d[2],
               "D3", (unsigned long) state->d[3]);
    kio_printf(format4reg,
               "D4", (unsigned long) state->d[4],
               "D5", (unsigned long) state->d[5],
               "D6", (unsigned long) state->d[6],
               "D7", (unsigned long) state->d[7]);
    kio_printf(format4reg,
               "A0", (unsigned long) state->a[0],
               "A1", (unsigned long) state->a[1],
               "A2", (unsigned long) state->a[2],
               "A3", (unsigned long) state->a[3]);
    kio_printf(format4reg,
               "A4", (unsigned long) state->a[4],
               "A5", (unsigned long) state->a[5],
               "A6", (unsigned long) state->a[6],
               "USP", (unsigned long) state->usp);
    kio_printf("%4s 0x%08lX\n",
               "SSP", (unsigned long) ssp);
    kio_printf("%4s 0x%04hX\t%4s 0x%08lX\t%4s 0x%01hX\t%4s 0x%03hX\n",
               "SR", (unsigned short) state->sr,
               "PC", (unsigned long) state->pc,
               "F", (unsigned short) (state->format_vector_offset >> 12),
               "VO", (unsigned short) (state->format_vector_offset & 0xFFFU));
}

static void exception_unhandled(struct exception_state *state) {
    kwarn("Unhandled Exception: 0x%hX", (unsigned short) ((state->format_vector_offset & 0xFFFU) / 4));
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}

static void exception_access_fault(struct exception_state *state) {
    kwarn("Access Fault:");
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}

static void exception_address_error(struct exception_state *state) {
    kwarn("Address Error:");
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}

static void exception_illegal_instruction(struct exception_state *state) {
    kwarn("Illegal Instruction:");
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}

static void exception_integer_divide_by_zero(struct exception_state *state) {
    kwarn("Integer Divide by Zero:");
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}

static void exception_chk_chk2_instructions(struct exception_state *state) {
    kwarn("CHK, CHK2 Instructions:");
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}

static void exception_ftrapcc_trapcc_trapv_instruction(struct exception_state *state) {
    kwarn("FTRAPcc, TRAPcc, TRAPV Instructions:");
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}

static void exception_privilege_violation(struct exception_state *state) {
    kwarn("Privilege Violation:");
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}

static void exception_trace(struct exception_state *state) {
#if THREAD_TRACE_COUNT
    struct exception_state *trace_state = &thread_current->trace_state[thread_current->trace_state_tail];
    uint32_t *trace_ssp = &thread_current->trace_state_ssp[thread_current->trace_state_tail];
    uint16_t *trace_instr = thread_current->trace_state_instr[thread_current->trace_state_tail];

    thread_current->trace_state_tail += 1;
    if (thread_current->trace_state_tail >= THREAD_TRACE_COUNT) {
        thread_current->trace_state_tail = 0;
    }

    memcpy(trace_state, state, sizeof(*trace_state));
    *trace_ssp = (uintptr_t) (state + 1);
    memcpy(trace_instr, (void *) state->pc, 4 * sizeof(*trace_instr));
#endif

    // Check for errors in registers
    bool found_error = false;
    exception_stack_frame_print(state, state + 1);
    kio_printf("\n");
    // TODO: Check that relevant pointers are in relevant spaces
    // if (!object_is_in_range(state + 1, 0, thread_current->sup_stack->range)) {
    //     kwarn("Out-of-range SSP");
    //     found_error = true;
    // }
    // if (!(state->sr & 0x2000)) {
    //     if (!object_is_in_range((void *) state->usp, 0, thread_current->usr_stack->range)) {
    //         kwarn("Out-of-range USP");
    //         found_error = true;
    //     }
    //     if (!object_is_in_range((void *) state->pc, 2, thread_current->owner_process->load_space->range)) {
    //         kwarn("Out-of-range PC");
    //         found_error = true;
    //     }
    // }

    if (found_error) {
        exception_stack_frame_print(state, state + 1);
        kpanic("No recovery implemented");
    }
}

static void exception_line_1010_emulator(struct exception_state *state) {
    kwarn("Line 1010 Emulator:");
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}

static void exception_line_1111_emulator(struct exception_state *state) {
    kwarn("Line 1111 Emulator:");
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}

static void exception_format_error(struct exception_state *state) {
    kwarn("Format Error:");
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}

static void exception_uninitialized_interrupt(struct exception_state *state) {
    kwarn("Uninitialized Interrupt:");
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}

static void exception_spurious_interrupt(struct exception_state *state) {
    kwarn("Spurious Interrupt:");
    exception_stack_frame_print(state, state + 1);
    kpanic("No recovery implemented");
}
