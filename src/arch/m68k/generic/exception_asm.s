    .text

    .globl  exception_entry
    .type   exception_entry, @function
exception_entry:
    sub.l   #4,%sp  | Leave space for USP
    movem.l %d0-%d7/%a0-%fp,-(%sp)
    move.l  %usp,%a1
    move.l  %a1,(60,%sp)

    | Add pointer to full exception frame onto stack before calling exception()
    move.l  %sp,-(%sp)
    jsr     exception
    add.l   #4,%sp

    .globl  exception_leave
    .type   exception_leave, @function
exception_leave:
    move.l  (60,%sp),%a1
    move.l  %a1,%usp
    movem.l (%sp)+,%d0-%d7/%a0-%fp
    add.l   #4,%sp  | Remove space for USP
    rte
