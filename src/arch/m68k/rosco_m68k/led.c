#include "led.h"
#include "gpio.h"
#include "kio/klog.h"

static bool led_states[LED_END];

static const gpio_num led_gpio_nums[LED_END] = {
    [LED_RED] = GPIO_NUM_LED_RED,
    [LED_GREEN] = GPIO_NUM_LED_GREEN,
};

void led_init(void) {
    for (led ld = 0; ld < LED_END; ++ld) {
        led_set(ld, false);
    }
}

bool led_get(led ld) {
    kassert(ld < LED_END);
    return led_states[ld];
}

void led_set(led ld, bool state) {
    kassert(ld < LED_END);
    led_states[ld] = state;
    gpio_output_write(led_gpio_nums[ld], !led_states[ld]);
}

void led_toggle(led ld) {
    kassert(ld < LED_END);
    led_states[ld] = !led_states[ld];
    gpio_output_write(led_gpio_nums[ld], !led_states[ld]);
}
