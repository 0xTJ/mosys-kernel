#include "mem_arch.h"
#include "kio/klog.h"
#include "mem/mem.h"
#include "ram_expansion.h"
#include "util/util.h"
#include <limits.h>

extern max_align_t _kheap_block0[];
extern max_align_t _kheap_block0_end[];
extern max_align_t _kheap_block1[];
extern max_align_t _kheap_block1_end[];

static void mem_arch_add_onboard(mem_addr start, mem_addr end) {
    kassert(start <= end);

    kinfo("Physical memory block at %p-%p", (void *) start, (void *) end);
    for (mem_addr this_page_addr = start; this_page_addr < end; this_page_addr += MEM_PAGE_SIZE) {
        mem_page this_page = this_page_addr / MEM_PAGE_SIZE;
        size_t map_block = this_page / MEM_BITMAP_ENTRY_SIZE;
        unsigned map_bit = this_page % MEM_BITMAP_ENTRY_SIZE;
        mem_bitmap[map_block] &= ~(MEM_BITMAP_C(1) << map_bit);
    }
}

void mem_arch_init(void) {
    const mem_addr onboard_ram0_start = round_up((mem_addr) _kheap_block0, MEM_PAGE_SIZE);
    const mem_addr onboard_ram0_end = round_down((mem_addr) _kheap_block0_end, MEM_PAGE_SIZE);
    mem_arch_add_onboard(onboard_ram0_start, onboard_ram0_end);

    const mem_addr onboard_ram1_start = round_up((mem_addr) _kheap_block1, MEM_PAGE_SIZE);
    const mem_addr onboard_ram1_end = round_down((mem_addr) _kheap_block1_end, MEM_PAGE_SIZE);
    mem_arch_add_onboard(onboard_ram1_start, onboard_ram1_end);

    // Add all installed 16-bit-wide expansion RAM
    for (ram_expansion_block block = 0; ; ++block) {
        void *base;
        size_t size;
        ram_expansion_chips this_chips = ram_expansion_get_block(block, &base, &size);

        if (this_chips == RAM_EXPANSION_CHIPS_BOTH) {
            // Useable block, add to free memory
            mem_addr usage_start = (mem_addr) base;
            mem_addr usage_end = usage_start + size;

            size_t usage_page_start = usage_start / MEM_PAGE_SIZE;
            size_t usage_page_end = usage_end / MEM_PAGE_SIZE;

            if (usage_page_start % MEM_BITMAP_ENTRY_SIZE || usage_page_end % MEM_BITMAP_ENTRY_SIZE) {
                kpanic("Physical memory blocks don't have %u page alignment", (unsigned) MEM_BITMAP_ENTRY_SIZE);
            }

            size_t entries_start = usage_page_start / MEM_BITMAP_ENTRY_SIZE;
            size_t entries_end = usage_page_end / MEM_BITMAP_ENTRY_SIZE;

            kinfo("Physical memory block at %p-%p", (void *) usage_start, (void *) usage_end);

            for (size_t i = entries_start; i < entries_end; ++i) {
                mem_bitmap[i] = 0;
            }
        } else if (this_chips == RAM_EXPANSION_CHIPS_INVALID) {
            // No more blocks, leave loop
            break;
        } else {
            // At least one chip in each bank not installed, try next block
        }
    }
}
