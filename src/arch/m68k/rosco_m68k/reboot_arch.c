#include "reboot_arch.h"
#include "util/util.h"
#include "thread/thread.h"
#include "mem_arch.h"

noreturn void reboot_arch_restart(const char *arg) {
    UNUSED(arg);
    __asm__ volatile (
        "ori #(7 << 8), %%sr\n"
        "movea.l %[inital_pc], %%a0\n"
        "movea.l %[inital_ssp], %%sp\n"
        "reset\n"
        "jmp (%%a0)\n"
        :
        : [inital_ssp] "m" (((void **) (mem_rom_start))[0]),
          [inital_pc]  "m" (((void **) (mem_rom_start))[1])
    );
    unreachable();
}

noreturn void reboot_arch_halt(void) {
    reboot_arch_restart(NULL);
}

noreturn void reboot_arch_power_off(void) {
    while (1) {
        __asm__ ("stop #0x2700");
    }
}
