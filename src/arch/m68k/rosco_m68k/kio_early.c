#include "kio/kio.h"
#include "firmware.h"

inline void kio_early_putc(int c) {
    firmware_trap_14_printchar(c);
}

void kio_early_puts(char s[]) {
    for (size_t i = 0; s[i]; ++i) {
        kio_early_putc(s[i]);
    }
}
