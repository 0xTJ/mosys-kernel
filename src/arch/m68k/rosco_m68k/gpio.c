#include "gpio.h"
#include "kio/klog.h"

#if GPIO_VERIFY
void gpio_assert_valid_pindir(gpio_mask mask, gpio_pindir pindir) {
    const gpio_mask mask_to_inputs_start = gpio_to_mask(GPIO_NUM_INPUTS_START) - 1;
    const gpio_mask mask_to_inputs_end = gpio_to_mask(GPIO_NUM_INPUTS_END) - 1;
    const gpio_mask mask_to_outputs_start = gpio_to_mask(GPIO_NUM_OUTPUTS_START) - 1;
    const gpio_mask mask_to_outputs_end = gpio_to_mask(GPIO_NUM_OUTPUTS_END) - 1;
    const gpio_mask mask_inputs = mask_to_inputs_end & ~mask_to_inputs_start;
    const gpio_mask mask_outputs = mask_to_outputs_end & ~mask_to_outputs_start;

    if (pindir == GPIO_PINDIR_INPUT && mask & ~mask_inputs) {
        kpanic("Asserted non-input pins as input: %08lX", (unsigned long) (mask & ~mask_inputs));
    } else if (pindir == GPIO_PINDIR_OUTPUT && mask & ~mask_outputs) {
        kpanic("Asserted non-output pins as output: %08lX", (unsigned long) (mask & ~mask_outputs));
    }
}
#endif
