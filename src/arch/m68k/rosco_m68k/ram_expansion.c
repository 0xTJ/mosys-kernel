#include "ram_expansion.h"
#include "mem/mem.h"
#include "safeprobe.h"
#include "util/util.h"

static const uintptr_t expansion_base = (size_t) mem_expansion_start;

ram_expansion_chips ram_expansion_populated[RAM_EXPANSION_BANKS_MAX];   // Filled with RAM_EXPANSION_CHIPS_NONE

static ram_expansion_chips probe_bank(ram_expansion_bank bank) {
    uintptr_t bank_base = expansion_base + bank * RAM_EXPANSION_BANK_SIZE;
    volatile uint16_t *bank_base_ptr = (volatile uint16_t *) bank_base;

    safeprobe_begin();
    bank_base_ptr[0] = (bswap_u16(bank) ^ bank);
    bank_base_ptr[1] = ~(bswap_u16(bank) ^ bank);
    register uint16_t eval_val = ~(bank_base_ptr[0] ^ bank_base_ptr[1]) | (bank_base_ptr[0] ^ (bswap_u16(bank) ^ bank)) | (bank_base_ptr[1] ^ ~(bswap_u16(bank) ^ bank));
    if (!safeprobe_end_was_clean()) {
        return RAM_EXPANSION_CHIPS_NONE;
    }

    if ((eval_val & 0xFFFF) == 0) {
        return RAM_EXPANSION_CHIPS_BOTH;
    } else if ((eval_val & 0xFF00) == 0) {
        return RAM_EXPANSION_CHIPS_EVEN;
    } else if ((eval_val & 0x00FF) == 0) {
        return RAM_EXPANSION_CHIPS_ODD;
    } else {
        return RAM_EXPANSION_CHIPS_NONE;
    }
}

ram_expansion_chip_count ram_expansion_scan(void) {
    ram_expansion_chip_count chips_present = 0;
    for (ram_expansion_bank bank = 0; bank < RAM_EXPANSION_BANKS; ++bank) {
        ram_expansion_chips probe_result = probe_bank(bank);
        ram_expansion_populated[bank] = probe_result;
        chips_present +=
            probe_result == RAM_EXPANSION_CHIPS_BOTH ?
                2 :
            probe_result == RAM_EXPANSION_CHIPS_EVEN ?
                1 :
            probe_result == RAM_EXPANSION_CHIPS_ODD  ?
                1 :
            //              RAM_EXPANSION_CHIPS_NONE
                0;
    }
    return chips_present;
}

ram_expansion_bank_count ram_expansion_block_length_at_bank(ram_expansion_bank bank) {
    if (bank >= RAM_EXPANSION_BANKS) {
        return 0;
    }

    ram_expansion_chips bank_chips = ram_expansion_populated[bank];
    ram_expansion_bank this_bank;
    for (this_bank = bank; this_bank < RAM_EXPANSION_BANKS; ++this_bank) {
        if (ram_expansion_populated[this_bank] != bank_chips) {
            break;
        }
    }

    return this_bank - bank;
}

ram_expansion_bank ram_expansion_bank_from_block(ram_expansion_block block) {
    ram_expansion_bank found_bank = 0;

    for (ram_expansion_block this_block = 0; this_block < block; ++this_block) {
        ram_expansion_bank_count this_length = ram_expansion_block_length_at_bank(found_bank);
        found_bank += this_length;
        if (this_length == 0) {
            break;
        }
    }

    return found_bank;
}

ram_expansion_chips ram_expansion_get_block(unsigned short block, void **base, size_t *size) {
    ram_expansion_bank found_bank = ram_expansion_bank_from_block(block);
    if (found_bank == RAM_EXPANSION_BANKS) {
        return RAM_EXPANSION_CHIPS_INVALID;
    }

    *base = (void *) (expansion_base + found_bank * RAM_EXPANSION_BANK_SIZE);
    *size = ram_expansion_block_length_at_bank(found_bank) * RAM_EXPANSION_BANK_SIZE;

    return ram_expansion_populated[found_bank];
}
