#include "ubsan/ubsan_value.h"
#include <stddef.h>
#include <string.h>

const char *ubsan_get_objc_class_name(ubsan_value_handle pointer) {
    (void) pointer;
    return NULL;
}

ubsan_s_int_max ubsan_value_get_s_int_value(const ubsan_value *this) {
    assert(ubsan_type_descriptor_is_signed_integer_ty(ubsan_value_get_type(this)));
    if (ubsan_value_is_inline_int(this)) {
        const unsigned extra_bits =
            sizeof(ubsan_s_int_max) * 8 -
            ubsan_type_descriptor_get_integer_bit_width(ubsan_value_get_type(this));
        return (ubsan_s_int_max) ((ubsan_u_int_max) this->val << extra_bits) >> extra_bits;
    }
    if (ubsan_type_descriptor_get_integer_bit_width(ubsan_value_get_type(this)) == 64) {
        return *(int64_t *)this->val;
    } else if (ubsan_type_descriptor_get_integer_bit_width(ubsan_value_get_type(this)) == 128) {
#if __SIZEOF_INT128__
        return *(__int128 *)this->val;
#else
        __builtin_unreachable();
#endif
    }
    __builtin_unreachable();
}

ubsan_u_int_max ubsan_value_get_u_int_value(const ubsan_value *this) {
    assert(ubsan_type_descriptor_is_unsigned_integer_ty(ubsan_value_get_type(this)));
    if (ubsan_value_is_inline_int(this)) {
        return this->val;
    }
    if (ubsan_type_descriptor_get_integer_bit_width(ubsan_value_get_type(this)) == 64) {
        return *(uint64_t *)this->val;
    } else if (ubsan_type_descriptor_get_integer_bit_width(ubsan_value_get_type(this)) == 128) {
#if __SIZEOF_INT128__
        return *(unsigned __int128 *)this->val;
#else
        __builtin_unreachable();
#endif
    }
    __builtin_unreachable();
}

ubsan_u_int_max ubsan_value_get_positive_int_value(const ubsan_value *this) {
    if (ubsan_type_descriptor_is_unsigned_integer_ty(ubsan_value_get_type(this))) {
        return ubsan_value_get_u_int_value(this);
    }
    ubsan_s_int_max val = ubsan_value_get_s_int_value(this);
    assert(val >= 0);
    return val;
}

ubsan_float_max ubsan_value_get_float_value(const ubsan_value *this) {
    assert(ubsan_type_descriptor_is_float_ty(ubsan_value_get_type(this)));
    if (ubsan_value_is_inline_float(this)) {
        switch(ubsan_type_descriptor_get_float_bit_width(ubsan_value_get_type(this))) {
        case 32: {
            float value;
#if defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
            memcpy(&value, ((const char *) (&this->val + 1)) - 4, 4);
#else
            memcpy(&value, &this->val, 4);
#endif
            return value;
        }
        case 64: {
            double value;
            memcpy(&value, &this->val, 8);
            return value;
        }
        }
    } else {
        switch(ubsan_type_descriptor_get_float_bit_width(ubsan_value_get_type(this))) {
        case 64: return *(double *) this->val;
        case 80: return *(long double *) this->val;
        case 96: return *(long double *) this->val;
        case 128: return *(long double *) this->val;
        }
    }
    __builtin_unreachable();
}
