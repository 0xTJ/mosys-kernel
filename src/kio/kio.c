#include "kio/kio.h"
#include "kio/klog.h"
#include "thread/thread.h"
#include "device/console.h"
#include "util/mutex.h"
#include <stdbool.h>
#include <string.h>

#define NANOPRINTF_IMPLEMENTATION
#define NANOPRINTF_USE_FIELD_WIDTH_FORMAT_SPECIFIERS    1
#define NANOPRINTF_USE_PRECISION_FORMAT_SPECIFIERS      1
#define NANOPRINTF_USE_FLOAT_FORMAT_SPECIFIERS          0
#define NANOPRINTF_USE_LARGE_FORMAT_SPECIFIERS          1
#define NANOPRINTF_USE_BINARY_FORMAT_SPECIFIERS         1
#define NANOPRINTF_USE_WRITEBACK_FORMAT_SPECIFIERS      1
#define NANOPRINTF_VISIBILITY_STATIC

#include "kio/nanoprintf.h"

static bool kio_use_console = false;
static mutex kio_mtx = MUTEX_INIT;

void kio_set_use_console(bool use_console) {
    MUTEX_WITH(&kio_mtx, {
        kio_use_console = use_console;
    })
}

static void kio_putc_nolock(int c, void *ctx) {
    UNUSED(ctx);

    if (!kio_use_console) {
        switch (c) {
        case '\n':
            kio_early_putc('\r');
            // Fall through
        default:
            kio_early_putc(c);
            break;
        }
    } else {
        int errno_dummy;
        char c_char = c;
        console_write(flex_const_kernel(&c_char, 1), 1, 0, &errno_dummy);
    }
}

void kio_putc(int c, void *ctx) {
    // Use a local dummy mutex if it's not safe to block yet
    mutex dummy_mtx = MUTEX_INIT;
    mutex *mtx = thread_inited() ? &kio_mtx : &dummy_mtx;
    MUTEX_WITH(mtx, {
        kio_putc_nolock(c, ctx);
    })
}

void kio_puts(char s[], void *ctx) {
    UNUSED(ctx);

    // Use a local dummy mutex if it's not safe to block yet
    mutex dummy_mtx = MUTEX_INIT;
    mutex *mtx = thread_inited() ? &kio_mtx : &dummy_mtx;
    MUTEX_WITH(mtx, {
        if (!kio_use_console) {
            kio_early_puts(s);
        } else {
            size_t s_size = strlen(s) + 1;
            int errno_dummy;
            console_write(flex_const_kernel(s, s_size), s_size, 0, &errno_dummy);
        }
    })
}

int kio_vprintf(char const *format, va_list vlist) {
    int result;

    // Use a local dummy mutex if it's not safe to block yet
    mutex dummy_mtx = MUTEX_INIT;
    mutex *mtx = thread_inited() ? &kio_mtx : &dummy_mtx;
    MUTEX_WITH(mtx, {
        result = npf_vpprintf(kio_putc_nolock, NULL, format, vlist);
    })

    return result;
}

int kio_printf(char const *format, ...) {
    va_list vlist;
    va_start(vlist, format);
    int result = kio_vprintf(format, vlist);
    va_end(vlist);
    return result;
}

int kio_vsnprintf(char *buffer, size_t bufsz, char const *format, va_list vlist) {
    return npf_vsnprintf(buffer, bufsz, format, vlist);
}

int kio_snprintf(char *buffer, size_t bufsz, char const *format, ...) {
    va_list vlist;
    va_start(vlist, format);
    int result = kio_vsnprintf(buffer, bufsz, format, vlist);
    va_end(vlist);
    return result;
}
