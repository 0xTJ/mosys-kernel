#include "mem/mem.h"
#include "config.h"
#include "kio/klog.h"
#include "util/mutex.h"
#include "ram_expansion.h"
#include "util/util.h"
#include <string.h>

mem_bitmap_entry mem_bitmap[MEM_BITMAP_ENTRY_COUNT];
static mutex mem_bitmap_mtx = MUTEX_INIT;

void mem_init(void) {
    MUTEX_WITH(&mem_bitmap_mtx, {
        fill_one(&mem_bitmap);
        mem_arch_init();
    })
}

mem_page_block mem_alloc_pages(size_t count) {
    if (count > MEM_BITMAP_ENTRY_SIZE) {
        kpanic("Can't allocate more than %u pages", (unsigned) MEM_BITMAP_ENTRY_SIZE);
    }

    mem_bitmap_entry pages_mask = (count < MEM_BITMAP_ENTRY_SIZE ? MEM_BITMAP_C(1) << count : 0) - 1;

    size_t map_index;
    unsigned char bit_index;
    MUTEX_WITH(&mem_bitmap_mtx, {
        for (map_index = 0; map_index < MEM_BITMAP_ENTRY_COUNT; ++map_index) {
            mem_bitmap_entry this_map_block = mem_bitmap[map_index];
            mem_bitmap_entry next_map_block = map_index + 1 < MEM_BITMAP_ENTRY_COUNT ? mem_bitmap[map_index + 1] : ~MEM_BITMAP_C(0);

            // If a block is completely used, skip it
            if (this_map_block == ~MEM_BITMAP_C(0)) {
                continue;
            }

            for (bit_index = 0; bit_index <= MEM_BITMAP_ENTRY_SIZE; ++bit_index) {
                mem_bitmap_entry shifted_map_block = bit_index < MEM_BITMAP_ENTRY_SIZE ? this_map_block >> bit_index : 0;
                if (bit_index > 0) {
                    shifted_map_block |= next_map_block << (MEM_BITMAP_ENTRY_SIZE - bit_index);
                }

                mem_bitmap_entry used_pages = shifted_map_block & pages_mask;
                if (used_pages == 0) {
                    // Found free pages
                    if (bit_index < MEM_BITMAP_ENTRY_SIZE) {
                        mem_bitmap[map_index] |= pages_mask << bit_index;
                    }
                    if (map_index + 1 < MEM_BITMAP_ENTRY_COUNT && bit_index > 0) {
                        mem_bitmap[map_index + 1] |= pages_mask >> (MEM_BITMAP_ENTRY_SIZE - bit_index);
                    }
                    break;
                }
            }

            if (bit_index <= MEM_BITMAP_ENTRY_SIZE) {
                break;
            }
        }
    })

    if (map_index < MEM_BITMAP_ENTRY_COUNT) {
        mem_page base = (map_index * MEM_BITMAP_ENTRY_SIZE) + bit_index;
        return (mem_page_block) { base, count };
    } else {
        // mem_dump_free_pages();
        return MEM_PAGE_BLOCK_NULL;
    }
}

void mem_free_pages(mem_page_block page_block) {
    if (page_block.count > MEM_BITMAP_ENTRY_SIZE) {
        kpanic("Can't free more than %u pages", (unsigned) MEM_BITMAP_ENTRY_SIZE);
    }

    size_t map_index = page_block.base / MEM_BITMAP_ENTRY_SIZE;
    unsigned char bit_index = page_block.base % MEM_BITMAP_ENTRY_SIZE;

    mem_bitmap_entry pages_mask = (page_block.count < MEM_BITMAP_ENTRY_SIZE ? MEM_BITMAP_C(1) << page_block.count : 0) - 1;


    MUTEX_WITH(&mem_bitmap_mtx, {
        mem_bitmap[map_index] &= ~(pages_mask << bit_index);
        if (map_index + 1 < MEM_BITMAP_ENTRY_COUNT && bit_index > 0) {
            mem_bitmap[map_index + 1] &= ~(pages_mask >> (MEM_BITMAP_ENTRY_SIZE - bit_index));
        }
    })
}

mem_page_block mem_clone_pages(mem_page_block page_block) {
    mem_page_block new_page_block = mem_alloc_pages(page_block.count);
    if (mem_page_block_is_present(new_page_block)) {
        // TODO: Only handle direct physical memory
        memcpy((char *) (new_page_block.base * MEM_PAGE_SIZE),
               (char *) (page_block.base * MEM_PAGE_SIZE),
               page_block.count * MEM_PAGE_SIZE);
    }
    return new_page_block;
}

mem_block mem_alloc(size_t size) {
    if (size % MEM_PAGE_SIZE) {
        kpanic("Tried to allocate a non-page-sized number of physical bytes");
    }

    return mem_page_block_to_block(mem_alloc_pages(size / MEM_PAGE_SIZE));
}

void mem_free(mem_block block) {
    if (!mem_block_is_page_aligned(block)) {
        kpanic("Tried to free a non-page-sized number of physical bytes");
    }

    mem_free_pages(mem_block_to_page_block(block));
}

mem_block mem_clone(mem_block block) {
    if (!mem_block_is_page_aligned(block)) {
        kpanic("Tried to clone a non-page-sized number of physical bytes");
    }

    return mem_page_block_to_block(mem_clone_pages(mem_block_to_page_block(block)));
}

void mem_dump_free_pages(void) {
    MUTEX_WITH(&mem_bitmap_mtx, {
        for (mem_page block_page = 0;
            block_page < MEM_PAGE_COUNT;
            ++block_page
        ) {
            size_t map_block = block_page / MEM_BITMAP_ENTRY_SIZE;
            unsigned map_bit = block_page % MEM_BITMAP_ENTRY_SIZE;
            if (!(mem_bitmap[map_block] & (MEM_BITMAP_C(1) << map_bit))) {
                kinfo("Page %u is free", (unsigned) block_page);
            }
        }
    })
}

#if MEM_USE_MMU
#error Not implemented
#endif
