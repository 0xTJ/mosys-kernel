#include "mem/dynobj.h"

void *dynobj_base_new_with_type(const char *type, size_t size, dynobj_cleanup cleanup) {
    ptr_range obj_range = kalloc_range_with_desc(size, type);
    if (ptr_range_is_null(obj_range)) {
        return NULL;
    }

    dynobj_base *dnobj = obj_range.start;
    dnobj->dyn_mem = obj_range;
    dnobj->ref_count = 1;
    dnobj->cleanup = cleanup;

    return obj_range.start;
}

void *dynobj_base_new_no_kheap_with_type(const char *type, size_t size, dynobj_cleanup cleanup, void *obj) {
    UNUSED(size);
    UNUSED(type);

    dynobj_base *dnobj = obj;
    dnobj->dyn_mem = PTR_RANGE_NULL;
    dnobj->ref_count = 1;
    dnobj->cleanup = cleanup;

    return obj;
}

void dynobj_base_ref_with_type(const char *type, dynobj_base *dnobj) {
    if (dnobj->ref_count == 0) {
        kpanic("Referenced an unreferenced dynobj");
    } else if (dnobj->ref_count == SIZE_MAX) {
        kpanic("Overflowed dynobj ref_count");
    }
    dnobj->ref_count += 1;
}

// Returns true if the dnobj still exists
bool dynobj_base_unref_with_type(const char *type, dynobj_base *dnobj) {
    if (dnobj->ref_count == 0) {
        kpanic("Underflowed dynobj ref_count");
    }
    dnobj->ref_count -= 1;

    if (dnobj->ref_count == 0) {
        dnobj->cleanup(dnobj);
        if (!ptr_range_is_null(dnobj->dyn_mem)) {
            kfree_range(dnobj->dyn_mem);
        }
        return false;
    } else {
        return true;
    }
}
