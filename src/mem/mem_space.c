#include "mem/mem_space.h"
#include "kio/klog.h"

static void mem_space_delete_maps_nolock(mem_space *mm_spc);
static void mem_space_release_maps_array_nolock(mem_space *mm_spc);
static bool mem_space_expand_nolock(mem_space *mm_spc);
static bool mem_space_append_nolock(mem_space *mm_spc, mem_mapping *map);

void mem_space_init(mem_space *mm_spc) {
    *mm_spc = MEM_SPACE_INIT;
}

void mem_space_delete_maps(mem_space *mm_spc) {
    MUTEX_WITH(&mm_spc->mtx, {
        mem_space_delete_maps_nolock(mm_spc);
    })
}

static void mem_space_delete_maps_nolock(mem_space *mm_spc) {
    for (size_t i = 0; i < mm_spc->maps_used; ++i) {
        mem_mapping_delete(mm_spc->maps[i]);
    }
    mm_spc->maps_used = 0;
}

void mem_space_release_maps_array(mem_space *mm_spc) {
    MUTEX_WITH(&mm_spc->mtx, {
        mem_space_release_maps_array_nolock(mm_spc);
    })
}

static void mem_space_release_maps_array_nolock(mem_space *mm_spc) {
    if (mm_spc->maps_used) {
        mem_space_delete_maps_nolock(mm_spc);
    }
    mm_spc->maps_space = 0;
    kfree(mm_spc->maps);
    mm_spc->maps = NULL;
}

void mem_space_replace(mem_space *dest_mm_spc, mem_space *src_mm_spc) {
    MUTEX_WITH(&dest_mm_spc->mtx, {
        mem_space_release_maps_array_nolock(dest_mm_spc);

        MUTEX_WITH(&src_mm_spc->mtx, {
            dest_mm_spc->maps_space = src_mm_spc->maps_space;
            dest_mm_spc->maps_used = src_mm_spc->maps_used;
            dest_mm_spc->maps = src_mm_spc->maps;

            src_mm_spc->maps_space = 0;
            src_mm_spc->maps_used = 0;
            src_mm_spc->maps = NULL;
        })
    })
}

bool mem_space_expand(mem_space *mm_spc) {
    bool result;
    MUTEX_WITH(&mm_spc->mtx, {
        result = mem_space_expand_nolock(mm_spc);
    })
    return result;
}

static bool mem_space_expand_nolock(mem_space *mm_spc) {
    if (mm_spc->maps_space > SIZE_MAX / 2) {
        return false;
    }

    // Allocate doubled space
    size_t new_maps_space = mm_spc->maps_space == 0 ? MEM_SPACE_MAPS_INIT_COUNT : mm_spc->maps_space * 2;
    mem_mapping **new_maps = kalloc(new_maps_space * sizeof(mem_mapping *));
    if (!new_maps) {
        return false;
    }

    // Copy all existing entries
    memcpy(new_maps, mm_spc->maps, mm_spc->maps_space * sizeof(mem_mapping *));

    // Update values
    mm_spc->maps_space = new_maps_space;
    mm_spc->maps = new_maps;

    return true;
}

bool mem_space_append(mem_space *mm_spc, mem_mapping *map) {
    bool result;
    MUTEX_WITH(&mm_spc->mtx, {
        result = mem_space_append_nolock(mm_spc, map);
    })
    return result;
}

static bool mem_space_append_nolock(mem_space *mm_spc, mem_mapping *map) {
    kassert(mm_spc->maps_used <= mm_spc->maps_space);

    // Expand if needed
    if (mm_spc->maps_used == mm_spc->maps_space) {
        if (!mem_space_expand_nolock(mm_spc)) {
            return true;
        }
    }

    mm_spc->maps[mm_spc->maps_used++] = map;
    return true;
}

bool mem_space_clone(mem_space *dest_mm_spc, mem_space *src_mm_spc) {
    mem_mapping **new_maps;
    size_t new_maps_count;

    MUTEX_WITH(&src_mm_spc->mtx, {
        new_maps_count = src_mm_spc->maps_used;

        // Allocate a maps table, only if needed
        if (new_maps_count > 0) {
            new_maps = kalloc(new_maps_count * sizeof(mem_mapping *));
            if (!new_maps) {
                MUTEX_LEAVE_UNLOCK();
            }
        } else {
            new_maps = NULL;
        }

        // Clone with reference (points to same memory) all mappings
        for (size_t i = 0; i < new_maps_count; ++i) {
            new_maps[i] = mem_mapping_clone_ref(src_mm_spc->maps[i]);

            // On failed cloning, undo all work
            if (!new_maps[i]) {
                for (size_t undo_i = 0; undo_i < new_maps_count; ++undo_i) {
                    mem_mapping_delete(new_maps[undo_i]);
                }
                kfree(new_maps);
                new_maps = NULL;
                break;  // Break for loop, allow mutex to unlock
            }
        }
    })

    if (!new_maps && new_maps_count > 0) {
        return false;
    }

    MUTEX_WITH(&dest_mm_spc->mtx, {
        mem_space_release_maps_array_nolock(dest_mm_spc);

        dest_mm_spc->maps_space = new_maps_count;
        dest_mm_spc->maps_used = new_maps_count;
        dest_mm_spc->maps = new_maps;
    })

    return true;
}

mem_mapping *mem_space_find_map(mem_space *mm_spc, umem_ptr u_ptr) {
    mem_mapping *result;
    MUTEX_WITH(&mm_spc->mtx, {
        result = mem_space_find_map_nolock(mm_spc, u_ptr);
    })
    return result;
}

mem_mapping *mem_space_find_map_nolock(mem_space *mm_spc, umem_ptr u_ptr) {
    for (size_t i = 0; i < mm_spc->maps_used; ++i) {
        mem_mapping *map = mm_spc->maps[i];
        uintptr_t u_at = (uintptr_t) mem_mapping_u_at(map);
        if ((uintptr_t) u_ptr >= u_at &&
            (uintptr_t) u_ptr < u_at + map->size) {
            return map;
        }
    }
    return NULL;
}

void *mem_space_u_to_k(mem_space *mm_spc, umem_ptr u_ptr) {
    void *result;
    MUTEX_WITH(&mm_spc->mtx, {
        result = mem_space_u_to_k_nolock(mm_spc, u_ptr);
    })
    return result;
}

void *mem_space_u_to_k_nolock(mem_space *mm_spc, umem_ptr u_ptr) {
    mem_mapping *map = mem_space_find_map_nolock(mm_spc, u_ptr);
    if (!map) {
        return NULL;
    }
    return mem_mapping_u_to_k(map, u_ptr);
}

void *mem_space_u_to_k_sized(mem_space *mm_spc, umem_ptr u_ptr, size_t size) {
    void *result;
    MUTEX_WITH(&mm_spc->mtx, {
        result = mem_space_u_to_k_sized_nolock(mm_spc, u_ptr, size);
    })
    return result;
}

void *mem_space_u_to_k_sized_nolock(mem_space *mm_spc, umem_ptr u_ptr, size_t size) {
    // TODO: Allow bridging across maps
    mem_mapping *map = mem_space_find_map_nolock(mm_spc, u_ptr);
    if (!map || mem_mapping_offset_in(map, u_ptr) + size >= map->size) {
        return NULL;
    }
    return mem_mapping_u_to_k(map, u_ptr);
}
