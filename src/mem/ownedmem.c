#include "mem/ownedmem.h"
#include "kalloc.h"
#include "kio/klog.h"

ownedmem_ptr ownedmem_new_from_mem_block(mem_block block) {
    ownedmem_ptr owndmm = dynobj_mtx_new(ownedmem, ownedmem_cleanup);
    if (owndmm) {
        owndmm->block = block;
    }
    return owndmm;
}

void ownedmem_new_from_mem_block_no_kheap(mem_block block, ownedmem *owndmm) {
    dynobj_mtx_new_no_kheap(ownedmem, ownedmem_cleanup, owndmm);
    owndmm->block = block;
}

ownedmem_ptr ownedmem_new(size_t size) {
    mem_block block = mem_alloc(size);
    ownedmem_ptr owndmm = NULL;
    if (mem_block_is_present(block)) {
        owndmm = ownedmem_new_from_mem_block(block);
        if (!owndmm) {
            mem_free(block);
        }
    }
    return owndmm;
}

bool ownedmem_new_no_kheap(size_t size, ownedmem *owndmm) {
    mem_block block = mem_alloc(size);
    if (mem_block_is_present(block)) {
        ownedmem_new_from_mem_block_no_kheap(block, owndmm);
        return true;
    }
    return false;
}

void ownedmem_cleanup(void *owndmm_arg) {
    ownedmem_ptr owndmm = owndmm_arg;
    if (!owndmm) {
        return;
    }

    if (mem_block_is_present(owndmm->block)) {
        mem_free(owndmm->block);
        owndmm->block = MEM_BLOCK_NULL;
    }
}

ownedmem_ptr ownedmem_ref(ownedmem_ptr owndmm) {
    if (!owndmm) {
        return NULL;
    }

    dynobj_mtx_ref(ownedmem, &owndmm->dnobj);

    return owndmm;
}

void ownedmem_unref(ownedmem_ptr owndmm) {
    if (!owndmm) {
        return;
    }

    dynobj_mtx_unref(ownedmem, &owndmm->dnobj);
}

ownedmem_ptr ownedmem_clone(ownedmem_ptr owndmm_orig) {
    if (!owndmm_orig) {
        return NULL;
    }

    mem_block cloned_block;
    MUTEX_WITH(&owndmm_orig->dnobj.mtx, {
        cloned_block = mem_clone(owndmm_orig->block);
    })

    ownedmem_ptr owndmm = NULL;
    if (mem_block_is_present(cloned_block)) {
        owndmm = ownedmem_new_from_mem_block(cloned_block);
    }

    if (!owndmm) {
        mem_free(cloned_block);
    }

    return owndmm;
}
