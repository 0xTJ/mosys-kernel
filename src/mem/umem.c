#include "mem/umem.h"
#include "config.h"
#include "mem/mem.h"
#include "process/process.h"
#include "thread/thread.h"
#include <string.h>

#if MEM_USE_MMU
#   error "Umem is not implemented for MEM_USE_MMU."
#else
bool umem_copy_kk(void *dest, const void *src, size_t count) {
    memmove(dest, src, count);
    return true;
}

bool umem_copy_ku(void *dest, umem_ptr_const src, size_t count) {
    memmove(dest, (const void *) src, count);
    return true;
}

bool umem_copy_uk(umem_ptr dest, const void *src, size_t count) {
    memmove((void *) dest, src, count);
    return true;
}

bool umem_copy_uu(umem_ptr dest, umem_ptr_const src, size_t count) {
    memmove((void *) dest, (const void *) src, count);
    return true;
}

#endif
