#include "mem/mem_mapping.h"
#include "kio/klog.h"
#include <string.h>

mem_mapping *mem_mapping_new_from_ownedmem_subset(ownedmem_ptr backing, size_t offset, size_t size) {
    mem_mapping *map = kalloc(sizeof(mem_mapping));
    if (map) {
        map->backing = backing;
        map->offset = offset;
        map->size = size;
    }
    return map;
}

void mem_mapping_new_from_ownedmem_subset_no_kheap(ownedmem_ptr backing, size_t offset, size_t size, mem_mapping *map) {
    map->backing = backing;
    map->offset = offset;
    map->size = size;
}

mem_mapping *mem_mapping_new_from_ownedmem(ownedmem_ptr backing, size_t size) {
    return mem_mapping_new_from_ownedmem_subset(backing, 0, size);
}

void mem_mapping_new_from_ownedmem_no_kheap(ownedmem_ptr backing, size_t size, mem_mapping *map) {
    mem_mapping_new_from_ownedmem_subset_no_kheap(backing, 0, size, map);
}

mem_mapping *mem_mapping_new(size_t size) {
    ownedmem_ptr backing = ownedmem_new(size);
    mem_mapping *map = NULL;
    if (backing) {
        map = mem_mapping_new_from_ownedmem(backing, size);
        if (!map) {
            ownedmem_unref(backing);
        }
    }
    return map;
}

bool mem_mapping_new_no_kheap(size_t size, ownedmem *backing, mem_mapping *map) {
    if (!ownedmem_new_no_kheap(size, backing)) {
        return false;
    }
    mem_mapping_new_from_ownedmem_no_kheap(backing, size, map);
    return true;
}

void mem_mapping_delete(mem_mapping *map) {
    mem_mapping_delete_no_kheap(map);
    kfree(map);
}

void mem_mapping_delete_no_kheap(mem_mapping *map) {
    ownedmem_unref(map->backing);
    memset(map, 0, sizeof(*map));
}

mem_mapping *mem_mapping_clone_ref(mem_mapping *map) {
    ownedmem_ref(map->backing);
    mem_mapping *new_map = mem_mapping_new_from_ownedmem_subset(map->backing, map->offset, map->size);
    if (!new_map) {
        ownedmem_unref(map->backing);
    }
    return new_map;
}

mem_mapping *mem_mapping_clone_copy(mem_mapping *map) {
    mem_mapping *new_map = mem_mapping_new(map->size);
    if (new_map) {
        // TODO: Don't do it this way
        memcpy(mem_mapping_k_at(new_map), mem_mapping_k_at(map), map->size);
    }
    return new_map;
}
