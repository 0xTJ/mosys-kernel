#include "mem/flex.h"
#include "kio/klog.h"

flex_mut flex_mut_str_measure(flex_mut_str flx_str) {
    flex_mut flx = flx_str;

    switch (flx.type) {
    case FLEX_TYPE_NULL:
        break;
    case FLEX_TYPE_KERNEL:
        flx.size = min(strnlen(flx.ptr.kernel, flx.size) + 1, flx.size);
        break;
    case FLEX_TYPE_USER:
        // TODO: THIS IS WRONG!!!
        flx.size = min(strnlen((void *) flx.ptr.user, flx.size) + 1, flx.size);
        break;
    default:
        kpanic("Unexpected flex_mut type %d", (int) flx.type);
    }

    return flx;
}

flex_const flex_const_str_measure(flex_const_str flx_str) {
    flex_const flx = flx_str;

    switch (flx.type) {
    case FLEX_TYPE_NULL:
        break;
    case FLEX_TYPE_KERNEL:
        flx.size = min(strnlen(flx.ptr.kernel, flx.size) + 1, flx.size);
        break;
    case FLEX_TYPE_USER:
        // TODO: THIS IS WRONG!!!
        flx.size = min(strnlen((void *) flx.ptr.user, flx.size) + 1, flx.size);
        break;
    default:
        kpanic("Unexpected flex_mut type %d", (int) flx.type);
    }

    return flx;
}

static size_t flex_mut_vect_len(flex_ptr_mut *ptr) {
    size_t count = 1; // Start at 1 for NULL
    while (!flex_ptr_mut_is_null(*(ptr++))) {
        count += 1;
    }
    return count;
}

static size_t flex_const_vect_len(const flex_ptr_const *ptr) {
    size_t count = 1; // Start at 1 for NULL
    while (!flex_ptr_const_is_null(*(ptr++))) {
        count += 1;
    }
    return count;
}

flex_mut flex_mut_vect_measure(flex_mut_vect flx_vect) {
    flex_mut flx = flx_vect;

    switch (flx.type) {
    case FLEX_TYPE_NULL:
        break;
    case FLEX_TYPE_KERNEL:
        flx.size = min(flex_mut_vect_len(flx.ptr.kernel) * sizeof(flex_ptr_mut), flx.size);
        break;
    case FLEX_TYPE_USER:
        // TODO: THIS IS WRONG!!!
        flx.size = min(flex_mut_vect_len((void *) flx.ptr.user) * sizeof(flex_ptr_mut), flx.size);
        break;
    default:
        kpanic("Unexpected flex_mut type %d", (int) flx.type);
    }

    return flx;
}

flex_const flex_const_vect_measure(flex_const_vect flx_vect) {
    flex_const flx = flx_vect;

    switch (flx.type) {
    case FLEX_TYPE_NULL:
        break;
    case FLEX_TYPE_KERNEL:
        flx.size = min(flex_const_vect_len(flx.ptr.kernel) * sizeof(flex_ptr_mut), flx.size);
        break;
    case FLEX_TYPE_USER:
        // TODO: THIS IS WRONG!!!
        flx.size = min(flex_const_vect_len((void *) flx.ptr.user) * sizeof(flex_ptr_mut), flx.size);
        break;
    default:
        kpanic("Unexpected flex_const type %d", (int) flx.type);
    }

    return flx;
}

flex_result flex_set(flex_mut dest, int ch, size_t count) {
    // TODO: do this properly
    memset(dest.ptr.kernel, ch, count);
    return FLEX_RESULT_SUCCESS;
}

flex_result flex_from_mut(void *dest, flex_mut src, size_t count) {
    flex_result result = FLEX_RESULT_SUCCESS;
    if (src.type == FLEX_TYPE_NULL) {
        result |= FLEX_RESULT_SRC_FAULT;
    }
    if (src.size < count) {
        result |= FLEX_RESULT_SRC_SMALL;
    }
    if (result != FLEX_RESULT_SUCCESS) {
        return result;
    }

    if (src.type == FLEX_TYPE_KERNEL) {
        if (!umem_copy_kk(dest, src.ptr.kernel, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else if (src.type == FLEX_TYPE_USER) {
        if (!umem_copy_ku(dest, src.ptr.user, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else {
        kpanic("Unexpected flex type %d", (int) src.type);
    }

    return result;
}

flex_result flex_from_const(void *dest, flex_const src, size_t count) {
    flex_result result = FLEX_RESULT_SUCCESS;
    if (src.type == FLEX_TYPE_NULL) {
        result |= FLEX_RESULT_SRC_FAULT;
    }
    if (src.size < count) {
        result |= FLEX_RESULT_SRC_SMALL;
    }
    if (result != FLEX_RESULT_SUCCESS) {
        return result;
    }

    if (src.type == FLEX_TYPE_KERNEL) {
        if (!umem_copy_kk(dest, src.ptr.kernel, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else if (src.type == FLEX_TYPE_USER) {
        if (!umem_copy_ku(dest, src.ptr.user, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else {
        kpanic("Unexpected flex_mut type %d", (int) src.type);
    }

    return result;
}

// TODO: These need to fail when underlying copy fails

flex_result flex_to(flex_mut dest, const void *src, size_t count) {
    flex_result result = FLEX_RESULT_SUCCESS;
    if (dest.type == FLEX_TYPE_NULL) {
        result |= FLEX_RESULT_DEST_FAULT;
    }
    if (dest.size < count) {
        result |= FLEX_RESULT_DEST_SMALL;
    }
    if (result != FLEX_RESULT_SUCCESS) {
        return result;
    }

    if (dest.type == FLEX_TYPE_KERNEL) {
        if (!umem_copy_kk(dest.ptr.kernel, src, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else if (dest.type == FLEX_TYPE_USER) {
        if (!umem_copy_uk(dest.ptr.user, src, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else {
        kpanic("Unexpected flex_mut type %d", (int) dest.type);
    }

    return result;
}

flex_result flex_btwn_mut_mut(flex_mut dest, flex_mut src, size_t count) {
    flex_result result = FLEX_RESULT_SUCCESS;
    if (src.type == FLEX_TYPE_NULL) {
        result |= FLEX_RESULT_SRC_FAULT;
    }
    if (dest.type == FLEX_TYPE_NULL) {
        result |= FLEX_RESULT_DEST_FAULT;
    }
    if (dest.size < count) {
        result |= FLEX_RESULT_DEST_SMALL;
    }
    if (src.size < count) {
        result |= FLEX_RESULT_SRC_SMALL;
    }
    if (result != FLEX_RESULT_SUCCESS) {
        return result;
    }

    if (dest.type == FLEX_TYPE_KERNEL && src.type == FLEX_TYPE_KERNEL) {
        if (!umem_copy_kk(dest.ptr.kernel, src.ptr.kernel, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else if (dest.type == FLEX_TYPE_KERNEL && src.type == FLEX_TYPE_USER) {
        if (!umem_copy_ku(dest.ptr.kernel, src.ptr.user, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else if (dest.type == FLEX_TYPE_USER && src.type == FLEX_TYPE_KERNEL) {
        if (!umem_copy_uk(dest.ptr.user, src.ptr.kernel, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else if (dest.type == FLEX_TYPE_USER && src.type == FLEX_TYPE_USER) {
        if (!umem_copy_uu(dest.ptr.user, src.ptr.user, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else {
        kpanic("Unexpected flex_mut type %d and/or %d", (int) dest.type, (int) src.type);
    }

    return result;
}

flex_result flex_btwn_mut_const(flex_mut dest, flex_const src, size_t count) {
    flex_result result = FLEX_RESULT_SUCCESS;
    if (dest.size < count) {
        result |= FLEX_RESULT_DEST_SMALL;
    }
    if (src.size < count) {
        result |= FLEX_RESULT_SRC_SMALL;
    }
    if (result != FLEX_RESULT_SUCCESS) {
        return result;
    }

    if (dest.type == FLEX_TYPE_KERNEL && src.type == FLEX_TYPE_KERNEL) {
        if (!umem_copy_kk(dest.ptr.kernel, src.ptr.kernel, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else if (dest.type == FLEX_TYPE_KERNEL && src.type == FLEX_TYPE_USER) {
        if (!umem_copy_ku(dest.ptr.kernel, src.ptr.user, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else if (dest.type == FLEX_TYPE_USER && src.type == FLEX_TYPE_KERNEL) {
        if (!umem_copy_uk(dest.ptr.user, src.ptr.kernel, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else if (dest.type == FLEX_TYPE_USER && src.type == FLEX_TYPE_USER) {
        if (!umem_copy_uu(dest.ptr.user, src.ptr.user, count)) {
            result |= FLEX_RESULT_SRC_FAULT;
        }
    } else {
        kpanic("Unexpected flex_mut type %d and/or %d", (int) dest.type, (int) src.type);
    }

    return result;
}

flex_mut flex_add_mut(flex_mut base, size_t addend) {
    if (base.size < addend) {
        addend = base.size;
    }

    base.size -= addend;

    switch (base.type) {
    case FLEX_TYPE_NULL:
        kwarn("Tried to add to a null flex");
        break;
    case FLEX_TYPE_KERNEL:
        base.ptr.kernel = (char *) base.ptr.kernel + addend;
        break;
    case FLEX_TYPE_USER:
        base.ptr.user = UMEM_ADD(base.ptr.user, addend, char);
        break;
    }

    return base;
}

flex_const flex_add_const(flex_const base, size_t addend) {
    if (base.size < addend) {
        addend = base.size;
    }

    base.size -= addend;

    switch (base.type) {
    case FLEX_TYPE_NULL:
        kwarn("Tried to add to a null flex");
        break;
    case FLEX_TYPE_KERNEL:
        base.ptr.kernel = (const char *) base.ptr.kernel + addend;
        break;
    case FLEX_TYPE_USER:
        base.ptr.user = UMEM_ADD(base.ptr.user, addend, const char);
        break;
    default:
        kpanic("Unexpected flex_mut type %d", (int) base.type);
    }

    return base;
}
