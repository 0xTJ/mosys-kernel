#include "syscall.h"
#include "exception.h"
#include "reboot.h"
#include "mem/flex.h"
#include "kio/klog.h"
#include "thread/timing.h"
#include "mem/umem.h"
#include "process/mem.h"
#include "process/process.h"
#include "vfs/vfs.h"
#include "uapi/errno.h"
#include "uapi/time.h"
#include "uapi/mosys/reboot.h"
#include "uapi/sys/mman.h"
#include "uapi/sys/syscall.h"
#include "uapi/sys/types.h"
#include <stdarg.h>
#include <stdint.h>
#include <string.h>

typedef long (syscall_handler)(long, long, long, long, long, long);
typedef syscall_handler *syscall_handler_ptr;

#define SYSCALL_0_ARGS(name)                                                    \
    long name(long, long, long, long, long, long)
#define SYSCALL_1_ARGS(name, arg1)                                              \
    long name(long arg1, long, long, long, long, long)
#define SYSCALL_2_ARGS(name, arg1, arg2)                                        \
    long name(long arg1, long arg2, long, long, long, long)
#define SYSCALL_3_ARGS(name, arg1, arg2, arg3)                                  \
    long name(long arg1, long arg2, long arg3, long, long, long)
#define SYSCALL_4_ARGS(name, arg1, arg2, arg3, arg4)                            \
    long name(long arg1, long arg2, long arg3, long arg4, long, long)
#define SYSCALL_5_ARGS(name, arg1, arg2, arg3, arg4, arg5)                      \
    long name(long arg1, long arg2, long arg3, long arg4, long arg5, long)
#define SYSCALL_6_ARGS(name, arg1, arg2, arg3, arg4, arg5, arg6)                \
    long name(long arg1, long arg2, long arg3, long arg4, long arg5, long arg6)

#define LONG_INVALID_INT(val)       \
    (((long) (val) < INT_MIN) || ((long) (val) > INT_MAX))

#define LONG_INVALID_SIZE(val)    \
    (((long) (val) < 0) || ((unsigned long) (val) > SSIZE_MAX))

// TODO: Implement in a way that doesn't assume that off_t is long
#define LONG_INVALID_OFF_T(val)     \
    (0)

// TODO: Implement in a way that doesn't assume that pid_t is int
#define LONG_INVALID_PID_T(val)     \
    LONG_INVALID_INT(val)

void syscall_init(void) {
    exception_vector_replace(syscall, 0x20);
}

syscall_handler syscall_succeed;
syscall_handler syscall_fail;
syscall_handler syscall_getpid;
syscall_handler syscall_getppid;
syscall_handler syscall_open;
syscall_handler syscall_close;
syscall_handler syscall_read;
syscall_handler syscall_write;
syscall_handler syscall_nanosleep;
syscall_handler syscall_clone;
syscall_handler syscall_execve;
syscall_handler syscall__exit;
// TODO: Implement something like exit_group() if multi-threaded processes are added
syscall_handler syscall_waitpid;
syscall_handler syscall_getdents;
syscall_handler syscall_ioctl;
syscall_handler syscall_lseek;
syscall_handler syscall_socket;
syscall_handler syscall_socketpair;
syscall_handler syscall_bind;
syscall_handler syscall_recvfrom;
syscall_handler syscall_sendto;
syscall_handler syscall_accept;
syscall_handler syscall_connect;
syscall_handler syscall_mount;
syscall_handler syscall_mmap;
syscall_handler syscall_munmap;
syscall_handler syscall_pread;
syscall_handler syscall_pwrite;
syscall_handler syscall_chdir;
syscall_handler syscall_fchdir;
syscall_handler syscall_reboot;

syscall_handler_ptr syscall_table[] = {
    [SYS_succeed]       = syscall_succeed,
    [SYS_fail]          = syscall_fail,
    [SYS_getpid]        = syscall_getpid,
    [SYS_getppid]       = syscall_getppid,
    [SYS_open]          = syscall_open,
    [SYS_close]         = syscall_close,
    [SYS_read]          = syscall_read,
    [SYS_write]         = syscall_write,
    [SYS_nanosleep]     = syscall_nanosleep,
    [SYS_clone]         = syscall_clone,
    [SYS_execve]        = syscall_execve,
    [SYS__exit]         = syscall__exit,
    [SYS_waitpid]       = syscall_waitpid,
    [SYS_getdents]      = syscall_getdents,
    [SYS_ioctl]         = syscall_ioctl,
    [SYS_lseek]         = syscall_lseek,
    [SYS_socket]        = syscall_socket,
    [SYS_socketpair]    = syscall_socketpair,
    [SYS_bind]          = syscall_bind,
    [SYS_recvfrom]      = syscall_recvfrom,
    [SYS_sendto]        = syscall_sendto,
    [SYS_accept]        = syscall_accept,
    [SYS_connect]       = syscall_connect,
    [SYS_mount]         = syscall_mount,
    [SYS_mmap]          = syscall_mmap,
    [SYS_munmap]        = syscall_munmap,
    [SYS_pread]         = syscall_pread,
    [SYS_pwrite]        = syscall_pwrite,
    [SYS_chdir]         = syscall_chdir,
    [SYS_fchdir]        = syscall_fchdir,
    [SYS_reboot]        = syscall_reboot,
};

bool syscall_number_is_valid(unsigned long number) {
    return number < countof(syscall_table) && syscall_table[number];
}

void syscall(struct exception_state *state) {
    thread_current->user_state = state;
    unsigned long number = state->d[0];

#if ALLOW_TRACE_ON_SYSCALLS
    if (number & 0x80000000) {
        // Enable trace bit until syscall is over
        __asm__ ("ori #0x8000, %SR");
    }
#endif
    number &= ~0x80000000;

#if SYSCALLS_DEBUG
    kinfo("Syscall by PID %d: %ld(%ld, %ld, %ld, %ld, %ld, %ld) from %08lX", thread_current->owner_process->pid, number, state->d[1], state->d[2], state->d[3], state->d[4], state->d[5], state->a[0], state->pc);
#endif

    if (!syscall_number_is_valid(number)) {
        state->d[0] = -ENOSYS;
        return;
    }

    long result = syscall_table[number](state->d[1], state->d[2], state->d[3], state->d[4], state->d[5], state->a[0]);

#if SYSCALLS_DEBUG
    kinfo("Syscall by PID %d: %ld() -> %ld", thread_current->owner_process->pid, number, result);
#endif

    state->d[0] = result;
    thread_current->user_state = NULL;
}

SYSCALL_0_ARGS(syscall_succeed) {
    return 0;
}

SYSCALL_1_ARGS(syscall_fail, err) {
    if (err <= 0) {
        return -EINVAL;
    }
    return -err;
}

SYSCALL_0_ARGS(syscall_getpid) {
    return thread_current->owner_process->pid;
}

SYSCALL_0_ARGS(syscall_getppid) {
    return thread_current->owner_process->parent->pid;
}

SYSCALL_3_ARGS(syscall_open, pathname_long, flags_long, mode_long) {
    UMEM_PTR(const char) pathname = pathname_long;
    if (LONG_INVALID_INT(flags_long)) return -EBADF;
    int flags = flags_long;
    // TODO: Check mode_long
    mode_t mode = mode_long;

    flex_const_str pathname_flx = flex_const_str_user(pathname);
    int errno;
    int result = vfs_open(pathname_flx, flags, mode, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_1_ARGS(syscall_close, fd_long) {
    if (LONG_INVALID_INT(fd_long)) return -EBADF;
    int fd = fd_long;

    int errno;
    ssize_t result = vfs_close(fd, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_3_ARGS(syscall_read, fd_long, buf_long, count_long) {
    if (LONG_INVALID_INT(fd_long)) return -EBADF;
    int fd = fd_long;
    umem_ptr buf = buf_long;
    if (LONG_INVALID_SIZE(count_long)) return -EFAULT;
    size_t count = count_long;

    flex_mut buf_flx = flex_mut_user(buf, count);
    int errno;
    ssize_t result = vfs_read(fd, buf_flx, count, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_3_ARGS(syscall_write, fd_long, buf_long, count_long) {
    if (LONG_INVALID_INT(fd_long)) return -EBADF;
    int fd = fd_long;
    umem_ptr_const buf = buf_long;
    if (LONG_INVALID_SIZE(count_long)) return -EFAULT;
    size_t count = count_long;

    flex_const buf_flx = flex_const_user(buf, count);
    int errno;
    ssize_t result = vfs_write(fd, buf_flx, count, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_2_ARGS(syscall_nanosleep, req_long, rem_long) {
    UMEM_PTR(const struct timespec) req = req_long;
    UMEM_PTR(struct timespec) rem = rem_long;

    struct timespec req_k;
    if (!umem_copy_ku(&req_k, req, sizeof(req_k))) {
        return -EFAULT;
    }

    // Validate *req contents
    if (req_k.tv_nsec < 0 || req_k.tv_nsec > 999999999 || req_k.tv_sec < 0) {
        return -EINVAL;
    }

    // Divide nsec by 1000, rounding up
    unsigned long long usec = (req_k.tv_nsec + 999UL) / 1000U;
    // Multiply sec by 1000000
    usec += req_k.tv_sec * 1000000UL;    // * 1000000ULL

    // TODO: Interrupt on signal
    timing_sleep_us(usec);
    bool was_interrupted = false;
    struct timespec rem_k = { .tv_sec = 0, .tv_nsec = 0 };

    if (was_interrupted) {
        if (!umem_copy_uk(rem, &rem_k, sizeof(rem_k))) {
            return -EFAULT;
        }
        return -EINTR;
    }

    return 0;
}

SYSCALL_3_ARGS(syscall_clone, flags_long, stack_long, tls_long) {
    unsigned long flags = flags_long;
    umem_ptr stack = stack_long;
    unsigned long tls = tls_long;

    int errno;
    process *result = process_clone(flags, stack, tls, &errno);
    return result ? result->pid : -errno;
}

SYSCALL_3_ARGS(syscall_execve, pathname_long, argv_long, envp_long) {
    UMEM_PTR(const char) pathname = pathname_long;
    UMEM_PTR(UMEM_CONST_PTR(char)) argv = argv_long;
    UMEM_PTR(UMEM_CONST_PTR(char)) envp = envp_long;

    int errno;
    process_execve_info exec_info = PROCESS_EXECVE_INFO_INIT;
    exec_info.pathname = flex_const_str_user(pathname);
    exec_info.argv = flex_const_user(argv, FLEX_INF);
    exec_info.envp = flex_const_user(envp, FLEX_INF);
    int result = process_execve(&exec_info, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_1_ARGS(syscall__exit, status_long) {
    int status = status_long & 0377;

    process_exit_thread(status);
    kpanic("Returned from process_exit()");
}

SYSCALL_3_ARGS(syscall_waitpid, pid_long, wstatus_long, options_long) {
    if (LONG_INVALID_PID_T(pid_long)) return -ECHILD;
    pid_t pid = pid_long;
    UMEM_PTR(int) wstatus = wstatus_long;
    if (LONG_INVALID_INT(options_long)) return -EINVAL;
    int options = options_long;

    FLEX_MUT(int) wstatus_flx = flex_mut_user(wstatus, sizeof(int));
    int errno;
    pid_t result = process_waitpid(pid, wstatus_flx, options, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_3_ARGS(syscall_getdents, fd_long, dirp_long, count_long) {
    if (LONG_INVALID_INT(fd_long)) return -EBADF;
    int fd = fd_long;
    UMEM_PTR(struct dirent) dirp = dirp_long;
    if (LONG_INVALID_SIZE(count_long)) return -EFAULT;
    size_t count = count_long;

    FLEX_MUT(struct dirent) dirp_flx = flex_mut_user(dirp, count * sizeof(struct dirent));
    int errno;
    ssize_t result = vfs_getdents(fd, dirp_flx, count, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_3_ARGS(syscall_ioctl, fd_long, request_long, argp_long) {
    if (LONG_INVALID_INT(fd_long)) return -EBADF;
    int fd = fd_long;
    unsigned long request = request_long;
    umem_ptr argp = argp_long;

    flex_mut argp_flx = flex_mut_user(argp, FLEX_INF);
    int errno;
    int result = vfs_ioctl(fd, request, argp_flx, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_3_ARGS(syscall_lseek, fd_long, offset_long, whence_long) {
    if (LONG_INVALID_INT(fd_long)) return -EBADF;
    int fd = fd_long;
    if (LONG_INVALID_OFF_T(offset_long)) return -EOVERFLOW;
    off_t offset = offset_long;
    if (LONG_INVALID_INT(whence_long)) return -EINVAL;
    int whence = whence_long;

    int errno;
    int result = vfs_lseek(fd, offset, whence, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_3_ARGS(syscall_socket, domain_long, type_long, protocol_long) {
    if (LONG_INVALID_INT(domain_long)) return -EINVAL;
    int domain = domain_long;
    if (LONG_INVALID_INT(type_long)) return -EINVAL;
    int type = type_long;
    if (LONG_INVALID_INT(protocol_long)) return -EINVAL;
    int protocol = protocol_long;

    int errno;
    int result = vfs_socket(domain, type, protocol, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_4_ARGS(syscall_socketpair, domain_long, type_long, protocol_long, sv_long) {
    if (LONG_INVALID_INT(domain_long)) return -EINVAL;
    int domain = domain_long;
    if (LONG_INVALID_INT(type_long)) return -EINVAL;
    int type = type_long;
    if (LONG_INVALID_INT(protocol_long)) return -EINVAL;
    int protocol = protocol_long;
    UMEM_PTR(int[2]) sv = sv_long;

    FLEX_MUT(int[2]) sv_flx = flex_mut_user(sv, sizeof(int[2]));
    int errno;
    int result = vfs_socketpair(domain, type, protocol, sv_flx, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_3_ARGS(syscall_bind, sockfd_long, addr_long, addrlen_long) {
    if (LONG_INVALID_INT(sockfd_long)) return -EBADF;
    int sockfd = sockfd_long;
    UMEM_PTR(const struct sockaddr) addr = addr_long;
    socklen_t addrlen = addrlen_long;

    FLEX_CONST(struct sockaddr) addr_flx = flex_const_user(addr, addrlen);
    int errno;
    int result = vfs_bind(sockfd, addr_flx, addrlen, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_6_ARGS(syscall_recvfrom, sockfd_long, buf_long, len_long, flags_long, src_addr_long, addrlen_long) {
    if (LONG_INVALID_INT(sockfd_long)) return -EBADF;
    int sockfd = sockfd_long;
    umem_ptr buf = buf_long;
    if (LONG_INVALID_SIZE(len_long)) return -EFAULT;
    size_t len = len_long;
    if (LONG_INVALID_INT(flags_long)) return -EINVAL;
    int flags = flags_long;
    UMEM_PTR(struct sockaddr) src_addr = src_addr_long;
    UMEM_PTR(socklen_t) addrlen = addrlen_long;

    flex_mut buf_flx = flex_mut_user(buf, len);
    FLEX_MUT(socklen_t) addrlen_flx = flex_mut_user(addrlen, sizeof(socklen_t));
    socklen_t addrlen_value = 0;
    if (!flex_is_null(addrlen_flx)) {
        flex_mut_pinned addrlen_pin = flex_pin_must_cleanup(addrlen_flx);
        if (addrlen_pin.fault) {
            return -EFAULT;
        }
        addrlen_value = *(socklen_t *) addrlen_pin.ptr;
        flex_unpin(&addrlen_pin);
    }
    FLEX_MUT(struct sockaddr) src_addr_flx = flex_mut_user(src_addr, addrlen_value);
    int errno;
    ssize_t result = vfs_recvfrom(sockfd, buf_flx, len, flags, src_addr_flx, addrlen_flx, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_6_ARGS(syscall_sendto, sockfd_long, buf_long, len_long, flags_long, dest_addr_long, addrlen_long) {
    if (LONG_INVALID_INT(sockfd_long)) return -EBADF;
    int sockfd = sockfd_long;
    umem_ptr_const buf = buf_long;
    if (LONG_INVALID_SIZE(len_long)) return -EFAULT;
    size_t len = len_long;
    if (LONG_INVALID_INT(flags_long)) return -EINVAL;
    int flags = flags_long;
    UMEM_PTR(const struct sockaddr) dest_addr = dest_addr_long;
    socklen_t addrlen = addrlen_long;

    flex_const buf_flx = flex_const_user(buf, len);
    FLEX_CONST(struct sockaddr) dest_addr_flx = flex_const_user(dest_addr, addrlen);
    int errno;
    ssize_t result = vfs_sendto(sockfd, buf_flx, len, flags, dest_addr_flx, addrlen, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_3_ARGS(syscall_accept, sockfd_long, addr_long, addrlen_long) {
    if (LONG_INVALID_INT(sockfd_long)) return -EBADF;
    int sockfd = sockfd_long;
    UMEM_PTR(struct sockaddr) addr = addr_long;
    UMEM_PTR(socklen_t) addrlen = addrlen_long;

    FLEX_MUT(socklen_t) addrlen_flx = flex_mut_user(addrlen, sizeof(socklen_t));
    socklen_t addrlen_value = 0;
    if (!flex_is_null(addrlen_flx)) {
        flex_mut_pinned addrlen_pin = flex_pin_must_cleanup(addrlen_flx);
        if (addrlen_pin.fault) {
            return -EFAULT;
        }
        addrlen_value = *(socklen_t *) addrlen_pin.ptr;
        flex_unpin(&addrlen_pin);
    }
    FLEX_MUT(struct sockaddr) addr_flx = flex_mut_user(addr, addrlen_value);
    int errno;
    int result = vfs_accept(sockfd, addr_flx, addrlen_flx, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_3_ARGS(syscall_connect, sockfd_long, addr_long, addrlen_long) {
    if (LONG_INVALID_INT(sockfd_long)) return -EBADF;
    int sockfd = sockfd_long;
    UMEM_PTR(const struct sockaddr) addr = addr_long;
    socklen_t addrlen = addrlen_long;

    FLEX_CONST(struct sockaddr) addr_flx = flex_const_user(addr, addrlen);
    int errno;
    int result = vfs_connect(sockfd, addr_flx, addrlen, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_5_ARGS(syscall_mount, source_long, target_long, filesystemtype_long, mountflags_long, data_long) {
    UMEM_PTR(const char) source = source_long;
    UMEM_PTR(const char) target = target_long;
    UMEM_PTR(const char) filesystemtype = filesystemtype_long;
    unsigned long mountflags = mountflags_long;
    umem_ptr_const data = data_long;

    flex_const_str source_flx = flex_const_str_user(source);
    flex_const_str target_flx = flex_const_str_user(target);
    flex_const_str filesystemtype_flx = flex_const_str_user(filesystemtype);
    flex_const data_flx = flex_const_user(data, FLEX_INF);
    int errno;
    int result = vfs_mount(source_flx, target_flx,
                           filesystemtype_flx, mountflags,
                           data_flx,
                           &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_6_ARGS(syscall_mmap, addr_long, length_long, prot_long, flags_long, fd_long, offset_long) {
    umem_ptr addr = addr_long;
    if (LONG_INVALID_SIZE(length_long)) return -EINVAL;
    size_t length = length_long;
    if (LONG_INVALID_INT(prot_long)) return -EINVAL;
    int prot = prot_long;
    if (LONG_INVALID_INT(flags_long)) return -EINVAL;
    int flags = flags_long;
    if (LONG_INVALID_INT(fd_long)) return -EBADF;
    int fd = fd_long;
    if (LONG_INVALID_OFF_T(offset_long)) return -EINVAL;
    off_t offset = offset_long;


    int errno;
    umem_ptr result = process_mmap(addr, length, prot, flags, fd, offset, &errno);
    return result != MAP_FAILED ? (long) result : -errno;
}

SYSCALL_2_ARGS(syscall_munmap, addr_long, length_long) {
    umem_ptr addr = addr_long;
    if (LONG_INVALID_SIZE(length_long)) return -EINVAL;
    size_t length = length_long;

    int errno;
    int result = process_munmap(addr, length, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_4_ARGS(syscall_pread, fd_long, buf_long, count_long, offset_long) {
    if (LONG_INVALID_INT(fd_long)) return -EBADF;
    int fd = fd_long;
    umem_ptr buf = buf_long;
    if (LONG_INVALID_SIZE(count_long)) return -EFAULT;
    size_t count = count_long;
    if (LONG_INVALID_OFF_T(offset_long)) return -EOVERFLOW;
    off_t offset = offset_long;

    flex_mut buf_flx = flex_mut_user(buf, count);
    int errno;
    ssize_t result = vfs_pread(fd, buf_flx, count, offset, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_4_ARGS(syscall_pwrite, fd_long, buf_long, count_long, offset_long) {
    if (LONG_INVALID_INT(fd_long)) return -EBADF;
    int fd = fd_long;
    umem_ptr_const buf = buf_long;
    if (LONG_INVALID_SIZE(count_long)) return -EFAULT;
    size_t count = count_long;
    if (LONG_INVALID_OFF_T(offset_long)) return -EOVERFLOW;
    off_t offset = offset_long;

    flex_const buf_flx = flex_const_user(buf, count);
    int errno;
    ssize_t result = vfs_pwrite(fd, buf_flx, count, offset, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_1_ARGS(syscall_chdir, path_long) {
    UMEM_PTR(const char) path = path_long;

    flex_const_str path_flx = flex_const_str_user(path);
    int errno;
    int result = vfs_chdir(path_flx, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_1_ARGS(syscall_fchdir, fd_long) {
    if (LONG_INVALID_INT(fd_long)) return -EBADF;
    int fd = fd_long;

    int errno;
    int result = vfs_fchdir(fd, &errno);
    return result != -1 ? result : -errno;
}

SYSCALL_4_ARGS(syscall_reboot, magic_long, magic2_long, cmd_long, arg_long) {
    if (LONG_INVALID_INT(magic_long)) return -EINVAL;
    int magic = magic_long;
    if (LONG_INVALID_INT(magic2_long)) return -EINVAL;
    int magic2 = magic2_long;
    if (LONG_INVALID_INT(cmd_long)) return -EINVAL;
    int cmd = cmd_long;
    umem_ptr arg = arg_long;

    flex_const_str arg_flx = flex_const_str_user(arg);
    int errno;
    int result = reboot(magic, magic2, cmd, arg_flx, &errno);
    return result != -1 ? result : -errno;
}
