#include "vfs/vnode.h"
#include "mem/dynobj.h"

static void vfs_vnode_cleanup(void *vn_arg);

vnode *vfs_vnode_new(superblock *sb) {
    vnode *vn = dynobj_mtx_new(vnode, vfs_vnode_cleanup);
    if (vn) {
        vn->sb = sb;
    }

    return vn;
}

static void vfs_vnode_cleanup(void *vn_arg) {
    vnode *vn = vn_arg;

    superblock *sb = vn->sb;    // Does this select the right superblock in the case of devices, etc.?
    if (!sb->type || !sb->type->fsops || !sb->type->fsops->cleanup_vnode) {
        kpanic("Expected to find cleanup_vnode for mount");
    }
    sb->type->fsops->cleanup_vnode(sb, vn);

    if (vn->parent) {
        vfs_vnode_unref(vn->parent);
        vn->parent = NULL;
    }
}

void vfs_vnode_ref(vnode *vn) {
    dynobj_mtx_ref(vnode, &vn->dnobj);
}

void vfs_vnode_unref(vnode *vn) {
    dynobj_mtx_unref(vnode, &vn->dnobj);
}
