#include "vfs/initrd.h"
#include "initrd.cpio.h"

const unsigned char *initrd_get(size_t *size) {
    if (size) {
        *size = initrd_cpio_len;
    }
    return initrd_cpio;
}
