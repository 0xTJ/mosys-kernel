#include "vfs/fs.h"
#include "uapi/errno.h"

static filesystem_type *filesystem_type_list = NULL;
static rwlock filesystem_type_list_rwl = RWLOCK_INIT;

void vfs_filesystem_type_register(filesystem_type *fst) {
    RWLOCK_WRITE_WITH(&filesystem_type_list_rwl, {
        // Advance next_ptr to point to the current NULL pointer in list
        filesystem_type **next_ptr = &filesystem_type_list;
        while (*next_ptr != NULL) {
            next_ptr = &((*next_ptr)->next);
        }

        // Add fst to list
        fst->next = NULL;
        *next_ptr = fst;
    })
}

vnode *vfs_filesystem_mount(flex_const_str source,
                            flex_const_str filesystemtype, unsigned long mountflags,
                            flex_const data,
                            int *errno_ptr) {
    flex_const filesystemtype_meas = flex_str_measure(filesystemtype);
    flex_const_pinned filesystemtype_pin = flex_pin_must_cleanup(filesystemtype_meas);
    if (filesystemtype_pin.fault) {
        *errno_ptr = EFAULT;
        return NULL;
    }

    vnode *mount_vn = NULL;

    RWLOCK_READ_WITH(&filesystem_type_list_rwl, {
        // Lookup filesystem type by name
        filesystem_type *this_fst;
        for (this_fst = filesystem_type_list; this_fst; this_fst = this_fst->next) {
            if (0 == strncmp(this_fst->name, filesystemtype_pin.ptr, filesystemtype_pin.size)) {
                break;
            }
        }

        if (!this_fst) {
            *errno_ptr = ENODEV;
            RWLOCK_LEAVE_UNLOCK();
        }

        if (!this_fst->fsops || !this_fst->fsops->mount) {
            *errno_ptr = ENODEV;
            RWLOCK_LEAVE_UNLOCK();
        }
        mount_vn = this_fst->fsops->mount(source, mountflags, data, errno_ptr);
    })

    flex_unpin(&filesystemtype_pin);
    return mount_vn;
}
