#include "vfs/fdesc.h"
#include "mem/flex.h"
#include "kalloc.h"
#include "kio/klog.h"
#include "fs/devfs.h"
#include "fs/sockfs.h"
#include "process/process.h"
#include "socket/socket.h"
#include "uapi/errno.h"
#include "uapi/sys/types.h"
#include "util/mutex.h"
#include "util/rwlock.h"
#include "util/util.h"
#include "vfs/devreg.h"
#include "vfs/vfs.h"
#include "vfs/superblock.h"
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

static vnode *vfs_lookup_element(vnode *base, size_t name_len, const char name[name_len], int *errno_ptr);
static vnode *vfs_lookup_path(vnode *base, size_t path_len, const char path[path_len], int *errno_ptr);

static ssize_t vfs_pread_fdesc_nolock(fdesc *fdsc, flex_mut buf, size_t count, off_t offset, int *errno_ptr);
static ssize_t vfs_pwrite_fdesc_nolock(fdesc *fdsc, flex_const buf, size_t count, off_t offset, int *errno_ptr);

fdesc *vfs_fdesc_new(void) {
    fdesc *fdsc = dynobj_mtx_new(fdesc, dynobj_no_cleanup);
    return fdsc;
}

void vfs_fdesc_ref(fdesc *fdsc) {
    dynobj_mtx_ref(fdesc, &fdsc->dnobj);
}

void vfs_fdesc_unref(fdesc *fdsc) {
    dynobj_mtx_unref(fdesc, &fdsc->dnobj);
}

fdesc *vfs_fdesc_clone(fdesc *src_fdsc) {
    fdesc *dest_fdsc;

    MUTEX_WITH(&src_fdsc->dnobj.mtx, {
        dest_fdsc = vfs_fdesc_new();

        if (dest_fdsc) {
            vfs_vnode_ref(src_fdsc->vn);
            dest_fdsc->vn = src_fdsc->vn;

            dest_fdsc->mode = src_fdsc->mode;
            dest_fdsc->flags = src_fdsc->flags;

            dest_fdsc->offset = src_fdsc->offset;
        }
    })

    return dest_fdsc;
}

fdesc_list *vfs_fdesc_list_new(void) {
    fdesc_list *fdl = dynobj_mtx_new(fdesc_list, dynobj_no_cleanup);
    return fdl;
}

void vfs_fdesc_list_ref(fdesc_list *fdl) {
    dynobj_mtx_ref(fdesc_list, &fdl->dnobj);
}

void vfs_fdesc_list_unref(fdesc_list *fdl) {
    dynobj_mtx_unref(fdesc_list, &fdl->dnobj);
}

fdesc_list *vfs_fdesc_list_clone(fdesc_list *src_fdl) {
    fdesc_list *dest_fdl = vfs_fdesc_list_new();

    if (!dest_fdl) {
        return NULL;
    }

    MUTEX_WITH(&src_fdl->dnobj.mtx, {
        for (size_t i = 0; i < VFS_MAX_OPEN_FILES; ++i) {
            if (src_fdl->fds[i]) {
                dest_fdl->fds[i] = vfs_fdesc_clone(src_fdl->fds[i]);
            }
        }
    })

    return dest_fdl;
}

int vfs_register_fd(fdesc *fdsc, int *errno_ptr) {
    // Add file to process's file descriptor list
    fdesc_list *fdl = thread_current->owner_process->fdl;
    if (!fdl) {
        kpanic("Process has no file descriptor list");
    }

    int fd = -1;

    MUTEX_WITH(&fdl->dnobj.mtx, {
        for (size_t i = 0; i < VFS_MAX_OPEN_FILES; ++i) {
            if (!fdl->fds[i]) {
                fdl->fds[i] = fdsc;
                fd = i;
                break;
            }
        }
    })

    if (fd < 0) {
        *errno_ptr = EMFILE;
    }
    return fd;
}

// TODO: For POSIX compliance, paths ending in a '/' should have a '.' appended.
// This isn't currently done because the filesystems can't handle "." or "..".
// This will also fix trailing slashes on files being ignored.

static vnode *vfs_lookup_element(vnode *base, size_t name_len, const char name[name_len], int *errno_ptr) {
    if (!base->sb->type || !base->sb->type->fsops || !base->sb->type->fsops->lookup) {
        kpanic("Expected to find lookup for mount");
    }
    vnode *vn = base->sb->type->fsops->lookup(base, name, errno_ptr);
    return vn;
}

static vnode *vfs_lookup_path(vnode *base, size_t path_len, const char path[path_len], int *errno_ptr) {
    while (path_len > 0 && path[0] == '/') {
        path_len -= 1;
        path += 1;
    }

    if (path_len <= 1) {
        vfs_vnode_ref(base);
        return base;
    }

    const char *next_slash = strnchr(path, path_len, '/');
    if (!next_slash) {
        next_slash = path + (path_len - 1);
    }

    const size_t element_strlen = next_slash - path;
    if (element_strlen > NAME_MAX) {
        *errno_ptr = ENAMETOOLONG;
        return NULL;
    }
    const size_t element_len = element_strlen + 1;

    // TODO: This will probably overflow the stack on long and/or deeply-nested paths
    char element[element_len];
    strncpy(element, path, element_len);
    element[element_strlen] = '\0';

    vnode *vn = vfs_lookup_element(base, element_len, element, errno_ptr);
    if (vn) {
        vfs_vnode_ref(base);
        vn->parent = base;
    } else {
        return NULL;
    }

    const char *path_left = path + element_len;
    const size_t path_left_len = path_len - element_len;

    if (path_left_len == 0) {
        return vn;
    } else {
        vnode *vn_result = vfs_lookup_path(vn, path_left_len, path_left, errno_ptr);
        vfs_vnode_unref(vn);
        return vn_result;
    }
}

// TODO: Need to release found_mount
fdesc *vfs_open_fdesc(flex_const_str pathname, int flags, mode_t mode, int *errno_ptr) {
    // TODO: Handle O_EXCL

    char pathname_buf[PATH_MAX];
    flex_const pathname_meas = flex_str_measure(pathname);
    if (pathname_meas.size > sizeof pathname_buf) {
        *errno_ptr = ENAMETOOLONG;
        return NULL;
    }
    if (flex_from_const(pathname_buf, pathname_meas, pathname_meas.size) != FLEX_RESULT_SUCCESS) {
        *errno_ptr = EFAULT;
        return NULL;
    }

    bool is_absolute_path = pathname_buf[0] == '/';

    const char *subpath;
    size_t subpath_len;
    vnode *base_vn;

    if (is_absolute_path) {
        // Find mount point from path
        mountpoint *found_mount = vfs_mountpoint_lookup(pathname_buf, &subpath);
        if (!found_mount) {
            *errno_ptr = ENOENT;
            return NULL;
        }

        // TODO: This should be a derived thing, not measured
        subpath_len = strlen(subpath) + 1;

        // Get superblock from found mount point
        kassert(found_mount->sb);
        base_vn = found_mount->sb->mounted;
        kassert(base_vn);
        vfs_vnode_ref(base_vn);
        vfs_mountpoint_unref(found_mount);
    } else {
        subpath = pathname_buf;
        subpath_len = strlen(subpath) + 1;

        MUTEX_WITH(&thread_current->owner_process->cwd_mtx, {
            if (thread_current->owner_process->cwd) {
                base_vn = thread_current->owner_process->cwd->vn;
                kassert(base_vn);
                vfs_vnode_ref(base_vn);
            } else {
                base_vn = NULL;
            }
        })

        if (!base_vn) {
            kwarn("Process %d with no CWD tried to open a relative path", thread_current->owner_process->pid);
            *errno_ptr = ENOENT;
            return NULL;
        }
    }

    // base_vn is referenced here

    vnode *vn = vfs_lookup_path(base_vn, subpath_len, subpath, errno_ptr);

    // Try to create if requested
    if (!vn && *errno_ptr == ENOENT && (flags & O_CREAT)) {
        superblock *sb = base_vn->sb;
        kassert(sb);
        vfs_superblock_ref(sb);

        if (sb->type && sb->type->fsops && sb->type->fsops->create) {
            const char *deslashed_subpath = subpath;
            while (deslashed_subpath[0] == '/' && deslashed_subpath[0] != '\0') {
                deslashed_subpath += 1;
            }
            vn = sb->type->fsops->create(base_vn, deslashed_subpath, errno_ptr);
        }

        vfs_superblock_unref(sb);
    }

    // Un-reference base_vn
    vfs_vnode_unref(base_vn);
    base_vn = NULL;

    if (!vn) {
        goto failed_lookup;
    }

    fdesc *fdsc = vfs_fdesc_new();
    if (!fdsc) {
        *errno_ptr = ENOMEM;
        goto failed_fdesc_new;
    }

    mutex_lock(&fdsc->dnobj.mtx);

    fdsc->vn = vn;

    fdsc->flags = flags;
    fdsc->mode = mode;
    fdsc->offset = 0;

    // TODO: Check actual file information in vnode from fsops->lookup()?

    // Call fops->open()
    if (vn->fops && vn->fops->open) {
        if (vn->fops->open(fdsc, errno_ptr) == -1) {
            goto failed_open;
        }
    }

    // TODO: Seek to end if O_APPEND

    mutex_unlock(&fdsc->dnobj.mtx);
    return fdsc;

failed_open:
    mutex_unlock(&fdsc->dnobj.mtx);
    vfs_fdesc_unref(fdsc);
    fdsc = NULL;
failed_fdesc_new:
    vfs_vnode_unref(vn);
    vn = NULL;
failed_lookup:
    return NULL;
}

int vfs_close_fdesc(fdesc *fdsc, int *errno_ptr) {
    int result;
    mutex_lock(&fdsc->dnobj.mtx);

    if (!fdsc->vn) {
        kpanic("Expected vnode");
    }

    switch (fdsc->vn->type) {
    case FILE_TYPE_PLAIN:
        if (!fdsc->vn->fops) {
            kpanic("Expected file_operations");
        } else if (fdsc->vn->fops->release) {
            result = fdsc->vn->fops->release(fdsc, errno_ptr);
        } else {
            result = 0;
        }
        break;
    case FILE_TYPE_DIRECTORY:
        *errno_ptr = EISDIR;
        result = -1;
        break;
    case FILE_TYPE_DEVICE:
        if (!devfs_sb->mounted->fops) {
            kpanic("Expected file_operations");
        } else if (devfs_sb->mounted->fops->release) {
            result = devfs_sb->mounted->fops->release(fdsc, errno_ptr);
        } else {
            result = 0;
        }
        break;
#if SOCKFS_ENABLE
    case FILE_TYPE_SOCKET:
        if (!sockfs_sb->mounted->fops) {
            kpanic("Expected file_operations");
        } else if (sockfs_sb->mounted->fops->release) {
            result = sockfs_sb->mounted->fops->release(fdsc, errno_ptr);
        } else {
            result = 0;
        }
        break;
#endif
    default:
        kpanic("Unknown file_type");
    }

    mutex_unlock(&fdsc->dnobj.mtx);
    return result;
}

ssize_t vfs_read_fdesc(fdesc *fdsc, flex_mut buf, size_t count, int *errno_ptr) {
    ssize_t result;
    MUTEX_WITH(&fdsc->dnobj.mtx, {
        result = vfs_pread_fdesc_nolock(fdsc, buf, count, fdsc->offset, errno_ptr);
        if (result >= 0) {
            fdsc->offset += result;
        }
    })
    return result;
}

ssize_t vfs_pread_fdesc(fdesc *fdsc, flex_mut buf, size_t count, off_t offset, int *errno_ptr) {
    ssize_t result;
    MUTEX_WITH(&fdsc->dnobj.mtx, {
        result = vfs_pread_fdesc_nolock(fdsc, buf, count, offset, errno_ptr);
    })
    return result;
}

static ssize_t vfs_pread_fdesc_nolock(fdesc *fdsc, flex_mut buf, size_t count, off_t offset, int *errno_ptr) {
    ssize_t result;

    if (!fdsc->vn) {
        kpanic("Expected vnode");
    }

    if (count > SSIZE_MAX) {
        *errno_ptr = EINVAL;
        result = -1;
        goto done;
    }

    if (!(fdsc->flags & O_RDONLY)) {
        *errno_ptr = EBADF;
        result = -1;
        goto done;
    }

    switch (fdsc->vn->type) {
    case FILE_TYPE_PLAIN:
        if (!fdsc->vn->fops) {
            kpanic("Expected file_operations");
        } else if (fdsc->vn->fops->pread) {
            result = fdsc->vn->fops->pread(fdsc, buf, count, offset, errno_ptr);
        } else {
            *errno_ptr = EINVAL;
            result = -1;
            goto done;
        }
        break;
    case FILE_TYPE_DIRECTORY:
        *errno_ptr = EISDIR;
        result = -1;
        break;
    case FILE_TYPE_DEVICE:
        if (!devfs_sb->mounted->fops) {
            kpanic("Expected file_operations");
        } else if (devfs_sb->mounted->fops->pread) {
            result = devfs_sb->mounted->fops->pread(fdsc, buf, count, offset, errno_ptr);
        } else {
            kpanic("No pread() on device");
        }
        break;
#if SOCKFS_ENABLE
    case FILE_TYPE_SOCKET:
        result = socket_recvfrom(fdsc->vn->socket.sock, buf, count, 0, FLEX_MUT_NULL, FLEX_MUT_NULL, errno_ptr);
        break;
#endif
    default:
        kpanic("Unknown file_type");
    }

done:
    return result;
}

ssize_t vfs_write_fdesc(fdesc *fdsc, flex_const buf, size_t count, int *errno_ptr) {
    ssize_t result;
    MUTEX_WITH(&fdsc->dnobj.mtx, {
        result = vfs_pwrite_fdesc_nolock(fdsc, buf, count, fdsc->offset, errno_ptr);
        if (result >= 0) {
            fdsc->offset += result;
        }
    })
    return result;
}

ssize_t vfs_pwrite_fdesc(fdesc *fdsc, flex_const buf, size_t count, off_t offset, int *errno_ptr) {
    ssize_t result;
    MUTEX_WITH(&fdsc->dnobj.mtx, {
        result = vfs_pwrite_fdesc_nolock(fdsc, buf, count, offset, errno_ptr);
    })
    return result;
}

static ssize_t vfs_pwrite_fdesc_nolock(fdesc *fdsc, flex_const buf, size_t count, off_t offset, int *errno_ptr) {
    ssize_t result;

    if (!fdsc->vn) {
        kpanic("Expected vnode");
    }

    if (count > SSIZE_MAX) {
        *errno_ptr = EINVAL;
        result = -1;
        goto done;
    }

    if (!(fdsc->flags & O_WRONLY)) {
        *errno_ptr = EBADF;
        result = -1;
        goto done;
    }

    switch (fdsc->vn->type) {
    case FILE_TYPE_PLAIN:
        if (!fdsc->vn->fops) {
            kpanic("Expected file_operations");
        } else if (fdsc->vn->fops->pwrite) {
            result = fdsc->vn->fops->pwrite(fdsc, buf, count,  offset, errno_ptr);
        } else {
            *errno_ptr = EINVAL;
            result = -1;
            goto done;
        }
        break;
    case FILE_TYPE_DIRECTORY:
        *errno_ptr = EISDIR;
        result = -1;
        break;
    case FILE_TYPE_DEVICE:
        if (!devfs_sb->mounted->fops) {
            kpanic("Expected file_operations");
        } else if (devfs_sb->mounted->fops->pwrite) {
            result = devfs_sb->mounted->fops->pwrite(fdsc, buf, count, offset, errno_ptr);
        } else {
            kpanic("No write() on device");
        }
        break;
#if SOCKFS_ENABLE
    case FILE_TYPE_SOCKET:
        result = socket_sendto(fdsc->vn->socket.sock, buf, count, 0, FLEX_CONST_NULL, 0, errno_ptr);
        break;
#endif
    default:
        kpanic("Unknown file_type");
    }

done:
    return result;
}

int vfs_ioctl_fdesc(fdesc *fdsc, unsigned long request, flex_mut argp, int *errno_ptr) {
    int result;
    mutex_lock(&fdsc->dnobj.mtx);

    if (!fdsc->vn) {
        kpanic("Expected vnode");
    }

    switch (fdsc->vn->type) {
    case FILE_TYPE_PLAIN:
    case FILE_TYPE_DIRECTORY:
    case FILE_TYPE_SOCKET:
        *errno_ptr = ENOTTY;
        result = -1;
        break;
    case FILE_TYPE_DEVICE:
        if (!devfs_sb->mounted->fops) {
            kpanic("Expected file_operations");
        } else if (devfs_sb->mounted->fops->ioctl) {
            result = devfs_sb->mounted->fops->ioctl(fdsc, request, argp, errno_ptr);
        } else {
            kpanic("No ioctl() on device");
        }
        break;
    default:
        kpanic("Unknown file_type");
    }

    if (result == -1) {
        goto done;
    }

done:
    mutex_unlock(&fdsc->dnobj.mtx);
    return result;
}

ssize_t vfs_getdents_fdesc(fdesc *fdsc, FLEX_MUT(dirent) drnt, size_t count, int *errno_ptr) {
    ssize_t result;
    mutex_lock(&fdsc->dnobj.mtx);

    if (!fdsc->vn) {
        kpanic("Expected vnode");
    }

    if (count > SSIZE_MAX) {
        *errno_ptr = EINVAL;
        result = -1;
        goto done;
    }

    switch (fdsc->vn->type) {
    case FILE_TYPE_PLAIN:
    case FILE_TYPE_DEVICE:
    case FILE_TYPE_SOCKET:
        *errno_ptr = ENOTDIR;
        result = -1;
        break;
    case FILE_TYPE_DIRECTORY:
        if (!fdsc->vn->fops) {
            kpanic("Expected file_operations");
        } else if (fdsc->vn->fops->getdents) {
            result = fdsc->vn->fops->getdents(fdsc, drnt, count, errno_ptr);
        } else {
            *errno_ptr = EINVAL;
            result = -1;
            goto done;
        }
        break;
    default:
        kpanic("Unknown file_type");
    }

    if (result == -1) {
        goto done;
    }

done:
    mutex_unlock(&fdsc->dnobj.mtx);
    return result;
}

off_t vfs_lseek_fdesc(fdesc *fdsc, off_t offset, int whence, int *errno_ptr) {
    off_t result;
    mutex_lock(&fdsc->dnobj.mtx);

    switch(whence) {
    case SEEK_SET:
        fdsc->offset = offset;
        result = fdsc->offset;
        break;
    case SEEK_CUR:
        fdsc->offset += offset;
        result = fdsc->offset;
        break;
    case SEEK_END:
        // TODO
    default:
        *errno_ptr = EINVAL;
        result = -1;
        goto done;
    }

done:
    mutex_unlock(&fdsc->dnobj.mtx);
    return result;
}

fdesc *vfs_socket_fdesc(int domain, int type, int protocol, int *errno_ptr) {
#if SOCKFS_ENABLE
    // Get create new sockfs vnode
    if (!sockfs_sb->type || !sockfs_sb->type->fsops || !sockfs_sb->type->fsops->new_vnode) {
        kpanic("Expected to find new_vnode for sockfs");
    }
    vnode *vn = sockfs_sb->type->fsops->new_vnode(sockfs_sb, errno_ptr);
    if (!vn) {
        goto failed_new_vnode;
    }
    vn->type = FILE_TYPE_SOCKET;

    fdesc *fdsc = vfs_fdesc_new();
    if (!fdsc) {
        *errno_ptr = ENOMEM;
        goto failed_fdesc_new;
    }

    vn->socket.sock = socket_socket(domain, type, protocol, errno_ptr);
    if (!vn->socket.sock) {
        goto failed_socket;
    }

    MUTEX_WITH(&fdsc->dnobj.mtx) {
        fdsc->vn = vn;

        fdsc->flags = O_RDWR;
        fdsc->mode = 0;
        fdsc->offset = 0;
    }

    return fdsc;

failed_socket:
    mutex_unlock(&fdsc->dnobj.mtx);
    vfs_fdesc_unref(fdsc);
    fdsc = NULL;
failed_fdesc_new:
    vfs_vnode_unref(vn);
    vn = NULL;
failed_new_vnode:
    return NULL;
#else
    UNUSED(domain);
    UNUSED(type);
    UNUSED(protocol);
    UNUSED(errno_ptr);

    *errno_ptr = ENOSYS;
    return NULL;
#endif
}

int vfs_socketpair_fdesc(int domain, int type, int protocol, fdesc *fdscs[2], int *errno_ptr) {
#if SOCKFS_ENABLE
    // Get create new sockfs vnodes
    if (!sockfs_sb->type || !sockfs_sb->type->fsops || !sockfs_sb->type->fsops->new_vnode) {
        kpanic("Expected to find new_vnode for sockfs");
    }
    vnode *vns[2];
    vns[0] = sockfs_sb->type->fsops->new_vnode(sockfs_sb, errno_ptr);
    if (!vns[0]) {
        goto failed_new_vnode_0;
    }
    vns[1] = sockfs_sb->type->fsops->new_vnode(sockfs_sb, errno_ptr);
    if (!vns[1]) {
        goto failed_new_vnode_1;
    }
    vns[0]->type = FILE_TYPE_SOCKET;
    vns[1]->type = FILE_TYPE_SOCKET;

    fdscs[0] = vfs_fdesc_new();
    if (!fdscs[0]) {
        *errno_ptr = ENOMEM;
        goto failed_fdesc_new_0;
    }
    fdscs[1] = vfs_fdesc_new();
    if (!fdscs[1]) {
        *errno_ptr = ENOMEM;
        goto failed_fdesc_new_1;
    }

    socket_info *socks[2];
    int socketpair_result = socket_socketpair(domain, type, protocol, socks, errno_ptr);
    if (socketpair_result < 0) {
        goto failed_socketpair;
    }
    vns[0]->socket.sock = socks[0];
    vns[1]->socket.sock = socks[1];

    mutex_lock(&fdscs[0]->dnobj.mtx);
    mutex_lock(&fdscs[1]->dnobj.mtx);

    fdscs[0]->vn = vns[0];
    fdscs[1]->vn = vns[1];

    fdscs[0]->flags = O_RDWR;
    fdscs[1]->flags = O_RDWR;
    fdscs[0]->mode = 0;
    fdscs[1]->mode = 0;
    fdscs[0]->offset = 0;
    fdscs[1]->offset = 0;

    mutex_unlock(&fdscs[1]->dnobj.mtx);
    mutex_unlock(&fdscs[0]->dnobj.mtx);
    return 0;

failed_socketpair:
    vfs_fdesc_unref(fdscs[1]);
    fdscs[1] = NULL;
failed_fdesc_new_1:
    vfs_fdesc_unref(fdscs[0]);
    fdscs[0] = NULL;
failed_fdesc_new_0:
    vfs_vnode_unref(vns[1]);
    vns[1] = NULL;
failed_new_vnode_1:
    vfs_vnode_unref(vns[0]);
    vns[0] = NULL;
failed_new_vnode_0:
    return -1;
#else
    UNUSED(domain);
    UNUSED(type);
    UNUSED(protocol);
    UNUSED(fdscs);
    UNUSED(errno_ptr);

    *errno_ptr = ENOSYS;
    return -1;
#endif
}

int vfs_bind_fdesc(fdesc *fdsc, FLEX_CONST(struct sockaddr) addr, socklen_t addrlen, int *errno_ptr) {
#if !SOCKFS_ENABLE
    UNUSED(addr);
    UNUSED(addrlen);
#endif

    int result;
    mutex_lock(&fdsc->dnobj.mtx);

    if (!fdsc->vn) {
        kpanic("Expected vnode");
    }

    switch (fdsc->vn->type) {
    case FILE_TYPE_PLAIN:
    case FILE_TYPE_DIRECTORY:
    case FILE_TYPE_DEVICE:
        *errno_ptr = ENOTSOCK;
        result = -1;
        break;
#if SOCKFS_ENABLE
    case FILE_TYPE_SOCKET:
        result = socket_bind(fdsc->vn->socket.sock, addr, addrlen, errno_ptr);
        break;
#endif
    default:
        kpanic("Unknown file_type");
    }

    if (result == -1) {
        goto done;
    }

done:
    mutex_unlock(&fdsc->dnobj.mtx);
    return result;
}

ssize_t vfs_recvfrom_fdesc(fdesc *fdsc, flex_mut buf, size_t len, int flags,
                           FLEX_MUT(struct sockaddr) src_addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr) {
#if !SOCKFS_ENABLE
    UNUSED(buf);
    UNUSED(len);
    UNUSED(flags);
    UNUSED(src_addr);
    UNUSED(addrlen);
#endif

    ssize_t result;
    mutex_lock(&fdsc->dnobj.mtx);

    if (!fdsc->vn) {
        kpanic("Expected vnode");
    }

    if (len > SSIZE_MAX) {
        *errno_ptr = EINVAL;
        result = -1;
        goto done;
    }

    // TODO: Do we check flags?
    if (!(fdsc->flags & O_RDONLY)) {
        *errno_ptr = EINVAL;
        result = -1;
        goto done;
    }

    switch (fdsc->vn->type) {
    case FILE_TYPE_PLAIN:
    case FILE_TYPE_DIRECTORY:
    case FILE_TYPE_DEVICE:
        *errno_ptr = ENOTSOCK;
        result = -1;
        break;
#if SOCKFS_ENABLE
    case FILE_TYPE_SOCKET:
        result = socket_recvfrom(fdsc->vn->socket.sock, buf, len, flags, src_addr, addrlen, errno_ptr);
        break;
#endif
    default:
        kpanic("Unknown file_type");
    }

    if (result == -1) {
        goto done;
    }

    // TODO: Do we do this?
    fdsc->offset += result;

done:
    mutex_unlock(&fdsc->dnobj.mtx);
    return result;
}

ssize_t vfs_sendto_fdesc(fdesc *fdsc, flex_const buf, size_t len, int flags,
                         FLEX_CONST(struct sockaddr) dest_addr, socklen_t addrlen, int *errno_ptr) {
#if !SOCKFS_ENABLE
    UNUSED(buf);
    UNUSED(len);
    UNUSED(flags);
    UNUSED(dest_addr);
    UNUSED(addrlen);
#endif

    ssize_t result;
    mutex_lock(&fdsc->dnobj.mtx);

    if (!fdsc->vn) {
        kpanic("Expected vnode");
    }

    if (len > SSIZE_MAX) {
        *errno_ptr = EINVAL;
        result = -1;
        goto done;
    }

    // TODO: Do we check flags?
    if (!(fdsc->flags & O_WRONLY)) {
        *errno_ptr = EINVAL;
        result = -1;
        goto done;
    }

    switch (fdsc->vn->type) {
    case FILE_TYPE_PLAIN:
    case FILE_TYPE_DIRECTORY:
    case FILE_TYPE_DEVICE:
        *errno_ptr = ENOTSOCK;
        result = -1;
        break;
#if SOCKFS_ENABLE
    case FILE_TYPE_SOCKET:
        result = socket_sendto(fdsc->vn->socket.sock, buf, len, flags, dest_addr, addrlen, errno_ptr);
        break;
#endif
    default:
        kpanic("Unknown file_type");
    }

    if (result == -1) {
        goto done;
    }

    // TODO: Do we do this?
    fdsc->offset += result;

done:
    mutex_unlock(&fdsc->dnobj.mtx);
    return result;
}

int vfs_accept_fdesc(fdesc *fdsc, FLEX_MUT(struct sockaddr) addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr) {
#if !SOCKFS_ENABLE
    UNUSED(addr);
    UNUSED(addrlen);
#endif

    int result;
    mutex_lock(&fdsc->dnobj.mtx);

    if (!fdsc->vn) {
        kpanic("Expected vnode");
    }

    switch (fdsc->vn->type) {
    case FILE_TYPE_PLAIN:
    case FILE_TYPE_DIRECTORY:
    case FILE_TYPE_DEVICE:
        *errno_ptr = ENOTSOCK;
        result = -1;
        break;
#if SOCKFS_ENABLE
    case FILE_TYPE_SOCKET:
        result = socket_accept(fdsc->vn->socket.sock, addr, addrlen, errno_ptr);
        break;
#endif
    default:
        kpanic("Unknown file_type");
    }

    if (result == -1) {
        goto done;
    }

done:
    mutex_unlock(&fdsc->dnobj.mtx);
    return result;
}

int vfs_connect_fdesc(fdesc *fdsc, FLEX_CONST(struct sockaddr) addr, socklen_t addrlen, int *errno_ptr) {
#if !SOCKFS_ENABLE
    UNUSED(addr);
    UNUSED(addrlen);
#endif

    int result;
    mutex_lock(&fdsc->dnobj.mtx);

    if (!fdsc->vn) {
        kpanic("Expected vnode");
    }

    switch (fdsc->vn->type) {
    case FILE_TYPE_PLAIN:
    case FILE_TYPE_DIRECTORY:
    case FILE_TYPE_DEVICE:
        *errno_ptr = ENOTSOCK;
        result = -1;
        break;
#if SOCKFS_ENABLE
    case FILE_TYPE_SOCKET:
        result = socket_connect(fdsc->vn->socket.sock, addr, addrlen, errno_ptr);
        break;
#endif
    default:
        kpanic("Unknown file_type");
    }

    if (result == -1) {
        goto done;
    }

done:
    mutex_unlock(&fdsc->dnobj.mtx);
    return result;
}

int vfs_fchdir_fdesc(fdesc *fdsc, int *errno_ptr) {
    // TODO: Does this need to lock fdsc->dnobj.mtx and/or fdsc->vn->mtx? Or does that happen in vfs.c?
    if (fdsc->vn->type == FILE_TYPE_DIRECTORY) {
        // TODO: This should require locking owner_process or owner_process->cwd in some way
        if (thread_current->owner_process->cwd) {
            vfs_fdesc_unref(thread_current->owner_process->cwd);
        }
        vfs_fdesc_ref(fdsc);
        thread_current->owner_process->cwd = fdsc;
        return 0;
    } else {
        *errno_ptr = ENOTDIR;
        return -1;
    }
}
