#include "vfs/devreg.h"
#include "kio/klog.h"
#include "util/util.h"
#include <string.h>

static devreg *devreg_list = NULL;
static rwlock devreg_list_rwl = RWLOCK_INIT;

// TODO: Fix all the places where the result of this isn't checked
devreg *devreg_new(const char *name, int major, int minor) {
    devreg *dr = dynobj_rwl_new(devreg, devreg_cleanup);
    if (dr) {
        strncpy(dr->name, name, sizeof(dr->name));
        dr->name[sizeof(dr->name) - 1] = '\0';
        dr->major = major;
        dr->minor = minor;

        RWLOCK_WRITE_WITH(&devreg_list_rwl, {
            // Advance next_ptr to point to the current NULL pointer in list
            devreg **next_ptr = &devreg_list;
            while (*next_ptr) {
                next_ptr = &((*next_ptr)->next);
            }

            // Add fst to list
            dr->next = NULL;
            *next_ptr = dr;
        })
    }

    return dr;
}

void devreg_cleanup(void *dr_arg) {
    devreg *dr = dr_arg;

    // Advance next_ptr to point to an entry pointing to dr
    devreg **next_ptr = &devreg_list;
    while (*next_ptr && *next_ptr != dr) {
        next_ptr = &((*next_ptr)->next);
    }

    if (!*next_ptr) {
        kpanic("Tried to cleanup devreg not in list");
    }

    // Remove dr from the list
    *next_ptr = (*next_ptr)->next;
}

void devreg_ref(devreg *dr) {
    dynobj_rwl_ref(devreg, &dr->dnobj);
}

void devreg_unref(devreg *dr) {
    dynobj_rwl_unref(devreg, &dr->dnobj);
}

devreg *devreg_lookup(const char *name) {
    devreg *dr;

    RWLOCK_READ_WITH(&devreg_list_rwl, {
        for (dr = devreg_list; dr; dr = dr->next) {
            if (strcmp(dr->name, name) == 0) {
                break;
            }
        }

        if (dr)
            devreg_ref(dr);
    })

    return dr;
}

devreg *devreg_get_nth(unsigned long n) {
    devreg *dr;

    RWLOCK_READ_WITH(&devreg_list_rwl, {
        dr = devreg_list;
        for (; n > 0 && dr; --n) {
            dr = dr->next;
        }

        if (dr)
            devreg_ref(dr);
    })

    return dr;
}

bool devreg_exists(const char *name) {
    bool result = false;

    RWLOCK_READ_WITH(&devreg_list_rwl, {
        for (devreg *dr = devreg_list; dr; dr = dr->next) {
            if (strcmp(dr->name, name) == 0) {
                result = true;
                break;
            }
        }
    })

    return result;
}
