#include "vfs/superblock.h"

superblock *vfs_superblock_new(filesystem_type *type) {
    superblock *sb = dynobj_mtx_new(superblock, dynobj_no_cleanup);

    if (!sb) {
        return NULL;
    }

    sb->type = type;

    sb->vn_hshtbl = (hashtable) HASHTABLE_INIT;
    mutex_init(&sb->vn_hshtbl_mtx);

    return sb;
}

void vfs_superblock_ref(superblock *sb) {
    dynobj_mtx_ref(superblock, &sb->dnobj);
}

void vfs_superblock_unref(superblock *sb) {
    dynobj_mtx_unref(superblock, &sb->dnobj);
}
