#pragma once

#include "config.h"
#include "device/hardware/v99x8.h"
#include "mem/mem.h"

#if DEV_V99X8_ENABLE

#define V99X8_BASE  (mem_io_start + 0x80000)

#define V99X8_VRAM_DATA_RW(base)                    (base + 0x0)
#define V99X8_STATUS_REGISTER_R(base)               (base + 0x2)
#define V99X8_VRAM_ADDRESS_W(base)                  (base + 0x2)
#define V99X8_REGISTER_SETUP_W(base)                (base + 0x2)
#define V99X8_PALETTE_REGISTERS_W(base)             (base + 0x4)
#define V99X8_REGISTER_INDIRECT_ADDRESSING_W(base)  (base + 0x6)

#endif
