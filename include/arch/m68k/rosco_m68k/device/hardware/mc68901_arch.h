#pragma once

#include "config.h"
#include "movep.h"
#include "device/hardware/mc68901.h"
#include "mem/mem.h"

#if DEV_MC68901_ENABLE

static inline unsigned long mc68901_xtal_freq_get(void) {
    return 3686400;
}

#define MC68901_BASE    (mem_io_start + 0x01)

#define MC68901_GPDR(base)  (base + 0x00)
#define MC68901_AER(base)   (base + 0x02)
#define MC68901_DDR(base)   (base + 0x04)
#define MC68901_IERA(base)  (base + 0x06)
#define MC68901_IERB(base)  (base + 0x08)
#define MC68901_IPRA(base)  (base + 0x0A)
#define MC68901_IPRB(base)  (base + 0x0C)
#define MC68901_ISRA(base)  (base + 0x0E)
#define MC68901_ISRB(base)  (base + 0x10)
#define MC68901_IMRA(base)  (base + 0x12)
#define MC68901_IMRB(base)  (base + 0x14)
#define MC68901_VR(base)    (base + 0x16)
#define MC68901_TACR(base)  (base + 0x18)
#define MC68901_TBCR(base)  (base + 0x1A)
#define MC68901_TCDCR(base) (base + 0x1C)
#define MC68901_TADR(base)  (base + 0x1E)
#define MC68901_TBDR(base)  (base + 0x20)
#define MC68901_TCDR(base)  (base + 0x22)
#define MC68901_TDDR(base)  (base + 0x24)
#define MC68901_SCR(base)   (base + 0x26)
#define MC68901_UCR(base)   (base + 0x28)
#define MC68901_RSR(base)   (base + 0x2A)
#define MC68901_TSR(base)   (base + 0x2C)
#define MC68901_UDR(base)   (base + 0x2E)

#if DEV_MC68901_USE_MOVEP

static inline enum mc68901_interrupt mc68901_ier_get(void) {
    return movep_from_w(MC68901_BASE, MC68901_IERA(0));
}

static inline enum mc68901_interrupt mc68901_ier_set(enum mc68901_interrupt val) {
    movep_to_w(MC68901_BASE, MC68901_IERA(0), val);
    return val;
}

static inline enum mc68901_interrupt mc68901_ipr_get(void) {
    return movep_from_w(MC68901_BASE, MC68901_IPRA(0));
}

static inline enum mc68901_interrupt mc68901_ipr_set(enum mc68901_interrupt val) {
    movep_to_w(MC68901_BASE, MC68901_IPRA(0), val);
    return val;
}

static inline enum mc68901_interrupt mc68901_isr_get(void) {
    return movep_from_w(MC68901_BASE, MC68901_ISRA(0));
}

static inline enum mc68901_interrupt mc68901_isr_set(enum mc68901_interrupt val) {
    movep_to_w(MC68901_BASE, MC68901_ISRA(0), val);
    return val;
}

static inline enum mc68901_interrupt mc68901_imr_get(void) {
    return movep_from_w(MC68901_BASE, MC68901_IMRA(0));
}

static inline enum mc68901_interrupt mc68901_imr_set(enum mc68901_interrupt val) {
    movep_to_w(MC68901_BASE, MC68901_IMRA(0), val);
    return val;
}

#else

#define mc68901_ier_get mc68901_ier_get_generic
#define mc68901_ier_set mc68901_ier_set_generic

#define mc68901_ipr_get mc68901_ipr_get_generic
#define mc68901_ipr_set mc68901_ipr_set_generic

#define mc68901_isr_get mc68901_isr_get_generic
#define mc68901_isr_set mc68901_isr_set_generic

#define mc68901_imr_get mc68901_imr_get_generic
#define mc68901_imr_set mc68901_imr_set_generic

#endif
#endif
