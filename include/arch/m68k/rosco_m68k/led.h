#pragma once

#include <stdbool.h>

typedef enum led {
    LED_RED,
    LED_GREEN,

    LED_END,
} led;

bool led_get(led ld);
void led_set(led ld, bool state);
void led_toggle(led ld);
