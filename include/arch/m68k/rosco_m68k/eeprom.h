#pragma once

#include <stdint.h>

#define EEPROM_DUAL_BYTE(val) ((uint16_t) ((uint8_t) val) << 8 | (uint16_t) ((uint8_t) val) << 0)

static inline uint8_t eeprom_1x8_read(volatile uint8_t *base, unsigned char shift, uint32_t offset) {
    volatile uint8_t *ptr = base + (offset << shift);
    return *ptr;
}

static inline void eeprom_1x8_write(volatile uint8_t *base, unsigned char shift, uint32_t offset, uint8_t data) {
    volatile uint8_t *ptr = base + (offset << shift);
    *ptr = data;
}

static inline void eeprom_1x8_software_command_3_offset(volatile uint8_t *base, unsigned char shift, uint32_t write_3_offset, uint8_t write_3_data) {
    eeprom_1x8_write(base, shift, 0x5555, 0xAA);
    eeprom_1x8_write(base, shift, 0x2AAA, 0x55);
    eeprom_1x8_write(base, shift, write_3_offset, write_3_data);
}

static inline void eeprom_1x8_software_command_3(volatile uint8_t *base, unsigned char shift, uint8_t write_3_data) {
    eeprom_1x8_software_command_3_offset(base, shift, 0x5555, write_3_data);
}

static inline void eeprom_1x8_software_command_6_offset(volatile uint8_t *base, unsigned char shift, uint32_t write_6_offset, uint8_t write_6_data) {
    eeprom_1x8_software_command_3(base, shift, 0x80);
    eeprom_1x8_software_command_3_offset(base, shift, write_6_offset, write_6_data);
}

static inline void eeprom_1x8_software_command_6(volatile uint8_t *base, unsigned char shift, uint8_t write_6_data) {
    eeprom_1x8_software_command_6_offset(base, shift, 0x5555, write_6_data);
}

static inline void eeprom_1x8_software_read_reset(volatile uint8_t *base, unsigned char shift) {
#if 1
    eeprom_1x8_write(base, shift, 0, 0xF0);
#else
    eeprom_1x8_software_command(base, shift, 0xF0);
#endif
}

static inline void eeprom_1x8_software_autoselect(volatile uint8_t *base, unsigned char shift) {
    eeprom_1x8_software_command_3(base, shift, 0x90);
}

static inline void eeprom_1x8_byte_program(volatile uint8_t *base, unsigned char shift, uint32_t offset, uint8_t data) {
    eeprom_1x8_software_command_3(base, shift, 0xA0);
    eeprom_1x8_write(base, shift, offset, data);
}

static inline void eeprom_1x8_chip_erase(volatile uint8_t *base, unsigned char shift) {
    eeprom_1x8_software_command_6(base, shift, 0x10);
}

static inline void eeprom_1x8_sector_erase(volatile uint8_t *base, unsigned char shift, uint32_t offset) {
    eeprom_1x8_software_command_6_offset(base, shift, offset, 0x30);
}

static inline void eeprom_1x8_sector_erase_suspend(volatile uint8_t *base, unsigned char shift) {
    eeprom_1x8_write(base, shift, 0, 0xB0);
}

static inline void eeprom_1x8_sector_erase_resume(volatile uint8_t *base, unsigned char shift) {
    eeprom_1x8_write(base, shift, 0, 0x30);
}

static inline uint16_t eeprom_2x8_read(volatile uint16_t *base, unsigned char shift, uint32_t offset) {
    volatile uint16_t *ptr = base + (offset << shift);
    return *ptr;
}

static inline void eeprom_2x8_write(volatile uint16_t *base, unsigned char shift, uint32_t offset, uint16_t data) {
    volatile uint16_t *ptr = base + (offset << shift);
    *ptr = data;
}

static inline void eeprom_2x8_software_command_3_offset(volatile uint16_t *base, unsigned char shift, uint32_t write_3_offset, uint16_t write_3_data) {
    eeprom_2x8_write(base, shift, 0x5555, EEPROM_DUAL_BYTE(0xAA));
    eeprom_2x8_write(base, shift, 0x2AAA, EEPROM_DUAL_BYTE(0x55));
    eeprom_2x8_write(base, shift, write_3_offset, write_3_data);
}

static inline void eeprom_2x8_software_command_3(volatile uint16_t *base, unsigned char shift, uint16_t write_3_data) {
    eeprom_2x8_software_command_3_offset(base, shift, EEPROM_DUAL_BYTE(0x55), write_3_data);
}

static inline void eeprom_2x8_software_command_6_offset(volatile uint16_t *base, unsigned char shift, uint32_t write_6_offset, uint16_t write_6_data) {
    eeprom_2x8_software_command_3(base, shift, EEPROM_DUAL_BYTE(0x80));
    eeprom_2x8_software_command_3_offset(base, shift, write_6_offset, write_6_data);
}

static inline void eeprom_2x8_software_command_6(volatile uint16_t *base, unsigned char shift, uint16_t write_6_data) {
    eeprom_2x8_software_command_6_offset(base, shift, EEPROM_DUAL_BYTE(0x55), write_6_data);
}

static inline void eeprom_2x8_software_read_reset(volatile uint16_t *base, unsigned char shift) {
#if 1
    eeprom_2x8_write(base, shift, 0, EEPROM_DUAL_BYTE(0xF0));
#else
    eeprom_2x8_software_command(base, shift, EEPROM_DUAL_BYTE(0xF0));
#endif
}

static inline void eeprom_2x8_software_autoselect(volatile uint16_t *base, unsigned char shift) {
    eeprom_2x8_software_command_3(base, shift, EEPROM_DUAL_BYTE(0x90));
}

static inline void eeprom_2x8_byte_program(volatile uint16_t *base, unsigned char shift, uint32_t offset, uint16_t data) {
    eeprom_2x8_software_command_3(base, shift, EEPROM_DUAL_BYTE(0xA0));
    eeprom_2x8_write(base, shift, offset, data);
}

static inline void eeprom_2x8_chip_erase(volatile uint16_t *base, unsigned char shift) {
    eeprom_2x8_software_command_6(base, shift, EEPROM_DUAL_BYTE(0x10));
}

static inline void eeprom_2x8_sector_erase(volatile uint16_t *base, unsigned char shift, uint32_t offset) {
    eeprom_2x8_software_command_6_offset(base, shift, offset, EEPROM_DUAL_BYTE(0x30));
}

static inline void eeprom_2x8_sector_erase_suspend(volatile uint16_t *base, unsigned char shift) {
    eeprom_2x8_write(base, shift, 0, EEPROM_DUAL_BYTE(0xB0));
}

static inline void eeprom_2x8_sector_erase_resume(volatile uint16_t *base, unsigned char shift) {
    eeprom_2x8_write(base, shift, 0, EEPROM_DUAL_BYTE(0x30));
}
