#pragma once

#include <stdbool.h>

typedef void (*timer_tick_function)(long actual_interval_ns);
extern timer_tick_function timer_on_tick;

bool timer_init(long target_interval_ns, timer_tick_function on_tick);
