#pragma once

#include <stdbool.h>

void safeprobe_begin(void);
bool safeprobe_trybegin(void);
bool safeprobe_end_was_clean(void);
