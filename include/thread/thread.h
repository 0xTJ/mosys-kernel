#pragma once

#include "kalloc.h"
#include "util/list.h"
#include "util/spinlock.h"
#include "util/with.h"
#include <stdbool.h>
#include <stdnoreturn.h>

#define THREAD_TRACE_COUNT 0

typedef enum thread_state {
    THREAD_STATE_SLEEPING = 0,  // Waiting to be runnable
    THREAD_STATE_ACTIVE,        // Running or able to run
    THREAD_STATE_TERMINATED,    // Permanently terminated
} thread_state;

typedef struct thread {
    // Assembly-used fields
    void *ssp;  // Must be the 1st element
    void *usp;  // Must be the 2nd element

    struct exception_state *user_state;

    thread_state state;
    struct mem_mapping *sup_stack;
    struct process *owner_process;
    unsigned long long sleep_until_us;

    list_member owned_threads_link; // Used to associate with owning process
    list_member scheduler_link;     // Used for scheduling
} thread;

extern thread *thread_current;

void thread_init(void);

thread *thread_new(void);
void thread_delete(thread *this);

// This happens in timer tick interrupt context
void thread_system_tick(long actual_interval_ns);

void thread_setup(thread *this, void (*func)(), struct mem_mapping *sup_stack);
void thread_setup_stack(thread *this, void (*func)());
noreturn void thread_idle_loop(void);

void thread_disable_irq(void);
void thread_undisable_irq(void);

void thread_scheduler_lock(void);
void thread_scheduler_unlock(void);

void thread_enter_critical(void);
void thread_leave_critical(void);

#define CRITICAL(statement) WITH(_in_critical, thread_enter_critical(), thread_leave_critical(), statement)

bool thread_inited(void);
void thread_assert_init(void);

thread *thread_switch(thread *this);
void thread_schedule(void);
void thread_terminate(void);

void thread_release_resources(void);

bool thread_wake(thread *this);

// Declared here to give new threads to sched
struct process *process_get_sched(void);
