#pragma once

#define CONSOLE_DEVICE_NAME "tty0"
#define CONSOLE_BACKUP_DEVICE_NAME "ttyS0"

#define DEVICE_NUM 32
#define DEVICE_MAJOR_UNUSED_START 16

#define DEV_AT25_ENABLE         0
#define DEV_MC68681_ENABLE      1
#define DEV_MC68901_ENABLE      0
#define DEV_ROSCOKBD_ENABLE     1
#define DEV_SPIDEV_ENABLE       1
#define DEV_SPISD_ENABLE        1
#define DEV_SST39SF0X0_ENABLE   1
#define DEV_UARTUSB_ENABLE      0
#define DEV_V99X8_ENABLE        0
#define DEV_VIRTTERM_ENABLE     1
#define DEV_XOSERA_ENABLE       1

#define DEV_LOOP_NUM            1

#define DEV_MC68901_USE_MOVEP   1

#define DEV_MC68681_USE_INTERRUPTS          1
#define DEV_MC68681_BUFFER_SIZE             1024
#define DEV_MC68681_TICK_CLOCK_ON_RED_LED   0
#define DEV_MC68681_TICK_FREQUENCY          100

#define DEV_ROSCOKBD_TTY        "ttyS1"

#define DEV_SPISD_MAX_DEVICES   8

#define DEV_VIRTTERM_KBD_ENABLE         1
#define DEV_VIRTTERM_ALT_RAW_TTY_ENABLE 1
#define DEV_VIRTTERM_REQUIRE_DEV_IN     1
#define DEV_VIRTTERM_DEV_OUT_FB         "fb0"
#define DEV_VIRTTERM_DEV_IN_KBD         "kbd"
#define DEV_VIRTTERM_DEV_IN_ALT_RAW_TTY "ttyS1"

// Enabling EXCEPTION_TABLE_USE_ATOMIC allows chained vector replacement to be thread-safe
#define EXCEPTION_TABLE_USE_ATOMIC 0

#define FATFS_VERBOSE_DEBUG 0

#define HASHTABLE_DEFAULT_SIZE  64

#define KLOG_USE_COLOURS        1
#define KLOG_INFO_HAS_LINE_NUM  0
#define KLOG_WARN_HAS_LINE_NUM  0
#define KLOG_PANIC_HAS_LINE_NUM 1

#define MEM_USE_MMU 0

#define NETWORK_INTERNET_ENABLE         0
#define NETWORK_INTERNET_IPV4_ENABLE    0

#define NETWORK_LINK_ENABLE             0
#define NETWORK_LINK_ETHERNET_ENABLE    0
#define NETWORK_LINK_LOOPBACK_ENABLE    0
#define NETWORK_LINK_SLIP_ENABLE        0

#define PROCESS_ARGS_MAX_PAGES                  1
#define PROCESS_CLONE_USER_STACK_BYTES_SAVED    256
#define PROCESS_EXEC_MAGIC_SIZE                 128
#define PROCESS_INTERP_LIMIT                    4
#define PROCESS_USR_STACK_SIZE                  0x2000
#define PROCESS_SUP_STACK_SIZE                  0x2000

#define SOCKET_ENABLE       0
#define SOCKET_IP_ENABLE    0
#define SOCKET_LOCAL_ENABLE 0

#define SOCKFS_ENABLE   0

#define SPISD_VERBOSE_DEBUG 0

#define STRING_USE_UNSAFE_MEMMOVE   1

#define SYMMETRIC_MULTIPROCESSING   0

#define SYSCALLS_DEBUG 0
#define ALLOW_TRACE_ON_SYSCALLS 0

#define TTY_NUM 64

#define UARTUSB_MODULE_DEPS     "mc68681"
#define UARTUSB_DEVICE          "ttyS1"

#define UTIL_WITH_CLEANUP_CHECK 1

#define VFS_MAX_OPEN_FILES  16

#define VIRTTERM_IS_XOSERA  1
