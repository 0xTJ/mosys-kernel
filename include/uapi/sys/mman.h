#pragma once

#include "mem/umem.h"

#define PROT_EXEC   0x04
#define PROT_NONE   0x00
#define PROT_READ   0x01
#define PROT_WRITE  0x02

#define MAP_FIXED       0x01
#define MAP_PRIVATE     0x02
#define MAP_SHARED      0x04
#define MAP_ANONYMOUS   0x08
#define MAP_ANON        MAP_ANONYMOUS

#define MAP_FAILED ((umem_ptr) -1)
