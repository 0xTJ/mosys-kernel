#pragma once

#include <limits.h>

#define MAX_CANON   255
#define MAX_INPUT   255
#define NAME_MAX    32
#define PATH_MAX    256

#define SSIZE_MAX LONG_MAX
