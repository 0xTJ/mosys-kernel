#pragma once

// This uses the same macros as Linux with the same values, for compatibility

#include "uapi/asm/ioctl.h"
#include "mem/flex.h"
#include "mem/mem.h"
#include <stdint.h>
#include <stdbool.h>

#define FB_MAX 32

#define FB_MAGIC 'F'

#define FBIOGET_VSCREENINFO     _IO(FB_MAGIC, 0x00)
#define FBIOPUT_VSCREENINFO     _IO(FB_MAGIC, 0x01)
#define FBIOGET_FSCREENINFO     _IO(FB_MAGIC, 0x02)
#define FBIOGETCMAP             _IO(FB_MAGIC, 0x04)
#define FBIOPUTCMAP             _IO(FB_MAGIC, 0x05)
#define FBIOPAN_DISPLAY         _IO(FB_MAGIC, 0x06)
#define FBIO_CURSOR             _IOWR(FB_MAGIC, 0x08, struct fb_cursor)
#define FBIOGET_CON2FBMAP       _IO(FB_MAGIC, 0x0F)
#define FBIOPUT_CON2FBMAP       _IO(FB_MAGIC, 0x10)
#define FBIOBLANK               _IO(FB_MAGIC, 0x11)
#define FBIOGET_VBLANK          _IOR(FB_MAGIC, 0x12, struct fb_vblank)
#define FBIO_ALLOC              _IO(FB_MAGIC, 0x13)
#define FBIO_FREE               _IO(FB_MAGIC, 0x14)
#define FBIOGET_GLYPH           _IO(FB_MAGIC, 0x15)
#define FBIOGET_HWCINFO         _IO(FB_MAGIC, 0x16)
#define FBIOPUT_MODEINFO        _IO(FB_MAGIC, 0x17)
#define FBIOGET_DISPINFO        _IO(FB_MAGIC, 0x18)
#define FBIO_WAITFORVSYNC       _IOW(FB_MAGIC, 0x20, uint32_t)

enum fb_type {
    FB_TYPE_PACKED_PIXELS       = 0,
    FB_TYPE_PLANES              = 1,
    FB_TYPE_INTERLEAVED_PLANES  = 2,
    FB_TYPE_TEXT                = 3,
    FB_TYPE_VGA_PLANES          = 4,
    FB_TYPE_FOURCC              = 5,
};

enum fb_visual {
    FB_VISUAL_MONO01                = 0,
    FB_VISUAL_MONO10                = 1,
    FB_VISUAL_TRUECOLOR             = 2,
    FB_VISUAL_PSEUDOCOLOR           = 3,
    FB_VISUAL_DIRECTCOLOR           = 4,
    FB_VISUAL_STATIC_PSEUDOCOLOR    = 5,
    FB_VISUAL_FOURCC                = 6,
};

enum fb_accel {
    FB_ACCEL_NONE           = 0,
    FB_ACCEL_YAMAHA_V9938   = 0xB1,
    FB_ACCEL_YAMAHA_V9958   = 0xB2,
    FB_ACCEL_XOSERA         = 0xB3,
};

struct fb_fix_screeninfo {
    char id[16];
    mem_addr smem_start;
    size_t smem_len;
    enum fb_type type;
    uint32_t type_aux;
    enum fb_visual visual;
    uint16_t xpanstep;
    uint16_t ypanstep;
    uint16_t ywrapstep;
    uint32_t line_length;
    mem_addr mmio_start;
    size_t mmio_len;
    uint32_t accel;
    uint16_t capabilities;
    uint16_t reserved[2];
};

struct fb_bitfield {
    uint32_t offset;
    uint32_t length;
    bool msb_right;
};

#define PICOS2KHZ(a)    (1000000000UL / (a))
#define KHZ2PICOS(a)    (1000000000UL / (a))

struct fb_var_screeninfo {
    uint32_t xres;
    uint32_t yres;
    uint32_t xres_virtual;
    uint32_t yres_virtual;
    uint32_t xoffset;
    uint32_t yoffset;

    uint32_t bits_per_pixel;
    bool grayscale;
    struct fb_bitfield red;
    struct fb_bitfield green;
    struct fb_bitfield blue;
    struct fb_bitfield transp;

    uint32_t nonstd;

    uint32_t activate;

    uint32_t height;
    uint32_t width;

    uint32_t pixclock;
    uint32_t left_margin;
    uint32_t right_margin;
    uint32_t upper_margin;
    uint32_t lower_margin;
    uint32_t hsync_len;
    uint32_t vsync_len;
    uint32_t sync;
    uint32_t vmode;
    uint32_t rotate;
    uint32_t colorspace;
    uint32_t reserved[4];
};

struct fb_cmap {
    uint32_t start;
    uint32_t len;
    FLEX_PTR_MUT(uint16_t) red;
    FLEX_PTR_MUT(uint16_t) green;
    FLEX_PTR_MUT(uint16_t) blue;
    FLEX_PTR_MUT(uint16_t) transp;
};

struct fb_con2fbmap {
    uint32_t console;
    uint32_t framebuffer;
};

#define VESA_NO_BLANKING    0
#define VESA_VSYNC_SUSPEND  1
#define VESA_HSYNC_SUSPEND  2
#define VESA_POWERDOWN      3

enum {
    FB_BLANK_UNBLANK       = VESA_NO_BLANKING,
    FB_BLANK_NORMAL        = VESA_NO_BLANKING + 1,
    FB_BLANK_VSYNC_SUSPEND = VESA_VSYNC_SUSPEND + 1,
    FB_BLANK_HSYNC_SUSPEND = VESA_HSYNC_SUSPEND + 1,
    FB_BLANK_POWERDOWN     = VESA_POWERDOWN + 1
};

#define FB_VBLANK_VBLANKING     0x001
#define FB_VBLANK_HBLANKING     0x002
#define FB_VBLANK_HAVE_VBLANK   0x004
#define FB_VBLANK_HAVE_HBLANK   0x008
#define FB_VBLANK_HAVE_COUNT    0x010
#define FB_VBLANK_HAVE_VCOUNT   0x020
#define FB_VBLANK_HAVE_HCOUNT   0x040
#define FB_VBLANK_VSYNCING      0x080
#define FB_VBLANK_HAVE_VSYNC    0x100

struct fb_vblank {
    uint32_t flags;
    uint32_t count;
    uint32_t vcount;
    uint32_t hcount;
    uint32_t reserved[4];
};

#define ROP_COPY    0
#define ROP_XOR     1

struct fb_copyarea {
    uint32_t dx;
    uint32_t dy;
    uint32_t width;
    uint32_t height;
    uint32_t sx;
    uint32_t sy;
};

struct fb_fillrect {
    uint32_t dx;
    uint32_t dy;
    uint32_t width;
    uint32_t height;
    uint32_t color;
    uint32_t rop;
};

struct fb_image {
    uint32_t dx;
    uint32_t dy;
    uint32_t width;
    uint32_t height;
    uint32_t fg_color;
    uint32_t bg_color;
    uint8_t  depth;
    FLEX_PTR_CONST(char) data;
    struct fb_cmap cmap;
};

#define FB_CUR_SETIMAGE 0x01
#define FB_CUR_SETPOS   0x02
#define FB_CUR_SETHOT   0x04
#define FB_CUR_SETCMAP  0x08
#define FB_CUR_SETSHAPE 0x10
#define FB_CUR_SETSIZE  0x20
#define FB_CUR_SETALL   0xFF

struct fbcurpos {
    uint16_t x, y;
};

struct fb_cursor {
    uint16_t set;
    uint16_t enable;
    uint16_t rop;
    FLEX_PTR_MUT(char) mask;
    struct fbcurpos hot;
    struct fb_image image;
};

#define FB_BACKLIGHT_LEVELS    128
#define FB_BACKLIGHT_MAX    0xFF
