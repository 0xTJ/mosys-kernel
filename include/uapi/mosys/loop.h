#pragma once

#include "uapi/asm/ioctl.h"
#include <stdint.h>

struct loop_info {
    int lo_offset;
};

#define LOOP_IOC_MAGIC 'L'

#define LOOP_SET_FD     _IO(LOOP_IOC_MAGIC, 0x00)
#define LOOP_CLR_FD     _IO(LOOP_IOC_MAGIC, 0x01)
#define LOOP_SET_STATUS _IOW(LOOP_IOC_MAGIC, 0x02, struct loop_info)
#define LOOP_GET_STATUS _IOR(LOOP_IOC_MAGIC, 0x03, struct loop_info)
