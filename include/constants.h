#pragma once

#include <stdint.h>

#define FACTOR_1    INTMAX_C(1)
#define FACTOR_10   INTMAX_C(10)
#define FACTOR_100  INTMAX_C(100)
#define FACTOR_1000 INTMAX_C(1000)
#define FACTOR_1024 INTMAX_C(1024)

#define DECA        (FACTOR_10)
#define HECTO       (FACTOR_100)
#define KILO        (FACTOR_1000)
#define MEGA        (FACTOR_1000 * KILO)
#define GIGA        (FACTOR_1000 * MEGA)
#define TERA        (FACTOR_1000 * GIGA)
#define PETA        (FACTOR_1000 * TERA)
#define EXA         (FACTOR_1000 * PETA)

#define DECI_PER    (FACTOR_10)
#define CENTI_PER   (FACTOR_100)
#define MILLI_PER   (FACTOR_1000)
#define MICRO_PER   (FACTOR_1000 * MILLI_PER)
#define NANO_PER    (FACTOR_1000 * MICRO_PER)
#define PICO_PER    (FACTOR_1000 * NANO_PER)
#define FEMTO_PER   (FACTOR_1000 * PICO_PER)
#define ATTO_PER    (FACTOR_1000 * FEMTO_PER)

#define KIBI        (FACTOR_1024)
#define MEBI        (FACTOR_1024 * KIBI)
#define GIBI        (FACTOR_1024 * MEBI)
#define TEBI        (FACTOR_1024 * GIBI)
#define PEBI        (FACTOR_1024 * TEBI)
#define EXBI        (FACTOR_1024 * PEBI)
