#pragma once

#include <stdbool.h>
#include <stddef.h>

typedef struct { void *start; void *end; } ptr_range;
#define PTR_RANGE_NULL ((ptr_range) { NULL, NULL })

static inline bool ptr_range_is_null(ptr_range range) {
    return range.start == PTR_RANGE_NULL.start &&
           range.end == PTR_RANGE_NULL.end;
}

void kalloc_validate(const char *action, const char *desc);
size_t kalloc_free_space(void);

ptr_range kalloc_range_with_desc(size_t size, const char *desc);
void* kalloc_with_desc(size_t size, const char *desc);
void* kalloc_end_with_desc(size_t size, const char *desc);
ptr_range kalloc_clone_with_desc(ptr_range orig, const char *desc);
void kfree_with_desc(void *ptr, const char *desc);
void kfree_range_with_desc(ptr_range range, const char *desc);

#define kalloc_range(size) kalloc_range_with_desc(size, #size)
#define kalloc(size) kalloc_with_desc(size, #size)
#define kalloc_end(size) kalloc_end_with_desc(size, #size)
#define kalloc_clone(size) kalloc_clone_with_desc(size, #size)
#define kfree(size) kfree_with_desc(size, #size)
#define kfree_range(size) kfree_range_with_desc(size, #size)
