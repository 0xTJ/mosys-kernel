#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdarg.h>

#undef printf

void kio_set_use_console(bool use_console);

void kio_early_putc(int c);
void kio_early_puts(char s[]);

void kio_putc(int c, void *ctx);
void kio_puts(char s[], void *ctx);

int kio_vprintf(char const *format, va_list vlist);
int kio_printf(char const *format, ...);
int kio_vsnprintf(char *buffer, size_t bufsz, char const *format, va_list vlist);
int kio_snprintf(char *buffer, size_t bufsz, char const *format, ...);
