#pragma once

#include "kernel/module.h"
#include <assert.h>
#include <stdint.h>

typedef enum exported_symbol_addr_type {
    EXPORTED_SYMBOL_ADDR_TYPE_OBJECT,
    EXPORTED_SYMBOL_ADDR_TYPE_FUNCTION,
} exported_symbol_addr_type;

typedef union exported_symbol_addr {
    void *object;
    void (*function)();
} exported_symbol_addr;

typedef struct exported_symbol {
    exported_symbol_addr_type addr_type;
    exported_symbol_addr addr;
    const char *name;
} exported_symbol;

#define EXPORT_OBJECT(symbol)                                   \
    static const exported_symbol const symbol_desc_ ## symbol   \
    __attribute__((used, section (".symbol_tab")))              \
    = {                                                         \
        .addr_type = EXPORTED_SYMBOL_ADDR_TYPE_OBJECT,          \
        .addr = { .object = &symbol },                          \
        .name = #symbol,                                        \
    }

#define EXPORT_FUNCTION(symbol)                                 \
    static const exported_symbol const symbol_desc_ ## symbol   \
    __attribute__((used, section (".symbol_tab")))              \
    = {                                                         \
        .addr_type = EXPORTED_SYMBOL_ADDR_TYPE_FUNCTION,        \
        .addr = { .function = (void (*)(void)) &symbol },       \
        .name = #symbol,                                        \
    }

const exported_symbol *exported_symbol_from_name(const char *name);
