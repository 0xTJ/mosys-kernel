#pragma once

#include "uapi/sys/types.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#define CPIO_MODE_TYPE_MASK             0170000
#define CPIO_MODE_TYPE_SOCKET           0140000
#define CPIO_MODE_TYPE_SYMLINK          0120000
#define CPIO_MODE_TYPE_REGULAR_FILE     0100000
#define CPIO_MODE_TYPE_BLOCK_DEVICE     0060000
#define CPIO_MODE_TYPE_DIRECTORY        0040000
#define CPIO_MODE_TYPE_CHARACTER_DEVICE 0020000
#define CPIO_MODE_TYPE_PIPE_FIFO        0010000
#define CPIO_MODE_TYPE_SUID             0004000
#define CPIO_MODE_TYPE_SGID             0002000
#define CPIO_MODE_TYPE_STICKY           0001000
#define CPIO_MODE_TYPE_PERM_MASK        0000777

struct cpio_odc_header {
    char magic[6];
    char dev[6];
    char ino[6];
    char mode[6];
    char uid[6];
    char gid[6];
    char nlink[6];
    char rdev[6];
    char mtime[11];
    char namesize[6];
    char filesize[11];
};

#define CPIO_ODC_FIELD_GET_U16(field)                                               \
    static inline uint16_t cpio_odc_get_ ## field(struct cpio_odc_header *ptr) {    \
        return cpio_oct_atoi(ptr->field, sizeof(ptr->field));                       \
    }

#define CPIO_ODC_FIELD_GET_U32(field)                                               \
    static inline uint32_t cpio_odc_get_ ## field(struct cpio_odc_header *ptr) {    \
        return cpio_oct_atoi(ptr->field, sizeof(ptr->field));                       \
    }

unsigned long cpio_oct_atoi(const char *str, ssize_t len);

CPIO_ODC_FIELD_GET_U16(magic)
CPIO_ODC_FIELD_GET_U16(dev)
CPIO_ODC_FIELD_GET_U16(ino)
CPIO_ODC_FIELD_GET_U16(mode)
CPIO_ODC_FIELD_GET_U16(uid)
CPIO_ODC_FIELD_GET_U16(gid)
CPIO_ODC_FIELD_GET_U16(nlink)
CPIO_ODC_FIELD_GET_U16(rdev)
CPIO_ODC_FIELD_GET_U32(mtime)
CPIO_ODC_FIELD_GET_U16(namesize)
CPIO_ODC_FIELD_GET_U32(filesize)
static inline char *cpio_odc_get_name(struct cpio_odc_header *ptr) { return (char *) ptr + sizeof(*ptr); }
char *cpio_odc_get_file(struct cpio_odc_header *ptr);

struct cpio_odc_header *cpio_odc_get_next_nocheck(struct cpio_odc_header *ptr);
struct cpio_odc_header *cpio_odc_get_next(struct cpio_odc_header *ptr);

bool cpio_odc_is_trailer(struct cpio_odc_header *ptr);

struct cpio_odc_header *cpio_odc_get_nth_entry(struct cpio_odc_header *ptr, size_t n);
struct cpio_odc_header *cpio_odc_lookup_name(struct cpio_odc_header *ptr, const char *name, size_t *result_index_ptr);
