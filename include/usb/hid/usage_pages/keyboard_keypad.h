#pragma once

#define USB_HID_USAGE_ID_KEYBOARD_KEYPAD(name)  usb_hid_usage_id_keyboard_keypad_ ## name

typedef enum usb_hid_usage_id_keyboard_keypad {
    USB_HID_USAGE_ID_KEYBOARD_KEYPAD(keyboard_errorrollover)    = 0x01,
    USB_HID_USAGE_ID_KEYBOARD_KEYPAD(keyboard_postfail)         = 0x02,
    USB_HID_USAGE_ID_KEYBOARD_KEYPAD(keyboard_errorundefined)   = 0x03,
    // TODO: Add the rest
} usb_hid_usage_id_keyboard_keypad;

const char *usb_hid_usage_id_keyboard_keypad_to_str(usb_hid_usage_id_keyboard_keypad usage_id);
