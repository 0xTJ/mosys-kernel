#pragma once

#include "usb/hid/usage_pages.h"
#include "usb/hid/usage_pages/generic_desktop.h"
#include "usb/hid/usage_pages/keyboard_keypad.h"
#include "usb/hid/usage_pages/led.h"
#include "usb/hid/usage_pages/button.h"
#include "usb/hid/usage_pages/consumer.h"
#include <stdint.h>

typedef uint32_t usb_hid_usage;
typedef uint16_t usb_hid_usage_id;

const char *usb_hid_usage_to_str(usb_hid_usage usage);
