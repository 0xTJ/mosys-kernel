#pragma once

#define USB_HID_USAGE_PAGE(name)    usb_hid_usage_page_ ## name

typedef enum usb_hid_usage_page {
    USB_HID_USAGE_PAGE(generic_desktop) = 0x01,
    USB_HID_USAGE_PAGE(keyboard_keypad) = 0x07,
    USB_HID_USAGE_PAGE(led)             = 0x08,
    USB_HID_USAGE_PAGE(button)          = 0x09,
    USB_HID_USAGE_PAGE(consumer)        = 0x0C,
} usb_hid_usage_page;

const char *usb_hid_usage_page_to_str(usb_hid_usage_page usage_page);
