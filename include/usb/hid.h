#pragma once

#include "usb/hid/usage.h"

#define USB_HID_TYPE(name)              usb_hid_type_               ## name
#define USB_HID_MAIN_ITEM_TAG(name)     usb_hid_main_item_tag_      ## name
#define USB_HID_GLOBAL_ITEM_TAG(name)   usb_hid_global_item_tag_    ## name
#define USB_HID_LOCAL_ITEM_TAG(name)    usb_hid_local_item_tag_     ## name

typedef enum usb_hid_type {
    USB_HID_TYPE(main)      = 0,
    USB_HID_TYPE(global)    = 1,
    USB_HID_TYPE(local)     = 2,
    USB_HID_TYPE(reserved)  = 3,
} usb_hid_type;

typedef enum usb_hid_main_item_tag {
    USB_HID_MAIN_ITEM_TAG(input)                = 0x8,
    USB_HID_MAIN_ITEM_TAG(output)               = 0x9,
    USB_HID_MAIN_ITEM_TAG(feature)              = 0xB,
    USB_HID_MAIN_ITEM_TAG(collection)           = 0xA,
    USB_HID_MAIN_ITEM_TAG(end_collection)       = 0xC,
} usb_hid_main_item_tag;

typedef enum usb_hid_global_item_tag {
    USB_HID_GLOBAL_ITEM_TAG(usage_page)         = 0x0,
    USB_HID_GLOBAL_ITEM_TAG(logical_minimum)    = 0x1,
    USB_HID_GLOBAL_ITEM_TAG(logical_maximum)    = 0x2,
    USB_HID_GLOBAL_ITEM_TAG(physical_minimum)   = 0x3,
    USB_HID_GLOBAL_ITEM_TAG(physical_maximum)   = 0x4,
    USB_HID_GLOBAL_ITEM_TAG(unit_exponent)      = 0x5,
    USB_HID_GLOBAL_ITEM_TAG(unit)               = 0x6,
    USB_HID_GLOBAL_ITEM_TAG(report_size)        = 0x7,
    USB_HID_GLOBAL_ITEM_TAG(report_id)          = 0x8,
    USB_HID_GLOBAL_ITEM_TAG(report_count)       = 0x9,
    USB_HID_GLOBAL_ITEM_TAG(push)               = 0xA,
    USB_HID_GLOBAL_ITEM_TAG(pop)                = 0xB,
} usb_hid_global_item_tag;

typedef enum usb_hid_local_item_tag {
    USB_HID_LOCAL_ITEM_TAG(usage)               = 0x0,
    USB_HID_LOCAL_ITEM_TAG(usage_minimum)       = 0x1,
    USB_HID_LOCAL_ITEM_TAG(usage_maximum)       = 0x2,
    USB_HID_LOCAL_ITEM_TAG(designator_index)    = 0x3,
    USB_HID_LOCAL_ITEM_TAG(designator_minimum)  = 0x4,
    USB_HID_LOCAL_ITEM_TAG(designator_maximum)  = 0x5,
    USB_HID_LOCAL_ITEM_TAG(string_index)        = 0x7,
    USB_HID_LOCAL_ITEM_TAG(string_maximum)      = 0x8,
    USB_HID_LOCAL_ITEM_TAG(string_minimum)      = 0x9,
    USB_HID_LOCAL_ITEM_TAG(delimiter)           = 0xA,
} usb_hid_local_item_tag;

const char *usb_hid_type_to_str(usb_hid_type type);
const char *usb_hid_main_item_tag_to_str(usb_hid_main_item_tag main_item_tag);
const char *usb_hid_global_item_tag_to_str(usb_hid_global_item_tag global_item_tag);
const char *usb_hid_local_item_tag_to_str(usb_hid_local_item_tag local_item_tag);
