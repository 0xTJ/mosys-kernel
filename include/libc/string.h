#pragma once

#include <stddef.h>

// String manipulation

char *strcpy(char *restrict dest, const char *restrict src);
char *strncpy(char *restrict dest, const char *restrict src, size_t count);

// String examination

size_t strlen(const char *str);
size_t strnlen(const char *str, size_t strsz);
int strcmp(const char *lhs, const char *rhs);
int strncmp(const char *lhs, const char *rhs, size_t count);
char *strchr(const char *str, int ch);
char *strnchr(const char *str, size_t count, int ch);

// Character array manipulation

void *memchr(const void *ptr, int ch, size_t count);
int memcmp(const void *lhs, const void *rhs, size_t count);
void *memset(void *dest, int ch, size_t count);
void *memcpy(void *restrict dest, const void *restrict src, size_t count);
void *memmove(void* dest, const void* src, size_t count);
