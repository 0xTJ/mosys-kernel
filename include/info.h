#pragma once

#include <stdbool.h>

#define INFO_VERSION_MAJOR      0
#define INFO_VERSION_MINOR      11
#define INFO_VERSION_PATCH      1
#define INFO_VERSION_IS_RELEASE false
#define INFO_VERSION_IS_CLEAN   false

#ifdef MOSYS_SOURCE_PATH_SIZE
#define INFO_SOURCE_PATH_SIZE MOSYS_SOURCE_PATH_SIZE
#else
#define INFO_SOURCE_PATH_SIZE 0
#endif

#define INFO_FILE (__FILE__ + INFO_SOURCE_PATH_SIZE)
#define INFO_LINE __LINE__
#define INFO_FUNC __func__
