#pragma once

#include "network/network_types.h"
#include <stdbool.h>
#include <stdint.h>

typedef union {
    uint8_t as_bytes[4];
    net_u32 as_net_u32;
} ipv4_addr;

static const ipv4_addr ipv4_addr_zeros = { .as_net_u32 = 0U };
static const ipv4_addr ipv4_addr_ones = { .as_net_u32 = ~0U };

static inline ipv4_addr ipv4_address_from_bytes(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3) {
    return (ipv4_addr) { .as_bytes = { b0, b1, b2, b3 } };
}

static inline ipv4_addr ipv4_address_from_u32(uint32_t addr) {
    return (ipv4_addr) { .as_net_u32 = hton_u32(addr) };
}

static inline uint32_t ipv4_address_to_u32(ipv4_addr addr) {
    return ntoh_u32(addr.as_net_u32);
}

static inline ipv4_addr ipv4_netmask_from_prefix_bits(uint8_t prefix_bits) {
    uint8_t shift_bits = prefix_bits > 32 ? 0 : 32 - prefix_bits;
    uint32_t netmask = shift_bits > 31 ? 0 : 0xFFFFFFFF << shift_bits;
    return ipv4_address_from_u32(netmask);
}

static inline ipv4_addr ipv4_addr_not(ipv4_addr a) {
    return (ipv4_addr) { .as_net_u32 = ~a.as_net_u32 };
}

static inline ipv4_addr ipv4_addr_and(ipv4_addr a, ipv4_addr b) {
    return (ipv4_addr) { .as_net_u32 = a.as_net_u32 & b.as_net_u32 };
}

static inline ipv4_addr ipv4_addr_or(ipv4_addr a, ipv4_addr b) {
    return (ipv4_addr) { .as_net_u32 = a.as_net_u32 | b.as_net_u32 };
}

static inline ipv4_addr ipv4_addr_xor(ipv4_addr a, ipv4_addr b) {
    return (ipv4_addr) { .as_net_u32 = a.as_net_u32 ^ b.as_net_u32 };
}

static inline bool ipv4_addr_are_equal(ipv4_addr a, ipv4_addr b) {
    return a.as_net_u32 == b.as_net_u32;
}

static inline ipv4_addr ipv4_subnet_network(ipv4_addr addr, ipv4_addr mask) {
    return ipv4_addr_and(addr, mask);
}

static inline ipv4_addr ipv4_subnet_host(ipv4_addr addr, ipv4_addr mask) {
    return ipv4_addr_and(addr, ipv4_addr_not(mask));
}

static inline bool ipv4_subnet_contains_addr(ipv4_addr addr, ipv4_addr mask, ipv4_addr test_addr) {
    ipv4_addr network = ipv4_subnet_network(addr, mask);
    ipv4_addr test_network = ipv4_subnet_network(test_addr, mask);
    return ipv4_addr_are_equal(network, test_network);
}

static inline ipv4_addr ipv4_subnet_broadcast(ipv4_addr addr, ipv4_addr mask) {
    return ipv4_addr_or(ipv4_subnet_network(addr, mask), ipv4_addr_not(mask));
}

static inline bool ipv4_subnet_is_broadcast(ipv4_addr addr, ipv4_addr mask, ipv4_addr test_addr) {
    bool is_in_network = ipv4_subnet_contains_addr(addr, mask, test_addr);
    ipv4_addr test_host = ipv4_subnet_host(test_addr, mask);
    bool is_broadcast = ipv4_addr_are_equal(ipv4_addr_not(mask), test_host);
    return is_in_network && is_broadcast;
}
