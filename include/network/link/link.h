#pragma once

#include "config.h"
#include "device/netdev.h"
#include "network/ethertype.h"
#include "network/internet/internet.h"
#include "uapi/sys/types.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#if NETWORK_LINK_ENABLE

typedef enum link_type {
    LINK_TYPE_NONE,

#if NETWORK_LINK_LOOPBACK_ENABLE
    LINK_TYPE_LOOPBACK,
#endif

#if NETWORK_LINK_ETHERNET_ENABLE
    LINK_TYPE_ETHERNET,
#endif

#if NETWORK_LINK_SLIP_ENABLE
    LINK_TYPE_SLIP,
#endif

} link_type;

typedef struct link_none_frame_info {
    char dummy;
} link_none_frame_info;

#if NETWORK_LINK_LOOPBACK_ENABLE
typedef struct link_loopback_frame_info {
    ethertype type;
} link_loopback_frame_info;
#endif

#if NETWORK_LINK_ETHERNET_ENABLE
typedef struct link_ethernet_frame_info {
    uint8_t mac_dst[6];
    uint8_t mac_src[6];
    ethertype type;
} link_ethernet_frame_info;
#endif

#if NETWORK_LINK_SLIP_ENABLE
typedef struct link_slip_frame_info {
    char _empty;
} link_slip_frame_info;
#endif

typedef struct link_frame_info {
    link_type type;

    bool to_link_broadcast;
    uint8_t tos;            // 5-bit type-of-service

    union {
        struct link_none_frame_info none;

#if NETWORK_LINK_LOOPBACK_ENABLE
        struct link_loopback_frame_info loopback;
#endif

#if NETWORK_LINK_ETHERNET_ENABLE
        struct link_ethernet_frame_info ethernet;
#endif

#if NETWORK_LINK_SLIP_ENABLE
        struct link_slip_frame_info slip;
#endif

    };
} link_frame_info;

struct netdev;

typedef struct link_ops {
    ssize_t (*needed_frame_length)(
        struct netdev *ntdv, const link_frame_info *info,
        size_t payload_length,
        int *errno_ptr
    );
    bool (*alloc_raw_frame)(
        struct netdev *ntdv,
        size_t frame_length, char **frame_out,
        int *errno_ptr
    );
    char *(*alloc_frame)(
        struct netdev *ntdv, const link_frame_info *info,
        size_t payload_length,
        size_t *frame_length_out,
        int *errno_ptr
    );
    void (*dealloc_frame)(
        struct netdev *ntdv, const link_frame_info *info,
        size_t frame_length, char frame[frame_length],
        int *errno_ptr
    );
    char *(*payload_in_frame)(
        struct netdev *ntdv, const link_frame_info *info,
        size_t frame_length, char frame[frame_length],
        size_t payload_length,
        int *errno_ptr
    );
    bool (*make_frame)(
        struct netdev *ntdv, const link_frame_info *info,
        size_t payload_length, char **payload_out,
        size_t *frame_length_out, char **frame_out,
        int *errno_ptr
    );
    size_t (*encode_frame)(
        struct netdev *ntdv, const link_frame_info *info,
        size_t frame_length, char frame[frame_length],
        size_t payload_length, char payload[payload_length],
        int *errno_ptr
    );
    bool (*decode_frame)(
        struct netdev *ntdv,
        size_t frame_length, char frame[frame_length],
        link_frame_info *info_out,
        size_t *payload_length_out, char **payload_out,
        int *errno_ptr
    );
    bool (*send_encoded_frame)(
        struct netdev *ntdv, const link_frame_info *info,
        size_t frame_length, char frame[frame_length],
        int *errno_ptr
    );
    bool (*poll)(
        struct netdev *ntdv,
        int *errno_ptr
    );
    internet_type (*identify_network_type)(
        struct netdev *ntdv,
        const link_frame_info *info
    );
} link_ops;

ssize_t link_needed_frame_length(
    struct netdev *ntdv, const link_frame_info *info,
    size_t payload_length,
    int *errno_ptr
);

bool link_alloc_raw_frame(
    struct netdev *ntdv,
    size_t frame_length, char **frame_out,
    int *errno_ptr
);

char *link_alloc_frame(
    struct netdev *ntdv, const link_frame_info *info,
    size_t payload_length,
    size_t *frame_length_out,
    int *errno_ptr
);

void link_dealloc_frame(
    struct netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
);

char *link_payload_in_frame(
    struct netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    size_t payload_length,
    int *errno_ptr
);

size_t link_encode_frame(
    struct netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    size_t payload_length, char payload[payload_length],
    int *errno_ptr
);

bool link_decode_frame(
    struct netdev *ntdv,
    size_t frame_length, char frame[frame_length],
    link_frame_info *info_out,
    size_t *payload_length_out, char **payload_out,
    int *errno_ptr
);

bool link_make_frame(
    struct netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char **payload_out,
    size_t *frame_length_out, char **frame_out,
    int *errno_ptr
);

bool link_send_encoded_frame(
    struct netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
);

void link_receive_decoded_frame(
    struct netdev *ntdv,
    size_t frame_length, char frame[frame_length],
    const link_frame_info *info,
    size_t payload_length, char payload[payload_length],
    int *errno_ptr
);

bool link_send_frame(
    struct netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char payload[payload_length],
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
);

void link_receive_frame(
    struct netdev *ntdv,
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
);

bool link_poll(
    struct netdev *ntdv,
    int *errno_ptr
);

internet_type link_identify_network_type(
    struct netdev *ntdv,
    const link_frame_info *info
);

#endif
