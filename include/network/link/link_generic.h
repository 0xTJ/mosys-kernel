#pragma once

#include "config.h"
#include "device/netdev.h"
#include "network/link/link.h"
#include "uapi/sys/types.h"

#if NETWORK_LINK_ENABLE

bool link_generic_alloc_raw_frame(
    netdev *ntdv,
    size_t frame_length, char **frame_out,
    int *errno_ptr
);

char *link_generic_alloc_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length,
    size_t *frame_length_out,
    int *errno_ptr
);

void link_generic_dealloc_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
);

bool link_generic_make_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char **payload_out,
    size_t *frame_length_out, char **frame_out,
    int *errno_ptr
);

#endif
