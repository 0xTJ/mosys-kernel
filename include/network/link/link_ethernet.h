#pragma once

#include "config.h"
#include "network/link/link.h"
#include "uapi/sys/types.h"

#if NETWORK_LINK_ETHERNET_ENABLE

#if !NETWORK_LINK_ENABLE
#error NETWORK_LINK_ETHERNET_ENABLE requires NETWORK_LINK_ENABLE
#endif

ssize_t link_ethernet_needed_frame_length(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length,
    int *errno_ptr
);

size_t link_ethernet_encode_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char payload[payload_length],
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
);

bool link_ethernet_decode_frame(
    netdev *ntdv,
    size_t frame_length, char frame[frame_length],
    link_frame_info *info_out,
    size_t *payload_length_out, char **payload_out,
    int *errno_ptr
);

internet_type link_ethernet_identify_network_type(
    struct netdev *ntdv, const link_frame_info *info
);

#endif
