#pragma once

#include "config.h"
#include "network/link/link.h"
#include "uapi/sys/types.h"

#if NETWORK_LINK_LOOPBACK_ENABLE

#if !NETWORK_LINK_ENABLE
#error NETWORK_LINK_LOOPBACK_ENABLE requires NETWORK_LINK_ENABLE
#endif

extern const link_ops link_loopback_dtlnk_ops;

ssize_t link_loopback_needed_frame_length(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length,
    int *errno_ptr
);

char *link_loopback_payload_in_frame(
    struct netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    size_t payload_length,
    int *errno_ptr
);

size_t link_loopback_encode_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char payload[payload_length],
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
);

bool link_loopback_decode_frame(
    netdev *ntdv,
    size_t frame_length, const char frame[frame_length],
    link_frame_info *info_out,
    size_t *payload_length_out, char **payload_out,
    int *errno_ptr
);

bool link_loopback_send_encoded_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
);

internet_type link_loopback_identify_network_type(
    struct netdev *ntdv, const link_frame_info *info
);

#endif
