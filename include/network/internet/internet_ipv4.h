#pragma once

#include "config.h"
#include "network/network_types.h"
#include "network/ipv4.h"
#include "network/internet/internet.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#if NETWORK_INTERNET_IPV4_ENABLE

#if !NETWORK_INTERNET_ENABLE
#error NETWORK_INTERNET_IPV4_ENABLE requires NETWORK_INTERNET_ENABLE
#endif

#define INTERNET_IPV4_PRIIPV4 "%hhu.%hhu.%hhu.%hhu"
#define INTERNET_IPV4_ARGIPV4(addr) (addr).as_bytes[0], (addr).as_bytes[1], (addr).as_bytes[2], (addr).as_bytes[3]

struct netdev;
struct link_frame_info;

typedef struct internet_ipv4_packet_info {
    // TODO: Implement other fields
    uint16_t identification;
    uint8_t time_to_live;
    uint8_t protocol;
    ipv4_addr src_ip_addr;
    ipv4_addr dst_ip_addr;
} internet_ipv4_packet_info;

bool internet_ipv4_make_frame(
    const internet_ipv4_packet_info *info,
    struct netdev *ntdv, const struct link_frame_info *dtlnk_info,
    size_t datagram_length, char **datagram_out,
    size_t *packet_length_out, char **packet_out,
    size_t *frame_length_out, char **frame_out,
    int *errno_ptr
);

size_t internet_ipv4_needed_header_length(const internet_ipv4_packet_info *info);
size_t internet_ipv4_header_length(size_t packet_length, const char packet[packet_length]);

bool internet_ipv4_encode_header(
    const internet_ipv4_packet_info *info,
    uint16_t total_length,
    size_t header_length, char header[header_length]
);

bool internet_ipv4_decode_packet(
    size_t packet_length, const char packet[packet_length],
    uint16_t *total_length_out,
    internet_ipv4_packet_info *info_out
);

uint16_t internet_ipv4_header_checksum(size_t header_length, const char header[header_length]);

bool internet_ipv4_send_packet(
    const internet_ipv4_packet_info *info,
    struct netdev *ntdv, const struct link_frame_info *dtlnk_info,
    size_t packet_length, char packet[packet_length],
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
);

void internet_ipv4_receive_packet(
    struct netdev *ntdv, const struct link_frame_info *dtlnk_info,
    size_t packet_length, char packet[packet_length]
);

#endif
