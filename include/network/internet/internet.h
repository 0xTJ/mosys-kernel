#pragma once

#include "config.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#if NETWORK_INTERNET_ENABLE

#if !NETWORK_LINK_ENABLE
#error NETWORK_INTERNET_ENABLE requires NETWORK_LINK_ENABLE
#endif

typedef enum internet_type {
    INTERNET_TYPE_NONE,

#if NETWORK_INTERNET_IPV4_ENABLE
    INTERNET_TYPE_IPV4,
#endif

} internet_type;

#include "device/netdev.h"
#include "network/ethertype.h"
#include "network/link/link.h"

struct netdev;
struct link_frame_info;

internet_type internet_type_from_ethertype(ethertype type);
ethertype internet_type_to_ethertype(internet_type type);

void internet_receive_packet(
    internet_type type,
    struct netdev *ntdv, const struct link_frame_info *dtlnk_info,
    size_t packet_length, char packet[packet_length]
);

#endif
