#pragma once

#include "fatfs.h"
#include "device/device.h"
#include "vfs/devreg.h"
#include "vfs/fdesc.h"
#include "mem/flex.h"
#include "kalloc.h"
#include "kernel/module.h"
#include "kio/klog.h"
#include "util/hashtable.h"
#include "util/util.h"
#include "vfs/vfs.h"
#include <assert.h>
#include <limits.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

// Require sb->mtx to be held
// TODO: Check that it is being held here
bool fatfs_cluster_offset_advance(superblock *sb, fatfs_cluster_fat_any *cluster, off_t *offset, int *errno_ptr);
