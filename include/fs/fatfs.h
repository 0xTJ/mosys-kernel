#pragma once

#include "util/endian.h"
#include <assert.h>
#include <stdint.h>

#define FATFS_BPB_SIZE 0x200
#define FATFS_ENTRY_SIZE 0x20

#define FATFS_CLUSTER_BITS_FAT12 12
#define FATFS_CLUSTER_BITS_FAT16 16
#define FATFS_CLUSTER_BITS_FAT32 32
#define FATFS_CLUSTER_BYTES_FAT12 (2)
#define FATFS_CLUSTER_BYTES_FAT16 ((FATFS_CLUSTER_BITS_FAT16) / 8)
#define FATFS_CLUSTER_BYTES_FAT32 ((FATFS_CLUSTER_BITS_FAT32) / 8)

typedef uint32_t fatfs_sector_idx;
typedef uint16_t fatfs_cluster_fat12;
typedef uint16_t fatfs_cluster_fat16;
typedef uint32_t fatfs_cluster_fat32;
typedef uint32_t fatfs_cluster_fat_any;

#define FATFS_CLUSTER_LIMIT_FAT12 ((fatfs_cluster_fat12) 0x0FF8)
#define FATFS_CLUSTER_LIMIT_FAT16 ((fatfs_cluster_fat16) 0xFFF8)
#define FATFS_CLUSTER_LIMIT_FAT32 ((fatfs_cluster_fat32) 0x0FFFFFF8)

#pragma pack(push, 1)

struct fatfs_bpb {
    uint8_t jmp_boot[3];
    uint8_t oem_name[8];
    u16le byts_per_sec;
    u8le sec_per_clus;
    u16le rsvd_sec_cnt;
    u8le num_fats;
    u16le root_ent_cnt;
    u16le tot_sec_16;
    u8le media;
    u16le fat_sz_16;
    u16le sec_per_trk;
    u16le num_heads;
    u32le hidd_sec;
    u32le tot_sec_32;
    union {
        struct {
            u8le drv_num;
            uint8_t reserved_1;
            u8le boot_sig;
            u32le vol_id;
            uint8_t vol_lab[11];
            uint8_t fil_sys_type[8];
            uint8_t code[448];
            uint8_t signature_word[2];
        } fat12_fat16;
        struct {
            u32le fat_sz_32;
            uint16_t ext_flags;
            uint16_t fs_ver;
            u32le root_clus;
            uint16_t fs_info;
            uint16_t bk_boot_sec;
            uint8_t reserved[12];
            u8le drv_num;
            uint8_t reserved_1;
            u8le boot_sig;
            u32le vol_id;
            uint8_t vol_lab[11];
            uint8_t fil_sys_type[8];
            uint8_t code[420];
            uint8_t signature_word[2];
        } fat32;
    };
};
static_assert(sizeof(struct fatfs_bpb) == 0x200, "FAT BPB has wrong size");

struct fatfs_entry {
    uint8_t name[11];
    u8le attr;
    u8le nt_res;
    u8le crt_time_tenth;
    u16le crt_time;
    u16le crt_date;
    u16le lst_acc_date;
    u16le fst_clus_hi;
    u16le wrt_time;
    u16le wrt_date;
    u16le fst_clus_lo;
    u32le file_size;
};
static_assert(sizeof(struct fatfs_entry) == FATFS_ENTRY_SIZE, "FAT index entry has wrong size");

#pragma pack(pop)

typedef enum fatfs_fat_type {
    FATFS_FAT_TYPE_FAT12,
    FATFS_FAT_TYPE_FAT16,
    FATFS_FAT_TYPE_FAT32,
} fatfs_fat_type;

typedef struct fatfs_bpb_data {
    uint8_t     sectors_per_cluster;
    uint16_t    reserved_sector_count;
    uint8_t     number_of_fats;
    uint16_t    root_entry_count;
    uint32_t    total_sector_count;
    uint32_t    fat_size;

    uint32_t root_dir_sectors;
    uint32_t first_data_sector;
    uint32_t data_sectors_count;
    uint32_t count_of_clusters;
    fatfs_fat_type fat_type;
    uint32_t root_cluster;

    struct fatfs_bpb disk_bpb;
} fatfs_bpb_data;
