#pragma once

#define BST_INFO_TYPE(type_word) struct bst_##type_word##_info
#define BST_INFO(type, type_word)   \
    BST_INFO_TYPE(type_word) {      \
        type (*left);               \
        type (*right);              \
    }
#define BST_INFO_OBJ(type, type_word, obj_name) BST_INFO(type, type_word) obj_name

#define BST_MIN(type_word)                                      \
    bst_##type_word##_min
#define BST_MIN_DECL(type, type_word)                           \
    type (*BST_MIN(type_word)(type (*self)))
#define BST_MIN_DEFN(type, type_word, obj_name)                 \
    BST_MIN_DECL(type, type_word) {                             \
        if (self && self->obj_name.left) {                      \
            return BST_MIN(type_word)(self->obj_name.left);     \
        } else {                                                \
            return self;                                        \
        }                                                       \
    }

#define BST_MAX(type_word)                                      \
    bst_##type_word##_max
#define BST_MAX_DECL(type, type_word)                           \
    type (*BST_MAX(type_word)(type (*self)))
#define BST_MAX_DEFN(type, type_word, obj_name)                 \
    BST_MAX_DECL(type, type_word) {                             \
        if (self && self->obj_name.right) {                     \
            return BST_MAX(type_word)(self->obj_name.right);    \
        } else {                                                \
            return self;                                        \
        }                                                       \
    }

#define BST_PRED(type_word)                                     \
    bst_##type_word##_pred
#define BST_PRED_DECL(type, type_word)                          \
    type (*BST_PRED(type_word)(type (*self)))
#define BST_PRED_DEFN(type, type_word, obj_name)                \
    BST_PRED_DECL(type, type_word) {                            \
        if (self && self->obj_name.left) {                      \
            return BST_MAX(type_word)(self->obj_name.left);     \
        } else {                                                \
            return NULL;                                        \
        }                                                       \
    }

#define BST_SUCC(type_word)                                     \
    bst_##type_word##_succ
#define BST_SUCC_DECL(type, type_word)                          \
    type (*BST_SUCC(type_word)(type (*self)))
#define BST_SUCC_DEFN(type, type_word, obj_name)                \
    BST_SUCC_DECL(type, type_word) {                            \
        if (self && self->obj_name.right) {                     \
            return BST_MIN(type_word)(self->obj_name.right);    \
        } else {                                                \
            return NULL;                                        \
        }                                                       \
    }

#define BST_INSERT(type_word)                                                               \
    bst_##type_word##_insert
#define BST_INSERT_DECL(type, type_word)                                                    \
    type (*BST_INSERT(type_word)(type (*self), type (*value)))
#define BST_INSERT_DEFN(type, type_word, obj_name, spaceship)                               \
    BST_INSERT_DECL(type, type_word) {                                                      \
        if (!self) {                                                                        \
            value->obj_name.left = NULL;                                                    \
            value->obj_name.right = NULL;                                                   \
            return value;                                                                   \
        }                                                                                   \
                                                                                            \
        int comp = spaceship(value, self);                                                  \
        kassert(comp);                                                                      \
        if (comp < 0) {                                                                     \
            self->obj_name.left = BST_INSERT(type_word)(self->obj_name.left, value);        \
        } else {                                                                            \
            self->obj_name.right = BST_INSERT(type_word)(self->obj_name.right, value);      \
        }                                                                                   \
                                                                                            \
        return self;                                                                        \
    }

#define BST_DELETE(type_word)                                                               \
    bst_##type_word##_delete
#define BST_DELETE_DECL(type, type_word)                                                    \
    type (*BST_DELETE(type_word)(type (*self), type (*value)))
#define BST_DELETE_DEFN(type, type_word, obj_name, spaceship)                               \
    BST_DELETE_DECL(type, type_word) {                                                      \
        if (!self) {                                                                        \
            kpanic("Tried to delete non-existent value *%p from heap for type \"%s\"",      \
                   (void *) value, #type);                                                  \
        }                                                                                   \
                                                                                            \
        int comp = spaceship(value, self);                                                  \
        if (comp < 0) {                                                                     \
            self->obj_name.left = BST_DELETE(type_word)(self->obj_name.left, value);        \
            return self;                                                                    \
        } else if (comp > 0) {                                                              \
            self->obj_name.right = BST_DELETE(type_word)(self->obj_name.right, value);      \
            return self;                                                                    \
        } else {                                                                            \
            /* Found node to delete */                                                      \
            if (!self->obj_name.left && !self->obj_name.right) {                            \
                return NULL;                                                                \
            } else if (self->obj_name.left && !self->obj_name.right) {                      \
                type (*tmp_left) = self->obj_name.left;                                     \
                self->obj_name.left = NULL;                                                 \
                self->obj_name.right = NULL;                                                \
                return tmp_left;                                                            \
            } else if (!self->obj_name.left && self->obj_name.right) {                      \
                type (*tmp_right) = self->obj_name.right;                                   \
                self->obj_name.left = NULL;                                                 \
                self->obj_name.right = NULL;                                                \
                return tmp_right;                                                           \
            } else {                                                                        \
                /* TODO: Always taking successor might lead to a very unbalanced tree */    \
                type (*succ) = BST_SUCC(type_word)(self);                                   \
                kassert(succ);                                                              \
                kassert(BST_DELETE(type_word)(self, succ) == self);                         \
                succ->obj_name.left = self->obj_name.left;                                  \
                succ->obj_name.right = self->obj_name.right;                                \
                return succ;                                                                \
            }                                                                               \
        }                                                                                   \
    }

#define BST_ROTR(type_word)                                 \
    bst_##type_word##_rotr
#define BST_ROTR_DECL(type, type_word)                      \
    type (*BST_ROTR(type_word)(type (*self)))
#define BST_ROTR_DEFN(type, type_word, obj_name)            \
    BST_ROTR_DECL(type, type_word) {                        \
        if (!self) {                                        \
            return NULL;                                    \
        }                                                   \
                                                            \
        if (self->obj_name.left) {                          \
            type (*new_root) = self->obj_name.left;         \
            self->obj_name.left = new_root->obj_name.right; \
            new_root->obj_name.right = self;                \
            return new_root;                                \
        } else {                                            \
            return self;                                    \
        }                                                   \
    }

#define BST_ROTL(type_word)                                 \
    bst_##type_word##_rotl
#define BST_ROTL_DECL(type, type_word)                      \
    type (*BST_ROTL(type_word)(type (*self)))
#define BST_ROTL_DEFN(type, type_word, obj_name)            \
    BST_ROTL_DECL(type, type_word) {                        \
        if (!self) {                                        \
            return NULL;                                    \
        }                                                   \
                                                            \
        if (self->obj_name.right) {                         \
            type (*new_root) = self->obj_name.right;        \
            self->obj_name.right = new_root->obj_name.left; \
            new_root->obj_name.left = self;                 \
            return new_root;                                \
        } else {                                            \
            return self;                                    \
        }                                                   \
    }

#define BST_DECL(type, type_word, attr)     \
    attr BST_MIN_DECL(type, type_word);     \
    attr BST_MAX_DECL(type, type_word);     \
    attr BST_PRED_DECL(type, type_word);    \
    attr BST_SUCC_DECL(type, type_word);    \
    attr BST_INSERT_DECL(type, type_word);  \
    attr BST_DELETE_DECL(type, type_word);  \
    attr BST_ROTR_DECL(type, type_word);    \
    attr BST_ROTL_DECL(type, type_word)

#define BST_DEFN(type, type_word, obj_name, spaceship, attr)    \
    attr BST_MIN_DEFN(type, type_word, obj_name)                \
    attr BST_MAX_DEFN(type, type_word, obj_name)                \
    attr BST_PRED_DEFN(type, type_word, obj_name)               \
    attr BST_SUCC_DEFN(type, type_word, obj_name)               \
    attr BST_INSERT_DEFN(type, type_word, obj_name, spaceship)  \
    attr BST_DELETE_DEFN(type, type_word, obj_name, spaceship)  \
    attr BST_ROTR_DEFN(type, type_word, obj_name)               \
    attr BST_ROTL_DEFN(type, type_word, obj_name)
