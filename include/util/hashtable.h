#pragma once

#include <stddef.h>
#include <stdint.h>

typedef struct hashtable_entry {
    uint64_t key;
    void *value;
} hashtable_entry;

typedef struct hashtable {
    hashtable_entry *table;
    size_t table_len;
    size_t table_index_mask;
} hashtable;

#define HASHTABLE_INIT { NULL, 0, 0 }

bool hashtable_insert(hashtable *hshtbl, uint64_t key, void *value);
void *hashtable_remove(hashtable *hshtbl, uint64_t key);
void *hashtable_lookup(hashtable *hshtbl, uint64_t key);
bool hashtable_resize(hashtable *hshtbl, size_t count);
