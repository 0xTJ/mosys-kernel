#pragma once

#include "util/util.h"
#include <stdint.h>

#define ENDIAN_CREATE(s_u, int_uint, size, le_be, little_big)                           \
    typedef struct endian_ ## s_u ## size ## le_be {                                    \
        int_uint ## size ## _t raw;                                                     \
    } s_u ## size ## le_be;                                                             \
                                                                                        \
    static inline int_uint ## size ## _t                                                \
    from_ ## s_u ## size ## le_be(s_u ## size ## le_be le_be) {                         \
        return from_ ## little_big ## _ ## s_u ## size(le_be.raw);                      \
    }                                                                                   \
                                                                                        \
    static inline s_u ## size ## le_be                                                  \
    to_ ## s_u ## size ## le_be(int_uint ## size ## _t le_be) {                         \
        return (s_u ## size ## le_be) { to_ ## little_big ## _ ## s_u ## size(le_be) }; \
    }

#define ENDIAN_CREATE_S_LE(size) ENDIAN_CREATE(s,  int, size, le, little)
#define ENDIAN_CREATE_S_BE(size) ENDIAN_CREATE(s,  int, size, be,    big)
#define ENDIAN_CREATE_U_LE(size) ENDIAN_CREATE(u, uint, size, le, little)
#define ENDIAN_CREATE_U_BE(size) ENDIAN_CREATE(u, uint, size, be,    big)

#pragma pack(push, 1)

ENDIAN_CREATE_S_LE(8)
ENDIAN_CREATE_S_LE(16)
ENDIAN_CREATE_S_LE(32)
ENDIAN_CREATE_S_LE(64)

ENDIAN_CREATE_S_BE(8)
ENDIAN_CREATE_S_BE(16)
ENDIAN_CREATE_S_BE(32)
ENDIAN_CREATE_S_BE(64)

ENDIAN_CREATE_U_LE(8)
ENDIAN_CREATE_U_LE(16)
ENDIAN_CREATE_U_LE(32)
ENDIAN_CREATE_U_LE(64)

ENDIAN_CREATE_U_BE(8)
ENDIAN_CREATE_U_BE(16)
ENDIAN_CREATE_U_BE(32)
ENDIAN_CREATE_U_BE(64)

#pragma pack(pop)

#undef ENDIAN_CREATE
