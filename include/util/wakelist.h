#pragma once

#include "thread/thread.h"
#include "util/spinlock.h"
#include "util/util.h"
#include "util/with.h"
#include <stdbool.h>
#include <stdint.h>

typedef struct wakelist_thread_node {
    struct thread *ptr;
    struct wakelist_thread_node *next;
} wakelist_thread_node;

typedef struct wakelist {
    spinlock spnlck;
    struct wakelist_thread_node *list;
} wakelist;

#define WAKELIST_INIT {                 \
    .spnlck = SPINLOCK_INIT, \
    .list = NULL,                       \
}

void wakelist_init(wakelist *wklst);
void wakelist_deinit(wakelist *wklst);

static inline void wakelist_lock(wakelist *wklst) {
    spinlock_lock(&wklst->spnlck);
}

static inline void wakelist_unlock(wakelist *wklst) {
    spinlock_unlock(&wklst->spnlck);
}

#define WAKELIST_WITH_LOCK(wklst, statement) WITH(_wakelist_with_info, wakelist_lock(wklst), wakelist_unlock(wklst), statement)
#define WAKELIST_LEAVE_UNLOCK() CONTINUE_IN_WITH(_wakelist_with_info)
#define WAKELIST_LEAVE_NO_UNLOCK() BREAK_IN_WITH(_wakelist_with_info)
#define WAKELIST_RETURN_NO_UNLOCK() RETURN_IN_WITH(_wakelist_with_info)
#define WAKELIST_RETURN_VALUE_NO_UNLOCK(value) RETURN_VALUE_IN_WITH(_wakelist_with_info, value)
#define WAKELIST_GOTO_NO_UNLOCK(label) GOTO_IN_WITH(_wakelist_with_info, label)

// Must be locked before calling
bool wakelist_wake_one(wakelist *wklst);

// Must be locked before calling
size_t wakelist_wake_all(wakelist *wklst);

// Must be locked before calling
void wakelist_add_self(wakelist *wklst, struct wakelist_thread_node *node);

// Must be locked before calling
void wakelist_remove_self(wakelist *wklst);
