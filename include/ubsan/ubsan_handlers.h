#pragma once

#include "ubsan/ubsan_value.h"
#include <stdnoreturn.h>

#define UBSAN_UNRECOVERABLE(checkname, def_args)                             \
    noreturn void __ubsan_handle_ ## checkname def_args;

#define UBSAN_RECOVERABLE(checkname, def_args)                               \
    void __ubsan_handle_ ## checkname def_args;                     \
    noreturn void __ubsan_handle_ ## checkname ## _abort def_args;

typedef struct ubsan_type_mismatch_data {
	ubsan_source_location loc;
	const ubsan_type_descriptor *type;
	unsigned char log_alignment;
	unsigned char type_check_kind;
} ubsan_type_mismatch_data;

UBSAN_RECOVERABLE(
    type_mismatch_v1,
    (
        ubsan_type_mismatch_data *data,
        ubsan_value_handle pointer
    )
)

typedef struct ubsan_alignment_assumption_data {
    ubsan_source_location loc;
    ubsan_source_location assumption_loc;
    const ubsan_type_descriptor *type;
} ubsan_alignment_assumption_data;

UBSAN_RECOVERABLE(
    alignment_assumption,
    (
        ubsan_alignment_assumption_data *data,
        ubsan_value_handle pointer,
        ubsan_value_handle alignment,
        ubsan_value_handle offset
    )
)

typedef struct ubsan_overflow_data {
    ubsan_source_location loc;
    const ubsan_type_descriptor *type;
} ubsan_overflow_data;

UBSAN_RECOVERABLE(add_overflow, (ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs))
UBSAN_RECOVERABLE(sub_overflow, (ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs))
UBSAN_RECOVERABLE(mul_overflow, (ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs))
UBSAN_RECOVERABLE(negate_overflow, (ubsan_overflow_data *data, ubsan_value_handle old_val))
UBSAN_RECOVERABLE(divrem_overflow, (ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs))

typedef struct ubsan_shift_out_of_bounds_data {
    ubsan_source_location loc;
    const ubsan_type_descriptor *lhs_type;
    const ubsan_type_descriptor *rhs_type;
} ubsan_shift_out_of_bounds_data;

UBSAN_RECOVERABLE(
    shift_out_of_bounds,
    (
        ubsan_shift_out_of_bounds_data *data,
        ubsan_value_handle lhs,
        ubsan_value_handle rhs
    )
)

typedef struct ubsan_out_of_bounds_data {
    ubsan_source_location loc;
    const ubsan_type_descriptor *array_type;
    const ubsan_type_descriptor *index_type;
} ubsan_out_of_bounds_data;

UBSAN_RECOVERABLE(
    out_of_bounds,
    (
        ubsan_out_of_bounds_data *data,
        ubsan_value_handle index
    )
)

typedef struct ubsan_unreachable_data {
    ubsan_source_location loc;
} ubsan_unreachable_data;

UBSAN_UNRECOVERABLE(builtin_unreachable, (ubsan_unreachable_data *data))
UBSAN_UNRECOVERABLE(missing_return, (ubsan_unreachable_data *data))

typedef struct ubsan_vla_bound_data {
    ubsan_source_location loc;
    const ubsan_type_descriptor *type;
} ubsan_vla_bound_data;

UBSAN_RECOVERABLE(
    vla_bound_not_positive,
    (
        ubsan_vla_bound_data *data,
        ubsan_value_handle bound
    )
)

typedef struct ubsan_float_cast_overflow_data {
    const ubsan_type_descriptor *from_type;
    const ubsan_type_descriptor *to_type;
} ubsan_float_cast_overflow_data;

typedef struct ubsan_float_cast_overflow_data_v2 {
    ubsan_source_location loc;
    const ubsan_type_descriptor *from_type;
    const ubsan_type_descriptor *to_type;
} ubsan_float_cast_overflow_data_v2;

// data is ubsan_float_cast_overflow_data * or ubsan_float_cast_overflow_data_v2 *
UBSAN_RECOVERABLE(
    float_cast_overflow,
    (
        void *data,
        ubsan_value_handle from
    )
)

typedef struct ubsan_invalid_value_data {
    ubsan_source_location loc;
    const ubsan_type_descriptor *type;
} ubsan_invalid_value_data;

UBSAN_RECOVERABLE(
    load_invalid_value,
    (
        ubsan_invalid_value_data *data,
        ubsan_value_handle val
    )
)

typedef enum ubsan_implicit_conversion_check_kind {
    UBSAN_ICCK_INTEGER_TRUNCATION                       = 0,
    UBSAN_ICCK_UNSIGNED_INTEGER_TRUNCATION              = 1,
    UBSAN_ICCK_SIGNED_INTEGER_TRUNCATION                = 2,
    UBSAN_ICCK_INTEGER_SIGN_CHANGE                      = 3,
    UBSAN_ICCK_SIGNED_INTEGER_TRUNCATION_OR_SIGN_CHANGE = 4,
} ubsan_implicit_conversion_check_kind;

typedef struct ubsan_implicit_conversion_data {
    ubsan_source_location loc;
    const ubsan_type_descriptor *from_type;
    const ubsan_type_descriptor *to_type;
    unsigned char kind; // ubsan_implicit_conversion_check_kind
} ubsan_implicit_conversion_data;

UBSAN_RECOVERABLE(
    implicit_conversion,
    (
        ubsan_implicit_conversion_data *data,
        ubsan_value_handle src,
        ubsan_value_handle dst
    )
)

typedef enum ubsan_builtin_check_kind {
    UBSAN_BCK_CTZ_PASSED_ZERO,
    UBSAN_BCK_CLZ_PASSED_ZERO,
} ubsan_builtin_check_kind;

typedef struct ubsan_invalid_builtin_data {
    ubsan_source_location loc;
    unsigned char kind; // ubsan_builtin_check_kind
} ubsan_invalid_builtin_data;

UBSAN_RECOVERABLE(
    invalid_builtin,
    (
        ubsan_invalid_builtin_data *data
    )
)

typedef struct ubsan_invalid_objc_cast_data {
    ubsan_source_location loc;
    const ubsan_type_descriptor *expected_type;
} ubsan_invalid_objc_cast_data;

UBSAN_RECOVERABLE(
    invalid_objc_cast,
    (
        ubsan_invalid_objc_cast_data *data,
        ubsan_value_handle pointer
    )
)

typedef struct ubsan_nonnull_return_data {
    ubsan_source_location attr_loc;
} ubsan_nonnull_return_data;

UBSAN_RECOVERABLE(
    nonnull_return_v1,
    (
        ubsan_nonnull_return_data *data,
        ubsan_source_location *loc
    )
)
UBSAN_RECOVERABLE(
    nullability_return_v1,
    (
        ubsan_nonnull_return_data *data,
        ubsan_source_location *loc
    )
)

typedef struct ubsan_nonnull_arg_data {
    ubsan_source_location loc;
    ubsan_source_location attr_loc;
    int arg_index;
} ubsan_nonnull_arg_data;

UBSAN_RECOVERABLE(
    nonnull_arg,
    (
        ubsan_nonnull_arg_data *data
    )
)
UBSAN_RECOVERABLE(
    nullability_arg,
    (
        ubsan_nonnull_arg_data *data
    )
)

typedef struct ubsan_pointer_overflow_data {
    ubsan_source_location loc;
} ubsan_pointer_overflow_data;

UBSAN_RECOVERABLE(
    pointer_overflow,
    (
        ubsan_pointer_overflow_data *data,
        ubsan_value_handle base,
        ubsan_value_handle result
    )
)

typedef enum ubsan_cfi_type_check_kind {
    UBSAN_CFITCK_V_CALL,
    UBSAN_CFITCK_NV_CALL,
    UBSAN_CFITCK_DERIVED_CAST,
    UBSAN_CFITCK_UNRELATED_CAST,
    UBSAN_CFITCK_I_CALL,
    UBSAN_CFITCK_NVMF_CALL,
    UBSAN_CFITCK_VMF_CALL,
} ubsan_cfi_type_check_kind;

typedef struct ubsan_cfi_check_fail_data {
    ubsan_cfi_type_check_kind check_kind;
    ubsan_source_location loc;
    const ubsan_type_descriptor *type;
} ubsan_cfi_check_fail_data;

UBSAN_RECOVERABLE(
    cfi_check_fail,
    (
        ubsan_cfi_check_fail_data *data,
        ubsan_value_handle function,
        uintptr_t vtable_is_valid
    )
)
