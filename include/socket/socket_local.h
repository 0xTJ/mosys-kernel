#pragma once

#include "socket/socket.h"
#include "uapi/sys/types.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#if SOCKET_LOCAL_ENABLE

#if !SOCKET_ENABLE
#error SOCKET_LOCAL_ENABLE requires SOCKET_ENABLE
#endif

int socket_local_socket(socket_info *sock, int *errno_ptr);
int socket_local_socketpair(socket_info *socks[2], int *errno_ptr);
int socket_local_close(struct socket_info *sock, int *errno_ptr);
ssize_t socket_local_recvfrom(socket_info *sock, flex_mut buf, size_t len, int flags,
                              FLEX_MUT(struct sockaddr_un) src_addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr);
ssize_t socket_local_sendto(socket_info *sock, flex_const buf, size_t len, int flags,
                            FLEX_CONST(struct sockaddr_un) dest_addr, socklen_t addrlen, int *errno_ptr);
int socket_local_bind(socket_info *sock, FLEX_CONST(struct sockaddr_un) addr, socklen_t addrlen, int *errno_ptr);
int socket_local_accept(socket_info *sock, FLEX_MUT(struct sockaddr_un) addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr);
int socket_local_connect(socket_info *sock, FLEX_CONST(struct sockaddr_un) addr, socklen_t addrlen, int *errno_ptr);

#endif
