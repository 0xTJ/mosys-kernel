#pragma once

#include "config.h"
#include "socket/socket.h"
#include "uapi/sys/types.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#if SOCKET_IP_ENABLE

#if !SOCKET_ENABLE
#error SOCKET_IP_ENABLE requires SOCKET_ENABLE
#endif

int socket_ip_socket(socket_info *sock, int *errno_ptr);

#endif
