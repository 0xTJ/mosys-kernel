#pragma once

#include "mem/flex.h"
#include <stdnoreturn.h>

int reboot(int magic, int magic2, int cmd, flex_const_str arg, int *errno_ptr);
noreturn void reboot_restart(const char *arg);
noreturn void reboot_halt(void);
noreturn void reboot_power_off(void);
