#pragma once

#include "mem/dynobj.h"
#include "util/rwlock.h"
#include <stdbool.h>

#define DEVREG_NAME_MAX_LENGTH 32

typedef struct devreg {
    dynobj_rwl dnobj;

    char name[DEVREG_NAME_MAX_LENGTH + 1];
    int major;
    int minor;

    struct devreg *next;
} devreg;

devreg *devreg_new(const char *name, int major, int minor);
void devreg_cleanup(void *dr_arg);
void devreg_ref(devreg *dr);
void devreg_unref(devreg *dr);
devreg *devreg_lookup(const char *name);
devreg *devreg_get_nth(unsigned long n);
bool devreg_exists(const char *name);
