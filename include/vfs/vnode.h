#pragma once

#include "config.h"
#include "mem/dynobj.h"
#include "util/mutex.h"
#include "vfs/devreg.h"
#include "vfs/superblock.h"
#include "vfs/vfs.h"

typedef enum file_type {
    FILE_TYPE_PLAIN,
    FILE_TYPE_DIRECTORY,
    FILE_TYPE_DEVICE,
    FILE_TYPE_SOCKET,
} file_type;

typedef struct vnode {
    dynobj_mtx dnobj;

    unsigned long block_count;

    file_type type;
    union {
        // struct {
        // } plain;
        // struct {
        // } directory;
        struct {
            devreg *dr;
        } device;
#if SOCKET_ENABLE
        struct {
            socket_info *sock;
        } socket;
#endif
    };

    struct vnode *parent;

    // Fields after this line until next will not change once created

    ino_t inode;
    struct superblock *sb;

    struct file_operations *fops;

    void *data_ptr;

    struct vnode *next; // To be used internally by filesystems
} vnode;

// sb must already have been referenced
vnode *vfs_vnode_new(struct superblock *sb);
void vfs_vnode_ref(vnode *vn);
void vfs_vnode_unref(vnode *vn);
