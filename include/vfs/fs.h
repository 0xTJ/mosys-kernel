#pragma once

#include "mem/flex.h"
#include "vfs/superblock.h"
#include "vfs/vnode.h"

struct superblock;

typedef struct fs_operations {
    struct vnode *(*new_vnode)(struct superblock *sb, int *errno_ptr);
    void (*cleanup_vnode)(struct superblock *sb, struct vnode *vn);
    struct vnode *(*mount)(flex_const_str source, unsigned long mountflags,
                           flex_const data,
                           int *errno_ptr);
    struct vnode *(*create)(struct vnode *base_vn, const char *subpath, int *errno_ptr);
    struct vnode *(*lookup)(struct vnode *base_vn, const char *subpath, int *errno_ptr);
    // Should unref the passed vnode
} fs_operations;

typedef struct filesystem_type {
    const char *name;
    fs_operations *fsops;

    struct filesystem_type *next;
} filesystem_type;

void vfs_filesystem_type_register(filesystem_type *fst);
struct vnode *vfs_filesystem_mount(flex_const_str source,
                                   flex_const_str filesystemtype, unsigned long mountflags,
                                   flex_const data,
                                   int *errno_ptr);
