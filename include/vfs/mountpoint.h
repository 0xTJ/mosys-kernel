#pragma once

#include "mem/dynobj.h"
#include "vfs/superblock.h"
#include <stdint.h>

typedef struct mountpoint {
    dynobj dnobj;

    struct superblock *sb;
    const char *mount_path;

    struct mountpoint *next;
} mountpoint;

mountpoint *vfs_mountpoint_new(struct superblock *sb, const char *mount_path);
void vfs_mountpoint_cleanup(void *mp);
void vfs_mountpoint_ref(mountpoint *mp);
void vfs_mountpoint_unref(mountpoint *mp);
mountpoint *vfs_mountpoint_lookup(const char *path, const char **subpath_ptr);
