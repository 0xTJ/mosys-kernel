#pragma once

#include "mem/dynobj.h"
#include "util/hashtable.h"
#include "util/mutex.h"
#include "vfs/fs.h"
#include "vfs/vnode.h"
#include <stdint.h>

typedef struct superblock {
    dynobj_mtx dnobj;

    int device_major;               // Major number of host device
    int device_minor;               // Minor number of host device

    unsigned long block_size;       // Block size in bytes
    unsigned char block_size_width; // Block size width in bits
    uint64_t timestamp;

    struct filesystem_type *type;
    struct vnode *mounted;
    struct vnode *covered;

    hashtable vn_hshtbl;
    mutex vn_hshtbl_mtx;

    void *sb_info;
} superblock;

superblock *vfs_superblock_new(struct filesystem_type *type);
void vfs_superblock_ref(superblock *sb);
void vfs_superblock_unref(superblock *sb);
