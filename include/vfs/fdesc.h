#pragma once

#include "mem/dynobj.h"
#include "vfs/vfs.h"

typedef struct fdesc {
    dynobj_mtx dnobj;

    // Fields set by filesystem on fsops->lookup()
    // TODO: The line above this one is wrong
    struct vnode *vn;

    // Fields set by filesystem on fsops->lookup() then refined by open() before fops->open()
    // TODO: The line above this one is wrong
    unsigned int mode;
    unsigned int flags;

    // Fields set by open() after fops->open()
    unsigned long long offset;  // TODO: Should this be off_t?
} fdesc;

typedef struct fdesc_list {
    dynobj_mtx dnobj;

    fdesc *fds[VFS_MAX_OPEN_FILES];
} fdesc_list;

typedef struct file_operations {
    int (*open)(fdesc *fdsc, int *errno_ptr);
    int (*release)(fdesc *fdsc, int *errno_ptr);
    ssize_t (*pread)(fdesc *fdsc, flex_mut buf, size_t count, off_t offset, int *errno_ptr);
    ssize_t (*pwrite)(fdesc *fdsc, flex_const buf, size_t count, off_t offset, int *errno_ptr);
    // Individual filesystems have to verify argp
    int (*ioctl)(fdesc *fdsc, unsigned long request, flex_mut argp, int *errno_ptr);
    ssize_t (*getdents)(fdesc *fdsc, FLEX_MUT(struct dirent) drnt, size_t count, int *errno_ptr);
} file_operations;

fdesc *vfs_fdesc_new(void);
void vfs_fdesc_ref(fdesc *fdsc);
void vfs_fdesc_unref(fdesc *fdsc);
fdesc *vfs_fdesc_clone(fdesc *src_fdsc);

fdesc_list *vfs_fdesc_list_new(void);
void vfs_fdesc_list_ref(fdesc_list *fdl);
void vfs_fdesc_list_unref(fdesc_list *fdl);
fdesc_list *vfs_fdesc_list_clone(fdesc_list *src_fdl);

// Takes ownership of fdsc reference
int vfs_register_fd(fdesc *fdsc, int *errno_ptr);

// FDesc file operations

fdesc *vfs_open_fdesc(flex_const_str pathname, int flags, mode_t mode, int *errno_ptr);
int vfs_close_fdesc(fdesc *fdsc, int *errno_ptr);
ssize_t vfs_read_fdesc(fdesc *fdsc, flex_mut buf, size_t count, int *errno_ptr);
ssize_t vfs_pread_fdesc(fdesc *fdsc, flex_mut buf, size_t count, off_t offset, int *errno_ptr);
ssize_t vfs_write_fdesc(fdesc *fdsc, flex_const buf, size_t count, int *errno_ptr);
ssize_t vfs_pwrite_fdesc(fdesc *fdsc, flex_const buf, size_t count, off_t offset, int *errno_ptr);
int vfs_ioctl_fdesc(fdesc *fdsc, unsigned long request, flex_mut argp, int *errno_ptr);
ssize_t vfs_getdents_fdesc(fdesc *fdsc, FLEX_MUT(dirent) drnt, size_t count, int *errno_ptr);
off_t vfs_lseek_fdesc(fdesc *fdsc, off_t offset, int whence, int *errno_ptr);
fdesc *vfs_socket_fdesc(int domain, int type, int protocol, int *errno_ptr);
int vfs_socketpair_fdesc(int domain, int type, int protocol, fdesc *fdscs[2], int *errno_ptr);
int vfs_bind_fdesc(fdesc *fdsc, FLEX_CONST(struct sockaddr) addr, socklen_t addrlen, int *errno_ptr);
ssize_t vfs_recvfrom_fdesc(fdesc *fdsc, flex_mut buf, size_t len, int flags,
                           FLEX_MUT(struct sockaddr) src_addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr);
ssize_t vfs_sendto_fdesc(fdesc *fdsc, flex_const buf, size_t len, int flags,
                         FLEX_CONST(struct sockaddr) dest_addr, socklen_t addrlen, int *errno_ptr);
int vfs_accept_fdesc(fdesc *fdsc, FLEX_MUT(struct sockaddr) addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr);
int vfs_connect_fdesc(fdesc *fdsc, FLEX_CONST(struct sockaddr) addr, socklen_t addrlen, int *errno_ptr);

// FDesc process operations

int vfs_fchdir_fdesc(fdesc *fdsc, int *errno_ptr);
