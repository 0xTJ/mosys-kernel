#pragma once

#include "mem_arch.h"
#include "config.h"
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

typedef uint32_t mem_page;

typedef struct {
    mem_page base;
    size_t count;
} mem_page_block;
#define MEM_PAGE_BLOCK_NULL ((mem_page_block) { 0, 0 })

typedef uintptr_t mem_addr;
#define MEM_ADDR_NULL ((mem_addr) NULL)

typedef struct {
    mem_addr base;
    size_t size;
} mem_block;
#define MEM_BLOCK_NULL ((mem_block) { MEM_ADDR_NULL, 0 })

#define MEM_PAGE_COUNT (MEM_LEN / MEM_PAGE_SIZE)
#define MEM_BITMAP_ENTRY_SIZE (sizeof(mem_bitmap_entry) * CHAR_BIT)
#define MEM_BITMAP_ENTRY_COUNT (MEM_PAGE_COUNT / MEM_BITMAP_ENTRY_SIZE)

extern mem_bitmap_entry mem_bitmap[MEM_BITMAP_ENTRY_COUNT];

static inline bool mem_page_block_is_present(mem_page_block page_block) {
    return page_block.base && page_block.count;
}

static inline mem_block mem_page_block_to_block(mem_page_block page_block) {
    return (mem_block) { page_block.base * MEM_PAGE_SIZE, page_block.count * MEM_PAGE_SIZE };
}

static inline bool mem_block_is_present(mem_block block) {
    return block.base && block.size;
}

static inline mem_page_block mem_block_to_page_block(mem_block page_block) {
    return (mem_page_block) { page_block.base / MEM_PAGE_SIZE, page_block.size / MEM_PAGE_SIZE };
}

static inline bool mem_block_is_page_aligned(mem_block block) {
    return block.base % MEM_PAGE_SIZE == 0 && block.size % MEM_PAGE_SIZE == 0;
}

void mem_init(void);

mem_page_block mem_alloc_pages(size_t count);
void mem_free_pages(mem_page_block page_block);
mem_page_block mem_clone_pages(mem_page_block block);

mem_block mem_alloc(size_t size);
void mem_free(mem_block block);
mem_block mem_clone(mem_block block);

void mem_dump_free_pages(void);

#if MEM_USE_MMU

void mem_set_v(void *dest, int ch, size_t count);
void mem_set_p(mem_addr dest, int ch, size_t count);

void mem_move_vv(void *dest, void *src, size_t count);
void mem_move_vp(void *dest, mem_addr src, size_t count);
void mem_move_pv(mem_addr dest, void *src, size_t count);
void mem_move_pp(mem_addr dest, mem_addr src, size_t count);

#else   // !MEM_USE_MMU

static inline void mem_set_v(void *dest, int ch, size_t count) {
    memset(dest, ch, count);
}

static inline void mem_set_p(mem_addr dest, int ch, size_t count) {
    mem_set_v((void *) dest, ch, count);
}

static inline void mem_move_vv(void *dest, void *src, size_t count) {
    memmove(dest, src, count);
}

static inline void mem_move_vp(void *dest, mem_addr src, size_t count) {
    mem_move_vv((void *) dest, (void *) src, count);
}

static inline void mem_move_pv(mem_addr dest, void *src, size_t count) {
    mem_move_vv((void *) dest, (void *) src, count);
}

static inline void mem_move_pp(mem_addr dest, mem_addr src, size_t count) {
    mem_move_vv((void *) dest, (void *) src, count);
}

#endif
