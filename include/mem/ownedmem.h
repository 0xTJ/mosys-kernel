#pragma once

#include "mem/dynobj.h"
#include "mem/mem.h"
#include "util/mutex.h"
#include "mem/dynobj.h"
#include <stdint.h>
#include <stdbool.h>

typedef struct ownedmem {
    dynobj_mtx dnobj;

    mem_block block;
} ownedmem;
typedef ownedmem *ownedmem_ptr;

ownedmem_ptr ownedmem_new_from_mem_block(mem_block block);
void ownedmem_new_from_mem_block_no_kheap(mem_block block, ownedmem *owndmm);
ownedmem_ptr ownedmem_new(size_t size);
bool ownedmem_new_no_kheap(size_t size, ownedmem *owndmm);
void ownedmem_cleanup(void *owndmm);
ownedmem_ptr ownedmem_ref(ownedmem_ptr owndmm);
void ownedmem_unref(ownedmem_ptr owndmm);
ownedmem_ptr ownedmem_clone(ownedmem_ptr owndmm_src);
