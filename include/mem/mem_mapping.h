#ifndef INLCUDE_MEM_MAPPING_H
#define INLCUDE_MEM_MAPPING_H

#include "config.h"
#include "mem/ownedmem.h"
#include "mem/umem.h"
#include <stdbool.h>

typedef struct mem_mapping {
    ownedmem_ptr backing;
    size_t offset;          // Offset of this mapping into backing memory
    size_t size;
#if MEM_USE_MMU
    void *k_at;             // Address at which this mapping is placed in kernel memory
    umem_ptr u_at;          // Address at which this mapping is placed in user memory
    int prot;
#endif
} mem_mapping;

static inline char *mem_mapping_k_at(mem_mapping *map) {
#if MEM_USE_MMU
    return map->k_at;
#else
    return (char *) (map->backing->block.base + map->offset);
#endif
}

static inline UMEM_PTR(char) mem_mapping_u_at(mem_mapping *map) {
#if MEM_USE_MMU
    return map->u_at;
#else
    return (UMEM_PTR(char)) mem_mapping_k_at(map);
#endif
}

static inline bool mem_mapping_contains_ptr_k(mem_mapping *map, void *ptr) {
    char *map_start = mem_mapping_k_at(map);
    char *map_end = map_start + map->size;
    return (char *) ptr >= map_start && (char *) ptr < map_end;
}

static inline bool mem_mapping_contains_ptr_u(mem_mapping *map, umem_ptr u_ptr) {
    UMEM_PTR(char) map_start = mem_mapping_u_at(map);
    UMEM_PTR(char) map_end = map_start + map->size;
    return (UMEM_PTR(char)) u_ptr >= map_start && (UMEM_PTR(char)) u_ptr < map_end;
}

static inline bool mem_mapping_contains_k(mem_mapping *map, void *ptr, size_t size) {
    char *map_start = mem_mapping_k_at(map);
    char *map_end = map_start + map->size;
    return (char *) ptr >= map_start && (char *) ptr + size <= map_end;
}

static inline bool mem_mapping_contains_u(mem_mapping *map, umem_ptr u_ptr, size_t size) {
    UMEM_PTR(char) map_start = mem_mapping_u_at(map);
    UMEM_PTR(char) map_end = map_start + map->size;
    return (UMEM_PTR(char)) u_ptr >= map_start && (UMEM_PTR(char)) u_ptr + size <= map_end;
}

static inline void *mem_mapping_u_to_k(mem_mapping *map, umem_ptr u_ptr) {
    return mem_mapping_k_at(map) + (u_ptr - mem_mapping_u_at(map));
}

static inline ptrdiff_t mem_mapping_offset_in(mem_mapping *map, umem_ptr u_ptr) {
    return u_ptr - mem_mapping_u_at(map);
}

void mem_mapping_delete(mem_mapping *map);
void mem_mapping_delete_no_kheap(mem_mapping *map);
#if MEM_USE_MMU
mem_mapping *mem_mapping_new_from_ownedmem_subset_at(ownedmem_ptr backing, size_t offset, size_t size, void *k_at, void *u_at);
mem_mapping *mem_mapping_new_from_ownedmem_at(ownedmem_ptr backing, void *k_at, void *u_at);
mem_mapping *mem_mapping_new_at(size_t size, void *k_at, void *u_at);
mem_mapping *mem_mapping_clone_ref_at(mem_mapping *map, void *k_at, void *u_at);
mem_mapping *mem_mapping_clone_copy_at(mem_mapping *map, void *k_at, void *u_at);
#else
mem_mapping *mem_mapping_new_from_ownedmem_subset(ownedmem_ptr backing, size_t offset, size_t size);
void mem_mapping_new_from_ownedmem_subset_no_kheap(ownedmem_ptr backing, size_t offset, size_t size, mem_mapping *map);
mem_mapping *mem_mapping_new_from_ownedmem(ownedmem_ptr backing, size_t size);
void mem_mapping_new_from_ownedmem_no_kheap(ownedmem_ptr backing, size_t size, mem_mapping *map);
mem_mapping *mem_mapping_new(size_t size);
bool mem_mapping_new_no_kheap(size_t size, ownedmem *backing, mem_mapping *map);
mem_mapping *mem_mapping_clone_ref(mem_mapping *map);
mem_mapping *mem_mapping_clone_copy(mem_mapping *map);
#endif

#endif
