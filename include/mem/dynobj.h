#pragma once

#include "kalloc.h"
#include "util/mutex.h"
#include "util/rwlock.h"
#include "util/util.h"
#include <stdbool.h>

typedef void (*dynobj_cleanup)(void *obj);
static void dynobj_no_cleanup(void *obj) { UNUSED(obj); }

typedef struct dynobj_base {
    ptr_range dyn_mem;
    size_t ref_count;
    dynobj_cleanup cleanup;
} dynobj_base;

typedef struct dynobj {
    dynobj_base base;
} dynobj;

typedef struct dynobj_mtx {
    dynobj_base base;
    mutex mtx;
} dynobj_mtx;

typedef struct dynobj_rwl {
    dynobj_base base;
    rwlock rwl;
} dynobj_rwl;

void *dynobj_base_new_with_type(const char *type, size_t size, dynobj_cleanup cleanup);
void *dynobj_base_new_no_kheap_with_type(const char *type, size_t size, dynobj_cleanup cleanup, void *obj);
void dynobj_base_ref_with_type(const char *type, dynobj_base *dnobj);
bool dynobj_base_unref_with_type(const char *type, dynobj_base *dnobj);

static inline void *dynobj_new_with_type(const char *type, size_t size, dynobj_cleanup cleanup) {
    return dynobj_base_new_with_type(type, size, cleanup);
}

static inline void *dynobj_new_no_kheap_with_type(const char *type, size_t size, dynobj_cleanup cleanup, void *obj) {
    return dynobj_base_new_no_kheap_with_type(type, size, cleanup, obj);
}

static inline void dynobj_ref_with_type(const char *type, dynobj *dnobj) {
    dynobj_base_ref_with_type(type, &dnobj->base);
}

static inline void dynobj_unref_with_type(const char *type, dynobj *dnobj) {
    dynobj_base_unref_with_type(type, &dnobj->base);
}

static inline void *dynobj_mtx_new_with_type(const char *type, size_t size, dynobj_cleanup cleanup) {
    void *obj = dynobj_base_new_with_type(type, size, cleanup);
    if (obj) {
        dynobj_mtx *dnobj = obj;
        mutex_init(&dnobj->mtx);
    }
    return obj;
}

static inline void *dynobj_mtx_new_no_kheap_with_type(const char *type, size_t size, dynobj_cleanup cleanup, void *obj) {
    obj = dynobj_base_new_no_kheap_with_type(type, size, cleanup, obj);
    kassert(obj);
    dynobj_mtx *dnobj = obj;
    mutex_init(&dnobj->mtx);
    return obj;
}

static inline void dynobj_mtx_ref_with_type(const char *type, dynobj_mtx *dnobj) {
    MUTEX_WITH(&dnobj->mtx, dynobj_base_ref_with_type(type, &dnobj->base);)
}

static inline void dynobj_mtx_unref_with_type(const char *type, dynobj_mtx *dnobj) {
    mutex_lock(&dnobj->mtx);
    if (dynobj_base_unref_with_type(type, &dnobj->base)) {
        mutex_unlock(&dnobj->mtx);
    }
}

static inline void *dynobj_rwl_new_with_type(const char *type, size_t size, dynobj_cleanup cleanup) {
    void *obj = dynobj_base_new_with_type(type, size, cleanup);
    if (obj) {
        dynobj_rwl *dnobj = obj;
        rwlock_init(&dnobj->rwl);
    }
    return obj;
}

static inline void *dynobj_rwl_new_no_kheap_with_type(const char *type, size_t size, dynobj_cleanup cleanup, void *obj) {
    obj = dynobj_base_new_no_kheap_with_type(type, size, cleanup, obj);
    kassert(obj);
    dynobj_rwl *dnobj = obj;
    rwlock_init(&dnobj->rwl);
    return obj;
}

static inline void dynobj_rwl_ref_with_type(const char *type, dynobj_rwl *dnobj) {
    RWLOCK_WRITE_WITH(&dnobj->rwl, dynobj_base_ref_with_type(type, &dnobj->base);)
}

static inline void dynobj_rwl_unref_with_type(const char *type, dynobj_rwl *dnobj) {
    rwlock_write_begin(&dnobj->rwl);
    if (dynobj_base_unref_with_type(type, &dnobj->base)) {
        rwlock_write_end(&dnobj->rwl);
    }
}

#define dynobj_new(type, cleanup)                   dynobj_new_with_type(#type, sizeof(type), (cleanup))
#define dynobj_new_no_kheap(type, cleanup, obj)     dynobj_new_no_kheap_with_type(#type, sizeof(type), (cleanup), (obj))
#define dynobj_ref(type, dnobj)                     dynobj_ref_with_type(#type, (dnobj))
#define dynobj_unref(type, dnobj)                   dynobj_unref_with_type(#type, (dnobj))

#define dynobj_mtx_new(type, cleanup)               dynobj_mtx_new_with_type(#type, sizeof(type), (cleanup))
#define dynobj_mtx_new_no_kheap(type, cleanup, obj) dynobj_mtx_new_no_kheap_with_type(#type, sizeof(type), (cleanup), (obj))
#define dynobj_mtx_ref(type, dnobj)                 dynobj_mtx_ref_with_type(#type, (dnobj))
#define dynobj_mtx_unref(type, dnobj)               dynobj_mtx_unref_with_type(#type, (dnobj))

#define dynobj_rwl_new(type, cleanup)               dynobj_rwl_new_with_type(#type, sizeof(type), (cleanup))
#define dynobj_rwl_new_no_kheap(type, cleanup, obj) dynobj_rwl_new_no_kheap_with_type(#type, sizeof(type), (cleanup), (obj))
#define dynobj_rwl_ref(type, dnobj)                 dynobj_rwl_ref_with_type(#type, (dnobj))
#define dynobj_rwl_unref(type, dnobj)               dynobj_rwl_unref_with_type(#type, (dnobj))
