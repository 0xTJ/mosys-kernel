#pragma once

#include "device/virtterm.h"
#include "config.h"

#if VIRTTERM_IS_XOSERA

int virtterm_xosera_init_vt_info(virtterm_info *vt_info, int *errno_ptr);
int virtterm_xosera_process_sgr_aspect(virtterm_info *vt_info, int aspect, int *errno_ptr);
int virtterm_xosera_fix_rows(virtterm_info *vt_info, int *errno_ptr);
int virtterm_xosera_draw_char_no_adv(virtterm_info *vt_info, int c, int *errno_ptr);

#   define virtterm_init_vt_info(vt_info, errno_ptr)                    \
        virtterm_xosera_init_vt_info(vt_info, errno_ptr)
#   define virtterm_process_sgr_aspect(vt_info, aspect, errno_ptr)      \
        virtterm_xosera_process_sgr_aspect(vt_info, aspect, errno_ptr)
#   define virtterm_fix_rows(vt_info, errno_ptr)                        \
        virtterm_xosera_fix_rows(vt_info, errno_ptr)
#   define virtterm_draw_char_no_adv(vt_info, c, errno_ptr)             \
        virtterm_xosera_draw_char_no_adv(vt_info, c, errno_ptr)

#endif
