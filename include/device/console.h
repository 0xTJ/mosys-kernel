#pragma once

#include "mem/flex.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <sys/types.h>

bool console_init(const char *console_name);

ssize_t console_read(flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
ssize_t console_write(flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
int console_ioctl(unsigned long request, flex_mut argp, int *errno_ptr);
