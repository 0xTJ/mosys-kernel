#pragma once

#include "config.h"
#include "exception.h"
#include "device/hardware/mc68681_arch.h"
#include "util/util.h"
#include <stdbool.h>
#include <stdint.h>

#if DEV_MC68681_ENABLE

#define MC68681_R(reg) (*(volatile uint8_t *) (reg))

enum mc68681_mr1 {
    MC68681_MR1_RXRTS_CONTROL_NO            = 0x00,
    MC68681_MR1_RXRTS_CONTROL_YES           = 0x80,
    MC68681_MR1_RXRTS_CONTROL_MASK          = 0x80,

    MC68681_MR1_RXINT_SELECT_RXRDY          = 0x00,
    MC68681_MR1_RXINT_SELECT_FFULL          = 0x40,
    MC68681_MR1_RXINT_SELECT_MASK           = 0x40,

    MC68681_MR1_ERROR_MODE_CHAR             = 0x00,
    MC68681_MR1_ERROR_MODE_BLOCK            = 0x20,
    MC68681_MR1_ERROR_MODE_MASK             = 0x20,

    MC68681_MR1_PARITY_MODE_WITH_PARITY     = 0x00,
    MC68681_MR1_PARITY_MODE_FORCE_PARITY    = 0x08,
    MC68681_MR1_PARITY_MODE_NO_PARITY       = 0x10,
    MC68681_MR1_PARITY_MODE_MULTIDROP_MODE  = 0x18,
    MC68681_MR1_PARITY_MODE_MASK            = 0x18,

    MC68681_MR1_PARITY_TYPE_EVEN            = 0x00,
    MC68681_MR1_PARITY_TYPE_ODD             = 0x04,
    MC68681_MR1_PARITY_TYPE_MASK            = 0x04,

    MC68681_MR1_BITS_PER_CHARACTER_5        = 0x00,
    MC68681_MR1_BITS_PER_CHARACTER_6        = 0x01,
    MC68681_MR1_BITS_PER_CHARACTER_7        = 0x02,
    MC68681_MR1_BITS_PER_CHARACTER_8        = 0x03,
    MC68681_MR1_BITS_PER_CHARACTER_MASK     = 0x03,

    MC68681_MR1_MASK                        = 0xFF,
    MC68681_MR1_MAX                         = 0xFF,
};

enum mc68681_mr2 {
    MC68681_MR2_CHANNEL_MODE_NORMAL         = 0x00,
    MC68681_MR2_CHANNEL_MODE_AUTO_ECHO      = 0x40,
    MC68681_MR2_CHANNEL_MODE_LOCAL_LOOP     = 0x80,
    MC68681_MR2_CHANNEL_MODE_REMOTE_LOOP    = 0xC0,
    MC68681_MR2_CHANNEL_MODE_MASK           = 0xC0,

    MC68681_MR2_TXRTS_CONTROL_NO            = 0x00,
    MC68681_MR2_TXRTS_CONTROL_YES           = 0x20,
    MC68681_MR2_TXRTS_CONTROL_MASK          = 0x20,

    MC68681_MR2_CTS_ENABLE_TX_NO            = 0x00,
    MC68681_MR2_CTS_ENABLE_TX_YES           = 0x10,
    MC68681_MR2_CTS_ENABLE_TX_MASK          = 0x10,

    // Add 0.5 to values shown for 0 - 7 if channel is programmed for 5 bits/char.
    MC68681_MR2_STOP_BIT_LENGTH_0_563       = 0x00,
    MC68681_MR2_STOP_BIT_LENGTH_0_625       = 0x01,
    MC68681_MR2_STOP_BIT_LENGTH_0_688       = 0x02,
    MC68681_MR2_STOP_BIT_LENGTH_0_750       = 0x03,
    MC68681_MR2_STOP_BIT_LENGTH_0_813       = 0x04,
    MC68681_MR2_STOP_BIT_LENGTH_0_875       = 0x05,
    MC68681_MR2_STOP_BIT_LENGTH_0_938       = 0x06,
    MC68681_MR2_STOP_BIT_LENGTH_1_000       = 0x07,
    MC68681_MR2_STOP_BIT_LENGTH_1_563       = 0x08,
    MC68681_MR2_STOP_BIT_LENGTH_1_625       = 0x09,
    MC68681_MR2_STOP_BIT_LENGTH_1_688       = 0x0A,
    MC68681_MR2_STOP_BIT_LENGTH_1_750       = 0x0B,
    MC68681_MR2_STOP_BIT_LENGTH_1_813       = 0x0C,
    MC68681_MR2_STOP_BIT_LENGTH_1_875       = 0x0D,
    MC68681_MR2_STOP_BIT_LENGTH_1_938       = 0x0E,
    MC68681_MR2_STOP_BIT_LENGTH_2_000       = 0x0F,
    MC68681_MR2_STOP_BIT_LENGTH_MASK        = 0x0F,

    MC68681_MR2_MASK                        = 0xFF,
    MC68681_MR2_MAX                         = 0xFF,
};

struct mc68681_mr {
    enum mc68681_mr1 mr1;
    enum mc68681_mr2 mr2;
};

enum mc68681_csr {
    MC68681_CSR_RECEIVER_CLOCK_SELECT_MASK      = 0xF0,
    MC68681_CSR_TRANSMITTER_CLOCK_SELECT_MASK   = 0x0F,

    MC68681_CSR_MASK                            = 0xFF,
    MC68681_CSR_MAX                             = 0xFF,
};

enum mc68681_cr {
    MC68681_CR_MISCELLANEOUS_COMMANDS_MASK      = 0xF0,
    MC68681_CR_NULL_COMMAND                     = 0x00,
    MC68681_CR_RESET_MRN_POINTER                = 0x10,
    MC68681_CR_RESET_RECEIVER                   = 0x20,
    MC68681_CR_RESET_TRANSMITTER                = 0x30,
    MC68681_CR_RESET_ERROR_STATUS               = 0x40,
    MC68681_CR_RESET_BREAK_CHANGE_INTERRUPT     = 0x50,
    MC68681_CR_START_BREAK                      = 0x60,
    MC68681_CR_STOP_BREAK                       = 0x70,
    MC68681_CR_SET_RX_BRG_SELECT_EXTEND_BIT     = 0x80,
    MC68681_CR_CLEAR_RX_BRG_SELECT_EXTEND_BIT   = 0x90,
    MC68681_CR_SET_TX_BRG_SELECT_EXTEND_BIT     = 0xA0,
    MC68681_CR_CLEAR_TX_BRG_SELECT_EXTEND_BIT   = 0xB0,
    MC68681_CR_SET_STANDBY_MODE                 = 0xC0, // Channel A only
    MC68681_CR_SET_ACTIVE_MODE                  = 0xD0, // Channel A only

    MC68681_CR_DISABLE_TX_NO                    = 0x00,
    MC68681_CR_DISABLE_TX_YES                   = 0x08,
    MC68681_CR_DISABLE_TX_MASK                  = 0x08,

    MC68681_CR_ENABLE_TX_NO                     = 0x00,
    MC68681_CR_ENABLE_TX_YES                    = 0x04,
    MC68681_CR_ENABLE_TX_MASK                   = 0x04,

    MC68681_CR_DISABLE_RX_NO                    = 0x00,
    MC68681_CR_DISABLE_RX_YES                   = 0x02,
    MC68681_CR_DISABLE_RX_MASK                  = 0x02,

    MC68681_CR_ENABLE_RX_NO                     = 0x00,
    MC68681_CR_ENABLE_RX_YES                    = 0x01,
    MC68681_CR_ENABLE_RX_MASK                   = 0x01,

    MC68681_CR_MASK                             = 0xFF,
    MC68681_CR_MAX                              = 0xDA,
};

enum mc68681_sr {
    MC68681_SR_RECEIVED_BREAK_NO            = 0x00,
    MC68681_SR_RECEIVED_BREAK_YES           = 0x80,
    MC68681_SR_RECEIVED_BREAK_MASK          = 0x80,

    MC68681_SR_FRAMING_ERROR_NO             = 0x00,
    MC68681_SR_FRAMING_ERROR_YES            = 0x40,
    MC68681_SR_FRAMING_ERROR_MASK           = 0x40,

    MC68681_SR_PARITY_ERROR_NO              = 0x00,
    MC68681_SR_PARITY_ERROR_YES             = 0x20,
    MC68681_SR_PARITY_ERROR_MASK            = 0x20,

    MC68681_SR_OVERRUN_ERROR_NO             = 0x00,
    MC68681_SR_OVERRUN_ERROR_YES            = 0x10,
    MC68681_SR_OVERRUN_ERROR_MASK           = 0x10,

    MC68681_SR_TXEMT_NO                     = 0x00,
    MC68681_SR_TXEMT_YES                    = 0x08,
    MC68681_SR_TXEMT_MASK                   = 0x08,

    MC68681_SR_TXRDY_NO                     = 0x00,
    MC68681_SR_TXRDY_YES                    = 0x04,
    MC68681_SR_TXRDY_MASK                   = 0x04,

    MC68681_SR_FFULL_NO                     = 0x00,
    MC68681_SR_FFULL_YES                    = 0x02,
    MC68681_SR_FFULL_MASK                   = 0x02,

    MC68681_SR_RXRDY_NO                     = 0x00,
    MC68681_SR_RXRDY_YES                    = 0x01,
    MC68681_SR_RXRDY_MASK                   = 0x01,

    MC68681_SR_MASK                         = 0xFF,
    MC68681_SR_MAX                          = 0xFF,
};

enum mc68681_opcr {
    MC68681_OPCR_OP7_OPR7           = 0x00,
    MC68681_OPCR_OP7_TXRDYB         = 0x80,
    MC68681_OPCR_OP7_MASK           = 0x80,

    MC68681_OPCR_OP6_OPR6           = 0x00,
    MC68681_OPCR_OP6_TXRDYA         = 0x40,
    MC68681_OPCR_OP6_MASK           = 0x40,

    MC68681_OPCR_OP5_OPR5           = 0x00,
    MC68681_OPCR_OP5_RXRDY_FFULLB   = 0x20,
    MC68681_OPCR_OP5_MASK           = 0x20,

    MC68681_OPCR_OP4_OPR4           = 0x00,
    MC68681_OPCR_OP4_RXRDY_FFULLA   = 0x10,
    MC68681_OPCR_OP4_MASK           = 0x10,

    MC68681_OPCR_OP3_OPR3           = 0x00,
    MC68681_OPCR_OP3_C_T_OUTPUT     = 0x04,
    MC68681_OPCR_OP3_TXCB_1X        = 0x08,
    MC68681_OPCR_OP3_RXCB_1X        = 0x0C,
    MC68681_OPCR_OP3_MASK           = 0x0C,

    MC68681_OPCR_OP2_OPR2           = 0x00,
    MC68681_OPCR_OP2_TXCA_16X       = 0x01,
    MC68681_OPCR_OP2_TXCA_1X        = 0x02,
    MC68681_OPCR_OP2_RXCA_1X        = 0x03,
    MC68681_OPCR_OP2_MASK           = 0x03,

    MC68681_OPCR_MASK               = 0xFF,
    MC68681_OPCR_MAX                = 0xFF,
};

enum mc68681_acr {
    MC68681_ACR_BRG_SET_SELECT_SET_1            = 0x00,
    MC68681_ACR_BRG_SET_SELECT_SET_2            = 0x80,
    MC68681_ACR_BRG_SET_SELECT_MASK             = 0x80,

    MC68681_ACR_CT_MODE_AND_SOURCE_MASK         = 0x70,
    MC68681_ACR_CT_MODE_COUNTER_IP2             = 0x00,
    MC68681_ACR_CT_MODE_COUNTER_TXCA_1X         = 0x10,
    MC68681_ACR_CT_MODE_COUNTER_TXCB_1X         = 0x20,
    MC68681_ACR_CT_MODE_COUNTER_CLK_OVER_16     = 0x30,
    MC68681_ACR_CT_MODE_TIMER_IP2               = 0x40,
    MC68681_ACR_CT_MODE_TIMER_IP2_DIVIDED_BY_16 = 0x50,
    MC68681_ACR_CT_MODE_TIMER_CLK               = 0x60,
    MC68681_ACR_CT_MODE_TIMER_CLK_DIVIDED_BY_16 = 0x70,

    MC68681_ACR_DELTA_IP3_INT_OFF               = 0x00,
    MC68681_ACR_DELTA_IP3_INT_ON                = 0x08,
    MC68681_ACR_DELTA_IP3_INT_MASK              = 0x08,

    MC68681_ACR_DELTA_IP2_INT_OFF               = 0x00,
    MC68681_ACR_DELTA_IP2_INT_ON                = 0x04,
    MC68681_ACR_DELTA_IP2_INT_MASK              = 0x04,

    MC68681_ACR_DELTA_IP1_INT_OFF               = 0x00,
    MC68681_ACR_DELTA_IP1_INT_ON                = 0x02,
    MC68681_ACR_DELTA_IP1_INT_MASK              = 0x02,

    MC68681_ACR_DELTA_IP0_INT_OFF               = 0x00,
    MC68681_ACR_DELTA_IP0_INT_ON                = 0x01,
    MC68681_ACR_DELTA_IP0_INT_MASK              = 0x01,

    MC68681_ACR_MASK                            = 0xFF,
    MC68681_ACR_MAX                             = 0xFF,
};

enum mc68681_ipcr {
    MC68681_IPCR_DELTA_IP3_NO   = 0x00,
    MC68681_IPCR_DELTA_IP3_YES  = 0x80,
    MC68681_IPCR_DELTA_IP3_MASK = 0x80,

    MC68681_IPCR_DELTA_IP2_NO   = 0x00,
    MC68681_IPCR_DELTA_IP2_YES  = 0x40,
    MC68681_IPCR_DELTA_IP2_MASK = 0x40,

    MC68681_IPCR_DELTA_IP1_NO   = 0x00,
    MC68681_IPCR_DELTA_IP1_YES  = 0x20,
    MC68681_IPCR_DELTA_IP1_MASK = 0x20,

    MC68681_IPCR_DELTA_IP0_NO   = 0x00,
    MC68681_IPCR_DELTA_IP0_YES  = 0x10,
    MC68681_IPCR_DELTA_IP0_MASK = 0x10,

    MC68681_IPCR_IP3_LOW        = 0x00,
    MC68681_IPCR_IP3_HIGH       = 0x08,
    MC68681_IPCR_IP3_MASK       = 0x08,

    MC68681_IPCR_IP2_LOW        = 0x00,
    MC68681_IPCR_IP2_HIGH       = 0x04,
    MC68681_IPCR_IP2_MASK       = 0x04,

    MC68681_IPCR_IP1_LOW        = 0x00,
    MC68681_IPCR_IP1_HIGH       = 0x02,
    MC68681_IPCR_IP1_MASK       = 0x02,

    MC68681_IPCR_IP0_LOW        = 0x00,
    MC68681_IPCR_IP0_HIGH       = 0x01,
    MC68681_IPCR_IP0_MASK       = 0x01,

    MC68681_IPCR_MASK           = 0xFF,
    MC68681_IPCR_MAX            = 0xFF,
};

enum mc68681_isr {
    MC68681_ISR_INPUT_PORT_CHANGE_NO    = 0x00,
    MC68681_ISR_INPUT_PORT_CHANGE_YES   = 0x80,
    MC68681_ISR_INPUT_PORT_CHANGE_MASK  = 0x80,

    MC68681_ISR_DELTA_BREAK_B_NO        = 0x00,
    MC68681_ISR_DELTA_BREAK_B_YES       = 0x40,
    MC68681_ISR_DELTA_BREAK_B_MASK      = 0x40,

    MC68681_ISR_RXRDY_FFULLB_NO         = 0x00,
    MC68681_ISR_RXRDY_FFULLB_YES        = 0x20,
    MC68681_ISR_RXRDY_FFULLB_MASK       = 0x20,

    MC68681_ISR_TXRDYB_NO               = 0x00,
    MC68681_ISR_TXRDYB_YES              = 0x10,
    MC68681_ISR_TXRDYB_MASK             = 0x10,

    MC68681_ISR_COUNTER_READY_NO        = 0x00,
    MC68681_ISR_COUNTER_READY_YES       = 0x08,
    MC68681_ISR_COUNTER_READY_MASK      = 0x08,

    MC68681_ISR_DELTA_BREAK_A_NO        = 0x00,
    MC68681_ISR_DELTA_BREAK_A_YES       = 0x04,
    MC68681_ISR_DELTA_BREAK_A_MASK      = 0x04,

    MC68681_ISR_RXRDY_FFULLA_NO         = 0x00,
    MC68681_ISR_RXRDY_FFULLA_YES        = 0x02,
    MC68681_ISR_RXRDY_FFULLA_MASK       = 0x02,

    MC68681_ISR_TXRDYA_NO               = 0x00,
    MC68681_ISR_TXRDYA_YES              = 0x01,
    MC68681_ISR_TXRDYA_MASK             = 0x01,

    MC68681_ISR_ALL_NO                  = 0x00,
    MC68681_ISR_ALL_YES                 = 0xFF,

    MC68681_ISR_MASK                    = 0xFF,
    MC68681_ISR_MAX                     = 0xFF,
};

enum mc68681_imr {
    MC68681_IMR_INPUT_PORT_CHANGE_OFF   = 0x00,
    MC68681_IMR_INPUT_PORT_CHANGE_ON    = 0x80,
    MC68681_IMR_INPUT_PORT_CHANGE_MASK  = 0x80,

    MC68681_IMR_DELTA_BREAK_B_OFF       = 0x00,
    MC68681_IMR_DELTA_BREAK_B_ON        = 0x40,
    MC68681_IMR_DELTA_BREAK_B_MASK      = 0x40,

    MC68681_IMR_RXRDY_FFULLB_OFF        = 0x00,
    MC68681_IMR_RXRDY_FFULLB_ON         = 0x20,
    MC68681_IMR_RXRDY_FFULLB_MASK       = 0x20,

    MC68681_IMR_TXRDYB_OFF              = 0x00,
    MC68681_IMR_TXRDYB_ON               = 0x10,
    MC68681_IMR_TXRDYB_MASK             = 0x10,

    MC68681_IMR_COUNTER_READY_OFF       = 0x00,
    MC68681_IMR_COUNTER_READY_ON        = 0x08,
    MC68681_IMR_COUNTER_READY_MASK      = 0x08,

    MC68681_IMR_DELTA_BREAK_A_OFF       = 0x00,
    MC68681_IMR_DELTA_BREAK_A_ON        = 0x04,
    MC68681_IMR_DELTA_BREAK_A_MASK      = 0x04,

    MC68681_IMR_RXRDY_FFULLA_OFF        = 0x00,
    MC68681_IMR_RXRDY_FFULLA_ON         = 0x02,
    MC68681_IMR_RXRDY_FFULLA_MASK       = 0x02,

    MC68681_IMR_TXRDYA_OFF              = 0x00,
    MC68681_IMR_TXRDYA_ON               = 0x01,
    MC68681_IMR_TXRDYA_MASK             = 0x01,

    MC68681_IMR_ALL_OFF                 = 0x00,
    MC68681_IMR_ALL_ON                  = 0xFF,

    MC68681_IMR_MASK                    = 0xFF,
    MC68681_IMR_MAX                     = 0xFF,
};

static inline void mc68681_cra_set(enum mc68681_cr crb);
static inline void mc68681_crb_set(enum mc68681_cr crb);

// Read Mode Register A (MR1A, MR2A)
static inline enum mc68681_mr1 mc68681_mr1a_get_raw(void) {
    return MC68681_R(MC68681_MRA_R(MC68681_BASE));
}
static inline enum mc68681_mr1 mc68681_mr1a_get(void) {
    mc68681_cra_set(MC68681_CR_RESET_MRN_POINTER);
    return mc68681_mr1a_get_raw();
}
static inline enum mc68681_mr2 mc68681_mr2a_get_raw(void) {
    return MC68681_R(MC68681_MRA_R(MC68681_BASE));
}
static inline enum mc68681_mr2 mc68681_mr2a_get(void) {
    mc68681_mr1a_get_raw();
    return mc68681_mr2a_get_raw();
}
static inline struct mc68681_mr mc68681_mra_get(void) {
    struct mc68681_mr result;
    result.mr1 = mc68681_mr1a_get();
    result.mr2 = mc68681_mr2a_get_raw();
    return result;
}

// Write Mode Register A (MR1A, MR2A)
static inline void mc68681_mr1a_set_raw(enum mc68681_mr1 mr1a) {
    MC68681_R(MC68681_MRA_W(MC68681_BASE)) = mr1a;
}
static inline void mc68681_mr1a_set(enum mc68681_mr1 mr1a) {
    mc68681_cra_set(MC68681_CR_RESET_MRN_POINTER);
    mc68681_mr1a_set_raw(mr1a);
}
static inline void mc68681_mr2a_set_raw(enum mc68681_mr2 mr2a) {
    MC68681_R(MC68681_MRA_W(MC68681_BASE)) = mr2a;
}
static inline void mc68681_mr2a_set(enum mc68681_mr2 mr2a) {
    mc68681_mr1a_get_raw();
    mc68681_mr2a_set_raw(mr2a);
}
static inline void mc68681_mra_set(struct mc68681_mr mra) {
    mc68681_mr1a_set(mra.mr1);
    mc68681_mr2a_set_raw(mra.mr2);
}

// Read Status Register A (SRA)
static inline enum mc68681_sr mc68681_sra_get(void) {
    return MC68681_R(MC68681_SRA_R(MC68681_BASE));
}

// Write Clock Select Register A (CSRA)
static inline void mc68681_csra_set(enum mc68681_csr csra) {
    MC68681_R(MC68681_CSRA_W(MC68681_BASE)) = csra;
}

// Read Masked Interrupt Status Register (MISR)
static inline enum mc68681_isr mc68681_misr_get(void) {
    return MC68681_R(MC68681_MISR_R(MC68681_BASE));
}

// Write Command Register A (CRA)
static inline void mc68681_cra_set(enum mc68681_cr cra) {
    MC68681_R(MC68681_CRA_W(MC68681_BASE)) = cra;
}

// Read Rx Holding Register A (RHRA)
static inline uint8_t mc68681_rhra_get(void) {
    return MC68681_R(MC68681_RHRA_R(MC68681_BASE));
}

// Write Tx Holding Register A (THRA)
static inline void mc68681_thra_set(uint8_t thra) {
    MC68681_R(MC68681_THRA_W(MC68681_BASE)) = thra;
}

// Read Input Port Change Register (IPCR)
static inline enum mc68681_ipcr mc68681_ipcr_get(void) {
    return MC68681_R(MC68681_IPCR_R(MC68681_BASE));
}

// Write Auxiliary Control Register (ACR)
static inline void mc68681_acr_set(enum mc68681_acr acr) {
    MC68681_R(MC68681_ACR_W(MC68681_BASE)) = acr;
}

// Read Interrupt Status Register (ISR)
static inline enum mc68681_isr mc68681_isr_get(void) {
    return MC68681_R(MC68681_ISR_R(MC68681_BASE));
}

// Write Interrupt Mask Register (IMR)
static inline void mc68681_imr_set(enum mc68681_imr imr) {
    MC68681_R(MC68681_IMR_W(MC68681_BASE)) = imr;
}

// Read Counter/Timer Register (CTR)
static inline uint8_t mc68681_ctur_get(void) {
    return MC68681_R(MC68681_CTUR_R(MC68681_BASE));
}
static inline uint8_t mc68681_ctlr_get(void) {
    return MC68681_R(MC68681_CTLR_R(MC68681_BASE));
}
static inline uint16_t mc68681_ctr_get(void) {
    return (uint16_t) mc68681_ctur_get() << 8 | mc68681_ctlr_get();
}

// Write Counter/Timer Register (CTR)
static inline void mc68681_ctur_set(uint8_t ctur) {
    MC68681_R(MC68681_CTUR_W(MC68681_BASE)) = ctur;
}
static inline void mc68681_ctlr_set(uint8_t ctlr) {
    MC68681_R(MC68681_CTLR_W(MC68681_BASE)) = ctlr;
}
static inline void mc68681_ctr_set(uint16_t ctr) {
    mc68681_ctur_set((uint8_t) (ctr >> 8));
    mc68681_ctlr_set((uint8_t) (ctr >> 0));
}

// Read Mode Register B (MR1B, MR2B)
static inline enum mc68681_mr1 mc68681_mr1b_get_raw(void) {
    return MC68681_R(MC68681_MRB_R(MC68681_BASE));
}
static inline enum mc68681_mr1 mc68681_mr1b_get(void) {
    mc68681_crb_set(MC68681_CR_RESET_MRN_POINTER);
    return mc68681_mr1b_get_raw();
}
static inline enum mc68681_mr2 mc68681_mr2b_get_raw(void) {
    return MC68681_R(MC68681_MRB_R(MC68681_BASE));
}
static inline enum mc68681_mr2 mc68681_mr2b_get(void) {
    mc68681_mr1b_get_raw();
    return mc68681_mr2b_get_raw();
}
static inline struct mc68681_mr mc68681_mrb_get(void) {
    struct mc68681_mr result;
    result.mr1 = mc68681_mr1b_get();
    result.mr2 = mc68681_mr2b_get_raw();
    return result;
}

// Write Mode Register B (MR1B, MR2B)
static inline void mc68681_mr1b_set_raw(enum mc68681_mr1 mr1b) {
    MC68681_R(MC68681_MRB_W(MC68681_BASE)) = mr1b;
}
static inline void mc68681_mr1b_set(enum mc68681_mr1 mr1b) {
    mc68681_crb_set(MC68681_CR_RESET_MRN_POINTER);
    mc68681_mr1b_set_raw(mr1b);
}
static inline void mc68681_mr2b_set_raw(enum mc68681_mr2 mr2b) {
    MC68681_R(MC68681_MRB_W(MC68681_BASE)) = mr2b;
}
static inline void mc68681_mr2b_set(enum mc68681_mr2 mr2b) {
    mc68681_mr1b_get_raw();
    mc68681_mr2b_set_raw(mr2b);
}
static inline void mc68681_mrb_set(struct mc68681_mr mrb) {
    mc68681_mr1b_set(mrb.mr1);
    mc68681_mr2b_set_raw(mrb.mr2);
}

// Read Status Register B (SRB)
static inline enum mc68681_sr mc68681_srb_get(void) {
    return MC68681_R(MC68681_SRB_R(MC68681_BASE));
}

// Write Clock Select Register B (CSRB)
static inline void mc68681_csrb_set(enum mc68681_csr csrb) {
    MC68681_R(MC68681_CSRB_W(MC68681_BASE)) = csrb;
}

// Write Command Register B (CRB)
static inline void mc68681_crb_set(enum mc68681_cr crb) {
    MC68681_R(MC68681_CRB_W(MC68681_BASE)) = crb;
}

// Read Rx Holding Register B (RHRB)
static inline uint8_t mc68681_rhrb_get(void) {
    return MC68681_R(MC68681_RHRB_R(MC68681_BASE));
}

// Write Tx Holding Register B (THRB)
static inline void mc68681_thrb_set(uint8_t thrb) {
    MC68681_R(MC68681_THRB_W(MC68681_BASE)) = thrb;
}

// Read Interrupt Vector Register (IVR)
static inline uint8_t mc68681_ivr_get(void) {
    return MC68681_R(MC68681_IVR_R(MC68681_BASE));
}

// Write Interrupt Vector Register (IVR)
static inline void mc68681_ivr_set(uint8_t ivr) {
    MC68681_R(MC68681_IVR_W(MC68681_BASE)) = ivr;
}

// Read Input Port (IP)
static inline uint8_t mc68681_ip_get(void) {
    return MC68681_R(MC68681_IP_R(MC68681_BASE));
}

// Write Output Port Configuration Register (OPCR)
static inline void mc68681_opcr_set(enum mc68681_opcr opcr) {
    MC68681_R(MC68681_OPCR_W(MC68681_BASE)) = opcr;
}

// Read Start Counter/Timer Command (SCC)
static inline void mc68681_scc(void) {
    UNUSED(MC68681_R(MC68681_SCC_R(MC68681_BASE)));
}

// Write Set Output Port Bits Command (SOPBC)
static inline void mc68681_sopbc_set(uint8_t sopbc) {
    MC68681_R(MC68681_SOPBC_W(MC68681_BASE)) = sopbc;
}

// Read Stop Counter/Timer Command (STC)
static inline void mc68681_stc(void) {
    UNUSED(MC68681_R(MC68681_STC_R(MC68681_BASE)));
}

// Write Clear Output Port Bits Command (COPBC)
static inline void mc68681_copbc_set(uint8_t copbc) {
    MC68681_R(MC68681_COPBC_W(MC68681_BASE)) = copbc;
}

static inline void mc68681_interrupt_install(exception_raw_func handler) {
    uint8_t vector_base = mc68681_ivr_get();
    exception_vector_raw_replace(handler, vector_base);
}

#undef MC68681_R

#endif
