#pragma once

#include "config.h"
#include "eeprom.h"
#include "mem/mem.h"
#include "util/util.h"
#include <stdint.h>

#if DEV_SST39SF0X0_ENABLE

typedef enum sst39sf0x0_chip {
    // Can return any 16-bit value
    SST39SF0X0_CHIP_MIN     = 0x0000,
    SST39SF0X0_CHIP_MAX     = 0xFFFF,

    SST39SF0X0_CHIP_010A    = 0xBFB5,
    SST39SF0X0_CHIP_020A    = 0xBFB6,
    SST39SF0X0_CHIP_040     = 0xBFB7,
} sst39sf0x0_chip;

typedef enum sst39sf0x0_sector_size {
    SST39SF0X0_SECTOR_SIZE  = 0x1000,
} sst39sf0x0_sector_size;

typedef enum sst39sf0x0_sector_count {
    SST39SF0X0_SECTOR_COUNT_010A    = 0x20,
    SST39SF0X0_SECTOR_COUNT_020A    = 0x40,
    SST39SF0X0_SECTOR_COUNT_040     = 0x80,
} sst39sf0x0_sector_count;

typedef enum sst39sf0x0_location {
    SST39SF0X0_LOCATION_EVEN    = 0,
    SST39SF0X0_LOCATION_ODD     = 1,
} sst39sf0x0_location;

typedef enum sst39sf0x0_dual_location {
    SST39SF0X0_DUAL_LOCATION_ALL    = SST39SF0X0_LOCATION_EVEN,
} sst39sf0x0_dual_location;

// Byte-interleaved chips must be paired
static volatile void *const sst39sf0x0_bases[] = {
    [SST39SF0X0_LOCATION_EVEN]  = (volatile uint8_t*) mem_rom_start + 0,
    [SST39SF0X0_LOCATION_ODD]   = (volatile uint8_t*) mem_rom_start + 1,
};

static inline uint8_t sst39sf0x0_byte_read(sst39sf0x0_location location, uint32_t addr) {
    return eeprom_1x8_read(sst39sf0x0_bases[location], 1, addr);
}

static inline void sst39sf0x0_byte_program(sst39sf0x0_location location, uint32_t addr, uint8_t data) {
    eeprom_1x8_byte_program(sst39sf0x0_bases[location], 1, addr, data);
}

static inline void sst39sf0x0_sector_erase(sst39sf0x0_location location, uint32_t addr) {
    eeprom_1x8_sector_erase(sst39sf0x0_bases[location], 1, addr);
}

static inline void sst39sf0x0_chip_erase(sst39sf0x0_location location) {
    // TODO: Maybe don't
    kpanic("Refusing to erase system ROM");
    eeprom_1x8_chip_erase(sst39sf0x0_bases[location], 1);
}

static inline void sst39sf0x0_software_id_entry(sst39sf0x0_location location) {
    eeprom_1x8_software_autoselect(sst39sf0x0_bases[location], 1);
}

static inline void sst39sf0x0_software_id_exit(sst39sf0x0_location location) {
    eeprom_1x8_software_read_reset(sst39sf0x0_bases[location], 1);
}

static inline sst39sf0x0_chip sst39sf0x0_identify(sst39sf0x0_location location) {
    sst39sf0x0_software_id_entry(location);
    uint16_t chip = (uint16_t) sst39sf0x0_byte_read(location, 0) << 8 | (uint16_t) sst39sf0x0_byte_read(location, 0) << 0;
    sst39sf0x0_software_id_exit(location);
    return (sst39sf0x0_chip) chip;
}

static inline uint8_t sst39sf0x0_dual_byte_read(sst39sf0x0_dual_location location, uint32_t addr) {
    unsigned char chip_slot = addr % 2;
    return eeprom_1x8_read(sst39sf0x0_bases[location + chip_slot], 1, addr >> 1);
}

static inline uint16_t sst39sf0x0_dual_word_read(sst39sf0x0_dual_location location, uint32_t addr) {
    return eeprom_2x8_read(sst39sf0x0_bases[location], 0, addr >> 1);
}

static inline void sst39sf0x0_dual_byte_program(sst39sf0x0_dual_location location, uint32_t addr, uint8_t data) {
    unsigned char chip_slot = addr % 2;
    eeprom_1x8_byte_program(sst39sf0x0_bases[location + chip_slot], 1, addr >> 1, data);
}

static inline void sst39sf0x0_dual_word_program(sst39sf0x0_dual_location location, uint32_t addr, uint16_t data) {
    eeprom_2x8_byte_program(sst39sf0x0_bases[location], 0, addr >> 1, data);
}

static inline void sst39sf0x0_dual_sector_erase(sst39sf0x0_dual_location location, uint32_t addr) {
    eeprom_2x8_sector_erase(sst39sf0x0_bases[location], 0, addr >> 1);
}

static inline void sst39sf0x0_dual_chip_erase(sst39sf0x0_dual_location location) {
    // TODO: Maybe don't
    kpanic("Refusing to erase system ROM");
    eeprom_2x8_chip_erase(sst39sf0x0_bases[location], 0);
}

#endif
