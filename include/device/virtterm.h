#pragma once

#include "config.h"
#include "device/device.h"
#include "vfs/devreg.h"
#include <stdint.h>

#define VT_ROWS 30
#define VT_COLS 106
#define VT_CSI_PARAM_COUNT_MAX 16
#define VT_CSI_INTER_COUNT_MAX 1

typedef enum virtterm_state {
    VIRTTERM_STATE_NORMAL = 0,
    VIRTTERM_STATE_FE,
    VIRTTERM_STATE_CSI,
} virtterm_state;

typedef struct virtterm_info {
    devreg *kbd_dr;
    devreg *screen_dr;

    uint8_t rows;
    uint8_t cols;
    uint8_t current_row;
    uint8_t current_col;

    uint8_t xosera_colour_code;

    // TODO: row_buffer should probably be dynamically allocated
    uint8_t row_buffer[VT_COLS * 2];

    virtterm_state state;
    bool csi_accept_param;
    int csi_param_count;
    int csi_inter_count;
    int csi_param[VT_CSI_PARAM_COUNT_MAX];
    int csi_inter[VT_CSI_INTER_COUNT_MAX];
    int csi_final;
} virtterm_info;

int virtterm_init_vt_info(virtterm_info *vt_info, int *errno_ptr);
int virtterm_handle_code(virtterm_info *vt_info, int c, int *errno_ptr);
int virtterm_handle_c0(virtterm_info *vt_info, int c, int *errno_ptr);
int virtterm_handle_fe(virtterm_info *vt_info, int c, int *errno_ptr);
int virtterm_handle_csi(virtterm_info *vt_info, int c, int *errno_ptr);
int virtterm_process_csi(virtterm_info *vt_info, int *errno_ptr);
int virtterm_process_sgr(virtterm_info *vt_info, int *errno_ptr);
int virtterm_process_sgr_aspect(virtterm_info *vt_info, int aspect, int *errno_ptr);
int virtterm_fix_rows(virtterm_info *vt_info, int *errno_ptr);
int virtterm_draw_char(virtterm_info *vt_info, int c, int *errno_ptr);
int virtterm_draw_char_no_adv(virtterm_info *vt_info, int c, int *errno_ptr);

#if VIRTTERM_IS_XOSERA
#   include "device/virtterm/xosera.h"
#endif
