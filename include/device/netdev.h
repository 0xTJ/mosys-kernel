#pragma once

#include "config.h"
#include "mem/dynobj.h"
#include "network/ipv4.h"
#include "network/link/link.h"
#include "network/internet/internet.h"
#include "network/internet/internet_ipv4.h"
#include "uapi/sys/types.h"
#include "util/mutex.h"
#include <stdbool.h>
#include <stdint.h>

#if NETWORK_LINK_ENABLE

#define NETDEV_NAME_SIZE 8
#define NETDEV_AUTO_NAME_LIMIT 32

typedef struct netdev {
    dynobj_mtx dnobj;

    char name[NETDEV_NAME_SIZE];
    struct netdev *next;    // Used for global netdev list

    const struct link_ops *dtlnk_ops; // Must be non-null

    size_t mtu;             // maximum transmission unit

#if NETWORK_INTERNET_IPV4_ENABLE
    bool has_ipv4_address;
    ipv4_addr ipv4_address;
    ipv4_addr ipv4_netmask;
#endif

    void *priv;             // Driver-dependant private data, is deallocated on delete
} netdev;

// name may contain up to one "%d" replacement pattern, but no others
netdev *netdev_new(size_t priv_size, const char *name, const struct link_ops *dtlnk_ops);
void netdev_ref(netdev *ntdv);
void netdev_unref(netdev *ntdv);

// Binding can fail if a devices exists with that name already
bool netdev_register(netdev *ntdv);

netdev *netdev_by_name(const char *name);
#if NETWORK_INTERNET_IPV4_ENABLE
netdev *netdev_by_ipv4(ipv4_addr target_addr, ipv4_addr *actual_addr_out);
#endif

#endif
