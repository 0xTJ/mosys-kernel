#pragma once

#include <stddef.h>
#include <stdint.h>

#define CH559_USB_HOST_TOKEN 0xFE

struct ch559_usb_host_message_raw {
    uint16_t length_le;
    uint8_t msgtype;
    uint8_t type;
    uint8_t device;
    uint8_t endpoint;
    uint16_t id_vendor_le;
    uint16_t id_product_le;
} __attribute__((packed));

typedef enum ch559_usb_host_message_type {
    ch559_usb_host_message_type_connected       = 0x01,
    ch559_usb_host_message_type_disconnected    = 0x02,
    ch559_usb_host_message_type_error           = 0x03,
    ch559_usb_host_message_type_device_poll     = 0x04,
    ch559_usb_host_message_type_device_string   = 0x05,
    ch559_usb_host_message_type_device_info     = 0x06,
    ch559_usb_host_message_type_hid_info        = 0x07,
    ch559_usb_host_message_type_startup         = 0x08,
} ch559_usb_host_message_type;

typedef enum ch559_usb_host_device_type {
    ch559_usb_host_device_type_unknown          = 0x00,
    ch559_usb_host_device_type_pointer          = 0x01,
    ch559_usb_host_device_type_mouse            = 0x02,
    ch559_usb_host_device_type_reserved         = 0x03,
    ch559_usb_host_device_type_joystick         = 0x04,
    ch559_usb_host_device_type_gamepad          = 0x05,
    ch559_usb_host_device_type_keyboard         = 0x06,
    ch559_usb_host_device_type_keypad           = 0x07,
    ch559_usb_host_device_type_multi_axis       = 0x08,
    ch559_usb_host_device_type_system           = 0x09,
} ch559_usb_host_device_type;

typedef enum ch559_usb_host_message_format {
    ch559_usb_host_message_format_protocol,
    ch559_usb_host_message_format_hid_poll,
} ch559_usb_host_message_format;

typedef struct ch559_usb_host_message {
    size_t length;
    ch559_usb_host_message_type message_type;
    ch559_usb_host_device_type device_type;
    unsigned char device;
    unsigned char endpoint;

    ch559_usb_host_message_format format;
    union {
        struct {
            unsigned short _dummy;
        } protocol;
        struct {
            unsigned short id_vendor;
            unsigned short id_product;
        } hid_poll;
    };
} ch559_usb_host_message;

void ch559_usb_host_message_decode(const struct ch559_usb_host_message_raw *message_raw, ch559_usb_host_message *message);
const char *ch559_usb_host_message_type_to_str(ch559_usb_host_message_type message_type);
const char *ch559_usb_host_device_type_to_str(ch559_usb_host_device_type device_type);
void ch559_usb_host_print_message(const struct ch559_usb_host_message *message, size_t payload_length, const uint8_t payload[payload_length]);
