#pragma once

#include "kernel/module.h"
#include "mem/flex.h"
#include "uapi/mosys/input.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <sys/types.h>

typedef enum input_type {
    INPUT_TYPE_NONE = 0,
    INPUT_TYPE_EVENT,
    INPUT_TYPE_MOUSE,
} input_type;

struct input_ops;

typedef struct input_info {
    input_type type;
    struct input_ops *ops;
} input_info;

typedef struct input_ops {
    int (*open)(struct input_info *info, int *errno_ptr);
    int (*close)(struct input_info *info, int *errno_ptr);

    ssize_t (*read)(struct input_info *info, FLEX_MUT(struct input_event) buf, size_t count, unsigned long long ppos, int *errno_ptr);
    ssize_t (*write)(struct input_info *info, FLEX_CONST(struct input_event) buf, size_t count, unsigned long long ppos, int *errno_ptr);

    int (*ioctl)(struct input_info *info, unsigned long request, flex_mut argp, int *errno_ptr);
} input_ops;

input_info *input_new(void);
void input_delete(input_info *input);
void input_register(input_info *input);
